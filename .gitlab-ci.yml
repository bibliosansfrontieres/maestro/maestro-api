---
stages:
  - dependencies
  - tests
  - release
  - deploy

default:
  image: node:21-bookworm
  tags:
    - k8s

variables:
  IMAGE_NAME: 'maestro-api'
  RELEASE_TAG: 'latest'

  GITLAB_CONTAINER_IMAGE: '$CI_REGISTRY_IMAGE'
  GITLAB_COMMIT_IMAGE: '$GITLAB_CONTAINER_IMAGE:$CI_COMMIT_SHORT_SHA'
  GITLAB_RELEASE_IMAGE: '$GITLAB_CONTAINER_IMAGE:$RELEASE_TAG'
  GITLAB_STAGING_RELEASE_IMAGE: '$GITLAB_CONTAINER_IMAGE:staging-$RELEASE_TAG'

  # k8s generic variables - you usually don't need to override these
  DOCKER_TLS_CERTDIR: '/certs'
  DOCKER_HOST: tcp://127.0.0.1:2376
  DOCKER_TLS_VERIFY: 1
  DOCKER_CERT_PATH: '${DOCKER_TLS_CERTDIR}/client'
  DOCKER_VERSION: 20.10.12
  BUILDX_VERSION: 0.5.1
  BUILD_ARCH_LIST: linux/amd64

install:
  stage: dependencies
  script:
    - npm install
  artifacts:
    paths:
      - node_modules
    expire_in: 1 hour

lint:
  stage: tests
  script:
    - npm run lint
  dependencies:
    - install

prettier:
  stage: tests
  script:
    - npm run format:check
  dependencies:
    - install

test:
  stage: tests
  script:
    - npm run test
  dependencies:
    - install

build:
  stage: tests
  script:
    - npm run build
  dependencies:
    - install

release-staging:
  stage: release
  image: docker:${DOCKER_VERSION}
  services:
    - name: docker:${DOCKER_VERSION}-dind
      command: ['--experimental']
  before_script:
    - mkdir --parent /usr/lib/docker/cli-plugins/
    - wget --output-document /usr/lib/docker/cli-plugins/docker-buildx https://github.com/docker/buildx/releases/download/v${BUILDX_VERSION}/buildx-v${BUILDX_VERSION}.linux-amd64
    - chmod a+rx /usr/lib/docker/cli-plugins/docker-buildx
    - docker run --rm --privileged multiarch/qemu-user-static --reset -p yes
    - docker version
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - apk update && apk add jq
    - export VERSION=`jq -r ".version" < ./package.json`
    - export GITLAB_STAGING_VERSIONED_IMAGE="$GITLAB_CONTAINER_IMAGE:staging-$VERSION"
    - docker pull $GITLAB_STAGING_VERSIONED_IMAGE || true
    - docker context create olip
    - docker buildx create olip --use
    - docker buildx inspect --bootstrap
    - |
      docker buildx build \
        --platform ${BUILD_ARCH_LIST} \
        --label "org.opencontainers.image.authors=Bibliothèques Sans Frontières" \
        --label "org.opencontainers.image.title=$CI_PROJECT_TITLE" \
        --label "org.opencontainers.image.description=$CI_PROJECT_DESCRIPTION" \
        --label "org.opencontainers.image.url=$CI_REGISTRY_IMAGE" \
        --label "org.opencontainers.image.source=$CI_PROJECT_URL" \
        --label "org.opencontainers.image.created=$CI_JOB_STARTED_AT" \
        --label "org.opencontainers.image.revision=$CI_COMMIT_SHA" \
        --label "org.opencontainers.image.ref.name=$CI_COMMIT_REF_NAME" \
        --label "org.opencontainers.image.version=$CI_COMMIT_REF_NAME" \
        -t ${GITLAB_STAGING_RELEASE_IMAGE} \
        -t ${GITLAB_STAGING_VERSIONED_IMAGE} \
        -t ${GITLAB_COMMIT_IMAGE} \
        . \
        --push
  dependencies: []
  only:
    - staging

release-prod:
  stage: release
  image: docker:${DOCKER_VERSION}
  services:
    - name: docker:${DOCKER_VERSION}-dind
      command: ['--experimental']
  before_script:
    - mkdir --parent /usr/lib/docker/cli-plugins/
    - wget --output-document /usr/lib/docker/cli-plugins/docker-buildx https://github.com/docker/buildx/releases/download/v${BUILDX_VERSION}/buildx-v${BUILDX_VERSION}.linux-amd64
    - chmod a+rx /usr/lib/docker/cli-plugins/docker-buildx
    - docker run --rm --privileged multiarch/qemu-user-static --reset -p yes
    - docker version
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - apk update && apk add jq
    - export VERSION=`jq -r ".version" < ./package.json`
    - export GITLAB_VERSIONED_IMAGE="$GITLAB_CONTAINER_IMAGE:$VERSION"
    - docker pull $GITLAB_VERSIONED_IMAGE || true
    - docker context create olip
    - docker buildx create olip --use
    - docker buildx inspect --bootstrap
    - |
      docker buildx build \
        --platform ${BUILD_ARCH_LIST} \
        --label "org.opencontainers.image.authors=Bibliothèques Sans Frontières" \
        --label "org.opencontainers.image.title=$CI_PROJECT_TITLE" \
        --label "org.opencontainers.image.description=$CI_PROJECT_DESCRIPTION" \
        --label "org.opencontainers.image.url=$CI_REGISTRY_IMAGE" \
        --label "org.opencontainers.image.source=$CI_PROJECT_URL" \
        --label "org.opencontainers.image.created=$CI_JOB_STARTED_AT" \
        --label "org.opencontainers.image.revision=$CI_COMMIT_SHA" \
        --label "org.opencontainers.image.ref.name=$CI_COMMIT_REF_NAME" \
        --label "org.opencontainers.image.version=$CI_COMMIT_REF_NAME" \
        -t ${GITLAB_RELEASE_IMAGE} \
        -t ${GITLAB_VERSIONED_IMAGE} \
        -t ${GITLAB_COMMIT_IMAGE} \
        . \
        --push
  dependencies: []
  only:
    - main

deploy-staging:
  stage: deploy
  before_script:
    - apt install openssh-client
    - eval $(ssh-agent -s)
    - mkdir -p ~/.ssh
    - ssh-keyscan -H $STAGING_SERVER_HOSTNAME >> ~/.ssh/known_hosts
  script:
    - echo "${STAGING_SERVER_SSH_KEY}" | tr -d '\r' | ssh-add -
    - ssh ${STAGING_SERVER_USER}@${STAGING_SERVER_HOSTNAME} "cd deploy/scripts && docker compose pull maestro-api && docker compose up -d --force-recreate maestro-api"
  dependencies: []
  only:
    - staging

deploy-prod:
  stage: deploy
  before_script:
    - apt install openssh-client
    - eval $(ssh-agent -s)
    - mkdir -p ~/.ssh
    - ssh-keyscan -H $PROD_SERVER_HOSTNAME >> ~/.ssh/known_hosts
  script:
    - echo "${PROD_SERVER_SSH_KEY}" | tr -d '\r' | ssh-add -
    - ssh ${PROD_SERVER_USER}@${PROD_SERVER_HOSTNAME} "cd deploy/ && docker compose pull maestro-api && docker compose up -d --force-recreate maestro-api"
  dependencies: []
  only:
    - main
