########
# BASE #
########

FROM node:21-bookworm AS base

WORKDIR /app

COPY package*.json .

RUN npm install --omit=dev

RUN apt-get update && apt-get install -y git

#########
# BUILD #
#########

FROM base AS build

WORKDIR /build

COPY --from=base /app/node_modules /build/node_modules

COPY package*.json .
RUN npm install

COPY . .
RUN npm run build

##############
# PRODUCTION #
##############

FROM base AS production

RUN curl -fsSL https://get.docker.com -o get-docker.sh
RUN sh get-docker.sh && rm get-docker.sh

RUN wget https://github.com/mikefarah/yq/releases/download/v4.44.5/yq_linux_amd64.tar.gz -O - | \
    tar xz && mv yq_linux_amd64 /usr/bin/yq

# Copy builded node project
COPY --from=build /build/dist /app/dist
COPY --from=build /build/data /app/data
COPY --from=build /build/docs/asyncapi /app/docs/asyncapi

RUN mkdir /app/downloads

CMD ["node", "dist/main.js"]