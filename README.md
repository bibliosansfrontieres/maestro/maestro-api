# Maestro API

## Usage for developement environment

### Dependencies

- NodeJS 21 and NPM 10 (version are arbitrary)

### Steps

Clone repository (SSH) :

```shell
git clone git@gitlab.com:bibliosansfrontieres/maestro/maestro-api.git
```

Clone repository (HTTPS) :

```shell
git clone https://gitlab.com/bibliosansfrontieres/maestro/maestro-api.git
```

Install node dependencies :

```shell
npm install
```

Copy environment variables :

```shell
cp .env.example .env
```

Run for development :

```shell
npm run start:dev
```

## Usage with Docker

Copy environment variables :

```shell
cp .env.example .env
```

Create a docker compose :

```yaml
---
version: '3.8'
services:
  maestro-api:
    image: registry.gitlab.com/bibliosansfrontieres/maestro/maestro-api:latest
    env_file:
      - '.env'
    ports:
      - '3000:3000'
```

Run docker compose :

```shell
docker compose up -d
```
