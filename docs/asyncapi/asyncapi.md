# Maestro WebSocket API 1.0.0 documentation

* Default content type: [application/json](https://www.iana.org/assignments/media-types/application/json)


## Table of Contents

* [Operations](#operations)
  * [SUB virtualMachine.{virtualMachineId}.status](#sub-virtualmachinevirtualmachineidstatus-operation)
  * [SUB virtualMachine.{virtualMachineId}.approval](#sub-virtualmachinevirtualmachineidapproval-operation)
  * [SUB virtualMachine.{virtualMachineId}.isArchived](#sub-virtualmachinevirtualmachineidisarchived-operation)

## Operations

### SUB `virtualMachine.{virtualMachineId}.status` Operation

* Operation ID: `virtualMachineStatus`

Any update from virtualMachine status

#### Parameters

| Name | Type | Description | Value | Constraints | Notes |
|---|---|---|---|---|---|
| virtualMachineId | string | The ID of the virtual machine | - | - | **required** |


#### Message Virtual machine status `virtualMachineStatus`

* Content type: [application/json](https://www.iana.org/assignments/media-types/application/json)

##### Payload

| Name | Type | Description | Value | Constraints | Notes |
|---|---|---|---|---|---|
| (root) | object | - | - | - | **additional properties are allowed** |
| status | string | Status of a virtual machine | allowed (`"ONLINE"`, `"OFFLINE"`) | - | - |

> Examples of payload _(generated)_

```json
{
  "status": "ONLINE"
}
```



### SUB `virtualMachine.{virtualMachineId}.approval` Operation

* Operation ID: `virtualMachineApproval`

Any update from virtualMachine approval

#### Parameters

| Name | Type | Description | Value | Constraints | Notes |
|---|---|---|---|---|---|
| virtualMachineId | string | The ID of the virtual machine | - | - | **required** |


#### Message Virtual machine approval `virtualMachineApproval`

* Content type: [application/json](https://www.iana.org/assignments/media-types/application/json)

##### Payload

| Name | Type | Description | Value | Constraints | Notes |
|---|---|---|---|---|---|
| (root) | object | - | - | - | **additional properties are allowed** |
| status | string | Approval status of a virtual machine | allowed (`"NONE"`, `"IN_PROGRESS"`, `"APPROVED"`) | - | - |

> Examples of payload _(generated)_

```json
{
  "status": "NONE"
}
```



### SUB `virtualMachine.{virtualMachineId}.isArchived` Operation

* Operation ID: `virtualMachineIsArchived`

Any update from virtualMachine archive status

#### Parameters

| Name | Type | Description | Value | Constraints | Notes |
|---|---|---|---|---|---|
| virtualMachineId | string | The ID of the virtual machine | - | - | **required** |


#### Message Virtual machine archive status `virtualMachineIsArchived`

* Content type: [application/json](https://www.iana.org/assignments/media-types/application/json)

##### Payload

| Name | Type | Description | Value | Constraints | Notes |
|---|---|---|---|---|---|
| (root) | boolean | - | - | - | - |

> Examples of payload _(generated)_

```json
true
```



