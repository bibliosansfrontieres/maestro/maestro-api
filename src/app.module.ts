import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { configurationSchema } from './config/schema';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ThrottlerModule } from '@nestjs/throttler';
import { AuthModule } from './auth/auth.module';
import { EntraModule } from './entra/entra.module';
import { JwtModule } from '@nestjs/jwt';
import { UsersModule } from './users/users.module';
import { LogsModule } from './logs/logs.module';
import { RolesModule } from './roles/roles.module';
import { PermissionsModule } from './permissions/permissions.module';
import { InitDatabaseModule } from './init-database/init-database.module';
import { GroupsModule } from './groups/groups.module';
import { ContextsModule } from './contexts/contexts.module';
import { ApplicationsModule } from './applications/applications.module';
import { VirtualMachinesModule } from './virtual-machines/virtual-machines.module';
import { CategoriesModule } from './categories/categories.module';
import { ItemsModule } from './items/items.module';
import { PlaylistsModule } from './playlists/playlists.module';
import { ProjectsModule } from './projects/projects.module';
import { OmekaModule } from './omeka/omeka.module';
import { DockerModule } from './docker/docker.module';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import { SocketModule } from './socket/socket.module';
import { VirtualMachineSavesModule } from './virtual-machine-saves/virtual-machine-saves.module';
import { S3Module } from './s3/s3.module';
import { DevicesModule } from './devices/devices.module';
import { FqdnMasksModule } from './fqdn-masks/fqdn-masks.module';
import { DeploysModule } from './deploys/deploys.module';
import { LabelsModule } from './labels/labels.module';
import { MonitoringModule } from './monitoring/monitoring.module';
import { NotificationsModule } from './notifications/notifications.module';
import { ScheduleModule } from '@nestjs/schedule';
import { typeormConfig } from './config/typeorm';

@Module({
  imports: [
    ScheduleModule.forRoot(),
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'docs/asyncapi'),
      renderPath: '/asyncapi',
      exclude: ['/'],
    }),
    JwtModule.registerAsync({
      useFactory: (config: ConfigService) => {
        return {
          global: true,
          secretOrPrivateKey: config.get<string>('JWT_SECRET_KEY') || 'test',
          signOptions: {
            expiresIn: config.get<string | number>('JWT_EXPIRATION_TIME') || 60,
          },
        };
      },
      inject: [ConfigService],
    }),
    ConfigModule.forRoot({
      isGlobal: true,
      validationSchema: configurationSchema,
    }),
    TypeOrmModule.forRoot(typeormConfig),
    ThrottlerModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => [
        {
          ttl: configService.get<number>('API_THROTTLE_TTL'),
          limit: configService.get<number>('API_THROTTLE_LIMIT'),
        },
      ],
    }),
    AuthModule,
    EntraModule,
    UsersModule,
    LogsModule,
    RolesModule,
    PermissionsModule,
    InitDatabaseModule,
    GroupsModule,
    ContextsModule,
    ApplicationsModule,
    VirtualMachinesModule,
    CategoriesModule,
    ItemsModule,
    PlaylistsModule,
    ProjectsModule,
    OmekaModule,
    DockerModule,
    VirtualMachineSavesModule,
    S3Module,
    DevicesModule,
    SocketModule,
    FqdnMasksModule,
    DeploysModule,
    LabelsModule,
    MonitoringModule,
    NotificationsModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
