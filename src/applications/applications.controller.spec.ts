import { Test, TestingModule } from '@nestjs/testing';
import { ApplicationsController } from './applications.controller';
import { ApplicationsService } from './applications.service';
import { Application } from './entities/application.entity';
import { Repository } from 'typeorm';
import { ApplicationTranslation } from './entities/application-translation.entity';
import { ApplicationCatalog } from './entities/application-catalog.entity';
import { getRepositoryToken } from '@nestjs/typeorm';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from 'src/users/users.service';
import { EntraService } from 'src/entra/entra.service';
import { ConfigService } from '@nestjs/config';
import { jwtServiceMock } from 'src/utils/mocks/services/jwt-service.mock';
import { usersServiceMock } from 'src/utils/mocks/services/users-service.mock';
import { entraServiceMock } from 'src/utils/mocks/services/entra-service.mock';
import { configServiceMock } from 'src/utils/mocks/services/config-service.mock';
import { ApplicationType } from './entities/application-type.entity';
import { PlaylistsService } from 'src/playlists/playlists.service';
import { ItemsService } from 'src/items/items.service';
import { playlistsServiceMock } from 'src/utils/mocks/services/playlists-service.mock';
import { itemsServiceMock } from 'src/utils/mocks/services/items-service.mock';

describe('ApplicationsController', () => {
  let controller: ApplicationsController;
  let applicationRepository: Repository<Application>;
  let applicationTranslationRepository: Repository<ApplicationTranslation>;
  let applicationCatalogRepository: Repository<ApplicationCatalog>;
  let applicationTypeRepository: Repository<ApplicationType>;

  const APPLICATION_REPOSITORY_TOKEN = getRepositoryToken(Application);
  const APPLICATION_TRANSLATION_REPOSITORY_TOKEN = getRepositoryToken(
    ApplicationTranslation,
  );
  const APPLICATION_CATALOG_REPOSITORY_TOKEN =
    getRepositoryToken(ApplicationCatalog);
  const APPLICATION_TYPE_REPOSITORY_TOKEN = getRepositoryToken(ApplicationType);

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ApplicationsController],
      providers: [
        ApplicationsService,
        JwtService,
        UsersService,
        EntraService,
        ConfigService,
        PlaylistsService,
        ItemsService,
        {
          provide: APPLICATION_REPOSITORY_TOKEN,
          useClass: Repository<Application>,
        },
        {
          provide: APPLICATION_TRANSLATION_REPOSITORY_TOKEN,
          useClass: Repository<ApplicationTranslation>,
        },
        {
          provide: APPLICATION_CATALOG_REPOSITORY_TOKEN,
          useClass: Repository<ApplicationCatalog>,
        },
        {
          provide: APPLICATION_TYPE_REPOSITORY_TOKEN,
          useClass: Repository<ApplicationType>,
        },
      ],
    })
      .overrideProvider(JwtService)
      .useValue(jwtServiceMock)
      .overrideProvider(UsersService)
      .useValue(usersServiceMock)
      .overrideProvider(EntraService)
      .useValue(entraServiceMock)
      .overrideProvider(ConfigService)
      .useValue(configServiceMock)
      .overrideProvider(PlaylistsService)
      .useValue(playlistsServiceMock)
      .overrideProvider(ItemsService)
      .useValue(itemsServiceMock)
      .compile();

    controller = module.get<ApplicationsController>(ApplicationsController);
    applicationRepository = module.get<Repository<Application>>(
      APPLICATION_REPOSITORY_TOKEN,
    );
    applicationTranslationRepository = module.get<
      Repository<ApplicationTranslation>
    >(APPLICATION_TRANSLATION_REPOSITORY_TOKEN);
    applicationCatalogRepository = module.get<Repository<ApplicationCatalog>>(
      APPLICATION_CATALOG_REPOSITORY_TOKEN,
    );
    applicationTypeRepository = module.get<Repository<ApplicationType>>(
      APPLICATION_TYPE_REPOSITORY_TOKEN,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
    expect(applicationRepository).toBeDefined();
    expect(applicationTranslationRepository).toBeDefined();
    expect(applicationCatalogRepository).toBeDefined();
    expect(applicationTypeRepository).toBeDefined();
  });
});
