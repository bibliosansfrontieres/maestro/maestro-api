import { Controller, Get, Param, Query } from '@nestjs/common';
import { ApplicationsService } from './applications.service';
import {
  GetApplicationsRequest,
  GetApplicationsResponse,
} from './definitions/get-applications.definition';
import {
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { Auth } from 'src/auth/decorators/auth.decorator';
import { PermissionNameEnum } from 'src/permissions/enums/permission-name.enum';
import { GetApplicationResponse } from './definitions/get-application.definition';
import { GitlabAuth } from 'src/auth/decorators/gitlab-auth.decorator';
import { GetCatalogUpdateRequest } from './definitions/get-catalog-update.definition';

@Controller('applications')
@ApiTags('applications')
export class ApplicationsController {
  constructor(private readonly applicationsService: ApplicationsService) {}

  @Get('catalog/update')
  @GitlabAuth()
  getCatalogUpdate(@Query() getCatalogUpdateRequest: GetCatalogUpdateRequest) {
    return this.applicationsService.getCatalogUpdate(getCatalogUpdateRequest);
  }

  @Get()
  @ApiOperation({ summary: 'Get applications' })
  @ApiOkResponse({ type: GetApplicationsResponse })
  getApplications(@Query() getApplicationsRequest: GetApplicationsRequest) {
    return this.applicationsService.getApplications(getApplicationsRequest);
  }

  @Get(':id')
  @Auth(PermissionNameEnum.READ_APPLICATION)
  @ApiOperation({ summary: 'Get application by ID' })
  @ApiOkResponse({ type: GetApplicationResponse })
  @ApiNotFoundResponse({ description: 'Application not found' })
  getApplication(@Param('id') id: string) {
    return this.applicationsService.getApplication(id);
  }

  @Get('types')
  getApplicationTypes() {
    return this.applicationsService.getApplicationTypes();
  }
}
