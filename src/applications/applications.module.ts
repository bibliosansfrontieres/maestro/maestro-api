import { Module } from '@nestjs/common';
import { ApplicationsService } from './applications.service';
import { ApplicationsController } from './applications.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Application } from './entities/application.entity';
import { ApplicationCatalog } from './entities/application-catalog.entity';
import { ApplicationTranslation } from './entities/application-translation.entity';
import { JwtModule } from '@nestjs/jwt';
import { UsersModule } from 'src/users/users.module';
import { EntraModule } from 'src/entra/entra.module';
import { ApplicationType } from './entities/application-type.entity';
import { ApplicationTypeTranslation } from './entities/application-type-translation.entity';
import { ItemsModule } from 'src/items/items.module';
import { PlaylistsModule } from 'src/playlists/playlists.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Application,
      ApplicationCatalog,
      ApplicationTranslation,
      ApplicationType,
      ApplicationTypeTranslation,
    ]),
    JwtModule,
    UsersModule,
    EntraModule,
    ItemsModule,
    PlaylistsModule,
  ],
  controllers: [ApplicationsController],
  providers: [ApplicationsService],
  exports: [ApplicationsService],
})
export class ApplicationsModule {}
