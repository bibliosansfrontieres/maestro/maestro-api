import { Test, TestingModule } from '@nestjs/testing';
import { ApplicationsService } from './applications.service';
import { Application } from './entities/application.entity';
import { Repository } from 'typeorm';
import { ApplicationTranslation } from './entities/application-translation.entity';
import { ApplicationCatalog } from './entities/application-catalog.entity';
import { getRepositoryToken } from '@nestjs/typeorm';
import { ApplicationType } from './entities/application-type.entity';
import { ConfigService } from '@nestjs/config';
import { configServiceMock } from 'src/utils/mocks/services/config-service.mock';
import { PlaylistsService } from 'src/playlists/playlists.service';
import { ItemsService } from 'src/items/items.service';
import { playlistsServiceMock } from 'src/utils/mocks/services/playlists-service.mock';
import { itemsServiceMock } from 'src/utils/mocks/services/items-service.mock';

describe('ApplicationsService', () => {
  let service: ApplicationsService;
  let applicationRepository: Repository<Application>;
  let applicationTranslationRepository: Repository<ApplicationTranslation>;
  let applicationCatalogRepository: Repository<ApplicationCatalog>;
  let applicationTypeRepository: Repository<ApplicationType>;

  const APPLICATION_REPOSITORY_TOKEN = getRepositoryToken(Application);
  const APPLICATION_TRANSLATION_REPOSITORY_TOKEN = getRepositoryToken(
    ApplicationTranslation,
  );
  const APPLICATION_CATALOG_REPOSITORY_TOKEN =
    getRepositoryToken(ApplicationCatalog);
  const APPLICATION_TYPE_REPOSITORY_TOKEN = getRepositoryToken(ApplicationType);

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ApplicationsService,
        {
          provide: APPLICATION_REPOSITORY_TOKEN,
          useClass: Repository<Application>,
        },
        {
          provide: APPLICATION_TRANSLATION_REPOSITORY_TOKEN,
          useClass: Repository<ApplicationTranslation>,
        },
        {
          provide: APPLICATION_CATALOG_REPOSITORY_TOKEN,
          useClass: Repository<ApplicationCatalog>,
        },
        {
          provide: APPLICATION_TYPE_REPOSITORY_TOKEN,
          useClass: Repository<ApplicationType>,
        },
        ConfigService,
        PlaylistsService,
        ItemsService,
      ],
    })
      .overrideProvider(ConfigService)
      .useValue(configServiceMock)
      .overrideProvider(PlaylistsService)
      .useValue(playlistsServiceMock)
      .overrideProvider(ItemsService)
      .useValue(itemsServiceMock)
      .compile();

    service = module.get<ApplicationsService>(ApplicationsService);
    applicationRepository = module.get<Repository<Application>>(
      APPLICATION_REPOSITORY_TOKEN,
    );
    applicationTranslationRepository = module.get<
      Repository<ApplicationTranslation>
    >(APPLICATION_TRANSLATION_REPOSITORY_TOKEN);
    applicationCatalogRepository = module.get<Repository<ApplicationCatalog>>(
      APPLICATION_CATALOG_REPOSITORY_TOKEN,
    );
    applicationTypeRepository = module.get<Repository<ApplicationType>>(
      APPLICATION_TYPE_REPOSITORY_TOKEN,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
    expect(applicationRepository).toBeDefined();
    expect(applicationTranslationRepository).toBeDefined();
    expect(applicationCatalogRepository).toBeDefined();
    expect(applicationTypeRepository).toBeDefined();
  });
});
