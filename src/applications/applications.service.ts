import {
  Injectable,
  InternalServerErrorException,
  Logger,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Application } from './entities/application.entity';
import { FindManyOptions, FindOneOptions, Repository } from 'typeorm';
import { TDescription } from 'src/utils/applications/validation/types/description.type';
import { THooks } from 'src/utils/applications/validation/types/hooks.type';
import { ApplicationTranslation } from './entities/application-translation.entity';
import { ApplicationTypeNameEnum } from './enums/application-type-name.enum';
import { applicationMd5, applicationTypeMd5 } from 'src/utils/md5.util';
import { ApplicationCatalog } from './entities/application-catalog.entity';
import { ApplicationCatalogTypeEnum } from './enums/application-catalog-type.enum';
import { getManyAndCount } from 'src/utils/get-many-and-count.util';
import {
  GetApplicationsResponse,
  GetApplicationsRequest,
} from './definitions/get-applications.definition';
import { PageOptions } from 'src/utils/pagination/page-options';
import { GetApplicationResponse } from './definitions/get-application.definition';
import { ApplicationType } from './entities/application-type.entity';
import { CreateApplicationTypeDto } from './dto/create-application-type.dto';
import { ApplicationTypeTranslation } from './entities/application-type-translation.entity';
import { v4 as uuidv4 } from 'uuid';
import { BooleanEnum } from 'src/utils/enums/boolean.enum';
import { ConfigService } from '@nestjs/config';
import axios from 'axios';
import * as YAML from 'yaml';
import { XMLParser } from 'fast-xml-parser';
import { writeFileSync } from 'fs';
import { PlaylistsService } from 'src/playlists/playlists.service';
import { ItemsService } from 'src/items/items.service';
import { CreatePlaylistDto } from 'src/playlists/dto/create-playlist.dto';
import { ItemSourceEnum } from 'src/items/enums/item-source.enum';
import { CreateItemDto } from 'src/items/dto/create-item.dto';
import { join } from 'path';
import { GetCatalogUpdateRequest } from './definitions/get-catalog-update.definition';
import { CatalogBranchEnum } from './enums/catalog-branch.enum';

@Injectable()
export class ApplicationsService {
  constructor(
    @InjectRepository(Application)
    private applicationRepository: Repository<Application>,
    @InjectRepository(ApplicationTranslation)
    private applicationTranslationRepository: Repository<ApplicationTranslation>,
    @InjectRepository(ApplicationCatalog)
    private applicationCatalogRepository: Repository<ApplicationCatalog>,
    @InjectRepository(ApplicationType)
    private applicationTypeRepository: Repository<ApplicationType>,
    private configService: ConfigService,
    private playlistsService: PlaylistsService,
    private itemsService: ItemsService,
  ) {}

  private async getCatalog(branch?: CatalogBranchEnum) {
    let publicCatalog = null;
    let privateCatalog = null;

    try {
      const publicApplicationsCatalogUrl = `${this.configService.get('PUBLIC_APPLICATIONS_CATALOG_URL')}?ref=${branch}`;
      const publicCatalogContent = await axios.get(
        publicApplicationsCatalogUrl,
      );
      publicCatalog = publicCatalogContent.data;
    } catch (e) {
      console.error(e);
    }

    try {
      const privateApplicationsCatalogUrl = `${this.configService.get('PRIVATE_APPLICATIONS_CATALOG_URL')}?ref=${branch}`;
      const privateCatalogContent = await axios.get(
        privateApplicationsCatalogUrl,
        {
          headers: {
            Authorization: 'Bearer ' + this.configService.get('GITLAB_TOKEN'),
          },
        },
      );
      privateCatalog = privateCatalogContent.data;
    } catch (e) {
      console.error(e);
    }

    if (!publicCatalog && !privateCatalog) {
      return undefined;
    }

    if (!publicCatalog) {
      return privateCatalog;
    }

    if (!privateCatalog) {
      return publicCatalog;
    }

    if (!publicCatalog.applications && !privateCatalog.applications) {
      return undefined;
    }

    if (!publicCatalog.applications) {
      return privateCatalog;
    }

    if (!privateCatalog.applications) {
      return publicCatalog;
    }

    return {
      applications: (publicCatalog.applications as any[]).concat(
        privateCatalog.applications,
      ),
    };
  }

  async getCatalogUpdate(getCatalogUpdateRequest: GetCatalogUpdateRequest) {
    const catalog = await this.getCatalog(getCatalogUpdateRequest.branch);
    if (!catalog) {
      throw new InternalServerErrorException('could not fetch catalog');
    }
    for (const catalogApplication of catalog.applications) {
      const application = await this.findOne({
        where: { id: catalogApplication.id },
        relations: {
          applicationTranslations: true,
          applicationCatalogs: true,
          applicationType: true,
        },
      });
      await this.createOrUpdate(
        application,
        catalogApplication,
        catalogApplication.hooks,
        catalogApplication.compose,
      );
    }
  }

  async getApplication(id: string) {
    const application = await this.findOne({
      where: { id },
      relations: { applicationTranslations: true },
    });
    if (application === null) {
      throw new NotFoundException('application not found');
    }
    return new GetApplicationResponse(application);
  }

  async getApplications(getApplicationsRequest: GetApplicationsRequest) {
    const pageOptions = new PageOptions(getApplicationsRequest);

    let applicationsQueryBuilder = this.applicationRepository
      .createQueryBuilder('application')
      .innerJoinAndSelect('application.applicationType', 'applicationType')
      .innerJoinAndSelect(
        'applicationType.applicationTypeTranslations',
        'applicationTypeTranslations',
      )
      .innerJoinAndSelect(
        'application.applicationTranslations',
        'applicationTranslation',
      )
      .take(pageOptions.take)
      .skip(pageOptions.skip)
      .orderBy('application.name', 'ASC');

    if (getApplicationsRequest.isOlipExcluded === BooleanEnum.TRUE) {
      applicationsQueryBuilder = applicationsQueryBuilder.andWhere(
        'application.id != "olip"',
      );
    }

    if (getApplicationsRequest.type) {
      applicationsQueryBuilder = applicationsQueryBuilder.andWhere(
        'applicationType.name = :type',
        { type: getApplicationsRequest.type },
      );
    }

    if (getApplicationsRequest.query) {
      applicationsQueryBuilder = applicationsQueryBuilder.andWhere(
        'LOWER(application.name) LIKE :query',
        { query: `%${getApplicationsRequest.query.toLowerCase()}%` },
      );
    }

    const [count, applications] = await getManyAndCount<Application>(
      applicationsQueryBuilder,
      ['application.id'],
      false,
    );

    return new GetApplicationsResponse(applications, count, pageOptions);
  }

  async getApplicationTypes() {
    return this.applicationTypeRepository.find({
      relations: { applicationTypeTranslations: true },
    });
  }

  async createTranslation(
    application: Application,
    translation: {
      shortDescription: string;
      longDescription: string;
      languageIdentifier: string;
    },
  ) {
    let applicationTranslation = new ApplicationTranslation(
      translation,
      application,
    );
    applicationTranslation = await this.applicationTranslationRepository.save(
      applicationTranslation,
    );
    return applicationTranslation;
  }

  async createTranslations(
    application: Application,
    description: TDescription,
  ) {
    const applicationTranslations = [];

    for (const descriptionTranslation of description.applicationTranslations) {
      const applicationTranslation = await this.createTranslation(
        application,
        descriptionTranslation,
      );
      applicationTranslations.push(applicationTranslation);
    }

    return applicationTranslations;
  }

  async createCatalogs(application: Application, description: TDescription) {
    const applicationCatalogs = [];

    if (description.catalog) {
      let applicationCatalog = new ApplicationCatalog(description, application);
      applicationCatalog =
        await this.applicationCatalogRepository.save(applicationCatalog);
      applicationCatalogs.push(applicationCatalog);
    }

    return applicationCatalogs;
  }

  async createApplicationType(
    createApplicationTypeDto: CreateApplicationTypeDto,
  ) {
    const id = uuidv4();
    const md5 = applicationTypeMd5(createApplicationTypeDto);
    createApplicationTypeDto.id = id;
    createApplicationTypeDto.md5 = md5;
    const applicationTypeTranslations = [];
    for (const createApplicationTypeTranslationDto of createApplicationTypeDto.applicationTypeTranslations) {
      const applicationTypeTranslation = new ApplicationTypeTranslation(
        createApplicationTypeTranslationDto,
      );
      applicationTypeTranslations.push(applicationTypeTranslation);
    }
    const applicationType = new ApplicationType(createApplicationTypeDto);
    applicationType.applicationTypeTranslations = applicationTypeTranslations;
    return this.applicationTypeRepository.save(applicationType);
  }

  addImageToCompose(description: TDescription, compose: string) {
    const yamlCompose = YAML.parse(compose);
    if (
      yamlCompose.services &&
      yamlCompose.services['olip-app-main'] !== undefined
    ) {
      if (yamlCompose.services['olip-app-main'] === null) {
        yamlCompose.services['olip-app-main'] = {};
      }
      if (!yamlCompose.services['olip-app-main'].image) {
        yamlCompose.services['olip-app-main'].image =
          `${this.configService.get('APPLICATIONS_REGISTRY_PATH')}/${description.name}:{{APPS_IMAGES_TAG}}`;
      }
    }
    return YAML.stringify(yamlCompose);
  }

  async create(description: TDescription, hooks: THooks, compose: string) {
    Logger.log(
      `Creating application ${description.name}`,
      'ApplicationsService',
    );
    const applicationType = await this.findOneApplicationType({
      where: { name: description.type as ApplicationTypeNameEnum },
    });
    console.log(applicationType);
    if (!applicationType) {
      throw new NotFoundException('application type not found');
    }

    compose = this.addImageToCompose(description, compose);

    let application = new Application(description, hooks, compose);
    application.applicationType = applicationType;
    application = await this.save(application);

    application.applicationTranslations = await this.createTranslations(
      application,
      description,
    );

    application.applicationCatalogs = await this.createCatalogs(
      application,
      description,
    );

    return application;
  }

  createOrUpdate(
    application: Application | null,
    description: TDescription,
    hooks: THooks,
    compose: string,
  ) {
    if (application === null) {
      return this.create(description, hooks, compose);
    }
    return this.update(application, description, hooks, compose);
  }

  async update(
    application: Application,
    description: TDescription,
    hooks: THooks,
    compose: string,
  ) {
    Logger.log(
      `Updating application ${description.name}`,
      'ApplicationsService',
    );
    application.name = description.name;
    application.size = description.size;
    application.version = description.version;
    application.logo = description.logo;
    application.catalogFunction = description.catalogFunction;
    application.image = description.image;
    application.compose = this.addImageToCompose(description, compose);
    application.hooks = JSON.stringify(hooks);
    application.md5 = applicationMd5(description, hooks, compose);
    application.exposeServices = JSON.stringify(description.exposeServices);
    application = await this.save(application);

    application.applicationTranslations = await this.updateTranslations(
      application,
      description,
    );

    application.applicationCatalogs = await this.updateCatalogs(
      application,
      description,
    );

    return application;
  }

  async updateTranslations(
    application: Application,
    description: TDescription,
  ) {
    const translations: ApplicationTranslation[] = [];
    for (const descriptionTranslation of description.applicationTranslations) {
      let applicationTranslation = application.applicationTranslations.find(
        (translation) =>
          translation.languageIdentifier ===
          descriptionTranslation.languageIdentifier,
      );

      if (!applicationTranslation) {
        const applicationTranslation = await this.createTranslation(
          application,
          descriptionTranslation,
        );
        translations.push(applicationTranslation);
        continue;
      }

      applicationTranslation.shortDescription =
        descriptionTranslation.shortDescription;
      applicationTranslation.longDescription =
        descriptionTranslation.longDescription;
      applicationTranslation = await this.applicationTranslationRepository.save(
        applicationTranslation,
      );
      translations.push(applicationTranslation);
    }

    for (const applicationTranslation of application.applicationTranslations) {
      if (
        !translations.find(
          (translation) =>
            translation.languageIdentifier ===
            applicationTranslation.languageIdentifier,
        )
      ) {
        await this.applicationTranslationRepository.remove(
          applicationTranslation,
        );
      }
    }

    return translations;
  }

  async updateCatalogs(application: Application, description: TDescription) {
    let applicationCatalogs: ApplicationCatalog[] = [];

    if (description.catalog) {
      let applicationCatalog = application.applicationCatalogs[0];
      if (!applicationCatalog) {
        applicationCatalogs = await this.createCatalogs(
          application,
          description,
        );
      } else {
        applicationCatalog.url = description.catalog.url;
        applicationCatalog.type = description.catalog
          .type as ApplicationCatalogTypeEnum;
        applicationCatalog =
          await this.applicationCatalogRepository.save(applicationCatalog);
        applicationCatalogs.push(applicationCatalog);
      }
    } else {
      if (application.applicationCatalogs.length > 0) {
        await this.applicationCatalogRepository.remove(
          application.applicationCatalogs[0],
        );
      }
    }

    return applicationCatalogs;
  }

  save(application: Application) {
    return this.applicationRepository.save(application);
  }

  findOne(options?: FindOneOptions<Application>) {
    return this.applicationRepository.findOne(options);
  }

  find(options?: FindManyOptions<Application>) {
    return this.applicationRepository.find(options);
  }

  remove(application: Application) {
    return this.applicationRepository.remove(application);
  }

  findOneApplicationType(options?: FindOneOptions<ApplicationType>) {
    return this.applicationTypeRepository.findOne(options);
  }

  async getApplicationCatalog(application: Application) {
    try {
      if (
        application.applicationCatalogs &&
        application.applicationCatalogs[0]
      ) {
        const catalog = application.applicationCatalogs[0];
        if (!application.catalogFunction) return [];
        const fetchCatalog = await fetch(catalog.url);
        const fetchedCatalog = await fetchCatalog.text();
        const parser = new XMLParser({ ignoreAttributes: false });
        const jsonObject = parser.parse(fetchedCatalog);
        const catalogPath = join(__dirname, './catalog.js');
        writeFileSync(catalogPath, application.catalogFunction);
        const catalogFunction = await import(catalogPath);
        return catalogFunction.catalog(jsonObject);
      }
      return [];
    } catch (e) {
      console.error(e);
      Logger.error(
        `Error getting ${application.name} catalog`,
        'ApplicationsService',
      );
      return [];
    }
  }

  async checkApplicationsCatalogs() {
    // get each applications
    // get each catalog
    // put new contents in playlists database
    const applications = await this.find({
      relations: { applicationCatalogs: true },
    });
    for (const application of applications) {
      const catalogContents = await this.getApplicationCatalog(application);
      for (const catalogContent of catalogContents) {
        const playlist = await this.playlistsService.findOne({
          where: { id: catalogContent.id },
        });
        if (playlist) {
          // todo update playlist and items
        } else {
          console.log(`installing ${Date.now()}`);
          const thumbnail = catalogContent.thumbnail;
          const createItemDto: CreateItemDto = {
            applicationId: application.id,
            dublinCoreTypeId: 5,
            id: catalogContent.id,
            thumbnail: thumbnail,
            source: ItemSourceEnum.APPLICATION,
            version: '0',
            createdCountry: catalogContent.languageIdentifier,
            dublinCoreItemTranslations: [
              {
                title: catalogContent.title,
                description: catalogContent.description,
                languageIdentifier: catalogContent.languageIdentifier,
                creator: '',
                publisher: '',
                source: application.name,
                extent: '',
                subject: '',
                dateCreated: '',
              },
            ],
            file: {
              name: catalogContent.title,
              path: catalogContent.url,
              size: '',
              duration: '',
              pages: '',
              extension: '',
              mimeType: '',
              updatedAt: '',
            },
          };
          const item = await this.itemsService.createOrUpdate(createItemDto);
          const createPlaylistDto: CreatePlaylistDto = {
            id: catalogContent.id,
            source: ItemSourceEnum.APPLICATION,
            image: thumbnail,
            applicationId: application.id,
            size: catalogContent.size,
            playlistTranslations: [
              {
                title: catalogContent.title,
                description: catalogContent.description,
                languageIdentifier: catalogContent.languageIdentifier,
              },
            ],
            itemIds: [item.id],
          };
          await this.playlistsService.createOrUpdate(createPlaylistDto);
        }
      }
    }
  }

  async updateApplicationsCatalogContents() {
    const applications = await this.find({
      relations: { applicationCatalogs: true },
    });
    for (const application of applications) {
      if (!application.applicationCatalogs) continue;
      const catalog = application.applicationCatalogs[0];
      if (!catalog) continue;
    }
  }
}
