import { ApiProperty } from '@nestjs/swagger';
import { ApplicationTranslation } from '../entities/application-translation.entity';
import { Application } from '../entities/application.entity';
import { ApplicationTypeNameEnum } from '../enums/application-type-name.enum';

export class GetApplicationResponse {
  @ApiProperty({ example: '76f01b66-55f4-420d-ba07-673d5a396699' })
  id: string;

  @ApiProperty({ example: 'Kiwix' })
  name: string;

  @ApiProperty({ example: ApplicationTypeNameEnum.CONTENT_HOST })
  type: ApplicationTypeNameEnum;

  @ApiProperty({ example: 1000000 })
  size: number;

  @ApiProperty({ example: '1.0' })
  version: string;

  @ApiProperty({
    example:
      'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAIAQMAAAD+wSzIAAAABlBMVEX///+/v7+jQ3Y5AAAADklEQVQI12P4AIX8EAgALgAD/aNpbtEAAAAASUVORK5CYII',
  })
  logo: string;

  @ApiProperty({
    example:
      'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAIAQMAAAD+wSzIAAAABlBMVEX///+/v7+jQ3Y5AAAADklEQVQI12P4AIX8EAgALgAD/aNpbtEAAAAASUVORK5CYII',
  })
  image: string;

  @ApiProperty({ example: 'string' })
  compose: string;

  @ApiProperty({ example: '{hooks:[]}' })
  hooks: string;

  @ApiProperty({ example: 'ed076287532e86365e841e92bfc50d8c' })
  md5: string;

  @ApiProperty({
    example: [
      {
        id: 1,
        shortDescription: 'My application description',
        longDescription: 'My application long description',
        languageIdentifier: 'eng',
      },
    ],
  })
  applicationTranslations: ApplicationTranslation[];

  constructor(application: Application) {
    this.id = application.id;
    this.name = application.name;
    this.size = application.size;
    this.version = application.version;
    this.logo = application.logo;
    this.image = application.image;
    this.compose = application.compose;
    this.hooks = application.hooks;
    this.md5 = application.md5;
    this.applicationTranslations = application.applicationTranslations;
  }
}
