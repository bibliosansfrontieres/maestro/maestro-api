import { ApiPropertyOptional } from '@nestjs/swagger';
import { PageOptionsRequest } from 'src/utils/pagination/dto/page-options.dto';
import { ApplicationTypeNameEnum } from '../enums/application-type-name.enum';
import { IsEnum, IsOptional, IsString } from 'class-validator';
import { PageResponse } from 'src/utils/pagination/dto/page-response';
import { Application } from '../entities/application.entity';
import { PageOptions } from 'src/utils/pagination/page-options';
import { BooleanEnum } from 'src/utils/enums/boolean.enum';

export class GetApplicationsRequest extends PageOptionsRequest {
  @ApiPropertyOptional({
    example: ApplicationTypeNameEnum.CONTENT_HOST,
    enum: ApplicationTypeNameEnum,
  })
  @IsEnum(ApplicationTypeNameEnum)
  @IsOptional()
  type?: ApplicationTypeNameEnum;

  @ApiPropertyOptional({
    example: 'kiwix',
    description: 'Query applications by name',
  })
  @IsString()
  @IsOptional()
  query?: string;

  @ApiPropertyOptional({
    example: BooleanEnum.FALSE,
    enum: BooleanEnum,
    default: BooleanEnum.FALSE,
  })
  @IsEnum(BooleanEnum)
  @IsOptional()
  isOlipExcluded?: BooleanEnum;
}

export class GetApplicationsResponse extends PageResponse<Application> {
  constructor(data: Application[], count: number, pageOptions: PageOptions) {
    super(data, count, pageOptions);
  }
}
