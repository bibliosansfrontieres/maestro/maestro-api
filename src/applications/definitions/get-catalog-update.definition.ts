import { IsEnum, IsOptional } from 'class-validator';
import { CatalogBranchEnum } from '../enums/catalog-branch.enum';

export class GetCatalogUpdateRequest {
  @IsOptional()
  @IsEnum(CatalogBranchEnum)
  branch?: CatalogBranchEnum;
}
