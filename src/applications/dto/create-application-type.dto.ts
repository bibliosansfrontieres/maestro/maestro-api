import { ApplicationTypeNameEnum } from '../enums/application-type-name.enum';
import { CreateApplicationTypeTranslationDto } from './create-application-type-translation.dto';

export class CreateApplicationTypeDto {
  id: string;
  name: ApplicationTypeNameEnum;
  md5: string;
  applicationTypeTranslations: CreateApplicationTypeTranslationDto[];
}
