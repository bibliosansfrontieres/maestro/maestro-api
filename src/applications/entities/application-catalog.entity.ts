import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { ApplicationCatalogTypeEnum } from '../enums/application-catalog-type.enum';
import { Application } from './application.entity';
import { TDescription } from 'src/utils/applications/validation/types/description.type';

@Entity()
export class ApplicationCatalog {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  type: ApplicationCatalogTypeEnum;

  @Column()
  url: string;

  @ManyToOne(
    () => Application,
    (application) => application.applicationCatalogs,
  )
  application: Application;

  constructor(description: TDescription, application: Application) {
    if (!description || !application) return;
    this.type = description.catalog.type as ApplicationCatalogTypeEnum;
    this.url = description.catalog.url;
    this.application = application;
  }
}
