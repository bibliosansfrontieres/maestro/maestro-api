import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Application } from './application.entity';

@Entity()
export class ApplicationTranslation {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  shortDescription: string;

  @Column()
  longDescription: string;

  @Column()
  languageIdentifier: string;

  @ManyToOne(
    () => Application,
    (application) => application.applicationTranslations,
  )
  application: Application;

  constructor(
    descriptionTranslation: {
      shortDescription: string;
      longDescription: string;
      languageIdentifier: string;
    },
    application: Application,
  ) {
    if (!descriptionTranslation || !application) return;
    this.application = application;
    this.shortDescription = descriptionTranslation.shortDescription;
    this.longDescription = descriptionTranslation.longDescription;
    this.languageIdentifier = descriptionTranslation.languageIdentifier;
  }
}
