import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { ApplicationType } from './application-type.entity';
import { CreateApplicationTypeTranslationDto } from '../dto/create-application-type-translation.dto';

@Entity()
export class ApplicationTypeTranslation {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  label: string;

  @Column()
  description: string;

  @Column()
  languageIdentifier: string;

  @ManyToOne(
    () => ApplicationType,
    (applicationType) => applicationType.applicationTypeTranslations,
  )
  applicationType: ApplicationType;

  constructor(
    createApplicationTypeTranslationDto: CreateApplicationTypeTranslationDto,
  ) {
    if (!createApplicationTypeTranslationDto) return;
    this.label = createApplicationTypeTranslationDto.label;
    this.description = createApplicationTypeTranslationDto.description;
    this.languageIdentifier =
      createApplicationTypeTranslationDto.languageIdentifier;
  }
}
