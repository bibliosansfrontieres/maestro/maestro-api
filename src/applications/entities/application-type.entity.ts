import { Column, Entity, OneToMany, PrimaryColumn } from 'typeorm';
import { ApplicationTypeNameEnum } from '../enums/application-type-name.enum';
import { ApplicationTypeTranslation } from './application-type-translation.entity';
import { CreateApplicationTypeDto } from '../dto/create-application-type.dto';
import { Application } from './application.entity';

@Entity()
export class ApplicationType {
  @PrimaryColumn()
  id: string;

  @Column()
  name: ApplicationTypeNameEnum;

  @Column()
  md5: string;

  @OneToMany(
    () => ApplicationTypeTranslation,
    (applicationTypeTranslation) => applicationTypeTranslation.applicationType,
    {
      cascade: true,
    },
  )
  applicationTypeTranslations: ApplicationTypeTranslation[];

  @OneToMany(() => Application, (application) => application.applicationType)
  applications: Application[];

  constructor(createApplicationTypeDto: CreateApplicationTypeDto) {
    if (!createApplicationTypeDto) return;
    this.id = createApplicationTypeDto.id;
    this.md5 = createApplicationTypeDto.md5;
    this.name = createApplicationTypeDto.name;
  }
}
