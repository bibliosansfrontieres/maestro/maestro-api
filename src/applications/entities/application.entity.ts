import { Context } from 'src/contexts/entities/context.entity';
import {
  Column,
  Entity,
  ManyToMany,
  ManyToOne,
  OneToMany,
  PrimaryColumn,
} from 'typeorm';
import { ApplicationCatalog } from './application-catalog.entity';
import { ApplicationTranslation } from './application-translation.entity';
import { TDescription } from 'src/utils/applications/validation/types/description.type';
import { applicationMd5 } from 'src/utils/md5.util';
import { THooks } from 'src/utils/applications/validation/types/hooks.type';
import { Item } from 'src/items/entities/item.entity';
import { Playlist } from 'src/playlists/entities/playlist.entity';
import { ApplicationType } from './application-type.entity';

@Entity()
export class Application {
  @PrimaryColumn()
  id: string;

  @Column()
  name: string;

  @Column()
  displayName: string;

  @Column({ nullable: true })
  exposeServices: string | null;

  @Column()
  port: string;

  @Column()
  size: number;

  @Column()
  version: string;

  @Column()
  logo: string;

  @Column()
  image: string;

  @Column()
  compose: string;

  @Column()
  hooks: string;

  @Column()
  md5: string;

  @Column({ nullable: true })
  catalogFunction: string | null;

  @ManyToOne(
    () => ApplicationType,
    (applicationType) => applicationType.applications,
  )
  applicationType: ApplicationType;

  @OneToMany(
    () => ApplicationCatalog,
    (applicationCatalog) => applicationCatalog.application,
  )
  applicationCatalogs: ApplicationCatalog[];

  @OneToMany(
    () => ApplicationTranslation,
    (applicationTranslation) => applicationTranslation.application,
  )
  applicationTranslations: ApplicationTranslation[];

  @ManyToMany(() => Context, (context) => context.applications)
  contexts: Context[];

  @OneToMany(() => Item, (item) => item.application)
  items: Item[];

  @OneToMany(() => Playlist, (playlist) => playlist.application)
  playlists: Playlist[];

  constructor(description: TDescription, hooks: THooks, compose: string) {
    if (!description || !compose) return;
    this.id = description.id;
    this.catalogFunction = description.catalogFunction;
    this.name = description.name;
    this.exposeServices = JSON.stringify(description.exposeServices);
    this.displayName = description.displayName;
    this.port = description.port;
    this.size = description.size;
    this.version = description.version;
    this.logo = description.logo;
    this.image = description.image;
    this.compose = compose;
    this.hooks = JSON.stringify(hooks);
    this.md5 = applicationMd5(description, hooks, compose);
  }
}
