import {
  BadRequestException,
  Body,
  Controller,
  Get,
  Post,
  Query,
  Res,
} from '@nestjs/common';
import { AuthService } from './auth.service';
import { FastifyReply } from 'fastify';
import {
  ApiBody,
  ApiCreatedResponse,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import {
  PostTokenRequest,
  PostTokenResponse,
} from './definitions/post-token.definition';

@Controller('auth')
@ApiTags('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Get('entra/callback')
  @ApiOperation({ summary: 'Endpoint used by Entra to forward access token' })
  async getEntraCallback(
    @Res() res: FastifyReply,
    @Query('code') code: string,
    @Query('state') state: string,
  ) {
    const decodedState = JSON.parse(
      decodeURIComponent(state.replaceAll('\\', '')),
    );
    if (!decodedState.dashboardUrl) {
      throw new BadRequestException('dashboardUrl is required in state');
    }
    return this.authService.getEntraCallback(res, code, decodedState);
  }

  @Post('token')
  @ApiOperation({ summary: 'Get accessToken from a refresh token' })
  @ApiBody({ type: PostTokenRequest })
  @ApiCreatedResponse({ type: PostTokenResponse })
  @ApiUnauthorizedResponse()
  async postToken(@Body() postTokenRequest: PostTokenRequest) {
    return this.authService.postToken(postTokenRequest);
  }
}
