import { Injectable, UnauthorizedException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { FastifyReply } from 'fastify';
import { EntraService } from 'src/entra/entra.service';
import { IEntraIdToken } from 'src/entra/interfaces/entra/entra-id-token.interface';
import { UsersService } from 'src/users/users.service';
import { CreateUserDto } from 'src/users/dto/create-user.dto';
import { IAuthPayload } from './interfaces/auth-payload.interface';
import ms from 'ms';
import { LogsService } from 'src/logs/logs.service';
import {
  PostTokenRequest,
  PostTokenResponse,
} from './definitions/post-token.definition';
import { LogActionEnum } from 'src/logs/enums/log-action.enum';

@Injectable()
export class AuthService {
  private authSuccessPath: string;
  private authErrorPath: string;
  private jwtExpiresIn: number;
  private jwtRefreshTokenExpiresIn: number;

  constructor(
    private configService: ConfigService,
    private entraService: EntraService,
    private jwtService: JwtService,
    private usersService: UsersService,
    private logsService: LogsService,
  ) {
    this.authSuccessPath = this.configService.get(
      'DASHBOARD_AUTH_SUCCESS_PATH',
    );
    this.authErrorPath = this.configService.get('DASHBOARD_AUTH_ERROR_PATH');
    this.jwtExpiresIn = ms(this.configService.get('JWT_EXPIRES_IN'));
    this.jwtRefreshTokenExpiresIn = ms(
      this.configService.get('JWT_REFRESH_TOKEN_EXPIRES_IN'),
    );
  }

  async getToken(
    entraId: IEntraIdToken,
  ): Promise<{ accessToken: string; expiresAt: number; refreshToken: string }> {
    const payload: IAuthPayload = {
      userId: entraId.oid,
    };
    const accessToken = await this.jwtService.signAsync(payload);
    const expiresAt = Date.now() + this.jwtExpiresIn;
    const refreshToken = await this.jwtService.signAsync(payload, {
      expiresIn: this.jwtRefreshTokenExpiresIn,
    });
    return { accessToken, expiresAt, refreshToken };
  }

  async getEntraCallback(
    res: FastifyReply,
    code: string,
    state: { dashboardUrl: string; redirect?: string },
  ): Promise<void> {
    const result = await this.entraService.getToken({ code });
    if (result) {
      // Get avatar from Entra
      const avatar = await this.entraService.getUserProfilePicture(
        result.entraId.oid,
        result.entraId.name,
      );

      const { entraToken, entraId } = result;
      const user = await this.usersService.createOrUpdate(
        new CreateUserDto(entraToken, entraId, avatar),
      );
      const { accessToken, expiresAt, refreshToken } = await this.getToken(
        result.entraId,
      );

      if (state.redirect === '/token') {
        res.status(200).send(accessToken);
        return;
      }

      const query = [];

      if (state.redirect) {
        query.push(`redirect=${state.redirect}`);
      }

      query.push(`accessToken=${accessToken}`);
      query.push(`expiresAt=${expiresAt}`);
      query.push(`refreshToken=${refreshToken}`);

      res.redirect(
        302,
        `${state.dashboardUrl}${this.authSuccessPath}?${query.join('&')}`,
      );
      await this.logsService.create({
        authorId: user.id,
        action: LogActionEnum.LOGIN,
        description: 'successfully logged in',
      });
    } else {
      res.redirect(302, `${state.dashboardUrl}${this.authErrorPath}`);
    }
  }

  async postToken(postTokenRequest: PostTokenRequest) {
    try {
      const payload = (await this.jwtService.verifyAsync(
        postTokenRequest.refreshToken,
        {
          secret: this.configService.get<string>('JWT_SECRET'),
        },
      )) as IAuthPayload;

      const user = await this.usersService.findOne({
        where: { id: payload.userId },
      });

      if (user === null) {
        throw new UnauthorizedException();
      }

      const { accessToken, expiresAt } = await this.getToken({
        oid: user.id,
      } as IEntraIdToken);

      await this.logsService.create({
        authorId: user.id,
        action: LogActionEnum.LOGIN,
        description: 'successfully refreshed token',
      });

      return new PostTokenResponse(accessToken, expiresAt);
    } catch {
      throw new UnauthorizedException();
    }
  }
}
