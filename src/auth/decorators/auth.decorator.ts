import { SetMetadata, UseGuards, applyDecorators } from '@nestjs/common';
import { AuthGuard } from '../guards/auth.guard';
import { ApiBearerAuth, ApiUnauthorizedResponse } from '@nestjs/swagger';
import { PermissionNameEnum } from 'src/permissions/enums/permission-name.enum';

export function Auth(permissionName?: PermissionNameEnum) {
  return applyDecorators(
    SetMetadata('permissionName', permissionName),
    UseGuards(AuthGuard),
    ApiUnauthorizedResponse({ description: 'Unauthorized' }),
    ApiBearerAuth(),
  );
}
