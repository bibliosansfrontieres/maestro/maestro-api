import { UseGuards, applyDecorators } from '@nestjs/common';
import { ApiBearerAuth, ApiUnauthorizedResponse } from '@nestjs/swagger';
import { GitlabAuthGuard } from '../guards/gitlab-auth.guard';

export function GitlabAuth() {
  return applyDecorators(
    UseGuards(GitlabAuthGuard),
    ApiUnauthorizedResponse({ description: 'Unauthorized' }),
    ApiBearerAuth(),
  );
}
