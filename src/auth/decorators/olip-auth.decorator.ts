import { UseGuards, applyDecorators } from '@nestjs/common';
import { ApiBearerAuth, ApiUnauthorizedResponse } from '@nestjs/swagger';
import { OlipAuthGuard } from '../guards/olip-auth.guard';

export function OlipAuth() {
  return applyDecorators(
    UseGuards(OlipAuthGuard),
    ApiUnauthorizedResponse({ description: 'Unauthorized' }),
    ApiBearerAuth(),
  );
}
