import { UseGuards, applyDecorators } from '@nestjs/common';
import { ApiBearerAuth } from '@nestjs/swagger';
import { OptionalAuthGuard } from '../guards/optional-auth.guard';

export function OptionalAuth() {
  return applyDecorators(UseGuards(OptionalAuthGuard), ApiBearerAuth());
}
