import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { IAuthPayload } from '../interfaces/auth-payload.interface';

/**
 * Creates a decorator that extracts the user ID from the request object in the execution context.
 * This user ID will exists only if a user is connected by Bearer token.
 * If the route that contains this decorator is not protected, the user ID will be undefined.
 *
 * @param {unknown} _ - The data parameter (useless here).
 * @param {ExecutionContext} ctx - The execution context.
 * @return {number} The user ID extracted from the request object.
 */
export const UserId = createParamDecorator(
  (_: unknown, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest();
    const payload = request.payload as IAuthPayload | undefined;
    if (!payload) return undefined;
    return payload.userId;
  },
);
