import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class PostTokenRequest {
  @ApiProperty({ example: 'aaaa000a0a00a00a0a0a0a0a00a000' })
  @IsString()
  refreshToken: string;
}

export class PostTokenResponse {
  @ApiProperty({ example: 'aaaa00000aaa0a0a0a0a0a0000' })
  accessToken: string;

  @ApiProperty({ example: '17091270360125' })
  expiresAt: number;

  constructor(accessToken: string, expiresAt: number) {
    this.accessToken = accessToken;
    this.expiresAt = expiresAt;
  }
}
