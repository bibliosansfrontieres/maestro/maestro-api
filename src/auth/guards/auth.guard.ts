import {
  CanActivate,
  ExecutionContext,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Reflector } from '@nestjs/core';
import { JwtService } from '@nestjs/jwt';
import { EntraService } from 'src/entra/entra.service';
import { PermissionNameEnum } from 'src/permissions/enums/permission-name.enum';
import { UsersService } from 'src/users/users.service';
import { extractTokenFromHeaders } from 'src/utils/extract-token-from-headers.util';
import { storeUserInRequest } from 'src/utils/store-user-in-request.util';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    private jwtService: JwtService,
    private usersService: UsersService,
    private configService: ConfigService,
    private entraService: EntraService,
    private reflector: Reflector,
  ) {}

  /**
   * Asynchronously checks if the user can activate the current route.
   *
   * @param {ExecutionContext} context - the execution context
   * @return {Promise<boolean>} - a promise that resolves to a boolean indicating if the user can activate the route
   */
  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();
    const token = extractTokenFromHeaders(request);

    const permissionName =
      this.reflector.get<PermissionNameEnum>(
        'permissionName',
        context.getHandler(),
      ) || undefined;

    const { isAuthorized, reason } = await storeUserInRequest(
      this.jwtService,
      this.configService,
      this.entraService,
      this.usersService,
      request,
      token,
      permissionName,
    );

    if (!isAuthorized) {
      throw new UnauthorizedException(reason);
    }

    return true;
  }
}
