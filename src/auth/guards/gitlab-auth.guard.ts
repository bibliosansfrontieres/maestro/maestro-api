import {
  CanActivate,
  ExecutionContext,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { extractTokenFromHeaders } from 'src/utils/extract-token-from-headers.util';

@Injectable()
export class GitlabAuthGuard implements CanActivate {
  constructor(private configService: ConfigService) {}

  /**
   * Asynchronously checks if the user can activate the current route.
   *
   * @param {ExecutionContext} context - the execution context
   * @return {Promise<boolean>} - a promise that resolves to a boolean indicating if the user can activate the route
   */
  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();
    const token = extractTokenFromHeaders(request);
    const gitlabToken = this.configService.get('GITLAB_MAESTRO_API_TOKEN');

    if (token !== gitlabToken) {
      throw new UnauthorizedException();
    }

    return true;
  }
}
