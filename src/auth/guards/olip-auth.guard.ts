import {
  CanActivate,
  ExecutionContext,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { extractTokenFromHeaders } from 'src/utils/extract-token-from-headers.util';
import { storeOlipVmInRequest } from 'src/utils/store-olip-vm-in-request.util';
import { VirtualMachinesService } from 'src/virtual-machines/virtual-machines.service';

@Injectable()
export class OlipAuthGuard implements CanActivate {
  constructor(
    private jwtService: JwtService,
    private configService: ConfigService,
    private virtualMachinesService: VirtualMachinesService,
  ) {}

  /**
   * Asynchronously checks if the user can activate the current route.
   *
   * @param {ExecutionContext} context - the execution context
   * @return {Promise<boolean>} - a promise that resolves to a boolean indicating if the user can activate the route
   */
  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();
    const token = extractTokenFromHeaders(request);

    const { isAuthorized, reason } = await storeOlipVmInRequest(
      this.jwtService,
      this.configService,
      this.virtualMachinesService,
      request,
      token,
    );

    if (!isAuthorized) {
      throw new UnauthorizedException(reason);
    }

    return true;
  }
}
