import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { EntraService } from 'src/entra/entra.service';
import { UsersService } from 'src/users/users.service';
import { extractTokenFromHeaders } from 'src/utils/extract-token-from-headers.util';
import { storeUserInRequest } from 'src/utils/store-user-in-request.util';

@Injectable()
export class OptionalAuthGuard implements CanActivate {
  constructor(
    private jwtService: JwtService,
    private usersService: UsersService,
    private configService: ConfigService,
    private entraService: EntraService,
  ) {}

  /**
   * Asynchronously checks if the user can activate the current route.
   *
   * @param {ExecutionContext} context - the execution context
   * @return {Promise<boolean>} - a promise that resolves to a boolean indicating if the user can activate the route
   */
  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();
    const token = extractTokenFromHeaders(request);

    await storeUserInRequest(
      this.jwtService,
      this.configService,
      this.entraService,
      this.usersService,
      request,
      token,
    );

    return true;
  }
}
