import { Test, TestingModule } from '@nestjs/testing';
import { CategoriesController } from './categories.controller';
import { CategoriesService } from './categories.service';
import { Repository } from 'typeorm';
import { Category } from './entities/category.entity';
import { getRepositoryToken } from '@nestjs/typeorm';

describe('CategoriesController', () => {
  let controller: CategoriesController;
  let categoryRepository: Repository<Category>;

  const CATEGORY_REPOSITORY_TOKEN = getRepositoryToken(Category);

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CategoriesController],
      providers: [
        CategoriesService,
        { provide: CATEGORY_REPOSITORY_TOKEN, useClass: Repository<Category> },
      ],
    }).compile();

    controller = module.get<CategoriesController>(CategoriesController);
    categoryRepository = module.get<Repository<Category>>(
      CATEGORY_REPOSITORY_TOKEN,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
    expect(categoryRepository).toBeDefined();
  });
});
