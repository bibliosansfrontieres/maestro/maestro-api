import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Category } from './entities/category.entity';
import { FindManyOptions, FindOneOptions, Repository } from 'typeorm';
import { CreateCategoryDto } from './dto/create-category.dto';
import { CategoryTranslation } from './entities/category-translation.entity';

@Injectable()
export class CategoriesService {
  constructor(
    @InjectRepository(Category)
    private categoryRepository: Repository<Category>,
  ) {}

  create(createCategoryDto: CreateCategoryDto) {
    const categoryTranslations = createCategoryDto.categoryTranslations.map(
      (categoryTranslationDto) =>
        new CategoryTranslation(categoryTranslationDto),
    );
    const category = new Category(createCategoryDto);
    category.categoryTranslations = categoryTranslations;

    return this.categoryRepository.save(category);
  }

  find(options?: FindManyOptions<Category>) {
    return this.categoryRepository.find(options);
  }

  findOne(options?: FindOneOptions<Category>) {
    return this.categoryRepository.findOne(options);
  }
}
