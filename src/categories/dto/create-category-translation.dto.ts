export class CreateCategoryTranslationDto {
  title: string;
  description?: string;
  languageIdentifier: string;
}
