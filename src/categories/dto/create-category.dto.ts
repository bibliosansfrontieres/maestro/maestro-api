import { CategorySourceEnum } from '../enums/category-source.enum';
import { CreateCategoryTranslationDto } from './create-category-translation.dto';

export class CreateCategoryDto {
  id: number;
  pictogram: string;
  source: CategorySourceEnum;
  categoryTranslations: CreateCategoryTranslationDto[];
}
