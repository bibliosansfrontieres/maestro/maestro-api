import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Category } from './category.entity';
import { CreateCategoryTranslationDto } from '../dto/create-category-translation.dto';

@Entity()
export class CategoryTranslation {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  title: string;

  @Column({ nullable: true })
  description: string | null;

  @Column()
  languageIdentifier: string;

  @ManyToOne(() => Category, (category) => category.categoryTranslations)
  category: Category;

  constructor(createCategoryTranslationDto: CreateCategoryTranslationDto) {
    if (!createCategoryTranslationDto) return;
    this.title = createCategoryTranslationDto.title;
    this.description = createCategoryTranslationDto.description;
    this.languageIdentifier = createCategoryTranslationDto.languageIdentifier;
  }
}
