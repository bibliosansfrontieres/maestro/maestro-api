import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToMany,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Playlist } from 'src/playlists/entities/playlist.entity';
import { CategoryTranslation } from './category-translation.entity';
import { CreateCategoryDto } from '../dto/create-category.dto';
import { CategorySourceEnum } from '../enums/category-source.enum';

@Entity()
export class Category {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  source: CategorySourceEnum;

  @Column()
  pictogram: string;

  @ManyToMany(() => Playlist, (playlist) => playlist.categories)
  playlists: Playlist[];

  @OneToMany(
    () => CategoryTranslation,
    (categoryTranslation) => categoryTranslation.category,
    { cascade: true },
  )
  categoryTranslations: CategoryTranslation[];

  @CreateDateColumn()
  createdAt: string;

  @UpdateDateColumn()
  updatedAt: string;

  constructor(createCategoryDto: CreateCategoryDto) {
    if (!createCategoryDto) return;
    this.id = createCategoryDto.id;
    this.source = createCategoryDto.source;
    this.pictogram = createCategoryDto.pictogram;
  }
}
