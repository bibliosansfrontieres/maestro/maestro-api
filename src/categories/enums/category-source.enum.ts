import { ItemSourceEnum } from 'src/items/enums/item-source.enum';

export type CategorySourceEnum = ItemSourceEnum;
