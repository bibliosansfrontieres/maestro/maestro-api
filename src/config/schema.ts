import * as Joi from 'joi';

export const configurationSchema = Joi.object({
  SLACK_WEBHOOK_URL_NOTIFICATIONS_CHANNEL: Joi.string().required(),
  SLACK_WEBHOOK_URL_MONITORING_CHANNEL: Joi.string().required(),
  SLACK_OAUTH_TOKEN: Joi.string().required(),
  MONITORING_MAX_MEMORY_USAGE_PERCENTAGE: Joi.number().required(),
  MONITORING_MAX_DISK_USAGE_PERCENTAGE: Joi.number().required(),
  MONITORING_MAX_AVERAGE_LOAD: Joi.number().required(),
  // Docker configuration
  MAESTRO_VMS_HOST: Joi.string().required(),
  MAESTRO_RESOLVER_ID: Joi.string().required(),
  MAESTRO_NETWORK_ID: Joi.string().required(),
  MAESTRO_VOLUME: Joi.string().required(),
  S3_BUCKET_VOLUME: Joi.string().required(),
  MAESTRO_API_URL_FOR_OLIP: Joi.string().required(),
  ELASTICSEARCH_API_URL_FOR_OLIP: Joi.string().required(),
  // Gitlab configuration
  GITLAB_MAESTRO_API_TOKEN: Joi.string().required(),
  PUBLIC_APPLICATIONS_CATALOG_URL: Joi.string().required(),
  PRIVATE_APPLICATIONS_CATALOG_URL: Joi.string().required(),
  // S3 configuration
  S3_ACCESS_KEY: Joi.string().required(),
  S3_SECRET_KEY: Joi.string().required(),
  S3_ENDPOINT: Joi.string().required(),
  S3_REGION: Joi.string().required(),
  S3_BUCKET: Joi.string().required(),
  // Entra configuration
  ENTRA_TENANT_ID: Joi.string().required(),
  ENTRA_CLIENT_ID: Joi.string().required(),
  ENTRA_CLIENT_SECRET: Joi.string().required(),
  ENTRA_SCOPES: Joi.string().required(),
  // Omeka configuration
  OMEKA_API_URL: Joi.string().required(),
  OMEKA_API_KEY: Joi.string().required(),
  OMEKA_API_FETCHING_THREADS: Joi.number().default(20),
  // Maestro configuration
  MAESTRO_BRANCH: Joi.string().required(),
  API_PORT: Joi.number().default(80),
  API_THROTTLE_TTL: Joi.number().default(60000),
  API_THROTTLE_LIMIT: Joi.number().default(100),
  API_BASE_URL: Joi.string().required(),
  APPLICATIONS_CATALOG_URL: Joi.string().required(),
  JWT_SECRET: Joi.string().required(),
  JWT_EXPIRES_IN: Joi.string().default('1h'),
  JWT_REFRESH_TOKEN_EXPIRES_IN: Joi.string().default('24h'),
  DASHBOARD_AUTH_SUCCESS_PATH: Joi.string().default('/auth'),
  DASHBOARD_AUTH_ERROR_PATH: Joi.string().default('/auth/error'),
  APPLICATIONS_REGISTRY_PATH: Joi.string().required(),
  OLIP_DEPLOY_REPOSITORY: Joi.string().required(),
  GITLAB_TOKEN: Joi.string().required(),
  DOCKER_PRIVATE_APPS_REPOSITORY_TOKEN: Joi.string().required(),
});
