import { registerAs } from '@nestjs/config';
import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import { DataSource, DataSourceOptions } from 'typeorm';

const config: TypeOrmModuleOptions = {
  type: 'sqlite',
  database: 'data/databases/maestro.db',
  autoLoadEntities: true,
  synchronize: true,
  migrations: ['dist/migrations/*{.ts,.js}'],
};

export default registerAs('typeorm', () => config);
export const connectionSource = new DataSource(config as DataSourceOptions);
export const typeormConfig = config;
