import { Test, TestingModule } from '@nestjs/testing';
import { ContextsController } from './contexts.controller';
import { ContextsService } from './contexts.service';
import { Repository } from 'typeorm';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Context } from './entities/context.entity';
import { ApplicationsService } from 'src/applications/applications.service';
import { applicationsServiceMock } from 'src/utils/mocks/services/applications-service.mock';
import { LogsService } from 'src/logs/logs.service';
import { logsServiceMock } from 'src/utils/mocks/services/logs-service.mock';
import { JwtService } from '@nestjs/jwt';
import { jwtServiceMock } from 'src/utils/mocks/services/jwt-service.mock';
import { UsersService } from 'src/users/users.service';
import { EntraService } from 'src/entra/entra.service';
import { ConfigService } from '@nestjs/config';
import { configServiceMock } from 'src/utils/mocks/services/config-service.mock';
import { entraServiceMock } from 'src/utils/mocks/services/entra-service.mock';
import { usersServiceMock } from 'src/utils/mocks/services/users-service.mock';
import { VirtualMachinesService } from 'src/virtual-machines/virtual-machines.service';
import { virtualMachinesServiceMock } from 'src/utils/mocks/services/virtual-machines-service.mock';

describe('ContextsController', () => {
  let controller: ContextsController;
  let contextRepository: Repository<Context>;

  const CONTEXT_REPOSITORY_TOKEN = getRepositoryToken(Context);

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ContextsController],
      providers: [
        ContextsService,
        ApplicationsService,
        LogsService,
        JwtService,
        UsersService,
        EntraService,
        ConfigService,
        VirtualMachinesService,
        {
          provide: CONTEXT_REPOSITORY_TOKEN,
          useClass: Repository<Context>,
        },
      ],
    })
      .overrideProvider(ApplicationsService)
      .useValue(applicationsServiceMock)
      .overrideProvider(LogsService)
      .useValue(logsServiceMock)
      .overrideProvider(JwtService)
      .useValue(jwtServiceMock)
      .overrideProvider(UsersService)
      .useValue(usersServiceMock)
      .overrideProvider(EntraService)
      .useValue(entraServiceMock)
      .overrideProvider(ConfigService)
      .useValue(configServiceMock)
      .overrideProvider(VirtualMachinesService)
      .useValue(virtualMachinesServiceMock)
      .compile();

    controller = module.get<ContextsController>(ContextsController);
    contextRepository = module.get<Repository<Context>>(
      CONTEXT_REPOSITORY_TOKEN,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
    expect(contextRepository).toBeDefined();
  });
});
