import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
} from '@nestjs/common';
import { ContextsService } from './contexts.service';
import {
  PatchDefaultContextRequest,
  PatchDefaultContextResponse,
} from './definitions/patch-default-context.definition';
import {
  ApiBadRequestResponse,
  ApiCreatedResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { Auth } from 'src/auth/decorators/auth.decorator';
import { PermissionNameEnum } from 'src/permissions/enums/permission-name.enum';
import { UserId } from 'src/auth/decorators/user-id.decorator';
import {
  PatchContextRequest,
  PatchContextResponse,
} from './definitions/patch-context.definition';
import { GetDefaultContextResponse } from './definitions/get-default-context.definition';
import { GetContextResponse } from './definitions/get-context.definition';
import { PostDefaultContextApplicationResponse } from './definitions/post-default-context.definition';
import { DeleteDefaultContextApplication } from './definitions/delete-default-context-application.definition';
import { PostContextApplicationResponse } from './definitions/post-context-application.definition';
import { DeleteContextApplicationResponse } from './definitions/delete-context-application.definition';

@Controller('contexts')
@ApiTags('contexts')
export class ContextsController {
  constructor(private readonly contextsService: ContextsService) {}

  @Get('default')
  @Auth(PermissionNameEnum.READ_VIRTUAL_MACHINE)
  @ApiOperation({ summary: 'Get default context' })
  @ApiOkResponse({ type: GetDefaultContextResponse })
  @ApiNotFoundResponse({ description: 'Default context not found' })
  getDefaultContext() {
    return this.contextsService.getDefaultContext();
  }

  @Patch('default')
  @Auth(PermissionNameEnum.UPDATE_DEFAULT_CONTEXT)
  @ApiOperation({ summary: 'Update default context' })
  @ApiOkResponse({ type: PatchDefaultContextResponse })
  @ApiNotFoundResponse({ description: 'Context not found' })
  @ApiBadRequestResponse({ description: 'Invalid body request parameters' })
  patchDefaultContext(
    @UserId() userId: string,
    @Body() patchDefaultContextRequest: PatchDefaultContextRequest,
  ) {
    return this.contextsService.patchDefaultContext(
      userId,
      patchDefaultContextRequest,
    );
  }

  @Post('default/:applicationId')
  @Auth(PermissionNameEnum.UPDATE_DEFAULT_CONTEXT)
  @ApiOperation({ summary: 'Add application authorization to default context' })
  @ApiCreatedResponse({ type: PostDefaultContextApplicationResponse })
  @ApiNotFoundResponse({ description: 'Context/application not found' })
  postDefaultContextApplication(
    @UserId() userId: string,
    @Param('applicationId') applicationId: string,
  ) {
    return this.contextsService.postDefaultContextApplication(
      userId,
      applicationId,
    );
  }

  @Delete('default/:applicationId')
  @Auth(PermissionNameEnum.UPDATE_DEFAULT_CONTEXT)
  @ApiOperation({
    summary: 'Remove application authorization to default context',
  })
  @ApiOkResponse({ type: DeleteDefaultContextApplication })
  @ApiNotFoundResponse({ description: 'Context/application not found' })
  deleteDefaultContextApplication(
    @UserId() userId: string,
    @Param('applicationId') applicationId: string,
  ) {
    return this.contextsService.deleteDefaultContextApplication(
      userId,
      applicationId,
    );
  }

  @Get(':id')
  @Auth(PermissionNameEnum.READ_VIRTUAL_MACHINE)
  @ApiOperation({ summary: 'Get context by ID' })
  @ApiOkResponse({ type: GetContextResponse })
  @ApiNotFoundResponse({ description: 'Context not found' })
  getContext(@Param('id') id: string) {
    return this.contextsService.getContext(+id);
  }

  @Patch(':id')
  @Auth(PermissionNameEnum.UPDATE_CONTEXT)
  @ApiOperation({ summary: 'Update context' })
  @ApiOkResponse({ type: PatchContextResponse })
  @ApiNotFoundResponse({ description: 'Context/application not found' })
  @ApiBadRequestResponse({
    description: 'Invalid body request parameters or default context ID sent',
  })
  patchContext(
    @UserId() userId: string,
    @Param('id') id: string,
    @Body() patchContextRequest: PatchContextRequest,
  ) {
    return this.contextsService.patchContext(userId, +id, patchContextRequest);
  }

  @Post(':id/:applicationId')
  @Auth(PermissionNameEnum.UPDATE_CONTEXT)
  @ApiOperation({ summary: 'Add application authorization to context' })
  @ApiCreatedResponse({ type: PostContextApplicationResponse })
  @ApiNotFoundResponse({ description: 'Context/application not found' })
  @ApiBadRequestResponse({
    description: 'Default context ID sent',
  })
  postContextApplication(
    @UserId() userId: string,
    @Param('id') id: string,
    @Param('applicationId') applicationId: string,
  ) {
    return this.contextsService.postContextApplication(
      userId,
      +id,
      applicationId,
    );
  }

  @Delete(':id/:applicationId')
  @Auth(PermissionNameEnum.UPDATE_CONTEXT)
  @ApiOperation({
    summary: 'Remove application authorization to context',
  })
  @ApiOkResponse({ type: DeleteContextApplicationResponse })
  @ApiNotFoundResponse({ description: 'Context/application not found' })
  @ApiBadRequestResponse({
    description: 'Default context ID sent',
  })
  deleteContextApplication(
    @UserId() userId: string,
    @Param('id') id: string,
    @Param('applicationId') applicationId: string,
  ) {
    return this.contextsService.deleteContextApplication(
      userId,
      +id,
      applicationId,
    );
  }
}
