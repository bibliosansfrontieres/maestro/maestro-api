import { forwardRef, Module } from '@nestjs/common';
import { ContextsService } from './contexts.service';
import { ContextsController } from './contexts.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Context } from './entities/context.entity';
import { ApplicationsModule } from 'src/applications/applications.module';
import { JwtModule } from '@nestjs/jwt';
import { UsersModule } from 'src/users/users.module';
import { EntraModule } from 'src/entra/entra.module';
import { LogsModule } from 'src/logs/logs.module';
import { VirtualMachinesModule } from 'src/virtual-machines/virtual-machines.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([Context]),
    ApplicationsModule,
    JwtModule,
    UsersModule,
    EntraModule,
    LogsModule,
    forwardRef(() => VirtualMachinesModule),
  ],
  controllers: [ContextsController],
  providers: [ContextsService],
  exports: [ContextsService],
})
export class ContextsModule {}
