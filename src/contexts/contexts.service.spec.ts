import { Test, TestingModule } from '@nestjs/testing';
import { ContextsService } from './contexts.service';
import { Context } from './entities/context.entity';
import { Repository } from 'typeorm';
import { getRepositoryToken } from '@nestjs/typeorm';
import { ApplicationsService } from 'src/applications/applications.service';
import { LogsService } from 'src/logs/logs.service';
import { applicationsServiceMock } from 'src/utils/mocks/services/applications-service.mock';
import { logsServiceMock } from 'src/utils/mocks/services/logs-service.mock';
import { UsersService } from 'src/users/users.service';
import { usersServiceMock } from 'src/utils/mocks/services/users-service.mock';
import { CreateContextDto } from './dto/create-context.dto';
import { applicationMock } from 'src/utils/mocks/objects/application.mock';
import { contextMock } from 'src/utils/mocks/objects/context.mock';
import {
  BadRequestException,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { virtualMachineMock } from 'src/utils/mocks/objects/virtual-machine.mock';
import { userMock } from 'src/utils/mocks/objects/user.mock';
import { logMock } from 'src/utils/mocks/objects/log.mock';
import * as userCanPatchVmUtil from 'src/utils/user-can-patch-vm.util';
import { LogActionEnum } from 'src/logs/enums/log-action.enum';
import { LogEntityEnum } from 'src/logs/enums/log-entity.enum';
import { PostContextApplicationResponse } from './definitions/post-context-application.definition';
import { DeleteContextApplicationResponse } from './definitions/delete-context-application.definition';
import { VirtualMachinesService } from 'src/virtual-machines/virtual-machines.service';
import { virtualMachinesServiceMock } from 'src/utils/mocks/services/virtual-machines-service.mock';

describe('ContextsService', () => {
  let service: ContextsService;
  let applicationsService: ApplicationsService;
  let usersService: UsersService;
  let logsService: LogsService;
  let virtualMachinesService: VirtualMachinesService;
  let contextRepository: Repository<Context>;

  const CONTEXT_REPOSITORY_TOKEN = getRepositoryToken(Context);

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ContextsService,
        ApplicationsService,
        LogsService,
        UsersService,
        VirtualMachinesService,
        { provide: CONTEXT_REPOSITORY_TOKEN, useClass: Repository<Context> },
      ],
    })
      .overrideProvider(ApplicationsService)
      .useValue(applicationsServiceMock)
      .overrideProvider(LogsService)
      .useValue(logsServiceMock)
      .overrideProvider(UsersService)
      .useValue(usersServiceMock)
      .overrideProvider(VirtualMachinesService)
      .useValue(virtualMachinesServiceMock)
      .compile();

    service = module.get<ContextsService>(ContextsService);
    contextRepository = module.get<Repository<Context>>(
      CONTEXT_REPOSITORY_TOKEN,
    );

    applicationsService = module.get<ApplicationsService>(ApplicationsService);
    usersService = module.get<UsersService>(UsersService);
    logsService = module.get<LogsService>(LogsService);
    virtualMachinesService = module.get<VirtualMachinesService>(
      VirtualMachinesService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
    expect(contextRepository).toBeDefined();
  });

  describe('create', () => {
    const createContextDto: CreateContextDto = {
      isOutsideProjectDownloadsAuthorized: true,
      applicationIds: [
        '413bcf99-ee71-4ac6-bc4d-106c482027dc',
        '113bcf99-ee71-4ac6-bc4d-106c482027dc',
      ],
    };
    const isDefaultContext = false;

    const mockApplication = applicationMock({
      id: createContextDto.applicationIds[0],
    });

    const mockContext = contextMock();

    beforeEach(async () => {
      jest.spyOn(service, 'findOne').mockResolvedValue(mockContext);
      jest
        .spyOn(applicationsService, 'findOne')
        .mockResolvedValue(mockApplication);
      jest.spyOn(contextRepository, 'save').mockResolvedValue(mockContext);
    });

    afterEach(() => {
      jest.clearAllMocks();
    });

    it('should be defined', () => {
      expect(service.create).toBeDefined();
    });

    it("should call findOne with right arguments if it's not default context", async () => {
      await service.create(createContextDto, isDefaultContext);
      expect(service.findOne).toHaveBeenCalledWith({
        where: { isDefaultContext: true },
      });
    });

    it("should not call findOne if it's default context", async () => {
      await service.create(createContextDto, true);
      expect(service.findOne).not.toHaveBeenCalled();
    });

    it('should throw if no application', async () => {
      jest.spyOn(applicationsService, 'findOne').mockResolvedValue(null);
      expect(
        service.create(createContextDto, isDefaultContext),
      ).rejects.toThrow(NotFoundException);
    });

    it('should call findOne for each applicationId', async () => {
      await service.create(createContextDto, isDefaultContext);

      for (const applicationId of createContextDto.applicationIds) {
        expect(applicationsService.findOne).toHaveBeenCalledWith({
          where: { id: applicationId },
        });
      }

      expect(applicationsService.findOne).toHaveBeenCalledTimes(
        createContextDto.applicationIds.length,
      );
    });

    it('should save Context', async () => {
      jest.spyOn(service, 'save').mockResolvedValue(mockContext);
      await service.create(createContextDto, isDefaultContext);
      expect(service.save).toHaveBeenCalled();
    });

    it('should return Context', async () => {
      const result = await service.create(createContextDto, isDefaultContext);
      expect(result).toEqual(mockContext);
    });
  });

  describe('patchContext', () => {
    const authorId = Date.now().toString();
    const id = Date.now();
    const patchContextRequest = {
      isOutsideProjectDownloadsAuthorized: true,
    };

    const mockApplications = applicationMock();
    const mockUser = userMock({ id: authorId });
    const mockVirtualMachine = virtualMachineMock({ user: mockUser });
    const mockContext = contextMock({
      id: id,
      applications: [mockApplications],
      virtualMachine: mockVirtualMachine,
    });

    beforeEach(async () => {
      jest.spyOn(service, 'findOne').mockResolvedValue(mockContext);
      jest.spyOn(service, 'update').mockResolvedValue(mockContext);
      jest.spyOn(usersService, 'findOne').mockResolvedValue(mockUser);
      jest.spyOn(userCanPatchVmUtil, 'userCanPatchVm').mockReturnValue(true);
    });

    afterEach(() => {
      jest.clearAllMocks();
    });

    it('should be defined', () => {
      expect(service.patchContext).toBeDefined();
    });

    it('should call findOne with right arguments', async () => {
      await service.patchContext(authorId, id, patchContextRequest);
      expect(service.findOne).toHaveBeenCalledWith({
        where: { id },
        relations: {
          applications: { applicationTranslations: true },
          virtualMachine: { user: true },
        },
      });
    });

    it('should throw if no context', async () => {
      jest.spyOn(service, 'findOne').mockResolvedValue(null);
      expect(
        service.patchContext(authorId, id, patchContextRequest),
      ).rejects.toThrow(NotFoundException);
    });

    it('should throw if virtual machine not found', async () => {
      const mockContextWithoutVirtualMachine = contextMock({
        id: id,
        virtualMachine: null,
      });
      jest
        .spyOn(service, 'findOne')
        .mockResolvedValue(mockContextWithoutVirtualMachine);
      expect(
        service.patchContext(authorId, id, patchContextRequest),
      ).rejects.toThrow(BadRequestException);
    });

    it('should throw if context is defaultContext', async () => {
      const mockDefaultContext = contextMock({
        id: id,
        isDefaultContext: true,
      });
      jest.spyOn(service, 'findOne').mockResolvedValue(mockDefaultContext);
      expect(
        service.patchContext(authorId, id, patchContextRequest),
      ).rejects.toThrow(BadRequestException);
    });

    it('should call user.findOne with right arguments', async () => {
      await service.patchContext(authorId, id, patchContextRequest);
      expect(usersService.findOne).toHaveBeenCalledWith({
        where: { id: authorId },
        relations: { role: true, groups: { virtualMachines: true } },
      });
    });

    it('should throw if user not found', async () => {
      jest.spyOn(usersService, 'findOne').mockReturnValue(null);
      expect(
        service.patchContext(authorId, id, patchContextRequest),
      ).rejects.toThrow(NotFoundException);
    });

    it('should throw if user cannot patch vm', async () => {
      jest.spyOn(userCanPatchVmUtil, 'userCanPatchVm').mockReturnValue(false);
      expect(
        service.patchContext(authorId, id, patchContextRequest),
      ).rejects.toThrow(UnauthorizedException);
    });

    it('should call update with right arguments', async () => {
      await service.patchContext(authorId, id, patchContextRequest);
      expect(service.update).toHaveBeenCalledWith(
        mockContext,
        patchContextRequest,
      );
    });

    it('should call virtualMachinesService.createOrUpdateDeployContextFile with right arguments', async () => {
      await service.patchContext(authorId, id, patchContextRequest);
      expect(
        virtualMachinesService.createOrUpdateDeployContextFile,
      ).toHaveBeenCalledWith(mockContext.virtualMachine, true);
    });

    it('should call logsService.create with right arguments', async () => {
      await service.patchContext(authorId, id, patchContextRequest);
      expect(logsService.create).toHaveBeenCalledWith({
        authorId,
        action: LogActionEnum.UPDATE,
        entity: LogEntityEnum.CONTEXT,
        entityId: id,
        values: patchContextRequest,
      });
    });

    it('should return context', async () => {
      const response = await service.patchContext(
        authorId,
        id,
        patchContextRequest,
      );
      expect(response).toBe(mockContext);
    });
  });

  describe('postContextApplication', () => {
    const contextId = Date.now();
    const applicationId = '413bcf99-ee71-4ac6-bc4d-106c482027dc';
    const authorId = '1';

    const mockLog = logMock();

    const mockApplication = applicationMock({
      id: applicationId,
    });
    const mockUser = userMock({ id: authorId });
    const mockVirtualMachine = virtualMachineMock({ user: mockUser });

    const mockContext = contextMock({
      id: contextId,
      applications: [mockApplication],
      virtualMachine: mockVirtualMachine,
    });

    beforeEach(async () => {
      jest.spyOn(service, 'findOne').mockResolvedValue(mockContext);
      jest.spyOn(usersService, 'findOne').mockResolvedValue(mockUser);
      jest.spyOn(service, 'addApplication').mockResolvedValue(mockContext);
      jest.spyOn(logsService, 'create').mockResolvedValue(mockLog);
      jest.spyOn(userCanPatchVmUtil, 'userCanPatchVm').mockReturnValue(true);
      jest
        .spyOn(virtualMachinesService, 'createOrUpdateDeployContextFile')
        .mockResolvedValue();
    });

    afterEach(() => {
      jest.clearAllMocks();
    });

    it('should be defined', () => {
      expect(service.postContextApplication).toBeDefined();
    });

    it('should call findOne with right arguments', async () => {
      await service.postContextApplication(authorId, contextId, applicationId);
      expect(service.findOne).toHaveBeenCalledWith({
        where: { id: contextId },
        relations: {
          applications: { applicationTranslations: true },
          virtualMachine: { user: true },
        },
      });
    });

    it('should throw if no context', async () => {
      jest.spyOn(service, 'findOne').mockResolvedValue(null);
      expect(
        service.postContextApplication(authorId, contextId, applicationId),
      ).rejects.toThrow(NotFoundException);
    });

    it('should throw if virtual machine not found', async () => {
      const mockContextWithoutVirtualMachine = contextMock({
        id: contextId,
        virtualMachine: null,
      });
      jest
        .spyOn(service, 'findOne')
        .mockResolvedValue(mockContextWithoutVirtualMachine);
      expect(
        service.postContextApplication(authorId, contextId, applicationId),
      ).rejects.toThrow(BadRequestException);
    });

    it('should throw if context is defaultContext', async () => {
      const mockDefaultContext = contextMock({
        id: contextId,
        isDefaultContext: true,
      });
      jest.spyOn(service, 'findOne').mockResolvedValue(mockDefaultContext);
      expect(
        service.postContextApplication(authorId, contextId, applicationId),
      ).rejects.toThrow(BadRequestException);
    });

    it('should call user.findOne with right arguments', async () => {
      await service.postContextApplication(authorId, contextId, applicationId);
      expect(usersService.findOne).toHaveBeenCalledWith({
        where: { id: authorId },
        relations: { role: true, groups: { virtualMachines: true } },
      });
    });

    it('should throw if user not found', async () => {
      jest.spyOn(usersService, 'findOne').mockReturnValue(null);
      expect(
        service.postContextApplication(authorId, contextId, applicationId),
      ).rejects.toThrow(NotFoundException);
    });

    it('should throw if user cannot patch vm', async () => {
      jest.spyOn(userCanPatchVmUtil, 'userCanPatchVm').mockReturnValue(false);
      expect(
        service.postContextApplication(authorId, contextId, applicationId),
      ).rejects.toThrow(UnauthorizedException);
    });

    it('should call addApplication with right arguments', async () => {
      await service.postContextApplication(authorId, contextId, applicationId);
      expect(service.addApplication).toHaveBeenCalledWith(
        mockContext,
        applicationId,
      );
    });

    it('should call virtualMachinesService.createOrUpdateDeployContextFile with right arguments', async () => {
      await service.postContextApplication(authorId, contextId, applicationId);
      expect(
        virtualMachinesService.createOrUpdateDeployContextFile,
      ).toHaveBeenCalledWith(mockContext.virtualMachine, true);
    });

    it('should call logsService.create with right arguments', async () => {
      await service.postContextApplication(authorId, contextId, applicationId);
      expect(logsService.create).toHaveBeenCalledWith({
        authorId,
        action: LogActionEnum.UPDATE,
        entity: LogEntityEnum.CONTEXT,
        entityId: contextId,
        values: { applicationId },
        description: 'added application to context',
      });
    });

    it('should return PostContextApplicationResponse', async () => {
      const response = await service.postContextApplication(
        authorId,
        contextId,
        applicationId,
      );
      expect(response).toBeInstanceOf(PostContextApplicationResponse);
    });
  });

  describe('deleteContextApplication', () => {
    const contextId = Date.now();
    const applicationId = '413bcf99-ee71-4ac6-bc4d-106c482027dc';
    const authorId = '1';

    const mockLog = logMock();

    const mockApplication = applicationMock({
      id: applicationId,
    });
    const mockUser = userMock({ id: authorId });
    const mockVirtualMachine = virtualMachineMock({ user: mockUser });

    const mockContext = contextMock({
      id: contextId,
      applications: [mockApplication],
      virtualMachine: mockVirtualMachine,
    });

    beforeEach(async () => {
      jest.spyOn(service, 'findOne').mockResolvedValue(mockContext);
      jest.spyOn(usersService, 'findOne').mockResolvedValue(mockUser);
      jest.spyOn(service, 'removeApplication').mockResolvedValue(mockContext);
      jest.spyOn(logsService, 'create').mockResolvedValue(mockLog);
      jest.spyOn(userCanPatchVmUtil, 'userCanPatchVm').mockReturnValue(true);
      jest
        .spyOn(virtualMachinesService, 'createOrUpdateDeployContextFile')
        .mockResolvedValue();
    });

    afterEach(() => {
      jest.clearAllMocks();
    });

    it('should be defined', () => {
      expect(service.deleteContextApplication).toBeDefined();
    });

    it('should call findOne with right arguments', async () => {
      await service.deleteContextApplication(
        authorId,
        contextId,
        applicationId,
      );
      expect(service.findOne).toHaveBeenCalledWith({
        where: { id: contextId },
        relations: {
          applications: { applicationTranslations: true },
          virtualMachine: { user: true },
        },
      });
    });

    it('should throw if no context', async () => {
      jest.spyOn(service, 'findOne').mockResolvedValue(null);
      expect(
        service.deleteContextApplication(authorId, contextId, applicationId),
      ).rejects.toThrow(NotFoundException);
    });

    it('should throw if virtual machine not found', async () => {
      const mockContextWithoutVirtualMachine = contextMock({
        id: contextId,
        virtualMachine: null,
      });
      jest
        .spyOn(service, 'findOne')
        .mockResolvedValue(mockContextWithoutVirtualMachine);
      expect(
        service.deleteContextApplication(authorId, contextId, applicationId),
      ).rejects.toThrow(BadRequestException);
    });

    it('should throw if context is defaultContext', async () => {
      const mockDefaultContext = contextMock({
        id: contextId,
        isDefaultContext: true,
      });
      jest.spyOn(service, 'findOne').mockResolvedValue(mockDefaultContext);
      expect(
        service.deleteContextApplication(authorId, contextId, applicationId),
      ).rejects.toThrow(BadRequestException);
    });

    it('should call user.findOne with right arguments', async () => {
      await service.deleteContextApplication(
        authorId,
        contextId,
        applicationId,
      );
      expect(usersService.findOne).toHaveBeenCalledWith({
        where: { id: authorId },
        relations: { role: true, groups: { virtualMachines: true } },
      });
    });

    it('should throw if user not found', async () => {
      jest.spyOn(usersService, 'findOne').mockReturnValue(null);
      expect(
        service.deleteContextApplication(authorId, contextId, applicationId),
      ).rejects.toThrow(NotFoundException);
    });

    it('should throw if user cannot patch vm', async () => {
      jest.spyOn(userCanPatchVmUtil, 'userCanPatchVm').mockReturnValue(false);
      expect(
        service.deleteContextApplication(authorId, contextId, applicationId),
      ).rejects.toThrow(UnauthorizedException);
    });

    it('should call removeApplication with right arguments', async () => {
      await service.deleteContextApplication(
        authorId,
        contextId,
        applicationId,
      );
      expect(service.removeApplication).toHaveBeenCalledWith(
        mockContext,
        applicationId,
      );
    });

    it('should call virtualMachinesService.createOrUpdateDeployContextFile with right arguments', async () => {
      await service.deleteContextApplication(
        authorId,
        contextId,
        applicationId,
      );
      expect(
        virtualMachinesService.createOrUpdateDeployContextFile,
      ).toHaveBeenCalledWith(mockContext.virtualMachine, true);
    });

    it('should call logsService.create with right arguments', async () => {
      await service.deleteContextApplication(
        authorId,
        contextId,
        applicationId,
      );
      expect(logsService.create).toHaveBeenCalledWith({
        authorId,
        action: LogActionEnum.UPDATE,
        entity: LogEntityEnum.CONTEXT,
        entityId: contextId,
        values: { applicationId },
        description: 'removed application from context',
      });
    });

    it('should return DeleteContextApplicationResponse', async () => {
      const response = await service.deleteContextApplication(
        authorId,
        contextId,
        applicationId,
      );
      expect(response).toBeInstanceOf(DeleteContextApplicationResponse);
    });
  });
});
