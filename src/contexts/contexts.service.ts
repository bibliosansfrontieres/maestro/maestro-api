import {
  BadRequestException,
  forwardRef,
  Inject,
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindOneOptions, Repository } from 'typeorm';
import { Context } from './entities/context.entity';
import { CreateContextDto } from './dto/create-context.dto';
import {
  PatchDefaultContextRequest,
  PatchDefaultContextResponse,
} from './definitions/patch-default-context.definition';
import { ApplicationsService } from 'src/applications/applications.service';
import { LogsService } from 'src/logs/logs.service';
import { LogActionEnum } from 'src/logs/enums/log-action.enum';
import { LogEntityEnum } from 'src/logs/enums/log-entity.enum';
import { PatchContextRequest } from './definitions/patch-context.definition';
import { GetDefaultContextResponse } from './definitions/get-default-context.definition';
import { GetContextResponse } from './definitions/get-context.definition';
import { PostDefaultContextApplicationResponse } from './definitions/post-default-context.definition';
import { PostContextApplicationResponse } from './definitions/post-context-application.definition';
import { DeleteContextApplicationResponse } from './definitions/delete-context-application.definition';
import { userCanPatchVm } from 'src/utils/user-can-patch-vm.util';
import { UsersService } from 'src/users/users.service';
import { VirtualMachinesService } from 'src/virtual-machines/virtual-machines.service';

@Injectable()
export class ContextsService {
  constructor(
    @InjectRepository(Context) private contextRepository: Repository<Context>,
    private applicationsService: ApplicationsService,
    @Inject(forwardRef(() => VirtualMachinesService))
    private virtualMachinesService: VirtualMachinesService,
    private logsService: LogsService,
    private usersService: UsersService,
  ) {}

  async create(
    createContextDto: CreateContextDto,
    isDefaultContext: boolean = false,
  ) {
    let originalContext = undefined;

    if (isDefaultContext === false) {
      originalContext = await this.findOne({
        where: { isDefaultContext: true },
      });
    }

    const applications = [];
    if (createContextDto.applicationIds) {
      for (const applicationId of createContextDto.applicationIds) {
        const application = await this.applicationsService.findOne({
          where: { id: applicationId },
        });
        if (!application) {
          throw new NotFoundException(`application ${applicationId} not found`);
        }
        applications.push(application);
      }
    }
    const context = new Context(
      createContextDto,
      isDefaultContext,
      originalContext,
      applications,
    );
    return this.save(context);
  }

  async createFromDefault() {
    const defaultContext = await this.findOne({
      where: { isDefaultContext: true },
      relations: { applications: { applicationTranslations: true } },
    });
    if (defaultContext === null) {
      throw new NotFoundException('default context not found');
    }

    const context = new Context(
      {
        isOutsideProjectDownloadsAuthorized:
          defaultContext.isOutsideProjectDownloadsAuthorized,
      },
      false,
      defaultContext,
      defaultContext.applications,
    );

    return context;
  }

  async cloneContext(id: number) {
    const contextToClone = await this.findOne({
      where: { id },
      relations: { applications: { applicationTranslations: true } },
    });
    if (contextToClone === null) {
      throw new NotFoundException('default context not found');
    }

    const context = new Context(
      {
        isOutsideProjectDownloadsAuthorized:
          contextToClone.isOutsideProjectDownloadsAuthorized,
      },
      false,
      contextToClone,
      contextToClone.applications,
    );

    return this.contextRepository.save(context);
  }

  async getDefaultContext() {
    const context = await this.findOne({
      where: { isDefaultContext: true },
      relations: { applications: { applicationTranslations: true } },
    });
    if (context === null) {
      throw new NotFoundException('default context not found');
    }
    return new GetDefaultContextResponse(context);
  }

  async getContext(id: number) {
    const context = await this.findOne({
      where: { id },
      relations: { applications: { applicationTranslations: true } },
    });
    if (context === null) {
      throw new NotFoundException('context not found');
    }
    return new GetContextResponse(context);
  }

  async patchDefaultContext(
    authorId: string,
    patchDefaultContextRequest: PatchDefaultContextRequest,
  ) {
    let context = await this.findOne({
      where: { isDefaultContext: true },
      relations: { applications: { applicationTranslations: true } },
    });
    if (context === null) {
      throw new NotFoundException('default context not found');
    }

    context = await this.update(context, patchDefaultContextRequest);

    await this.logsService.create({
      authorId,
      action: LogActionEnum.UPDATE,
      entity: LogEntityEnum.CONTEXT,
      entityId: context.id,
      values: patchDefaultContextRequest,
      description: 'default context',
    });

    return new PatchDefaultContextResponse(context);
  }

  async postDefaultContextApplication(authorId: string, applicationId: string) {
    const context = await this.findOne({
      where: { isDefaultContext: true },
      relations: { applications: { applicationTranslations: true } },
    });
    if (context === null) {
      throw new NotFoundException('default context not found');
    }

    await this.addApplication(context, applicationId);

    await this.logsService.create({
      authorId,
      action: LogActionEnum.UPDATE,
      entity: LogEntityEnum.CONTEXT,
      entityId: context.id,
      values: { applicationId },
      description: 'added application to default context',
    });

    return new PostDefaultContextApplicationResponse(context);
  }

  async deleteDefaultContextApplication(
    authorId: string,
    applicationId: string,
  ) {
    const context = await this.findOne({
      where: { isDefaultContext: true },
      relations: { applications: { applicationTranslations: true } },
    });
    if (context === null) {
      throw new NotFoundException('default context not found');
    }

    await this.removeApplication(context, applicationId);

    await this.logsService.create({
      authorId,
      action: LogActionEnum.UPDATE,
      entity: LogEntityEnum.CONTEXT,
      entityId: context.id,
      values: { applicationId },
      description: 'removed application from default context',
    });

    return new PostDefaultContextApplicationResponse(context);
  }

  async patchContext(
    authorId: string,
    id: number,
    patchContextRequest: PatchContextRequest,
  ) {
    let context = await this.findOne({
      where: { id },
      relations: {
        applications: { applicationTranslations: true },
        virtualMachine: { user: true },
      },
    });

    if (context === null) {
      throw new NotFoundException('context not found');
    }

    if (context.virtualMachine === null) {
      throw new BadRequestException('context has no virtual machines');
    }

    if (context.isDefaultContext) {
      throw new BadRequestException(
        'default context cannot be updated with this endpoint',
      );
    }

    const user = await this.usersService.findOne({
      where: { id: authorId },
      relations: { role: true, groups: { virtualMachines: true } },
    });

    if (user === null) {
      throw new NotFoundException('user not found');
    }

    if (!userCanPatchVm(user, context.virtualMachine)) {
      throw new UnauthorizedException('not allowed to patch context');
    }

    context = await this.update(context, patchContextRequest);

    this.virtualMachinesService.createOrUpdateDeployContextFile(
      context.virtualMachine,
      true,
    );

    await this.logsService.create({
      authorId,
      action: LogActionEnum.UPDATE,
      entity: LogEntityEnum.CONTEXT,
      entityId: context.id,
      values: patchContextRequest,
    });

    return context;
  }

  async postContextApplication(
    authorId: string,
    contextId: number,
    applicationId: string,
  ) {
    const context = await this.findOne({
      where: { id: contextId },
      relations: {
        applications: { applicationTranslations: true },
        virtualMachine: { user: true },
      },
    });

    if (context === null) {
      throw new NotFoundException('context not found');
    }

    if (context.virtualMachine === null) {
      throw new BadRequestException('context has no virtual machines');
    }

    if (context.isDefaultContext) {
      throw new BadRequestException(
        'default context cannot be updated with this endpoint',
      );
    }

    const user = await this.usersService.findOne({
      where: { id: authorId },
      relations: { role: true, groups: { virtualMachines: true } },
    });

    if (user === null) {
      throw new NotFoundException('user not found');
    }

    if (!userCanPatchVm(user, context.virtualMachine)) {
      throw new UnauthorizedException('not allowed to patch context');
    }

    await this.addApplication(context, applicationId);

    this.virtualMachinesService.createOrUpdateDeployContextFile(
      context.virtualMachine,
      true,
    );

    await this.logsService.create({
      authorId,
      action: LogActionEnum.UPDATE,
      entity: LogEntityEnum.CONTEXT,
      entityId: context.id,
      values: { applicationId },
      description: 'added application to context',
    });

    return new PostContextApplicationResponse(context);
  }

  async deleteContextApplication(
    authorId: string,
    contextId: number,
    applicationId: string,
  ) {
    const context = await this.findOne({
      where: { id: contextId },
      relations: {
        applications: { applicationTranslations: true },
        virtualMachine: { user: true },
      },
    });

    if (context === null) {
      throw new NotFoundException('context not found');
    }

    if (context.virtualMachine === null) {
      throw new BadRequestException('context has no virtual machines');
    }

    if (context.isDefaultContext) {
      throw new BadRequestException(
        'default context cannot be updated with this endpoint',
      );
    }

    const user = await this.usersService.findOne({
      where: { id: authorId },
      relations: { role: true, groups: { virtualMachines: true } },
    });

    if (user === null) {
      throw new NotFoundException('user not found');
    }

    if (!userCanPatchVm(user, context.virtualMachine)) {
      throw new UnauthorizedException('not allowed to patch context');
    }

    await this.removeApplication(context, applicationId);

    this.virtualMachinesService.createOrUpdateDeployContextFile(
      context.virtualMachine,
      true,
    );

    await this.logsService.create({
      authorId,
      action: LogActionEnum.UPDATE,
      entity: LogEntityEnum.CONTEXT,
      entityId: context.id,
      values: { applicationId },
      description: 'removed application from context',
    });

    return new DeleteContextApplicationResponse(context);
  }

  async update(
    context: Context,
    patchDefaultContextRequest: PatchDefaultContextRequest,
  ) {
    if (
      patchDefaultContextRequest.isOutsideProjectDownloadsAuthorized !==
      undefined
    ) {
      context.isOutsideProjectDownloadsAuthorized =
        patchDefaultContextRequest.isOutsideProjectDownloadsAuthorized;
    }

    return this.save(context);
  }

  async addApplication(context: Context, applicationId: string) {
    const application = await this.applicationsService.findOne({
      where: { id: applicationId },
      relations: { applicationTranslations: true },
    });
    if (application === null) {
      throw new NotFoundException('application not found');
    }

    const findApplication = context.applications.find(
      (app) => app.id === application.id,
    );
    if (findApplication) {
      throw new BadRequestException('application already added');
    }

    context.applications.push(application);

    return this.contextRepository.save(context);
  }

  async removeApplication(context: Context, applicationId: string) {
    const application = await this.applicationsService.findOne({
      where: { id: applicationId },
      relations: { applicationTranslations: true },
    });
    if (application === null) {
      throw new NotFoundException('application not found');
    }

    const findApplication = context.applications.find(
      (app) => app.id === application.id,
    );
    if (!findApplication) {
      throw new BadRequestException('application not added');
    }

    context.applications = context.applications.filter(
      (app) => app.id !== application.id,
    );

    return this.contextRepository.save(context);
  }

  async restoreContext(virtualMachineContext: Context, savedContext: Context) {
    virtualMachineContext.applications = savedContext.applications;
    virtualMachineContext.isOutsideProjectDownloadsAuthorized =
      savedContext.isOutsideProjectDownloadsAuthorized;
    return this.save(virtualMachineContext);
  }

  findOne(options?: FindOneOptions<Context>) {
    return this.contextRepository.findOne(options);
  }

  save(context: Context) {
    return this.contextRepository.save(context);
  }

  delete(context: Context) {
    return this.contextRepository.remove(context);
  }
}
