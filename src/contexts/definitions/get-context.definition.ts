import { ApiProperty } from '@nestjs/swagger';
import { ApplicationTypeNameEnum } from 'src/applications/enums/application-type-name.enum';
import { Context } from '../entities/context.entity';
import { GetDefaultContextResponse } from './get-default-context.definition';

export class GetContextResponse extends GetDefaultContextResponse {
  @ApiProperty({ example: false })
  isDefaultContext: boolean;

  @ApiProperty({
    example: JSON.stringify({
      id: 1,
      isOutsideProjectDownloadsAuthorized: false,
      isDefaultContext: true,
      originalContext: null,
      applications: [
        {
          id: '76f01b66-55f4-420d-ba07-673d5a396699',
          name: 'Kiwix',
          type: ApplicationTypeNameEnum.CONTENT_HOST,
          size: 1000000,
          version: '1.0',
          logo: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAIAQMAAAD+wSzIAAAABlBMVEX///+/v7+jQ3Y5AAAADklEQVQI12P4AIX8EAgALgAD/aNpbtEAAAAASUVORK5CYII',
          image:
            'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAIAQMAAAD+wSzIAAAABlBMVEX///+/v7+jQ3Y5AAAADklEQVQI12P4AIX8EAgALgAD/aNpbtEAAAAASUVORK5CYII',
          compose: 'string',
          hooks: '{hooks:[]}',
          md5Hash: 'ed076287532e86365e841e92bfc50d8c',
          applicationTranslations: [
            {
              id: 1,
              shortDescription: 'My application description',
              longDescription: 'My application long description',
              languageIdentifier: 'eng',
            },
          ],
        },
      ],
    }),
  })
  originalContext: string;

  constructor(context: Context) {
    super(context);
  }
}
