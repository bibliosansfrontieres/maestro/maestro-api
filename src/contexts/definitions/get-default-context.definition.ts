import { ApiProperty } from '@nestjs/swagger';
import { Application } from 'src/applications/entities/application.entity';
import { Context } from '../entities/context.entity';
import { GetApplicationResponse } from 'src/applications/definitions/get-application.definition';

export class GetDefaultContextResponse {
  @ApiProperty({ example: 1 })
  id: number;

  @ApiProperty({ example: true })
  isOutsideProjectDownloadsAuthorized: boolean;

  @ApiProperty({ example: true })
  isDefaultContext: boolean;

  @ApiProperty({ example: null })
  originalContext: string;

  @ApiProperty({ type: GetApplicationResponse, isArray: true })
  applications: Application[];

  constructor(context: Context) {
    this.id = context.id;
    this.isOutsideProjectDownloadsAuthorized =
      context.isOutsideProjectDownloadsAuthorized;
    this.isDefaultContext = context.isDefaultContext;
    this.originalContext = context.originalContext;
    this.applications = context.applications;
  }
}
