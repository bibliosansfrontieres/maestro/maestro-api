import { Context } from '../entities/context.entity';
import { PatchDefaultContextRequest } from './patch-default-context.definition';
import { GetContextResponse } from './get-context.definition';

export class PatchContextRequest extends PatchDefaultContextRequest {}

export class PatchContextResponse extends GetContextResponse {
  constructor(context: Context) {
    super(context);
  }
}
