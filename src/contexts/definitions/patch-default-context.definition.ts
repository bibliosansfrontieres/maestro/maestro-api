import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsBoolean, IsOptional } from 'class-validator';
import { Context } from '../entities/context.entity';
import { GetDefaultContextResponse } from './get-default-context.definition';

export class PatchDefaultContextRequest {
  @ApiPropertyOptional({ example: true })
  @IsBoolean()
  @IsOptional()
  isOutsideProjectDownloadsAuthorized?: boolean;
}

export class PatchDefaultContextResponse extends GetDefaultContextResponse {
  constructor(context: Context) {
    super(context);
  }
}
