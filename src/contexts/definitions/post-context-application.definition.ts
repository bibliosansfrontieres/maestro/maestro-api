import { Context } from '../entities/context.entity';
import { GetContextResponse } from './get-context.definition';

export class PostContextApplicationResponse extends GetContextResponse {
  constructor(context: Context) {
    super(context);
  }
}
