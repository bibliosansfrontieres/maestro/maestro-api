import { Context } from '../entities/context.entity';
import { GetDefaultContextResponse } from './get-default-context.definition';

export class PostDefaultContextApplicationResponse extends GetDefaultContextResponse {
  constructor(context: Context) {
    super(context);
  }
}
