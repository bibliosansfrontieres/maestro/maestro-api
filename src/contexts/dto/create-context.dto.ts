import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean } from 'class-validator';

export class CreateContextDto {
  @ApiProperty({ example: true })
  @IsBoolean()
  isOutsideProjectDownloadsAuthorized: boolean;

  @ApiProperty({ example: ['413bcf99-ee71-4ac6-bc4d-106c482027dc'] })
  applicationIds?: string[];
}
