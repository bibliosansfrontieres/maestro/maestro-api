import { Application } from 'src/applications/entities/application.entity';
import {
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { CreateContextDto } from '../dto/create-context.dto';
import { InternalServerErrorException } from '@nestjs/common';
import { VirtualMachine } from 'src/virtual-machines/entities/virtual-machine.entity';
import { VirtualMachineSave } from 'src/virtual-machine-saves/entities/virtual-machine-save.entity';

@Entity()
export class Context {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  isOutsideProjectDownloadsAuthorized: boolean;

  @Column()
  isDefaultContext: boolean;

  @Column({ nullable: true })
  originalContext: string | null;

  @ManyToMany(() => Application, (application) => application.contexts)
  @JoinTable()
  applications: Application[];

  @OneToOne(() => VirtualMachine, (virtualMachine) => virtualMachine.context, {
    nullable: true,
  })
  virtualMachine: VirtualMachine;

  @OneToOne(
    () => VirtualMachineSave,
    (virtualMachineSave) => virtualMachineSave.context,
    { nullable: true },
  )
  virtualMachineSave: VirtualMachineSave;

  constructor(
    createContextDto: CreateContextDto,
    isDefaultContext: boolean = false,
    originalContext?: Context,
    applications?: Application[],
  ) {
    if (!createContextDto) return;
    this.isDefaultContext = isDefaultContext;
    this.isOutsideProjectDownloadsAuthorized =
      createContextDto.isOutsideProjectDownloadsAuthorized;
    if (isDefaultContext === false) {
      if (!originalContext) {
        throw new InternalServerErrorException('no original context provided');
      }
      for (let app of originalContext.applications) {
        app = { id: app.id } as Application;
      }
      this.originalContext = JSON.stringify(originalContext);
    }
    this.applications = applications;
  }
}
