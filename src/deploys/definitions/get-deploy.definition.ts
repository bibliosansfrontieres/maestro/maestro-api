import { ApiProperty } from '@nestjs/swagger';
import { Deploy } from '../entities/deploy.entity';
import { DeployStatusEnum } from '../enums/deploy-status.enum';
import { VirtualMachineSave } from 'src/virtual-machine-saves/entities/virtual-machine-save.entity';
import { Device } from 'src/devices/entities/device.entity';
import { Label } from 'src/labels/entities/label.entity';
import { User } from 'src/users/entities/user.entity';

export class GetDeployResponse {
  @ApiProperty({ example: 1 })
  id: number;

  @ApiProperty({ example: DeployStatusEnum.DEPLOYING, enum: DeployStatusEnum })
  status: DeployStatusEnum;

  @ApiProperty()
  virtualMachineSave: VirtualMachineSave;

  @ApiProperty()
  device: Device;

  @ApiProperty()
  labels: Label[];

  @ApiProperty()
  createdAt: string;

  @ApiProperty()
  updatedAt: string;

  @ApiProperty()
  user: User;

  constructor(deploy: Deploy) {
    this.id = deploy.id;
    this.status = deploy.status;
    this.virtualMachineSave = deploy.virtualMachineSave;
    this.device = deploy.device;
    this.labels = deploy.labels;
    this.createdAt = deploy.createdAt;
    this.updatedAt = deploy.updatedAt;
    this.user = deploy.user;
  }
}
