import { PageOptionsRequest } from 'src/utils/pagination/dto/page-options.dto';
import { PageResponse } from 'src/utils/pagination/dto/page-response';
import { Deploy } from '../entities/deploy.entity';
import { PageOptions } from 'src/utils/pagination/page-options';
import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsEnum, IsOptional, IsString } from 'class-validator';
import { DeployOrderByEnum } from '../enums/deploy-order-by.enum';
import { DeployStatusEnum } from '../enums/deploy-status.enum';

export class GetDeploysRequest extends PageOptionsRequest {
  @ApiPropertyOptional({ example: '12/30/2021,12/31/2021' })
  @IsOptional()
  @IsString()
  createdDateRange?: string;

  @ApiPropertyOptional({ example: 'search' })
  @IsOptional()
  @IsString()
  query?: string;

  @ApiPropertyOptional({ example: '1,2' })
  @IsString()
  @IsOptional()
  labelIds?: string;

  @ApiPropertyOptional({
    example: `${DeployStatusEnum.DEPLOYED},${DeployStatusEnum.ORDER_SENT}`,
  })
  @IsString()
  @IsOptional()
  statuses?: string;

  @ApiPropertyOptional({ example: '1,2' })
  @IsString()
  @IsOptional()
  userIds?: string;

  @ApiPropertyOptional({
    enum: DeployOrderByEnum,
    example: DeployOrderByEnum.ID,
    default: DeployOrderByEnum.ID,
  })
  @IsOptional()
  @IsEnum(DeployOrderByEnum)
  orderBy?: DeployOrderByEnum;
}

export class GetDeploysResponse extends PageResponse<Deploy> {
  constructor(data: Deploy[], count: number, pageOptions: PageOptions) {
    super(data, count, pageOptions);
  }
}
