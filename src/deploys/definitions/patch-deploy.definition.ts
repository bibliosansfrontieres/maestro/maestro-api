import { ArrayMinSize, IsNumber } from 'class-validator';

export class PatchDeployRequest {
  @IsNumber({}, { each: true })
  @ArrayMinSize(1)
  labelIds: number[];
}
