import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsString, Min } from 'class-validator';

export class PostDeployRequest {
  @ApiProperty({ example: '1' })
  @IsString()
  virtualMachineSaveId: string;

  @ApiProperty({ example: 1 })
  @IsNumber()
  fqdnMaskId: number;

  @ApiProperty({ example: [1, 2] })
  @IsNumber({}, { each: true })
  labelIds: number[];

  @ApiProperty({ example: 5 })
  @IsNumber()
  @Min(1)
  devicesCount: number;
}
