import { Test, TestingModule } from '@nestjs/testing';
import { DeploysController } from './deploys.controller';
import { DeploysService } from './deploys.service';
import { Deploy } from './entities/deploy.entity';
import { Repository } from 'typeorm';
import { getRepositoryToken } from '@nestjs/typeorm';
import { LabelsService } from 'src/labels/labels.service';
import { labelsServiceMock } from 'src/utils/mocks/services/labels-service.mock';
import { FqdnMasksService } from 'src/fqdn-masks/fqdn-masks.service';
import { DevicesService } from 'src/devices/devices.service';
import { fqdnMasksServiceMock } from 'src/utils/mocks/services/fqdn-masks-service.mock';
import { devicesServiceMock } from 'src/utils/mocks/services/devices-service.mock';
import { VirtualMachineSavesService } from 'src/virtual-machine-saves/virtual-machine-saves.service';
import { virtualMachineSavesServiceMock } from 'src/utils/mocks/services/virtual-machine-saves-service.mock';
import { SocketGateway } from 'src/socket/socket.gateway';
import { socketGatewayMock } from 'src/utils/mocks/gateways/socket-gateway.mock';
import { LogsService } from 'src/logs/logs.service';
import { logsServiceMock } from 'src/utils/mocks/services/logs-service.mock';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from 'src/users/users.service';
import { EntraService } from 'src/entra/entra.service';
import { ConfigService } from '@nestjs/config';
import { jwtServiceMock } from 'src/utils/mocks/services/jwt-service.mock';
import { usersServiceMock } from 'src/utils/mocks/services/users-service.mock';
import { entraServiceMock } from 'src/utils/mocks/services/entra-service.mock';
import { configServiceMock } from 'src/utils/mocks/services/config-service.mock';
import { NotificationsService } from 'src/notifications/notifications.service';
import { notificationsServiceMock } from 'src/utils/mocks/services/notifications-service.mock';

describe('DeploysController', () => {
  let controller: DeploysController;
  let deployRepository: Repository<Deploy>;

  const DEPLOY_REPOSITORY_TOKEN = getRepositoryToken(Deploy);

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [DeploysController],
      providers: [
        DeploysService,
        LabelsService,
        FqdnMasksService,
        DevicesService,
        VirtualMachineSavesService,
        SocketGateway,
        LogsService,
        JwtService,
        UsersService,
        EntraService,
        ConfigService,
        NotificationsService,
        {
          provide: DEPLOY_REPOSITORY_TOKEN,
          useClass: Repository<Deploy>,
        },
      ],
    })
      .overrideProvider(LabelsService)
      .useValue(labelsServiceMock)
      .overrideProvider(FqdnMasksService)
      .useValue(fqdnMasksServiceMock)
      .overrideProvider(DevicesService)
      .useValue(devicesServiceMock)
      .overrideProvider(VirtualMachineSavesService)
      .useValue(virtualMachineSavesServiceMock)
      .overrideProvider(SocketGateway)
      .useValue(socketGatewayMock)
      .overrideProvider(LogsService)
      .useValue(logsServiceMock)
      .overrideProvider(JwtService)
      .useValue(jwtServiceMock)
      .overrideProvider(UsersService)
      .useValue(usersServiceMock)
      .overrideProvider(EntraService)
      .useValue(entraServiceMock)
      .overrideProvider(ConfigService)
      .useValue(configServiceMock)
      .overrideProvider(NotificationsService)
      .useValue(notificationsServiceMock)
      .compile();

    controller = module.get<DeploysController>(DeploysController);
    deployRepository = module.get<Repository<Deploy>>(DEPLOY_REPOSITORY_TOKEN);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
    expect(deployRepository).toBeDefined();
  });
});
