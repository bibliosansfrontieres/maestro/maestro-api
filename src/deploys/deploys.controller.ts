import {
  Body,
  Controller,
  Get,
  Param,
  Patch,
  Post,
  Query,
} from '@nestjs/common';
import { DeploysService } from './deploys.service';
import { PostDeployRequest } from './definitions/post-deploy.definition';
import { UserId } from 'src/auth/decorators/user-id.decorator';
import { Auth } from 'src/auth/decorators/auth.decorator';
import { PermissionNameEnum } from 'src/permissions/enums/permission-name.enum';
import {
  ApiBadRequestResponse,
  ApiNotFoundResponse,
  ApiTags,
} from '@nestjs/swagger';
import { GetDeploysRequest } from './definitions/get-deploys.definition';
import { PatchDeployRequest } from './definitions/patch-deploy.definition';

@ApiTags('deploys')
@Controller('deploys')
export class DeploysController {
  constructor(private readonly deploysService: DeploysService) {}

  @Post()
  @Auth(PermissionNameEnum.CREATE_DEPLOY)
  @ApiNotFoundResponse({ description: 'virtualMachine/label not found' })
  @ApiBadRequestResponse({
    description: 'bad body request / not enough devices available',
  })
  postDeploy(
    @UserId() userId: string,
    @Body() postDeployRequest: PostDeployRequest,
  ) {
    return this.deploysService.postDeploy(userId, postDeployRequest);
  }

  @Get(':id')
  @Auth(PermissionNameEnum.READ_DEPLOY)
  @ApiNotFoundResponse({ description: 'deploy not found' })
  getDeploy(@Param('id') id: string) {
    return this.deploysService.getDeploy(+id);
  }

  @Get()
  @Auth(PermissionNameEnum.READ_DEPLOY)
  @ApiBadRequestResponse({ description: 'bad query request' })
  getDeploys(@Query() getDeploysRequest: GetDeploysRequest) {
    return this.deploysService.getDeploys(getDeploysRequest);
  }

  @Patch(':id')
  @Auth(PermissionNameEnum.UPDATE_DEPLOY)
  @ApiNotFoundResponse({ description: 'deploy not found' })
  @ApiBadRequestResponse({ description: 'bad body request' })
  patchDeploy(
    @Param('id') id: string,
    @Body() patchDeployRequest: PatchDeployRequest,
  ) {
    return this.deploysService.patchDeploy(+id, patchDeployRequest);
  }
}
