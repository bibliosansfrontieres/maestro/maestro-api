import { Module, forwardRef } from '@nestjs/common';
import { DeploysService } from './deploys.service';
import { DeploysController } from './deploys.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Deploy } from './entities/deploy.entity';
import { LabelsModule } from 'src/labels/labels.module';
import { FqdnMasksModule } from 'src/fqdn-masks/fqdn-masks.module';
import { DevicesModule } from 'src/devices/devices.module';
import { VirtualMachineSavesModule } from 'src/virtual-machine-saves/virtual-machine-saves.module';
import { LogsModule } from 'src/logs/logs.module';
import { SocketModule } from 'src/socket/socket.module';
import { JwtModule } from '@nestjs/jwt';
import { UsersModule } from 'src/users/users.module';
import { EntraModule } from 'src/entra/entra.module';
import { NotificationsModule } from 'src/notifications/notifications.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([Deploy]),
    LabelsModule,
    FqdnMasksModule,
    forwardRef(() => DevicesModule),
    VirtualMachineSavesModule,
    LogsModule,
    forwardRef(() => SocketModule),
    JwtModule,
    UsersModule,
    EntraModule,
    forwardRef(() => NotificationsModule),
  ],
  controllers: [DeploysController],
  providers: [DeploysService],
  exports: [DeploysService],
})
export class DeploysModule {}
