import { Test, TestingModule } from '@nestjs/testing';
import { DeploysService } from './deploys.service';
import { Repository } from 'typeorm';
import { Deploy } from './entities/deploy.entity';
import { getRepositoryToken } from '@nestjs/typeorm';
import { LabelsService } from 'src/labels/labels.service';
import { labelsServiceMock } from 'src/utils/mocks/services/labels-service.mock';
import { FqdnMasksService } from 'src/fqdn-masks/fqdn-masks.service';
import { fqdnMasksServiceMock } from 'src/utils/mocks/services/fqdn-masks-service.mock';
import { DevicesService } from 'src/devices/devices.service';
import { devicesServiceMock } from 'src/utils/mocks/services/devices-service.mock';
import { VirtualMachineSavesService } from 'src/virtual-machine-saves/virtual-machine-saves.service';
import { virtualMachineSavesServiceMock } from 'src/utils/mocks/services/virtual-machine-saves-service.mock';
import { SocketGateway } from 'src/socket/socket.gateway';
import { socketGatewayMock } from 'src/utils/mocks/gateways/socket-gateway.mock';
import { LogsService } from 'src/logs/logs.service';
import { logsServiceMock } from 'src/utils/mocks/services/logs-service.mock';
import { UsersService } from 'src/users/users.service';
import { usersServiceMock } from 'src/utils/mocks/services/users-service.mock';
import { NotificationsService } from 'src/notifications/notifications.service';
import { notificationsServiceMock } from 'src/utils/mocks/services/notifications-service.mock';
import { ConfigService } from '@nestjs/config';
import { configServiceMock } from 'src/utils/mocks/services/config-service.mock';

describe('DeploysService', () => {
  let service: DeploysService;
  let deployRepository: Repository<Deploy>;

  const DEPLOY_REPOSITORY_TOKEN = getRepositoryToken(Deploy);

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        DeploysService,
        LabelsService,
        FqdnMasksService,
        DevicesService,
        VirtualMachineSavesService,
        SocketGateway,
        LogsService,
        UsersService,
        NotificationsService,
        ConfigService,
        {
          provide: DEPLOY_REPOSITORY_TOKEN,
          useClass: Repository<Deploy>,
        },
      ],
    })
      .overrideProvider(LabelsService)
      .useValue(labelsServiceMock)
      .overrideProvider(FqdnMasksService)
      .useValue(fqdnMasksServiceMock)
      .overrideProvider(DevicesService)
      .useValue(devicesServiceMock)
      .overrideProvider(VirtualMachineSavesService)
      .useValue(virtualMachineSavesServiceMock)
      .overrideProvider(SocketGateway)
      .useValue(socketGatewayMock)
      .overrideProvider(LogsService)
      .useValue(logsServiceMock)
      .overrideProvider(UsersService)
      .useValue(usersServiceMock)
      .overrideProvider(NotificationsService)
      .useValue(notificationsServiceMock)
      .overrideProvider(ConfigService)
      .useValue(configServiceMock)
      .compile();

    service = module.get<DeploysService>(DeploysService);
    deployRepository = module.get<Repository<Deploy>>(DEPLOY_REPOSITORY_TOKEN);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
    expect(deployRepository).toBeDefined();
  });
});
