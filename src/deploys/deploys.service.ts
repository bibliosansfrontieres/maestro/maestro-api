import {
  BadRequestException,
  Inject,
  Injectable,
  NotFoundException,
  forwardRef,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Deploy } from './entities/deploy.entity';
import { FindOneOptions, In, QueryRunner, Repository } from 'typeorm';
import { VirtualMachineSave } from 'src/virtual-machine-saves/entities/virtual-machine-save.entity';
import { Device } from 'src/devices/entities/device.entity';
import { Label } from 'src/labels/entities/label.entity';
import { LabelsService } from 'src/labels/labels.service';
import { PostDeployRequest } from './definitions/post-deploy.definition';
import { FqdnMasksService } from 'src/fqdn-masks/fqdn-masks.service';
import { DevicesService } from 'src/devices/devices.service';
import { DeviceStatusEnum } from 'src/devices/enums/device-status.enum';
import { VirtualMachineSavesService } from 'src/virtual-machine-saves/virtual-machine-saves.service';
import { SocketGateway } from 'src/socket/socket.gateway';
import { DeployStatusEnum } from './enums/deploy-status.enum';
import { LogsService } from 'src/logs/logs.service';
import { LogActionEnum } from 'src/logs/enums/log-action.enum';
import { LogEntityEnum } from 'src/logs/enums/log-entity.enum';
import { GetDeployResponse } from './definitions/get-deploy.definition';
import {
  GetDeploysRequest,
  GetDeploysResponse,
} from './definitions/get-deploys.definition';
import { PageOptions } from 'src/utils/pagination/page-options';
import { DeployOrderByEnum } from './enums/deploy-order-by.enum';
import { QueryHelper } from 'src/utils/query-helpers.util';
import { getManyAndCount } from 'src/utils/get-many-and-count.util';
import { User } from 'src/users/entities/user.entity';
import { UsersService } from 'src/users/users.service';
import { PatchDeployRequest } from './definitions/patch-deploy.definition';
import { NotificationsService } from 'src/notifications/notifications.service';
import { NotificationTypeEnum } from 'src/notifications/enums/notification-type.enum';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class DeploysService {
  constructor(
    @InjectRepository(Deploy) private deployRepository: Repository<Deploy>,
    private labelsService: LabelsService,
    private fqdnMasksService: FqdnMasksService,
    private virtualMachineSavesService: VirtualMachineSavesService,
    @Inject(forwardRef(() => SocketGateway))
    private socketGateway: SocketGateway,
    private logsService: LogsService,
    private usersService: UsersService,
    @Inject(forwardRef(() => NotificationsService))
    private notificationsService: NotificationsService,
    private configService: ConfigService,
    @Inject(forwardRef(() => DevicesService))
    private devicesService: DevicesService,
  ) {}

  async createFromUnknown(device: Device, virtualMachineSaveId: string) {
    const user = await this.usersService.findOne({ where: { id: 'unknown' } });
    if (!user) {
      throw new NotFoundException('Unknown user not found');
    }
    const virtualMachineSave = await this.virtualMachineSavesService.findOne({
      where: { id: virtualMachineSaveId },
      relations: { virtualMachine: true, context: { applications: true } },
    });
    if (!virtualMachineSave) {
      throw new NotFoundException('Virtual machine save not found');
    }
    const label = await this.labelsService.findOne({
      where: { name: 'Unknown' },
      order: { id: 'ASC' },
    });
    if (!label) {
      throw new NotFoundException('Unknown label not found');
    }
    const deploy = await this.create(user, virtualMachineSave, device, [label]);
    deploy.status = DeployStatusEnum.DEPLOYED;
    return this.deployRepository.save(deploy);
  }

  async create(
    author: User,
    virtualMachineSave: VirtualMachineSave,
    device: Device,
    labels: Label[],
  ) {
    // Archive old deploys
    const deploys = await this.deployRepository.find({
      where: { device: { id: device.id } },
    });
    for (const deploy of deploys) {
      deploy.status = DeployStatusEnum.ARCHIVED;
    }
    await this.deployRepository.save(deploys);

    // Create new deploy
    let deploy = new Deploy(virtualMachineSave, device, labels, author);
    deploy = await this.deployRepository.save(deploy);
    await this.logsService.create({
      authorId: author.id,
      action: LogActionEnum.CREATE,
      entity: LogEntityEnum.DEPLOY,
      entityId: deploy.id,
    });
    return deploy;
  }

  async postDeploy(authorId: string, postDeployRequest: PostDeployRequest) {
    const labels: Label[] = [];
    for (const labelId of postDeployRequest.labelIds) {
      const label = await this.labelsService.findOne({
        where: { id: labelId },
      });
      if (!label) {
        throw new NotFoundException(`label #${labelId} not found`);
      }
      labels.push(label);
    }

    const virtualMachineSave = await this.virtualMachineSavesService.findOne({
      where: { id: postDeployRequest.virtualMachineSaveId },
      relations: { virtualMachine: true, context: { applications: true } },
    });
    if (!virtualMachineSave) {
      throw new NotFoundException(`virtual machine's save not found`);
    }

    const fqdnMask = await this.fqdnMasksService.findOne({
      where: { id: postDeployRequest.fqdnMaskId },
    });
    if (!fqdnMask) {
      throw new NotFoundException(`fqdn mask not found`);
    }

    const devices = await this.devicesService.find({
      where: {
        fqdnMask: { id: fqdnMask.id },
        isAlive: true,
        status: DeviceStatusEnum.READY,
      },
    });

    if (devices.length < postDeployRequest.devicesCount) {
      throw new BadRequestException('not enough devices available');
    }

    const author = await this.usersService.findOne({ where: { id: authorId } });
    if (!author) {
      throw new NotFoundException('author not found');
    }

    for (let i = 0; i < postDeployRequest.devicesCount; i++) {
      const device = devices[i];
      await this.devicesService.setDeploying(device);
      const deploy = await this.create(
        author,
        virtualMachineSave,
        device,
        labels,
      );
      await this.setOrderSent(deploy);
      this.socketGateway.emitDeviceDeploy(deploy);
    }

    await this.notificationsService.notify(
      NotificationTypeEnum.DEPLOY,
      author,
      {
        'VM ID': virtualMachineSave.virtualMachine.id,
        'VM name': virtualMachineSave.virtualMachine.title,
        'VM URL': `http://${this.configService.get('MAESTRO_HOST')}/virtual-machines/${virtualMachineSave.virtualMachine.id}`,
        'VM approved save': virtualMachineSave.name,
        Ideascubes: postDeployRequest.devicesCount.toString(),
        Labels: labels.map((label) => `\`${label.name}\``).join(', '),
      },
    );
  }

  async getDeploy(id: number) {
    const deploy = await this.deployRepository.findOne({
      where: { id },
      relations: {
        virtualMachineSave: { virtualMachine: true },
        labels: true,
        device: true,
        user: true,
      },
    });

    if (!deploy) {
      throw new NotFoundException('deploy not found');
    }

    return new GetDeployResponse(deploy);
  }

  async getDeploys(getDeploysRequest: GetDeploysRequest) {
    const pageOptions = new PageOptions(getDeploysRequest);
    const deploysQuery = this.deployRepository
      .createQueryBuilder('deploy')
      .leftJoinAndSelect('deploy.virtualMachineSave', 'virtualMachineSave')
      .leftJoinAndSelect('virtualMachineSave.virtualMachine', 'virtualMachine')
      .leftJoinAndSelect('deploy.device', 'device')
      .leftJoinAndSelect('device.fqdnMask', 'fqdnMask')
      .leftJoinAndSelect('deploy.user', 'user');
    if (getDeploysRequest.labelIds) {
      const labelIds = getDeploysRequest.labelIds.split(',');
      for (let i = 0; i < labelIds.length; i++) {
        const params = {};
        params[`labelId${i}`] = labelIds[i];
        deploysQuery.innerJoinAndSelect(
          'deploy.labels',
          `label${i}`,
          `label${i}.id = :labelId${i}`,
          { ...params },
        );
      }
    }

    let { orderBy } = getDeploysRequest;

    orderBy = orderBy ? orderBy : DeployOrderByEnum.CREATED_AT;

    const queryHelper = new QueryHelper(deploysQuery);
    queryHelper.pagination(pageOptions, orderBy);

    if (getDeploysRequest.createdDateRange) {
      queryHelper.andWhereDateRange(
        'deploy.createdAt',
        getDeploysRequest.createdDateRange,
      );
    }

    if (getDeploysRequest.statuses) {
      const statuses = getDeploysRequest.statuses.split(',');
      queryHelper.andWhereMultipleEquals('deploy.status', statuses);
    }

    if (getDeploysRequest.userIds) {
      const userIds = getDeploysRequest.userIds.split(',');
      queryHelper.andWhereMultipleEquals('user.id', userIds);
    }

    queryHelper.andWhereQuery(getDeploysRequest.query, ['label.name']);

    const [count, deploys] = await getManyAndCount<Deploy>(
      deploysQuery,
      ['deploy.id'],
      false,
    );

    let sortedDeploys = deploys;

    for (const deploy of sortedDeploys) {
      deploy.labels = await this.labelsService.find({
        where: { deploys: { id: deploy.id } },
      });
    }
    if (getDeploysRequest.labelIds) {
      const labelIds = getDeploysRequest.labelIds.split(',');
      sortedDeploys = sortedDeploys.map((deploy) => {
        deploy.labels.sort((a, b) => {
          if (
            labelIds.includes(a.id.toString()) &&
            !labelIds.includes(b.id.toString())
          ) {
            return -1;
          } else if (
            !labelIds.includes(a.id.toString()) &&
            labelIds.includes(b.id.toString())
          ) {
            return 1;
          }
          return 0;
        });
        return deploy;
      });
    }

    return new GetDeploysResponse(sortedDeploys, count, pageOptions);
  }

  async patchDeploy(id: number, patchDeployRequest: PatchDeployRequest) {
    const deploy = await this.deployRepository.findOne({
      where: { id },
    });

    if (!deploy) {
      throw new NotFoundException('deploy not found');
    }

    if (patchDeployRequest.labelIds) {
      const labels = await this.labelsService.find({
        where: { id: In(patchDeployRequest.labelIds) },
      });
      deploy.labels = labels;
    }

    return this.deployRepository.save(deploy);
  }

  async setDeployStatus(deployId: number, status: DeployStatusEnum) {
    const deploy = await this.deployRepository.findOne({
      where: { id: deployId },
    });
    if (!deploy) {
      throw new NotFoundException('deploy not found');
    }

    deploy.status = status;
    await this.deployRepository.save(deploy);
  }

  async setStatus(deploy: Deploy, status: DeployStatusEnum) {
    deploy.status = status;
    return this.deployRepository.save(deploy);
  }

  async setOrderSent(deploy: Deploy) {
    deploy.status = DeployStatusEnum.ORDER_SENT;
    await this.deployRepository.save(deploy);
  }

  async setDeploying(deploy: Deploy) {
    deploy.status = DeployStatusEnum.DEPLOYING;
    await this.deployRepository.save(deploy);
  }

  getQueryBuilder(alias?: string, queryRunner?: QueryRunner) {
    return this.deployRepository.createQueryBuilder(alias, queryRunner);
  }

  findOne(options?: FindOneOptions<Deploy>) {
    return this.deployRepository.findOne(options);
  }
}
