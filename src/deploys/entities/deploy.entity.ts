import { Device } from 'src/devices/entities/device.entity';
import { Label } from 'src/labels/entities/label.entity';
import { VirtualMachineSave } from 'src/virtual-machine-saves/entities/virtual-machine-save.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { DeployStatusEnum } from '../enums/deploy-status.enum';
import { User } from 'src/users/entities/user.entity';

@Entity()
export class Deploy {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  status: DeployStatusEnum;

  @ManyToOne(
    () => VirtualMachineSave,
    (virtualMachineSave) => virtualMachineSave.deploys,
  )
  virtualMachineSave: VirtualMachineSave;

  @ManyToOne(() => Device, (device) => device.deploys)
  device: Device;

  @ManyToMany(() => Label, (label) => label.deploys)
  @JoinTable()
  labels: Label[];

  @ManyToOne(() => User, (user) => user.deploys)
  user: User;

  @CreateDateColumn()
  createdAt: string;

  @UpdateDateColumn()
  updatedAt: string;

  constructor(
    virtualMachineSave: VirtualMachineSave,
    device: Device,
    labels: Label[],
    user: User,
  ) {
    if (!virtualMachineSave || !device || !labels) return;
    this.virtualMachineSave = virtualMachineSave;
    this.device = device;
    this.labels = labels;
    this.status = DeployStatusEnum.PENDING;
    this.user = user;
  }
}
