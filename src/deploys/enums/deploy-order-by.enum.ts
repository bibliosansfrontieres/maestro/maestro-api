export enum DeployOrderByEnum {
  ID = 'deploy.id',
  STATUS = 'deploy.status',
  CREATED_AT = 'deploy.createdAt',
  DEVICE = 'device.id',
  USER = 'user.id',
  FQDN_MASK = 'fqdnMask.id',
}
