import { FqdnMask } from 'src/fqdn-masks/entities/fqdn-mask.entity';
import { Device } from '../entities/device.entity';
import { DeviceStatusEnum } from '../enums/device-status.enum';
import { Deploy } from 'src/deploys/entities/deploy.entity';

export class GetDeviceResponse {
  id: string;
  status: DeviceStatusEnum;
  vendor: string;
  os: string;
  macAddress: string;
  publicIpv4: string;
  ipv4FqdnName: string;
  ipv4FqdnArpa: string;
  privateIpv4: string;
  publicIpv6: string;
  ipv6FqdnName: string;
  ipv6FqdnArpa: string;
  serialId: string;
  isAlive: boolean;
  lastSeenAt: string;
  createdAt: string;
  updatedAt: string;
  fqdnMask: FqdnMask;
  deploys: Deploy[];
  blinking: boolean;

  constructor(device: Device) {
    this.id = device.id;
    this.status = device.status;
    this.vendor = device.vendor;
    this.os = device.os;
    this.macAddress = device.macAddress;
    this.publicIpv4 = device.publicIpv4;
    this.ipv4FqdnName = device.ipv4FqdnName;
    this.ipv4FqdnArpa = device.ipv4FqdnArpa;
    this.privateIpv4 = device.privateIpv4;
    this.publicIpv6 = device.publicIpv6;
    this.ipv6FqdnName = device.ipv6FqdnName;
    this.ipv6FqdnArpa = device.ipv6FqdnArpa;
    this.serialId = device.serialId;
    this.isAlive = device.isAlive;
    this.lastSeenAt = device.lastSeenAt;
    this.createdAt = device.createdAt;
    this.updatedAt = device.updatedAt;
    this.fqdnMask = device.fqdnMask;
    this.deploys = device.deploys;
    this.blinking = device.blinking;
  }
}
