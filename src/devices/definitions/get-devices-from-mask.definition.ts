import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class GetDevicesFromMaskRequest {
  @ApiProperty({ example: '*' })
  @IsString()
  mask: string;
}
