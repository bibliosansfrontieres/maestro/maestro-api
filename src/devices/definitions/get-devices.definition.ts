import { PageOptionsRequest } from 'src/utils/pagination/dto/page-options.dto';
import { PageResponse } from 'src/utils/pagination/dto/page-response';
import { PageOptions } from 'src/utils/pagination/page-options';
import { IsEnum, IsOptional, IsString } from 'class-validator';
import { BooleanEnum } from 'src/utils/enums/boolean.enum';
import { ApiPropertyOptional } from '@nestjs/swagger';
import { Device } from '../entities/device.entity';
import { DeviceOrderByEnum } from '../enums/device-order-by.enum';
import { DeviceStatusEnum } from '../enums/device-status.enum';

export class GetDevicesRequest extends PageOptionsRequest {
  @ApiPropertyOptional({ example: 'TRUE', enum: BooleanEnum })
  @IsOptional()
  @IsEnum(BooleanEnum)
  isAliveOnly?: BooleanEnum;

  @ApiPropertyOptional({ example: 'search' })
  @IsOptional()
  @IsString()
  query?: string;

  @ApiPropertyOptional({ example: '12/30/2021,12/31/2021' })
  @IsOptional()
  @IsString()
  lastSeenDateRange?: string;

  @ApiPropertyOptional({ example: '1,2,3' })
  @IsOptional()
  @IsString()
  fqdnMaskIds?: string;

  @ApiPropertyOptional({ example: '1,2,3' })
  @IsOptional()
  @IsString()
  labelIds?: string;

  @ApiPropertyOptional({
    example: `${DeviceStatusEnum.DEPLOYED},${DeviceStatusEnum.READY}`,
  })
  @IsOptional()
  @IsString()
  statuses?: string;

  @ApiPropertyOptional({
    example: DeviceOrderByEnum.ID,
    enum: DeviceOrderByEnum,
    default: DeviceOrderByEnum.ID,
  })
  @IsOptional()
  @IsEnum(DeviceOrderByEnum)
  orderBy?: DeviceOrderByEnum;
}

export class GetDevicesResponse extends PageResponse<Device> {
  constructor(data: Device[], count: number, pageOptions: PageOptions) {
    super(data, count, pageOptions);
  }
}
