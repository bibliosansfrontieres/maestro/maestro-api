import { ArrayMinSize, IsString } from 'class-validator';

export class PatchDevicesBlinkRequest {
  @IsString({ each: true })
  @ArrayMinSize(1)
  deviceIds: string[];
}
