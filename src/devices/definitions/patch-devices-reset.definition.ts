import { PatchDevicesBlinkRequest } from './patch-devices-blink.definition';

export class PatchDevicesResetRequest extends PatchDevicesBlinkRequest {}
