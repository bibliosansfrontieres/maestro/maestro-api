import { Test, TestingModule } from '@nestjs/testing';
import { DevicesController } from './devices.controller';
import { DevicesService } from './devices.service';
import { Device } from './entities/device.entity';
import { Repository } from 'typeorm';
import { getRepositoryToken } from '@nestjs/typeorm';
import { LogsService } from 'src/logs/logs.service';
import { logsServiceMock } from 'src/utils/mocks/services/logs-service.mock';
import { SocketGateway } from 'src/socket/socket.gateway';
import { socketGatewayMock } from 'src/utils/mocks/gateways/socket-gateway.mock';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from 'src/users/users.service';
import { EntraService } from 'src/entra/entra.service';
import { jwtServiceMock } from 'src/utils/mocks/services/jwt-service.mock';
import { usersServiceMock } from 'src/utils/mocks/services/users-service.mock';
import { entraServiceMock } from 'src/utils/mocks/services/entra-service.mock';
import { ConfigService } from '@nestjs/config';
import { configServiceMock } from 'src/utils/mocks/services/config-service.mock';
import { FqdnMasksService } from 'src/fqdn-masks/fqdn-masks.service';
import { fqdnMasksServiceMock } from 'src/utils/mocks/services/fqdn-masks-service.mock';
import { DeploysService } from 'src/deploys/deploys.service';
import { deploysServiceMock } from 'src/utils/mocks/services/deploys-service.mock';
import { LabelsService } from 'src/labels/labels.service';
import { labelsServiceMock } from 'src/utils/mocks/services/labels-service.mock';

describe('DevicesController', () => {
  let controller: DevicesController;
  let deviceRepository: Repository<Device>;

  const DEVICE_REPOSITORY_TOKEN = getRepositoryToken(Device);

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [DevicesController],
      providers: [
        DevicesService,
        FqdnMasksService,
        {
          provide: DEVICE_REPOSITORY_TOKEN,
          useClass: Repository<Device>,
        },
        LogsService,
        SocketGateway,
        JwtService,
        UsersService,
        EntraService,
        ConfigService,
        DeploysService,
        LabelsService,
      ],
    })
      .overrideProvider(LogsService)
      .useValue(logsServiceMock)
      .overrideProvider(SocketGateway)
      .useValue(socketGatewayMock)
      .overrideProvider(JwtService)
      .useValue(jwtServiceMock)
      .overrideProvider(UsersService)
      .useValue(usersServiceMock)
      .overrideProvider(EntraService)
      .useValue(entraServiceMock)
      .overrideProvider(ConfigService)
      .useValue(configServiceMock)
      .overrideProvider(FqdnMasksService)
      .useValue(fqdnMasksServiceMock)
      .overrideProvider(DeploysService)
      .useValue(deploysServiceMock)
      .overrideProvider(LabelsService)
      .useValue(labelsServiceMock)
      .compile();

    controller = module.get<DevicesController>(DevicesController);
    deviceRepository = module.get<Repository<Device>>(DEVICE_REPOSITORY_TOKEN);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
    expect(deviceRepository).toBeDefined();
  });
});
