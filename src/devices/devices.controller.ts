import { Body, Controller, Get, Param, Patch, Query } from '@nestjs/common';
import { DevicesService } from './devices.service';
import {
  GetDevicesRequest,
  GetDevicesResponse,
} from './definitions/get-devices.definition';
import { Auth } from 'src/auth/decorators/auth.decorator';
import { PermissionNameEnum } from 'src/permissions/enums/permission-name.enum';
import { UserId } from 'src/auth/decorators/user-id.decorator';
import {
  ApiBadRequestResponse,
  ApiNotFoundResponse,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { GetDeviceResponse } from './definitions/get-device.definition';
import { GetDevicesFromMaskRequest } from './definitions/get-devices-from-mask.definition';
import { PatchDevicesResetRequest } from './definitions/patch-devices-reset.definition';
import { PatchDevicesBlinkRequest } from './definitions/patch-devices-blink.definition';

@ApiTags('devices')
@Controller('devices')
export class DevicesController {
  constructor(private readonly devicesService: DevicesService) {}

  @Get('mask')
  @Auth(PermissionNameEnum.READ_DEVICE)
  getDevicesFromMask(
    @Query() getDevicesFromMasqRequest: GetDevicesFromMaskRequest,
  ) {
    return this.devicesService.getDevicesFromMask(getDevicesFromMasqRequest);
  }

  @Get()
  @Auth(PermissionNameEnum.READ_DEVICE)
  @ApiResponse({ type: GetDevicesResponse })
  getDevices(@Query() getDevicesRequest: GetDevicesRequest) {
    return this.devicesService.getDevices(getDevicesRequest);
  }

  @Patch('blink')
  @Auth(PermissionNameEnum.BLINK_DEVICE)
  @ApiNotFoundResponse({ description: 'device not found' })
  @ApiBadRequestResponse({ description: 'device not alive' })
  patchDevicesBlink(
    @UserId() userId: string,
    @Body() patchDevicesBlinkRequest: PatchDevicesBlinkRequest,
  ) {
    return this.devicesService.patchDevicesBlink(
      userId,
      patchDevicesBlinkRequest,
    );
  }

  @Patch('reset')
  @Auth(PermissionNameEnum.RESET_DEVICE)
  @ApiNotFoundResponse({ description: 'device not found' })
  @ApiBadRequestResponse({ description: 'device not alive' })
  patchDevicesReset(
    @UserId() userId: string,
    @Body() patchDevicesResetRequest: PatchDevicesResetRequest,
  ) {
    return this.devicesService.patchDevicesReset(
      userId,
      patchDevicesResetRequest,
    );
  }

  @Get(':id')
  @Auth(PermissionNameEnum.READ_DEVICE)
  @ApiNotFoundResponse({ description: 'device not found' })
  @ApiResponse({ type: GetDeviceResponse })
  getDevice(@Param('id') id: string) {
    return this.devicesService.getDevice(id);
  }
}
