import { Module, forwardRef } from '@nestjs/common';
import { DevicesService } from './devices.service';
import { DevicesController } from './devices.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Device } from './entities/device.entity';
import { LogsModule } from 'src/logs/logs.module';
import { SocketModule } from 'src/socket/socket.module';
import { JwtModule } from '@nestjs/jwt';
import { UsersModule } from 'src/users/users.module';
import { EntraModule } from 'src/entra/entra.module';
import { FqdnMasksModule } from 'src/fqdn-masks/fqdn-masks.module';
import { DeploysModule } from 'src/deploys/deploys.module';
import { LabelsModule } from 'src/labels/labels.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([Device]),
    LogsModule,
    JwtModule,
    UsersModule,
    EntraModule,
    forwardRef(() => SocketModule),
    forwardRef(() => FqdnMasksModule),
    forwardRef(() => DeploysModule),
    LabelsModule,
  ],
  controllers: [DevicesController],
  providers: [DevicesService],
  exports: [DevicesService],
})
export class DevicesModule {}
