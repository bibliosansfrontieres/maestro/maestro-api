import { Test, TestingModule } from '@nestjs/testing';
import { DevicesService } from './devices.service';
import { Device } from './entities/device.entity';
import { Repository } from 'typeorm';
import { getRepositoryToken } from '@nestjs/typeorm';
import { LogsService } from 'src/logs/logs.service';
import { SocketGateway } from 'src/socket/socket.gateway';
import { logsServiceMock } from 'src/utils/mocks/services/logs-service.mock';
import { socketGatewayMock } from 'src/utils/mocks/gateways/socket-gateway.mock';
import { FqdnMasksService } from 'src/fqdn-masks/fqdn-masks.service';
import { fqdnMasksServiceMock } from 'src/utils/mocks/services/fqdn-masks-service.mock';
import { DeploysService } from 'src/deploys/deploys.service';
import { deploysServiceMock } from 'src/utils/mocks/services/deploys-service.mock';
import { UsersService } from 'src/users/users.service';
import { usersServiceMock } from 'src/utils/mocks/services/users-service.mock';
import { LabelsService } from 'src/labels/labels.service';
import { labelsServiceMock } from 'src/utils/mocks/services/labels-service.mock';

describe('DevicesService', () => {
  let service: DevicesService;
  let deviceRepository: Repository<Device>;

  const DEVICE_REPOSITORY_TOKEN = getRepositoryToken(Device);

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        DevicesService,
        FqdnMasksService,
        {
          provide: DEVICE_REPOSITORY_TOKEN,
          useClass: Repository<Device>,
        },
        LogsService,
        SocketGateway,
        DeploysService,
        UsersService,
        LabelsService,
      ],
    })
      .overrideProvider(LogsService)
      .useValue(logsServiceMock)
      .overrideProvider(SocketGateway)
      .useValue(socketGatewayMock)
      .overrideProvider(FqdnMasksService)
      .useValue(fqdnMasksServiceMock)
      .overrideProvider(DeploysService)
      .useValue(deploysServiceMock)
      .overrideProvider(UsersService)
      .useValue(usersServiceMock)
      .overrideProvider(LabelsService)
      .useValue(labelsServiceMock)
      .compile();

    service = module.get<DevicesService>(DevicesService);
    deviceRepository = module.get<Repository<Device>>(DEVICE_REPOSITORY_TOKEN);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
    expect(deviceRepository).toBeDefined();
  });
});
