import {
  Inject,
  Injectable,
  Logger,
  NotFoundException,
  forwardRef,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Device } from './entities/device.entity';
import { v4 as uuidv4 } from 'uuid';
import { FindManyOptions, FindOneOptions, In, Not, Repository } from 'typeorm';
import { IDeviceIdentity } from 'src/socket/interfaces/device-identity.interface';
import { PageOptions } from 'src/utils/pagination/page-options';
import {
  GetDevicesRequest,
  GetDevicesResponse,
} from './definitions/get-devices.definition';
import { QueryHelper } from 'src/utils/query-helpers.util';
import { DeviceOrderByEnum } from './enums/device-order-by.enum';
import { BooleanEnum } from 'src/utils/enums/boolean.enum';
import { getManyAndCount } from 'src/utils/get-many-and-count.util';
import { LogsService } from 'src/logs/logs.service';
import { LogEntityEnum } from 'src/logs/enums/log-entity.enum';
import { LogActionEnum } from 'src/logs/enums/log-action.enum';
import { SocketGateway } from 'src/socket/socket.gateway';
import { GetDeviceResponse } from './definitions/get-device.definition';
import { FqdnMasksService } from 'src/fqdn-masks/fqdn-masks.service';
import { minimatch } from 'minimatch';
import { GetDevicesFromMaskRequest } from './definitions/get-devices-from-mask.definition';
import { FqdnMask } from 'src/fqdn-masks/entities/fqdn-mask.entity';
import { DeviceStatusEnum } from './enums/device-status.enum';
import { PatchDevicesBlinkRequest } from './definitions/patch-devices-blink.definition';
import { PatchDevicesResetRequest } from './definitions/patch-devices-reset.definition';
import { DeploysService } from 'src/deploys/deploys.service';
import { DeployStatusEnum } from 'src/deploys/enums/deploy-status.enum';
import { UsersService } from 'src/users/users.service';
import { LabelsService } from 'src/labels/labels.service';

@Injectable()
export class DevicesService {
  constructor(
    @InjectRepository(Device) private deviceRepository: Repository<Device>,
    private logsService: LogsService,
    @Inject(forwardRef(() => SocketGateway))
    private socketGateway: SocketGateway,
    private fqdnMasksService: FqdnMasksService,
    private usersService: UsersService,
    @Inject(forwardRef(() => DeploysService))
    private deploysService: DeploysService,
    private labelsService: LabelsService,
  ) {}

  async create(identity: IDeviceIdentity, isAlive: boolean) {
    const id = uuidv4();
    const device = new Device(identity, id, isAlive);
    const fqdnMask = await this.fqdnMasksService.assignOrCreate(device);
    device.fqdnMask = fqdnMask;
    device.status = identity.status;
    return this.deviceRepository.save(device);
  }

  setDeploying(device: Device) {
    device.status = DeviceStatusEnum.DEPLOYING;
    return this.deviceRepository.save(device);
  }

  update(device: Device, identity: IDeviceIdentity, isAlive: boolean) {
    device.ipv4FqdnName = identity.ipv4FqdnName;
    device.ipv4FqdnArpa = identity.ipv4FqdnArpa;
    device.ipv6FqdnName = identity.ipv6FqdnName;
    device.ipv6FqdnArpa = identity.ipv6FqdnArpa;
    device.publicIpv4 = identity.publicIpv4;
    device.publicIpv6 = identity.publicIpv6;
    device.privateIpv4 = identity.privateIpv4;
    // cpuSerialId here for first versions devices
    device.serialId = identity.serialId || identity.cpuSerialId;
    if ((device.isAlive && !isAlive) || isAlive) {
      device.lastSeenAt = new Date().toISOString();
    }
    device.isAlive = isAlive;
    device.status = identity.status;
    return this.deviceRepository.save(device);
  }

  async checkDeploy(device: Device, identity: IDeviceIdentity) {
    if (!identity.macAddress) return;

    const maestroDeploy = await this.deploysService.findOne({
      where: {
        device: { macAddress: identity.macAddress },
        status: Not(DeployStatusEnum.ARCHIVED),
      },
    });

    if (maestroDeploy) {
      if (identity.status === DeviceStatusEnum.READY) {
        await this.deploysService.setStatus(
          maestroDeploy,
          DeployStatusEnum.ARCHIVED,
        );
        return;
      }
      if (
        identity.status === DeviceStatusEnum.DEPLOYED &&
        maestroDeploy.status !== DeployStatusEnum.DEPLOYED
      ) {
        await this.deploysService.setStatus(
          maestroDeploy,
          DeployStatusEnum.DEPLOYED,
        );
      }
    }

    if (
      !identity.deployId ||
      isNaN(+identity.deployId) ||
      !identity.virtualMachineSaveId
    )
      return;

    if (maestroDeploy) {
      // If we found deploy linked to identity
      if (maestroDeploy.id !== +identity.deployId) {
        Logger.warn(
          `Found duplicated device #${device.id} (${identity.macAddress}), creating new deploy in database`,
          'DevicesService',
        );
        // MaestroDeploy is not correct we need to update Maestro DB
        await this.deploysService.setStatus(
          maestroDeploy,
          DeployStatusEnum.ARCHIVED,
        );
        const deviceDeploy = await this.deploysService.findOne({
          where: { id: +identity.deployId },
          relations: {
            labels: true,
            virtualMachineSave: {
              context: { applications: true },
              virtualMachine: true,
            },
            user: true,
          },
        });
        if (deviceDeploy) {
          try {
            // This might be a duplicated SD card so we duplicate deploy
            const newDeploy = await this.deploysService.create(
              deviceDeploy.user,
              deviceDeploy.virtualMachineSave,
              device,
              deviceDeploy.labels,
            );
            await this.deploysService.setStatus(
              newDeploy,
              DeployStatusEnum.DEPLOYED,
            );
          } catch (e) {
            console.error(e);
          }
        } else {
          try {
            // No deploy corresponding to deployId so we create a fake one
            const unknownUser = await this.usersService.findOne({
              where: { id: 'unknown' },
            });
            if (!unknownUser) {
              throw new NotFoundException('unknown user not found');
            }
            const newDeploy = await this.deploysService.createFromUnknown(
              device,
              identity.virtualMachineSaveId,
            );
            await this.deploysService.setStatus(
              newDeploy,
              DeployStatusEnum.DEPLOYED,
            );
          } catch (e) {
            console.error(e);
          }
        }
      }
    } else {
      const deviceDeploy = await this.deploysService.findOne({
        where: { id: +identity.deployId },
        relations: {
          labels: true,
          virtualMachineSave: {
            virtualMachine: true,
            context: { applications: true },
          },
          user: true,
        },
      });
      Logger.warn(
        `Found duplicated device #${device.id} (${identity.macAddress}), creating new deploy in database`,
        'DevicesService',
      );
      if (deviceDeploy) {
        try {
          // This might be a duplicated SD card so we duplicate deploy
          const newDeploy = await this.deploysService.create(
            deviceDeploy.user,
            deviceDeploy.virtualMachineSave,
            device,
            deviceDeploy.labels,
          );
          await this.deploysService.setStatus(
            newDeploy,
            DeployStatusEnum.DEPLOYED,
          );
        } catch (e) {
          console.error(e);
        }
      } else {
        try {
          // No deploy corresponding to deployId so we create a fake one
          const unknownUser = await this.usersService.findOne({
            where: { id: 'unknown' },
          });
          if (!unknownUser) {
            throw new NotFoundException('unknown user not found');
          }
          const newDeploy = await this.deploysService.createFromUnknown(
            device,
            identity.virtualMachineSaveId,
          );
          await this.deploysService.setStatus(
            newDeploy,
            DeployStatusEnum.DEPLOYED,
          );
        } catch (e) {
          console.error(e);
        }
      }
    }
  }

  async createOrUpdate(identity: IDeviceIdentity, isAlive: boolean) {
    const device = await this.deviceRepository.findOne({
      where: { macAddress: identity.macAddress },
    });
    if (!device) {
      return this.create(identity, isAlive);
    } else {
      return this.update(device, identity, isAlive);
    }
  }

  async getDevices(getDevicesRequest: GetDevicesRequest) {
    const pageOptions = new PageOptions(getDevicesRequest);
    const devicesQuery = this.deviceRepository
      .createQueryBuilder('device')
      .innerJoinAndSelect('device.fqdnMask', 'fqdnMask')
      .leftJoinAndSelect('device.deploys', 'deploy')
      .leftJoinAndSelect('deploy.user', 'user')
      .leftJoinAndSelect('deploy.virtualMachineSave', 'virtualMachineSave')
      .leftJoinAndSelect('virtualMachineSave.virtualMachine', 'virtualMachine');
    if (getDevicesRequest.labelIds) {
      const labelIds = getDevicesRequest.labelIds.split(',');
      for (let i = 0; i < labelIds.length; i++) {
        const params = {};
        params[`labelId${i}`] = labelIds[i];
        devicesQuery.innerJoinAndSelect(
          'deploy.labels',
          `label${i}`,
          `label${i}.id = :labelId${i}`,
          { ...params },
        );
      }
    }

    let { orderBy } = getDevicesRequest;

    orderBy = orderBy ? orderBy : DeviceOrderByEnum.LAST_SEEN_AT;

    const queryHelper = new QueryHelper(devicesQuery);
    queryHelper.pagination(pageOptions, orderBy);

    if (getDevicesRequest.isAliveOnly === BooleanEnum.TRUE) {
      queryHelper.andWhereEquals('device.isAlive', { isAlive: true });
    }

    if (getDevicesRequest.lastSeenDateRange) {
      queryHelper.andWhereDateRange(
        'device.lastSeenAt',
        getDevicesRequest.lastSeenDateRange,
      );
    }

    if (getDevicesRequest.fqdnMaskIds) {
      const fqdnMaskIds = getDevicesRequest.fqdnMaskIds.split(',');
      queryHelper.andWhereMultipleEquals('fqdnMask.id', fqdnMaskIds);
    }

    if (getDevicesRequest.statuses) {
      const statuses = getDevicesRequest.statuses.split(',');
      queryHelper.andWhereMultipleEquals('device.status', statuses);
    }

    queryHelper.andWhereQuery(getDevicesRequest.query, [
      'device.macAddress',
      'device.privateIpv4',
    ]);

    const [count, devices] = await getManyAndCount<Device>(
      devicesQuery,
      ['device.id'],
      false,
    );

    let sortedDevices = devices;
    for (const device of sortedDevices) {
      for (const deploy of device.deploys) {
        deploy.labels = await this.labelsService.find({
          where: { deploys: { id: deploy.id } },
        });
      }
    }
    if (getDevicesRequest.labelIds) {
      const labelIds = getDevicesRequest.labelIds.split(',');
      sortedDevices = devices.map((device) => {
        if (device.deploys && device.deploys.length > 0) {
          device.deploys = device.deploys.map((deploy) => {
            deploy.labels.sort((a, b) => {
              if (
                labelIds.includes(a.id.toString()) &&
                !labelIds.includes(b.id.toString())
              ) {
                return -1;
              } else if (
                !labelIds.includes(a.id.toString()) &&
                labelIds.includes(b.id.toString())
              ) {
                return 1;
              }
              return 0;
            });
            return deploy;
          });
        }
        return device;
      });
    }

    return new GetDevicesResponse(sortedDevices, count, pageOptions);
  }

  async getDevice(id: string) {
    const device = await this.deviceRepository.findOne({
      where: { id },
      relations: {
        fqdnMask: true,
        deploys: {
          virtualMachineSave: { virtualMachine: true },
          labels: true,
          user: true,
        },
      },
    });

    if (!device) {
      throw new NotFoundException('device not found');
    }

    return new GetDeviceResponse(device);
  }

  async patchDevicesBlink(
    authorId: string,
    patchDevicesBlinkRequest: PatchDevicesBlinkRequest,
  ) {
    const devices = await this.deviceRepository.find({
      where: { id: In(patchDevicesBlinkRequest.deviceIds) },
    });

    if (devices.length === 0) {
      throw new NotFoundException('no device found');
    }

    for (const device of devices) {
      if (!device.isAlive) {
        continue;
      }

      this.socketGateway.emitDeviceBlink(device);

      await this.logsService.create({
        authorId,
        entity: LogEntityEnum.DEVICE,
        entityId: device.id,
        action: LogActionEnum.BLINK,
      });
    }

    return;
  }

  async setBlinking(device: Device, blinking: boolean) {
    device.blinking = blinking;
    await this.deviceRepository.save(device);
  }

  async patchDevicesReset(
    authorId: string,
    patchDevicesResetRequest: PatchDevicesResetRequest,
  ) {
    const devices = await this.deviceRepository.find({
      where: { id: In(patchDevicesResetRequest.deviceIds) },
    });

    if (devices.length === 0) {
      throw new NotFoundException('no device found');
    }

    for (const device of devices) {
      if (!device.isAlive) {
        continue;
      }

      this.socketGateway.emitDeviceReset(device);

      await this.logsService.create({
        authorId,
        entity: LogEntityEnum.DEVICE,
        entityId: device.id,
        action: LogActionEnum.RESET,
      });
    }

    return;
  }

  async getDevicesFromMask({ mask }: GetDevicesFromMaskRequest) {
    const matchingDevices = [];
    const devices = await this.deviceRepository.find();
    for (const device of devices) {
      if (
        minimatch(device.ipv4FqdnName, mask) ||
        minimatch(device.ipv4FqdnArpa, mask)
      ) {
        matchingDevices.push(device);
      }
    }
    return matchingDevices;
  }

  async automaticAssociate(devices: Device[]) {
    for (const device of devices) {
      const fqdnMasks = await this.fqdnMasksService.find();
      let fqdnMaskToAssociate = undefined;
      for (const fqdnMask of fqdnMasks) {
        if (
          minimatch(device.ipv4FqdnName, fqdnMask.mask) ||
          minimatch(device.ipv4FqdnArpa, fqdnMask.mask)
        ) {
          fqdnMaskToAssociate = fqdnMask;
          break;
        }
      }
      if (!fqdnMaskToAssociate) {
        if (device.ipv4FqdnName) {
          const fqdnMask = await this.fqdnMasksService.create({
            mask: device.ipv4FqdnName,
            label: device.ipv4FqdnName,
          });
          device.fqdnMask = fqdnMask;
          await this.deviceRepository.save(device);
        } else if (device.ipv4FqdnArpa) {
          const fqdnMask = await this.fqdnMasksService.create({
            mask: device.ipv4FqdnArpa,
            label: device.ipv4FqdnArpa,
          });
          device.fqdnMask = fqdnMask;
          await this.deviceRepository.save(device);
        }
      } else {
        device.fqdnMask = fqdnMaskToAssociate;
        await this.deviceRepository.save(device);
      }
    }
  }

  async associate(devices: Device[], fqdnMask: FqdnMask) {
    for (const device of devices) {
      device.fqdnMask = fqdnMask;
      await this.deviceRepository.save(device);
    }
  }

  setStatus(device: Device, status: DeviceStatusEnum) {
    device.status = status;
    return this.deviceRepository.save(device);
  }

  find(options?: FindManyOptions<Device>) {
    return this.deviceRepository.find(options);
  }

  findOne(options?: FindOneOptions<Device>) {
    return this.deviceRepository.findOne(options);
  }
}
