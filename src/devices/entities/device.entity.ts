import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryColumn,
  UpdateDateColumn,
} from 'typeorm';
import { DeviceStatusEnum } from '../enums/device-status.enum';
import { IDeviceIdentity } from 'src/socket/interfaces/device-identity.interface';
import { FqdnMask } from 'src/fqdn-masks/entities/fqdn-mask.entity';
import { Deploy } from 'src/deploys/entities/deploy.entity';

@Entity()
export class Device {
  @PrimaryColumn()
  id: string;

  @Column()
  status: DeviceStatusEnum;

  @Column()
  vendor: string;

  @Column()
  os: string;

  @Column()
  macAddress: string;

  @Column()
  blinking: boolean;

  @Column({ nullable: true })
  publicIpv4: string;

  @Column({ nullable: true })
  ipv4FqdnName: string;

  @Column({ nullable: true })
  ipv4FqdnArpa: string;

  @Column({ nullable: true })
  privateIpv4: string;

  @Column({ nullable: true })
  publicIpv6: string;

  @Column({ nullable: true })
  ipv6FqdnName: string;

  @Column({ nullable: true })
  ipv6FqdnArpa: string;

  @Column()
  serialId: string;
  // TODO : migration bdd cpuSerialId

  @Column()
  isAlive: boolean;

  @Column()
  lastSeenAt: string;

  @CreateDateColumn()
  createdAt: string;

  @UpdateDateColumn()
  updatedAt: string;

  @ManyToOne(() => FqdnMask, (fqdnMask) => fqdnMask.devices)
  fqdnMask: FqdnMask;

  @OneToMany(() => Deploy, (deploy) => deploy.device)
  deploys: Deploy[];

  constructor(identity: IDeviceIdentity, id: string, isAlive: boolean) {
    if (!identity) return;
    this.id = id;
    this.macAddress = identity.macAddress;
    this.publicIpv4 = identity.publicIpv4;
    this.ipv4FqdnName = identity.ipv4FqdnName;
    this.ipv4FqdnArpa = identity.ipv4FqdnArpa;
    this.privateIpv4 = identity.privateIpv4;
    this.publicIpv6 = identity.publicIpv6;
    this.ipv6FqdnName = identity.ipv6FqdnName;
    this.ipv6FqdnArpa = identity.ipv6FqdnArpa;
    this.serialId = identity.serialId;
    this.status = DeviceStatusEnum.READY;
    this.isAlive = isAlive;
    // TODO : os/vendor
    this.vendor = '';
    this.os = '';
    this.lastSeenAt = new Date().toISOString();
    this.blinking = false;
  }
}
