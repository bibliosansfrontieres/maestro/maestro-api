import { Injectable, Logger } from '@nestjs/common';
import { dockerCommand } from 'docker-cli-js';
import { join } from 'path';
import Convert = require('ansi-to-html');
import { VirtualMachine } from 'src/virtual-machines/entities/virtual-machine.entity';
import { VirtualMachineContainerEnum } from 'src/virtual-machines/enums/virtual-machine-container.enum';

@Injectable()
export class DockerService {
  async pull(virtualMachine: VirtualMachine) {
    try {
      const command = this.getComposeCommand(virtualMachine);
      let commandResult = undefined;
      commandResult = await dockerCommand(`${command} pull`, {
        echo: false,
      });
      Logger.log(
        `Virtual machine ${virtualMachine.id} pulled !`,
        'DockerService',
      );
      return commandResult;
    } catch (error) {
      console.error(error);
      Logger.error(
        `Could not pull virtual machine #${virtualMachine.id}`,
        'DockerService',
      );
      return undefined;
    }
  }

  getComposeCommand(virtualMachine: VirtualMachine) {
    const dockerComposePath = join(
      __dirname,
      `../../data/virtual_machines/${virtualMachine.id}/composes/vm.compose.yml`,
    );
    const envFilePath = join(
      __dirname,
      `../../data/virtual_machines/${virtualMachine.id}/envs/.env`,
    );
    return `compose -f ${dockerComposePath} --env-file ${envFilePath}`;
  }

  async up(
    virtualMachine: VirtualMachine,
  ): Promise<{ raw: string; command: string } | undefined> {
    try {
      const command = this.getComposeCommand(virtualMachine);
      let commandResult = undefined;
      commandResult = await dockerCommand(`${command} up -d`, {
        echo: false,
      });
      Logger.log(
        `Virtual machine ${virtualMachine.id} started !`,
        'DockerService',
      );
      return commandResult;
    } catch (error) {
      console.error(error);
      Logger.error(
        `Could not set virtual machine #${virtualMachine.id} up`,
        'DockerService',
      );
      return undefined;
    }
  }

  async kill(virtualMachine: VirtualMachine) {
    const command = this.getComposeCommand(virtualMachine);
    let commandResult = undefined;
    commandResult = await dockerCommand(`${command} kill`, {
      echo: false,
    });
    Logger.log(
      `Virtual machine ${virtualMachine.id} killed !`,
      'DockerService',
    );
    return commandResult;
  }

  async down(virtualMachine: VirtualMachine) {
    const command = this.getComposeCommand(virtualMachine);
    let commandResult = undefined;
    commandResult = await dockerCommand(`${command} down`, {
      echo: false,
    });
    Logger.log(
      `Virtual machine ${virtualMachine.id} stopped !`,
      'DockerService',
    );
    return commandResult;
  }

  async stop(virtualMachine: VirtualMachine) {
    const command = this.getComposeCommand(virtualMachine);
    let commandResult = undefined;
    commandResult = await dockerCommand(`${command} stop`, {
      echo: false,
    });
    Logger.log(
      `Virtual machine ${virtualMachine.id} stopped !`,
      'DockerService',
    );
    return commandResult;
  }

  async inspect(
    virtualMachine: VirtualMachine,
    container: VirtualMachineContainerEnum,
  ) {
    try {
      const commandResult = await dockerCommand(
        `inspect ${virtualMachine.id}-${container}`,
        {
          echo: true,
        },
      );
      return JSON.parse(commandResult.raw);
    } catch (e) {
      console.error(e);
      return undefined;
    }
  }

  async getLogs(
    virtualMachine: VirtualMachine,
    container: VirtualMachineContainerEnum,
    maxLines?: number,
  ) {
    maxLines = maxLines !== undefined ? maxLines : 100;
    const dockerLines = maxLines === 0 ? '' : ` -n ${maxLines}`;
    try {
      const commandResult = await dockerCommand(
        `logs ${virtualMachine.id}-${container}${dockerLines}`,
        {
          echo: true,
        },
      );
      return commandResult.raw;
    } catch (e) {
      console.error(e);
      return undefined;
    }
  }

  async getParsedLogs(
    virtualMachine: VirtualMachine,
    container: VirtualMachineContainerEnum,
    maxLines?: number,
  ) {
    maxLines = maxLines ? maxLines : 100;
    try {
      const ansi = await this.getLogs(virtualMachine, container, maxLines);
      const ansiToHtml = new Convert();
      const htmlOutput = ansiToHtml.toHtml(ansi);
      const htmlOutputWithBreaks = htmlOutput.replace(/\n/g, '<br>');
      const lines = htmlOutputWithBreaks.split('<br>');
      return { html: lines.join('<br>') };
    } catch (e) {
      console.error(e);
      return { html: '' };
    }
  }
}
