import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';
import { EntraUserDto } from '../dto/entra-user.dto';
import { PageResponse } from 'src/utils/pagination/dto/page-response';
import { PageOptions } from 'src/utils/pagination/page-options';

export class GetEntraUsersRequest {
  @ApiProperty({ example: 'julien' })
  @IsString()
  @IsOptional()
  query?: string;
}

export class GetEntraUsersResponse extends PageResponse<EntraUserDto> {
  constructor(data: EntraUserDto[], count: number, pageOptions: PageOptions) {
    super(data, count, pageOptions);
  }
}
