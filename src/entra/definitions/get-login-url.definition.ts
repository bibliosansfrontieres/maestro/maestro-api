import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';

export class GetLoginUrlRequest {
  @ApiProperty({ example: 'http://localhost:3000' })
  @IsString()
  dashboardUrl: string;

  @ApiPropertyOptional({
    example: '/token',
    description:
      'Used to redirect user on front-end after login. Use "token" to only return a token after login',
  })
  @IsString()
  @IsOptional()
  redirect?: string;
}

export class GetLoginUrlResponse {
  @ApiProperty({ example: 'https://login.microsoft.com' })
  url: string;

  constructor(url: string) {
    this.url = url;
  }
}
