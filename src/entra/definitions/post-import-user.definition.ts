import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';
import { User } from 'src/users/entities/user.entity';

export class PostImportUserRequest {
  @ApiProperty({ example: 'aaaa0000a0a0a0a0a000' })
  @IsString()
  id: string;
}

export class PostImportUserResponse {
  @ApiProperty({ example: 'aaaa0000a0a0a0a0a000' })
  id: string;

  @ApiProperty({ example: 'John Doe' })
  name: string;

  @ApiProperty({ example: 'jVJ4O@example.com' })
  mail: string;

  @ApiProperty({ example: 'data:image/png;base64' })
  avatar?: string;

  constructor(user: User) {
    this.id = user.id;
    this.name = user.name;
    this.mail = user.mail;
    this.avatar = user.avatar;
  }
}
