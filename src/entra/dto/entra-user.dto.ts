import { IEntraUser } from '../interfaces/entra/entra-user.interface';

export class EntraUserDto implements IEntraUser {
  businessPhones: string[];
  displayName: string;
  givenName: string;
  jobTitle: string | null;
  mail: string;
  mobilePhone: string | null;
  officeLocation: string | null;
  preferredLanguage: string | null;
  surname: string;
  userPrincipalName: string;
  id: string;
  isReferenced: boolean;

  constructor(entraUser: IEntraUser, isReferenced: boolean) {
    this.businessPhones = entraUser.businessPhones;
    this.displayName = entraUser.displayName;
    this.givenName = entraUser.givenName;
    this.jobTitle = entraUser.jobTitle;
    this.mail = entraUser.mail;
    this.mobilePhone = entraUser.mobilePhone;
    this.officeLocation = entraUser.officeLocation;
    this.preferredLanguage = entraUser.preferredLanguage;
    this.surname = entraUser.surname;
    this.userPrincipalName = entraUser.userPrincipalName;
    this.id = entraUser.id;
    this.isReferenced = isReferenced;
  }
}
