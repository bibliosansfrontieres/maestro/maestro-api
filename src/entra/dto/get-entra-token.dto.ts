export class GetEntraTokenDto {
  code?: string;
  refreshToken?: string;
}
