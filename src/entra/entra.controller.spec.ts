import { Test, TestingModule } from '@nestjs/testing';
import { EntraController } from './entra.controller';
import { JwtService } from '@nestjs/jwt';
import { jwtServiceMock } from 'src/utils/mocks/services/jwt-service.mock';
import { UsersService } from 'src/users/users.service';
import { ConfigService } from '@nestjs/config';
import { configServiceMock } from 'src/utils/mocks/services/config-service.mock';
import { usersServiceMock } from 'src/utils/mocks/services/users-service.mock';
import { EntraService } from './entra.service';
import { entraServiceMock } from 'src/utils/mocks/services/entra-service.mock';

describe('EntraController', () => {
  let controller: EntraController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [EntraController],
      providers: [EntraService, JwtService, UsersService, ConfigService],
    })
      .overrideProvider(EntraService)
      .useValue(entraServiceMock)
      .overrideProvider(JwtService)
      .useValue(jwtServiceMock)
      .overrideProvider(UsersService)
      .useValue(usersServiceMock)
      .overrideProvider(ConfigService)
      .useValue(configServiceMock)
      .compile();

    controller = module.get<EntraController>(EntraController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
