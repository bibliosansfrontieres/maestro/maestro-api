import { Body, Controller, Get, Post, Query } from '@nestjs/common';
import { EntraService } from './entra.service';
import {
  ApiBadRequestResponse,
  ApiBody,
  ApiCreatedResponse,
  ApiInternalServerErrorResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import {
  GetLoginUrlRequest,
  GetLoginUrlResponse,
} from './definitions/get-login-url.definition';
import {
  GetEntraUsersRequest,
  GetEntraUsersResponse,
} from './definitions/get-entra-users.definition';
import {
  PostImportUserRequest,
  PostImportUserResponse,
} from './definitions/post-import-user.definition';
import { Auth } from 'src/auth/decorators/auth.decorator';
import { PermissionNameEnum } from 'src/permissions/enums/permission-name.enum';
import { UserId } from 'src/auth/decorators/user-id.decorator';

@Controller('entra')
@ApiTags('entra')
export class EntraController {
  constructor(private entraService: EntraService) {}

  @Get('users')
  @Auth(PermissionNameEnum.IMPORT_USER)
  @ApiOperation({ summary: 'Get Entra users' })
  @ApiOkResponse({ type: GetEntraUsersResponse })
  @ApiInternalServerErrorResponse({
    description: 'Communication failed with Entra',
  })
  getEntraUsers(@Query() getEntraUsersRequest: GetEntraUsersRequest) {
    return this.entraService.getEntraUsers(getEntraUsersRequest);
  }

  @Get('login-url')
  @ApiOperation({ summary: 'Get Entra login URL' })
  @ApiOkResponse({
    type: GetLoginUrlResponse,
  })
  getEntraUrl(@Query() getLoginUrlRequest: GetLoginUrlRequest) {
    return this.entraService.getLoginUrl(getLoginUrlRequest);
  }

  @Post('import-user')
  @Auth(PermissionNameEnum.IMPORT_USER)
  @ApiOperation({ summary: 'Import Entra user' })
  @ApiBody({ type: PostImportUserRequest })
  @ApiCreatedResponse({
    type: PostImportUserResponse,
  })
  @ApiNotFoundResponse({
    description: 'Entra user not found',
  })
  @ApiBadRequestResponse({ description: 'Invalid request body parameters' })
  @ApiInternalServerErrorResponse({
    description: 'Communication failed with Entra',
  })
  postImportUser(
    @UserId() userId: string,
    @Body() postImportUserRequest: PostImportUserRequest,
  ) {
    return this.entraService.postImportUser(userId, postImportUserRequest);
  }
}
