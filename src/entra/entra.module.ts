import { Module } from '@nestjs/common';
import { EntraService } from './entra.service';
import { EntraController } from './entra.controller';
import { UsersModule } from 'src/users/users.module';
import { JwtModule } from '@nestjs/jwt';
import { LogsModule } from 'src/logs/logs.module';

@Module({
  imports: [UsersModule, JwtModule, LogsModule],
  providers: [EntraService],
  exports: [EntraService],
  controllers: [EntraController],
})
export class EntraModule {}
