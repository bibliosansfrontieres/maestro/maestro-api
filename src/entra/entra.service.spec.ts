import { Test, TestingModule } from '@nestjs/testing';
import { EntraService } from './entra.service';
import { ConfigService } from '@nestjs/config';
import { configServiceMock } from 'src/utils/mocks/services/config-service.mock';
import { JwtService } from '@nestjs/jwt';
import { jwtServiceMock } from 'src/utils/mocks/services/jwt-service.mock';
import { usersServiceMock } from 'src/utils/mocks/services/users-service.mock';
import { UsersService } from 'src/users/users.service';
import { LogsService } from 'src/logs/logs.service';
import { logsServiceMock } from 'src/utils/mocks/services/logs-service.mock';

describe('EntraService', () => {
  let service: EntraService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        EntraService,
        JwtService,
        UsersService,
        ConfigService,
        LogsService,
      ],
    })
      .overrideProvider(JwtService)
      .useValue(jwtServiceMock)
      .overrideProvider(UsersService)
      .useValue(usersServiceMock)
      .overrideProvider(ConfigService)
      .useValue(configServiceMock)
      .overrideProvider(LogsService)
      .useValue(logsServiceMock)
      .compile();

    service = module.get<EntraService>(EntraService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
