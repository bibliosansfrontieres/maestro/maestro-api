import {
  Inject,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
  OnModuleInit,
  forwardRef,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import * as qs from 'qs';
import axios from 'axios';
import * as jwt from 'jsonwebtoken';
import { IEntraTokenResponse } from './interfaces/entra/entra-token-response.interface';
import { IEntraIdToken } from './interfaces/entra/entra-id-token.interface';
import { Client, ResponseType } from '@microsoft/microsoft-graph-client';
import { GetEntraTokenDto } from './dto/get-entra-token.dto';
import { IEntraToken } from './interfaces/entra-token.interface';
import { IEntraApplicationTokenResponse } from './interfaces/entra/entra-application-token-response.interface';
import {
  GetLoginUrlRequest,
  GetLoginUrlResponse,
} from './definitions/get-login-url.definition';
import {
  GetEntraUsersRequest,
  GetEntraUsersResponse,
} from './definitions/get-entra-users.definition';
import { IEntraUsersResponse } from './interfaces/entra/entra-users-response.interface';
import { PageOptions } from 'src/utils/pagination/page-options';
import { EntraUserDto } from './dto/entra-user.dto';
import { UsersService } from 'src/users/users.service';
import {
  PostImportUserRequest,
  PostImportUserResponse,
} from './definitions/post-import-user.definition';
import { CreateUserDto } from 'src/users/dto/create-user.dto';
import { IEntraUser } from './interfaces/entra/entra-user.interface';
import { LogsService } from 'src/logs/logs.service';
import { LogActionEnum } from 'src/logs/enums/log-action.enum';
import { LogEntityEnum } from 'src/logs/enums/log-entity.enum';

@Injectable()
export class EntraService implements OnModuleInit {
  private tenantId: string;
  private clientId: string;
  private clientSecret: string;
  private scopes: string;
  private redirectUri: string;
  private microsoftLoginBaseUrl: string;
  private apiBaseUrl: string;
  private accessToken: string;
  private expiresAt: number;

  constructor(
    private configService: ConfigService,
    @Inject(forwardRef(() => UsersService))
    private usersService: UsersService,
    private logsService: LogsService,
  ) {
    this.scopes = this.configService.get('ENTRA_SCOPES');
    this.clientId = this.configService.get('ENTRA_CLIENT_ID');
    this.clientSecret = this.configService.get('ENTRA_CLIENT_SECRET');
    this.tenantId = this.configService.get('ENTRA_TENANT_ID');
    this.microsoftLoginBaseUrl = `https://login.microsoftonline.com/${this.tenantId}`;
    this.apiBaseUrl = this.configService.get('API_BASE_URL');
    this.redirectUri = `${this.apiBaseUrl}/auth/entra/callback`;
  }

  onModuleInit() {
    this.getAccessToken();
  }

  private async getAccessToken(): Promise<string | undefined> {
    if (this.accessToken && this.expiresAt - 1 * 60 * 1000 > Date.now()) {
      return this.accessToken;
    }
    const data = {
      client_id: this.clientId,
      client_secret: this.clientSecret,
      grant_type: 'client_credentials',
      scope: 'https://graph.microsoft.com/.default',
    };
    try {
      const result = await axios.post(
        `${this.microsoftLoginBaseUrl}/oauth2/v2.0/token`,
        qs.stringify(data),
      );
      const token = result.data as IEntraApplicationTokenResponse;
      this.accessToken = token.access_token;
      this.expiresAt = Date.now() + token.expires_in * 1000;
      return this.accessToken;
    } catch (error) {
      console.error(error);
      return undefined;
    }
  }

  private getGraphClient(accessToken: string): Client {
    return Client.init({
      authProvider: (done) => {
        done(null, accessToken);
      },
    });
  }

  async getEntraUsers(
    getEntraUsersRequest: GetEntraUsersRequest,
  ): Promise<GetEntraUsersResponse> {
    const accessToken = await this.getAccessToken();
    if (!accessToken) {
      throw new InternalServerErrorException(
        'Failed to get access token for Entra application',
      );
    }

    const query = [];

    if (getEntraUsersRequest.query) {
      const search = getEntraUsersRequest.query.toLowerCase();
      query.push(`$search="displayName:${search}" OR "mail:${search}"`);
    }

    const entraUsers = (await this.getGraphClient(accessToken)
      .api(`/users?${query.join('&')}`)
      .header('ConsistencyLevel', 'eventual')
      .get()) as IEntraUsersResponse;

    if (!entraUsers) {
      throw new InternalServerErrorException('Failed to get Entra users');
    }

    const entraUserDtos = [];
    for (const entraUser of entraUsers.value) {
      const user = await this.usersService.findOne({
        where: { id: entraUser.id },
      });
      const isReferenced = !!user;
      const entraUserDto = new EntraUserDto(entraUser, isReferenced);
      entraUserDtos.push(entraUserDto);
    }

    const pageOptions = new PageOptions({
      page: 1,
      take: entraUserDtos.length,
    });

    return new GetEntraUsersResponse(
      entraUserDtos,
      entraUserDtos.length,
      pageOptions,
    );
  }

  async getUserProfilePicture(
    userId: string,
    name: string,
  ): Promise<string | undefined> {
    try {
      const photoArrayBuffer = await this.getGraphClient(
        await this.getAccessToken(),
      )
        .api(`/users/${userId}/photo/$value`)
        .responseType(ResponseType.ARRAYBUFFER)
        .get();

      const metadata = await this.getGraphClient(await this.getAccessToken())
        .api(`/users/${userId}/photo`)
        .get();

      return `data:${metadata['@odata.mediaContentType']};base64,${Buffer.from(photoArrayBuffer, 'binary').toString('base64')}`;
    } catch (error) {
      console.error('Error retrieving profile picture:', error);
      return `https://ui-avatars.com/api/?name=${name}`;
    }
  }

  async getToken({
    code,
    refreshToken,
  }: GetEntraTokenDto): Promise<IEntraToken | undefined> {
    let data = undefined;
    if (code) {
      data = {
        client_id: this.clientId,
        client_secret: this.clientSecret,
        grant_type: 'authorization_code',
        code,
        redirect_uri: this.redirectUri,
      };
    }
    if (refreshToken) {
      data = {
        client_id: this.clientId,
        client_secret: this.clientSecret,
        grant_type: 'refresh_token',
        refresh_token: refreshToken,
      };
    }
    if (!data) {
      return undefined;
    }
    try {
      const result = await axios.post(
        `${this.microsoftLoginBaseUrl}/oauth2/v2.0/token`,
        qs.stringify(data),
      );
      const entraToken = result.data as IEntraTokenResponse;
      const entraId = jwt.decode(entraToken.id_token) as IEntraIdToken;
      return { entraToken, entraId };
    } catch (e) {
      console.error(e);
      return undefined;
    }
  }

  async getUser(id: string): Promise<IEntraUser> {
    return this.getGraphClient(await this.getAccessToken())
      .api(`/users/${id}`)
      .get();
  }

  getLoginUrl({
    dashboardUrl,
    redirect,
  }: GetLoginUrlRequest): GetLoginUrlResponse {
    const query = [];

    const state = {
      redirect,
      dashboardUrl,
    };

    query.push(`state=${JSON.stringify(state)}`);
    query.push(`client_id=${this.clientId}`);
    query.push('response_type=code');
    query.push(`redirect_uri=${this.redirectUri}`);
    query.push('response_mode=query');
    query.push(`scope=${this.scopes}`);

    return new GetLoginUrlResponse(
      `${this.microsoftLoginBaseUrl}/oauth2/v2.0/authorize?${query.join('&')}`,
    );
  }

  async postImportUser(
    authorId: string,
    postImportUserRequest: PostImportUserRequest,
  ): Promise<PostImportUserResponse> {
    const entraUser = await this.getUser(postImportUserRequest.id);
    if (!entraUser) {
      throw new NotFoundException('Entra user not found');
    }
    const avatar = await this.getUserProfilePicture(
      entraUser.id,
      entraUser.displayName,
    );
    const user = await this.usersService.create({
      id: entraUser.id,
      mail: entraUser.mail,
      name: entraUser.displayName,
      avatar,
    } as CreateUserDto);

    await this.logsService.create({
      authorId,
      action: LogActionEnum.IMPORT,
      entity: LogEntityEnum.USER,
      entityId: user.id,
    });

    return new PostImportUserResponse(user);
  }
}
