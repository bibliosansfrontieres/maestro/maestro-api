import { IEntraIdToken } from './entra/entra-id-token.interface';
import { IEntraTokenResponse } from './entra/entra-token-response.interface';

export interface IEntraToken {
  entraToken: IEntraTokenResponse;
  entraId: IEntraIdToken;
}
