export interface IEntraIdToken {
  aud: string;
  iss: string;
  iat: number;
  nbf: number;
  exp: number;
  email: string;
  name: string;
  oid: string;
  preferred_username: string;
  rh: string;
  sub: string;
  tid: string;
  uti: string;
  ver: string;
}
