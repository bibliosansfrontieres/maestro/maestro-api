import { IEntraUser } from './entra-user.interface';

export interface IEntraUsersResponse {
  '@odata.context': string;
  '@odata.nextLink'?: string;
  value: IEntraUser[];
}
