export interface IUserPhotoMetadata {
  '@odata.context': string;
  '@odata.mediaContentType': string;
  '@odata.mediaEtag': string;
  id: string;
  height: number;
  width: number;
}
