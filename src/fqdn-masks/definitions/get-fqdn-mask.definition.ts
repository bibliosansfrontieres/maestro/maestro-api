import { ApiProperty } from '@nestjs/swagger';
import { FqdnMask } from '../entities/fqdn-mask.entity';
import { Device } from 'src/devices/entities/device.entity';

export class GetFqdnMaskResponse {
  @ApiProperty({ example: 1 })
  id: number;

  @ApiProperty({ example: 'My label' })
  label: string;

  @ApiProperty({ example: '*' })
  mask: string;

  @ApiProperty()
  devices: Device[];

  constructor(fqdnMask: FqdnMask) {
    this.id = fqdnMask.id;
    this.label = fqdnMask.label;
    this.mask = fqdnMask.mask;
  }
}
