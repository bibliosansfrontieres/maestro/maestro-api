import { ApiPropertyOptional } from '@nestjs/swagger';
import { PageOptionsRequest } from 'src/utils/pagination/dto/page-options.dto';
import { FqdnMask } from '../entities/fqdn-mask.entity';
import { PageResponse } from 'src/utils/pagination/dto/page-response';
import { PageOptions } from 'src/utils/pagination/page-options';
import { FqdnMaskOrderByEnum } from '../enums/fqdn-mask-order-by.enum';
import { IsEnum, IsOptional, IsString } from 'class-validator';

export class GetFqdnMasksRequest extends PageOptionsRequest {
  @ApiPropertyOptional({
    example: FqdnMaskOrderByEnum.ID,
    enum: FqdnMaskOrderByEnum,
    default: FqdnMaskOrderByEnum.ID,
  })
  @IsOptional()
  @IsEnum(FqdnMaskOrderByEnum)
  orderBy?: FqdnMaskOrderByEnum;

  @ApiPropertyOptional({ example: 'search label/mask' })
  @IsOptional()
  @IsString()
  query?: string;
}

export class GetFqdnMasksResponse extends PageResponse<FqdnMask> {
  constructor(data: FqdnMask[], count: number, pageOptions: PageOptions) {
    super(data, count, pageOptions);
  }
}
