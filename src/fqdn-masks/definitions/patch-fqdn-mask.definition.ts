import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';
import { GetFqdnMaskResponse } from './get-fqdn-mask.definition';
import { FqdnMask } from '../entities/fqdn-mask.entity';

export class PatchFqdnMaskRequest {
  @ApiProperty({ example: 'My label' })
  @IsString()
  @IsOptional()
  label?: string;

  @ApiProperty({ example: '*' })
  @IsString()
  @IsOptional()
  mask?: string;
}

export class PatchFqdnMaskResponse extends GetFqdnMaskResponse {
  constructor(fqdnMask: FqdnMask) {
    super(fqdnMask);
  }
}
