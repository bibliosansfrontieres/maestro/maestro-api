export class CreateFqdnMaskDto {
  label: string;
  mask: string;
}
