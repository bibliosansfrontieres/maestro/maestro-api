import { PartialType } from '@nestjs/swagger';
import { CreateFqdnMaskDto } from './create-fqdn-mask.dto';

export class UpdateFqdnMaskDto extends PartialType(CreateFqdnMaskDto) {}
