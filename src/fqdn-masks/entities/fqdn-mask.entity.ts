import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { CreateFqdnMaskDto } from '../dto/create-fqdn-mask.dto';
import { Device } from 'src/devices/entities/device.entity';

@Entity()
export class FqdnMask {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  label: string;

  @Column()
  mask: string;

  @OneToMany(() => Device, (device) => device.fqdnMask)
  devices: Device[];

  constructor(createFqdnMaskDto: CreateFqdnMaskDto) {
    if (!createFqdnMaskDto) return;
    this.label = createFqdnMaskDto.label;
    this.mask = createFqdnMaskDto.mask;
  }
}
