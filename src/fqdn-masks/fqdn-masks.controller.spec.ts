import { Test, TestingModule } from '@nestjs/testing';
import { FqdnMasksController } from './fqdn-masks.controller';
import { FqdnMasksService } from './fqdn-masks.service';
import { Repository } from 'typeorm';
import { FqdnMask } from './entities/fqdn-mask.entity';
import { getRepositoryToken } from '@nestjs/typeorm';
import { LogsService } from 'src/logs/logs.service';
import { logsServiceMock } from 'src/utils/mocks/services/logs-service.mock';
import { DevicesService } from 'src/devices/devices.service';
import { devicesServiceMock } from 'src/utils/mocks/services/devices-service.mock';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from 'src/users/users.service';
import { EntraService } from 'src/entra/entra.service';
import { ConfigService } from '@nestjs/config';
import { configServiceMock } from 'src/utils/mocks/services/config-service.mock';
import { entraServiceMock } from 'src/utils/mocks/services/entra-service.mock';
import { usersServiceMock } from 'src/utils/mocks/services/users-service.mock';
import { jwtServiceMock } from 'src/utils/mocks/services/jwt-service.mock';

describe('FqdnMasksController', () => {
  let controller: FqdnMasksController;
  let fqdnMaskRepository: Repository<FqdnMask>;

  const FQDN_MASK_REPOSITORY_TOKEN = getRepositoryToken(FqdnMask);

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [FqdnMasksController],
      providers: [
        FqdnMasksService,
        LogsService,
        DevicesService,
        JwtService,
        UsersService,
        EntraService,
        ConfigService,
        {
          provide: FQDN_MASK_REPOSITORY_TOKEN,
          useClass: Repository<FqdnMask>,
        },
      ],
    })
      .overrideProvider(LogsService)
      .useValue(logsServiceMock)
      .overrideProvider(DevicesService)
      .useValue(devicesServiceMock)
      .overrideProvider(JwtService)
      .useValue(jwtServiceMock)
      .overrideProvider(UsersService)
      .useValue(usersServiceMock)
      .overrideProvider(EntraService)
      .useValue(entraServiceMock)
      .overrideProvider(ConfigService)
      .useValue(configServiceMock)
      .compile();

    controller = module.get<FqdnMasksController>(FqdnMasksController);
    fqdnMaskRepository = module.get<Repository<FqdnMask>>(
      FQDN_MASK_REPOSITORY_TOKEN,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
    expect(fqdnMaskRepository).toBeDefined();
  });
});
