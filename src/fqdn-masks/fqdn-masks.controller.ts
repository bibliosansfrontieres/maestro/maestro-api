import { Body, Controller, Get, Param, Patch, Query } from '@nestjs/common';
import { FqdnMasksService } from './fqdn-masks.service';
import {
  PatchFqdnMaskRequest,
  PatchFqdnMaskResponse,
} from './definitions/patch-fqdn-mask.definition';
import { UserId } from 'src/auth/decorators/user-id.decorator';
import {
  GetFqdnMasksRequest,
  GetFqdnMasksResponse,
} from './definitions/get-fqdn-masks.definition';
import { Auth } from 'src/auth/decorators/auth.decorator';
import { PermissionNameEnum } from 'src/permissions/enums/permission-name.enum';
import {
  ApiBadRequestResponse,
  ApiNotFoundResponse,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { GetFqdnMaskResponse } from './definitions/get-fqdn-mask.definition';

@ApiTags('fqdn-masks')
@Controller('fqdn-masks')
export class FqdnMasksController {
  constructor(private readonly fqdnMasksService: FqdnMasksService) {}

  @Patch(':id')
  @Auth(PermissionNameEnum.UPDATE_FQDN)
  @ApiNotFoundResponse({ description: 'Fqdn mask not found' })
  @ApiResponse({ type: PatchFqdnMaskResponse })
  patchFqdnMask(
    @UserId() userId: string,
    @Param('id') id: string,
    @Body() patchFqdnMaskRequest: PatchFqdnMaskRequest,
  ) {
    return this.fqdnMasksService.patchFqdnMask(
      userId,
      +id,
      patchFqdnMaskRequest,
    );
  }

  @Get(':id')
  @Auth(PermissionNameEnum.READ_FQDN)
  @ApiNotFoundResponse({ description: 'Fqdn mask not found' })
  @ApiResponse({ type: GetFqdnMaskResponse })
  getFqdnMask(@Param('id') id: string) {
    return this.fqdnMasksService.getFqdnMask(+id);
  }

  @Get()
  @Auth(PermissionNameEnum.READ_FQDN)
  @ApiResponse({ type: GetFqdnMasksResponse })
  @ApiBadRequestResponse({ description: 'Bad query parameters' })
  getFqdnMasks(@Query() getFqdnMasksRequest: GetFqdnMasksRequest) {
    return this.fqdnMasksService.getFqdnMasks(getFqdnMasksRequest);
  }
}
