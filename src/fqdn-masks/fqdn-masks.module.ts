import { Module, forwardRef } from '@nestjs/common';
import { FqdnMasksService } from './fqdn-masks.service';
import { FqdnMasksController } from './fqdn-masks.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FqdnMask } from './entities/fqdn-mask.entity';
import { LogsModule } from 'src/logs/logs.module';
import { DevicesModule } from 'src/devices/devices.module';
import { JwtModule } from '@nestjs/jwt';
import { UsersModule } from 'src/users/users.module';
import { EntraModule } from 'src/entra/entra.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([FqdnMask]),
    LogsModule,
    forwardRef(() => DevicesModule),
    JwtModule,
    UsersModule,
    EntraModule,
  ],
  controllers: [FqdnMasksController],
  providers: [FqdnMasksService],
  exports: [FqdnMasksService],
})
export class FqdnMasksModule {}
