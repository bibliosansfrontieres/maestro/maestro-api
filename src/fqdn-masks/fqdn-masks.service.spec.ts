import { Test, TestingModule } from '@nestjs/testing';
import { FqdnMasksService } from './fqdn-masks.service';
import { FqdnMask } from './entities/fqdn-mask.entity';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { LogsService } from 'src/logs/logs.service';
import { logsServiceMock } from 'src/utils/mocks/services/logs-service.mock';
import { DevicesService } from 'src/devices/devices.service';
import { devicesServiceMock } from 'src/utils/mocks/services/devices-service.mock';

describe('FqdnMasksService', () => {
  let service: FqdnMasksService;
  let fqdnMaskRepository: Repository<FqdnMask>;

  const FQDN_MASK_REPOSITORY_TOKEN = getRepositoryToken(FqdnMask);

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        FqdnMasksService,
        LogsService,
        DevicesService,
        {
          provide: FQDN_MASK_REPOSITORY_TOKEN,
          useClass: Repository<FqdnMask>,
        },
      ],
    })
      .overrideProvider(LogsService)
      .useValue(logsServiceMock)
      .overrideProvider(DevicesService)
      .useValue(devicesServiceMock)
      .compile();

    service = module.get<FqdnMasksService>(FqdnMasksService);
    fqdnMaskRepository = module.get<Repository<FqdnMask>>(
      FQDN_MASK_REPOSITORY_TOKEN,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
    expect(fqdnMaskRepository).toBeDefined();
  });
});
