import {
  Inject,
  Injectable,
  NotFoundException,
  forwardRef,
} from '@nestjs/common';
import { FindManyOptions, FindOneOptions, Repository } from 'typeorm';
import { FqdnMask } from './entities/fqdn-mask.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateFqdnMaskDto } from './dto/create-fqdn-mask.dto';
import { Device } from 'src/devices/entities/device.entity';
import { minimatch } from 'minimatch';
import {
  PatchFqdnMaskRequest,
  PatchFqdnMaskResponse,
} from './definitions/patch-fqdn-mask.definition';
import { LogsService } from 'src/logs/logs.service';
import { LogEntityEnum } from 'src/logs/enums/log-entity.enum';
import { LogActionEnum } from 'src/logs/enums/log-action.enum';
import { DevicesService } from 'src/devices/devices.service';
import {
  GetFqdnMasksRequest,
  GetFqdnMasksResponse,
} from './definitions/get-fqdn-masks.definition';
import { PageOptions } from 'src/utils/pagination/page-options';
import { FqdnMaskOrderByEnum } from './enums/fqdn-mask-order-by.enum';
import { QueryHelper } from 'src/utils/query-helpers.util';
import { getManyAndCount } from 'src/utils/get-many-and-count.util';
import { GetFqdnMaskResponse } from './definitions/get-fqdn-mask.definition';
import { PageOrderEnum } from 'src/utils/pagination/enums/page-order.enum';

@Injectable()
export class FqdnMasksService {
  constructor(
    @InjectRepository(FqdnMask)
    private fqdnMaskRepository: Repository<FqdnMask>,
    private logsService: LogsService,
    @Inject(forwardRef(() => DevicesService))
    private devicesService: DevicesService,
  ) {}

  async create(createFqdnMaskDto: CreateFqdnMaskDto) {
    const fqdnMask = new FqdnMask(createFqdnMaskDto);
    return this.fqdnMaskRepository.save(fqdnMask);
  }

  async assign(device: Device): Promise<FqdnMask | undefined> {
    let foudnFqdnMask = undefined;
    const fqdnMasks = await this.fqdnMaskRepository.find();
    for (const fqdnMask of fqdnMasks) {
      if (minimatch(device.ipv4FqdnName, fqdnMask.mask)) {
        foudnFqdnMask = fqdnMask;
        break;
      }
      if (minimatch(device.ipv4FqdnArpa, fqdnMask.mask)) {
        foudnFqdnMask = fqdnMask;
        break;
      }
    }
    return foudnFqdnMask;
  }

  async assignOrCreate(device: Device): Promise<FqdnMask> {
    let fqdnMask = await this.assign(device);

    if (!fqdnMask) {
      fqdnMask = await this.create({
        label: device.ipv4FqdnName || device.ipv6FqdnArpa,
        mask: device.ipv4FqdnName || device.ipv6FqdnArpa,
      });
    }

    return fqdnMask;
  }

  async patchFqdnMask(
    authorId: string,
    id: number,
    patchFqdnMaskRequest: PatchFqdnMaskRequest,
  ) {
    let fqdnMask = await this.fqdnMaskRepository.findOne({
      where: { id },
      relations: { devices: true },
    });
    if (!fqdnMask) {
      throw new NotFoundException('fqdn mask not found');
    }

    fqdnMask.label = patchFqdnMaskRequest.label || fqdnMask.label;
    fqdnMask.mask = patchFqdnMaskRequest.mask || fqdnMask.mask;
    fqdnMask = await this.fqdnMaskRepository.save(fqdnMask);

    await this.logsService.create({
      authorId,
      entity: LogEntityEnum.FQDN_MASK,
      entityId: fqdnMask.id,
      action: LogActionEnum.UPDATE,
      values: patchFqdnMaskRequest,
    });

    await this.devicesService.automaticAssociate(fqdnMask.devices);

    return new PatchFqdnMaskResponse(fqdnMask);
  }

  async getFqdnMask(id: number) {
    const fqdnMask = await this.fqdnMaskRepository.findOne({
      where: { id },
      relations: { devices: true },
    });

    if (!fqdnMask) {
      throw new NotFoundException('fqdn mask not found');
    }

    return new GetFqdnMaskResponse(fqdnMask);
  }

  async getFqdnMasks(getFqdnMasksRequest: GetFqdnMasksRequest) {
    let { orderBy } = getFqdnMasksRequest;

    orderBy = orderBy ? orderBy : FqdnMaskOrderByEnum.LABEL;

    const pageOptions = new PageOptions(getFqdnMasksRequest, {
      order:
        // if no order specified, default to ASC when label
        orderBy === FqdnMaskOrderByEnum.LABEL ? PageOrderEnum.ASC : undefined,
    });
    const fqdnMasksQuery = this.fqdnMaskRepository
      .createQueryBuilder('fqdnMask')
      .leftJoinAndSelect('fqdnMask.devices', 'device');

    const queryHelper = new QueryHelper(fqdnMasksQuery);
    queryHelper.pagination(pageOptions, orderBy);

    queryHelper.andWhereQuery(getFqdnMasksRequest.query, [
      'fqdnMask.label',
      'fqdnMask.mask',
    ]);

    const [count, fqdnMasks] = await getManyAndCount<FqdnMask>(
      fqdnMasksQuery,
      ['fqdnMask.id'],
      false,
    );

    return new GetFqdnMasksResponse(fqdnMasks, count, pageOptions);
  }

  findOne(options?: FindOneOptions<FqdnMask>) {
    return this.fqdnMaskRepository.findOne(options);
  }

  find(options?: FindManyOptions<FqdnMask>) {
    return this.fqdnMaskRepository.find(options);
  }
}
