import { ApiProperty } from '@nestjs/swagger';
import { Group } from '../entities/group.entity';

export class GetGroupResponse {
  @ApiProperty({ example: 1 })
  id: number;

  @ApiProperty({ example: 'Group name' })
  name: string;

  @ApiProperty({ example: 'My group description' })
  description: string | null;

  @ApiProperty({ example: false })
  isArchived: boolean;

  constructor(group: Group) {
    this.id = group.id;
    this.name = group.name;
    this.description = group.description;
    this.isArchived = group.isArchived;
  }
}
