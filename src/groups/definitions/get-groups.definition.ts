import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsEnum, IsOptional, IsString } from 'class-validator';
import { BooleanEnum } from 'src/utils/enums/boolean.enum';
import { PageOptionsRequest } from 'src/utils/pagination/dto/page-options.dto';
import { PageResponse } from 'src/utils/pagination/dto/page-response';
import { Group } from '../entities/group.entity';
import { PageOptions } from 'src/utils/pagination/page-options';

export class GetGroupsRequest extends PageOptionsRequest {
  @ApiPropertyOptional({
    example: BooleanEnum.TRUE,
    default: BooleanEnum.TRUE,
    enum: BooleanEnum,
  })
  @IsEnum(BooleanEnum)
  @IsOptional()
  isExcludingArchived?: BooleanEnum;

  @ApiPropertyOptional({
    example: BooleanEnum.FALSE,
    default: BooleanEnum.FALSE,
    enum: BooleanEnum,
  })
  @IsEnum(BooleanEnum)
  @IsOptional()
  isOnlyTakingUserGroups?: BooleanEnum;

  @ApiPropertyOptional({
    description: 'Search for group names',
  })
  @IsString()
  @IsOptional()
  query?: string;
}

export class GetGroupsResponse extends PageResponse<Group> {
  constructor(groups: Group[], count: number, pageOptions: PageOptions) {
    super(groups, count, pageOptions);
  }
}
