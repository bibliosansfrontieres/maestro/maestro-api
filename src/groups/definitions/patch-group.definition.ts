import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsBoolean, IsOptional, IsString } from 'class-validator';
import { Group } from '../entities/group.entity';
import { GetGroupResponse } from './get-group.definition';

export class PatchGroupRequest {
  @ApiPropertyOptional({ example: 'Group name' })
  @IsString()
  @IsOptional()
  name?: string;

  @ApiPropertyOptional({ example: 'My group description' })
  @IsString()
  @IsOptional()
  description?: string;

  @ApiPropertyOptional({ example: false })
  @IsBoolean()
  @IsOptional()
  isArchived?: boolean;
}

export class PatchGroupResponse extends GetGroupResponse {
  constructor(group: Group) {
    super(group);
  }
}
