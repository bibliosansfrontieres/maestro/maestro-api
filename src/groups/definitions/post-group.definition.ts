import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';
import { Group } from '../entities/group.entity';
import { GetGroupResponse } from './get-group.definition';

export class PostGroupRequest {
  @ApiProperty({ example: 'Group name' })
  @IsString()
  name: string;

  @ApiPropertyOptional({ example: 'My group description' })
  @IsString()
  @IsOptional()
  description?: string;
}

export class PostGroupResponse extends GetGroupResponse {
  constructor(group: Group) {
    super(group);
  }
}
