import { User } from 'src/users/entities/user.entity';
import { Column, Entity, ManyToMany, PrimaryGeneratedColumn } from 'typeorm';
import { PostGroupRequest } from '../definitions/post-group.definition';
import { VirtualMachine } from 'src/virtual-machines/entities/virtual-machine.entity';

@Entity()
export class Group {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column({ nullable: true })
  description: string | null;

  @Column()
  isArchived: boolean;

  @ManyToMany(() => User, (user) => user.groups)
  users: User[];

  @ManyToMany(() => VirtualMachine, (virtualMachine) => virtualMachine.groups)
  virtualMachines: VirtualMachine[];

  constructor(postGroupRequest: PostGroupRequest) {
    if (!postGroupRequest) return;
    this.name = postGroupRequest.name;
    this.description = postGroupRequest.description;
    this.isArchived = false;
  }
}
