import { Test, TestingModule } from '@nestjs/testing';
import { GroupsController } from './groups.controller';
import { GroupsService } from './groups.service';
import { Group } from './entities/group.entity';
import { Repository } from 'typeorm';
import { getRepositoryToken } from '@nestjs/typeorm';
import { LogsService } from 'src/logs/logs.service';
import { logsServiceMock } from 'src/utils/mocks/services/logs-service.mock';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from 'src/users/users.service';
import { EntraService } from 'src/entra/entra.service';
import { entraServiceMock } from 'src/utils/mocks/services/entra-service.mock';
import { usersServiceMock } from 'src/utils/mocks/services/users-service.mock';
import { jwtServiceMock } from 'src/utils/mocks/services/jwt-service.mock';
import { ConfigService } from '@nestjs/config';
import { configServiceMock } from 'src/utils/mocks/services/config-service.mock';

describe('GroupsController', () => {
  let controller: GroupsController;
  let groupRepository: Repository<Group>;

  const GROUP_REPOSITORY_TOKEN = getRepositoryToken(Group);

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [GroupsController],
      providers: [
        GroupsService,
        LogsService,
        JwtService,
        UsersService,
        EntraService,
        ConfigService,
        {
          provide: GROUP_REPOSITORY_TOKEN,
          useClass: Repository<Group>,
        },
      ],
    })
      .overrideProvider(LogsService)
      .useValue(logsServiceMock)
      .overrideProvider(JwtService)
      .useValue(jwtServiceMock)
      .overrideProvider(UsersService)
      .useValue(usersServiceMock)
      .overrideProvider(EntraService)
      .useValue(entraServiceMock)
      .overrideProvider(ConfigService)
      .useValue(configServiceMock)
      .compile();

    controller = module.get<GroupsController>(GroupsController);
    groupRepository = module.get<Repository<Group>>(GROUP_REPOSITORY_TOKEN);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
    expect(groupRepository).toBeDefined();
  });
});
