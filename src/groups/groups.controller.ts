import {
  Body,
  Controller,
  Get,
  Param,
  Patch,
  Post,
  Query,
} from '@nestjs/common';
import { GroupsService } from './groups.service';
import {
  PostGroupRequest,
  PostGroupResponse,
} from './definitions/post-group.definition';
import {
  ApiBadRequestResponse,
  ApiBody,
  ApiConflictResponse,
  ApiCreatedResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { Auth } from 'src/auth/decorators/auth.decorator';
import { PermissionNameEnum } from 'src/permissions/enums/permission-name.enum';
import {
  PatchGroupRequest,
  PatchGroupResponse,
} from './definitions/patch-group.definition';
import { UserId } from 'src/auth/decorators/user-id.decorator';
import {
  GetGroupsRequest,
  GetGroupsResponse,
} from './definitions/get-groups.definition';
import { GetGroupResponse } from './definitions/get-group.definition';

@Controller('groups')
@ApiTags('groups')
export class GroupsController {
  constructor(private readonly groupsService: GroupsService) {}

  @Get()
  @Auth(PermissionNameEnum.READ_GROUP)
  @ApiOperation({ summary: 'Get all groups' })
  @ApiOkResponse({ type: GetGroupsResponse })
  @ApiBadRequestResponse({ description: 'Invalid request query parameters' })
  getGroups(
    @Query() getGroupsRequest: GetGroupsRequest,
    @UserId() userId: string,
  ) {
    return this.groupsService.getGroups(getGroupsRequest, userId);
  }

  @Get(':id')
  @Auth(PermissionNameEnum.READ_GROUP)
  @ApiOperation({ summary: 'Get group by ID' })
  @ApiOkResponse({ type: GetGroupResponse })
  @ApiNotFoundResponse({ description: 'Group not found' })
  getGroup(@Param('id') id: string) {
    return this.groupsService.getGroup(+id);
  }

  @Post()
  @Auth(PermissionNameEnum.CREATE_GROUP)
  @ApiOperation({ summary: 'Create a group' })
  @ApiCreatedResponse({ type: PostGroupResponse })
  @ApiBody({ type: PostGroupRequest })
  @ApiBadRequestResponse({
    description:
      'Invalid request body parameters/Group name is shorter than 2 characters',
  })
  @ApiConflictResponse({ description: 'Group name already exists' })
  postGroup(
    @UserId() userId: string,
    @Body() postGroupRequest: PostGroupRequest,
  ) {
    return this.groupsService.postGroup(userId, postGroupRequest);
  }

  @Patch(':id')
  @Auth(PermissionNameEnum.UPDATE_GROUP)
  @ApiOperation({ summary: 'Update a group' })
  @ApiOkResponse({ type: PatchGroupResponse })
  @ApiBody({ type: PatchGroupRequest })
  @ApiBadRequestResponse({
    description:
      'Invalid request body parameters/Group name is shorter than 2 characters',
  })
  @ApiConflictResponse({ description: 'Group name already exists' })
  @ApiNotFoundResponse({ description: 'Group not found' })
  patchGroup(
    @UserId() userId: string,
    @Param('id') id: string,
    @Body() patchGroupRequest: PatchGroupRequest,
  ) {
    return this.groupsService.patchGroup(userId, +id, patchGroupRequest);
  }
}
