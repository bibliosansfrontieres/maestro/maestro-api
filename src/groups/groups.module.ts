import { Module, forwardRef } from '@nestjs/common';
import { GroupsService } from './groups.service';
import { GroupsController } from './groups.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Group } from './entities/group.entity';
import { LogsModule } from 'src/logs/logs.module';
import { JwtModule } from '@nestjs/jwt';
import { UsersModule } from 'src/users/users.module';
import { EntraModule } from 'src/entra/entra.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([Group]),
    LogsModule,
    JwtModule,
    forwardRef(() => UsersModule),
    forwardRef(() => EntraModule),
  ],
  controllers: [GroupsController],
  providers: [GroupsService],
  exports: [GroupsService],
})
export class GroupsModule {}
