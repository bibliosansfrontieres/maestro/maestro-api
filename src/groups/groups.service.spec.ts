import { Test, TestingModule } from '@nestjs/testing';
import { GroupsService } from './groups.service';
import { Group } from './entities/group.entity';
import { Repository } from 'typeorm';
import { getRepositoryToken } from '@nestjs/typeorm';
import { LogsService } from 'src/logs/logs.service';
import { logsServiceMock } from 'src/utils/mocks/services/logs-service.mock';

describe('GroupsService', () => {
  let service: GroupsService;
  let groupRepository: Repository<Group>;

  const GROUP_REPOSITORY_TOKEN = getRepositoryToken(Group);

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        GroupsService,
        LogsService,
        { provide: GROUP_REPOSITORY_TOKEN, useClass: Repository<Group> },
      ],
    })
      .overrideProvider(LogsService)
      .useValue(logsServiceMock)
      .compile();

    service = module.get<GroupsService>(GroupsService);
    groupRepository = module.get<Repository<Group>>(GROUP_REPOSITORY_TOKEN);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
    expect(groupRepository).toBeDefined();
  });
});
