import {
  BadRequestException,
  ConflictException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Group } from './entities/group.entity';
import {
  FindManyOptions,
  FindOneOptions,
  QueryRunner,
  Repository,
} from 'typeorm';
import {
  PostGroupRequest,
  PostGroupResponse,
} from './definitions/post-group.definition';
import {
  PatchGroupRequest,
  PatchGroupResponse,
} from './definitions/patch-group.definition';
import { LogsService } from 'src/logs/logs.service';
import {
  GetGroupsRequest,
  GetGroupsResponse,
} from './definitions/get-groups.definition';
import { PageOptions } from 'src/utils/pagination/page-options';
import { getManyAndCount } from 'src/utils/get-many-and-count.util';
import { BooleanEnum } from 'src/utils/enums/boolean.enum';
import { GetGroupResponse } from './definitions/get-group.definition';
import { LogActionEnum } from 'src/logs/enums/log-action.enum';
import { LogEntityEnum } from 'src/logs/enums/log-entity.enum';
import {
  GetUserGroupsRequest,
  GetUserGroupsResponse,
} from 'src/users/definitions/get-user-groups.definition';
import { PageOrderEnum } from 'src/utils/pagination/enums/page-order.enum';

@Injectable()
export class GroupsService {
  constructor(
    @InjectRepository(Group) private groupRepository: Repository<Group>,
    private logsService: LogsService,
  ) {}

  createQueryBuilder(alias?: string, queryRunner?: QueryRunner) {
    return this.groupRepository.createQueryBuilder(alias, queryRunner);
  }

  async getUserGroups(id: string, getUserGroupsRequest: GetUserGroupsRequest) {
    const pageOptions = new PageOptions(getUserGroupsRequest);

    let userGroupsQueryBuilder = this.groupRepository
      .createQueryBuilder('group')
      .select('group')
      .leftJoin('group.users', 'users')
      .take(pageOptions.take)
      .skip(pageOptions.skip)
      .orderBy('group.id', pageOptions.order);

    if (getUserGroupsRequest.isUserInGroup === BooleanEnum.FALSE) {
      userGroupsQueryBuilder = userGroupsQueryBuilder.andWhere(
        (qb) => {
          const subQuery = qb
            .subQuery()
            .select('group.id')
            .from('Group', 'group')
            .leftJoin('group.users', 'users')
            .where('users.id = :id')
            .getQuery();
          return 'group.id NOT IN ' + subQuery;
        },
        { id },
      );
    } else {
      userGroupsQueryBuilder = userGroupsQueryBuilder.andWhere(
        'users.id = :id',
        { id },
      );
    }

    if (getUserGroupsRequest.query) {
      userGroupsQueryBuilder = userGroupsQueryBuilder.andWhere(
        'LOWER(group.name) LIKE :query',
        {
          query: `%${getUserGroupsRequest.query.toLowerCase()}%`,
        },
      );
    }

    const [count, groups] = await getManyAndCount<Group>(
      userGroupsQueryBuilder,
      ['group.id'],
      false,
    );

    return new GetUserGroupsResponse(groups, count, pageOptions);
  }

  async getGroups(getGroupsRequest: GetGroupsRequest, userId: string) {
    const pageOptions = new PageOptions(getGroupsRequest, {
      order: PageOrderEnum.ASC,
    });

    let groupsQueryBuilder = this.groupRepository
      .createQueryBuilder('group')
      .select('group')
      .leftJoinAndSelect('group.users', 'user')
      .leftJoinAndSelect('group.virtualMachines', 'virtualMachine')
      .take(pageOptions.take)
      .skip(pageOptions.skip)
      .orderBy('group.name', pageOptions.order);

    if (getGroupsRequest.isExcludingArchived !== BooleanEnum.FALSE) {
      groupsQueryBuilder = groupsQueryBuilder.andWhere('group.isArchived = 0');
    }

    if (getGroupsRequest.isOnlyTakingUserGroups === BooleanEnum.TRUE) {
      groupsQueryBuilder = groupsQueryBuilder.andWhere('user.id = :userId', {
        userId,
      });
    }

    if (getGroupsRequest.query) {
      groupsQueryBuilder = groupsQueryBuilder.andWhere(
        'LOWER(group.name) LIKE :query',
        {
          query: `%${getGroupsRequest.query.toLowerCase()}%`,
        },
      );
    }

    const [count, groups] = await getManyAndCount<Group>(
      groupsQueryBuilder,
      ['group.id'],
      false,
    );

    return new GetGroupsResponse(groups, count, pageOptions);
  }

  async getGroup(id: number) {
    const group = await this.findOne({
      where: { id },
      relations: { users: true },
    });

    if (group === null) {
      throw new NotFoundException('group not found');
    }

    return new GetGroupResponse(group);
  }

  async postGroup(authorId: string, postGroupRequest: PostGroupRequest) {
    let group = new Group(postGroupRequest);

    if (group.name.length < 2) {
      throw new BadRequestException(
        'group name must be at least 2 characters long',
      );
    }

    const groupNameExists = await this.groupRepository
      .createQueryBuilder('group')
      .where('LOWER(group.name) = :name', { name: group.name.toLowerCase() })
      .getOne();

    if (groupNameExists) {
      throw new ConflictException('group name already exists');
    }

    group = await this.save(group);

    await this.logsService.create({
      authorId,
      action: LogActionEnum.CREATE,
      entity: LogEntityEnum.GROUP,
      entityId: group.id,
      values: { name: group.name, description: group.description },
    });

    return new PostGroupResponse(group);
  }

  async patchGroup(
    authorId: string,
    id: number,
    patchGroupRequest: PatchGroupRequest,
  ) {
    const group = await this.findOne({ where: { id } });
    if (group === null) {
      throw new NotFoundException('group not found');
    }

    if (patchGroupRequest?.name) {
      if (patchGroupRequest.name.length < 2) {
        throw new BadRequestException(
          'group name must be at least 2 characters long',
        );
      }

      const groupNameExists = await this.groupRepository
        .createQueryBuilder('group')
        .where('LOWER(group.name) = :name', {
          name: patchGroupRequest.name.toLowerCase(),
        })
        .getOne();

      if (groupNameExists) {
        throw new ConflictException('group name already exists');
      }
    }

    group.name = patchGroupRequest?.name || group.name;

    if (patchGroupRequest.description === '') {
      group.description = null;
    } else {
      group.description = patchGroupRequest.description || group.description;
    }

    group.isArchived =
      patchGroupRequest?.isArchived !== undefined
        ? patchGroupRequest.isArchived
        : group.isArchived;
    await this.save(group);

    await this.logsService.create({
      authorId,
      action: LogActionEnum.UPDATE,
      entity: LogEntityEnum.GROUP,
      entityId: group.id,
      values: patchGroupRequest,
    });

    return new PatchGroupResponse(group);
  }

  findOne(options?: FindOneOptions<Group>) {
    return this.groupRepository.findOne(options);
  }

  find(options?: FindManyOptions<Group>) {
    return this.groupRepository.find(options);
  }

  save(group: Group) {
    return this.groupRepository.save(group);
  }
}
