import { Module } from '@nestjs/common';
import { InitDatabaseService } from './init-database.service';
import { RolesModule } from 'src/roles/roles.module';
import { PermissionsModule } from 'src/permissions/permissions.module';
import { UsersModule } from 'src/users/users.module';
import { ApplicationsModule } from 'src/applications/applications.module';
import { ContextsModule } from 'src/contexts/contexts.module';
import { ItemsModule } from 'src/items/items.module';
import { CategoriesModule } from 'src/categories/categories.module';
import { LabelsModule } from 'src/labels/labels.module';

@Module({
  imports: [
    RolesModule,
    PermissionsModule,
    UsersModule,
    ApplicationsModule,
    ContextsModule,
    ItemsModule,
    CategoriesModule,
    LabelsModule,
  ],
  providers: [InitDatabaseService],
})
export class InitDatabaseModule {}
