import { Test, TestingModule } from '@nestjs/testing';
import { InitDatabaseService } from './init-database.service';
import { RolesService } from 'src/roles/roles.service';
import { PermissionsService } from 'src/permissions/permissions.service';
import { rolesServiceMock } from 'src/utils/mocks/services/roles-service.mock';
import { permissionsServiceMock } from 'src/utils/mocks/services/permissions-service.mock';
import { UsersService } from 'src/users/users.service';
import { usersServiceMock } from 'src/utils/mocks/services/users-service.mock';
import { ApplicationsService } from 'src/applications/applications.service';
import { ContextsService } from 'src/contexts/contexts.service';
import { applicationsServiceMock } from 'src/utils/mocks/services/applications-service.mock';
import { contextsServiceMock } from 'src/utils/mocks/services/contexts-service.mock';
import { ItemsService } from 'src/items/items.service';
import { CategoriesService } from 'src/categories/categories.service';
import { itemsServiceMock } from 'src/utils/mocks/services/items-service.mock';
import { categoriesServiceMock } from 'src/utils/mocks/services/categories-service.mock';
import { LabelsService } from 'src/labels/labels.service';
import { labelsServiceMock } from 'src/utils/mocks/services/labels-service.mock';
import { ConfigService } from '@nestjs/config';
import { configServiceMock } from 'src/utils/mocks/services/config-service.mock';

describe('InitDatabaseService', () => {
  let service: InitDatabaseService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        InitDatabaseService,
        RolesService,
        PermissionsService,
        UsersService,
        ApplicationsService,
        ContextsService,
        ItemsService,
        CategoriesService,
        LabelsService,
        ConfigService,
      ],
    })
      .overrideProvider(RolesService)
      .useValue(rolesServiceMock)
      .overrideProvider(PermissionsService)
      .useValue(permissionsServiceMock)
      .overrideProvider(UsersService)
      .useValue(usersServiceMock)
      .overrideProvider(ApplicationsService)
      .useValue(applicationsServiceMock)
      .overrideProvider(ContextsService)
      .useValue(contextsServiceMock)
      .overrideProvider(ItemsService)
      .useValue(itemsServiceMock)
      .overrideProvider(CategoriesService)
      .useValue(categoriesServiceMock)
      .overrideProvider(LabelsService)
      .useValue(labelsServiceMock)
      .overrideProvider(ConfigService)
      .useValue(configServiceMock)
      .compile();

    service = module.get<InitDatabaseService>(InitDatabaseService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
