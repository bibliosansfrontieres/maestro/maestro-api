import { Injectable, Logger } from '@nestjs/common';
import { PermissionNameEnum } from 'src/permissions/enums/permission-name.enum';
import { PermissionsService } from 'src/permissions/permissions.service';
import { RoleNameEnum } from 'src/roles/enums/role-name.enum';
import { RolesService } from 'src/roles/roles.service';
import { rolesPermissions } from './initial-data/roles-permissions';
import { UsersService } from 'src/users/users.service';
import { adminUserIds } from './initial-data/admin-user-ids';
import { join } from 'path';
import { readFileSync, readdirSync } from 'fs';
import { applicationMd5, applicationTypeMd5 } from 'src/utils/md5.util';
import { ApplicationsService } from 'src/applications/applications.service';
import { ContextsService } from 'src/contexts/contexts.service';
import { CreateCategoryDto } from 'src/categories/dto/create-category.dto';
import { CreateDublinCoreTypeDto } from 'src/items/dto/create-dublin-core-type.dto';
import { CreateDublinCoreTypeTranslationDto } from 'src/items/dto/create-dublin-core-type-translation.dto';
import { ItemSourceEnum } from 'src/items/enums/item-source.enum';
import { ItemsService } from 'src/items/items.service';
import { CategoriesService } from 'src/categories/categories.service';
import { CreateApplicationTypeDto } from 'src/applications/dto/create-application-type.dto';
import { checkApplicationFiles } from 'src/utils/applications/check-application-files.util';
import { LabelsService } from 'src/labels/labels.service';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class InitDatabaseService {
  constructor(
    private rolesService: RolesService,
    private permissionsService: PermissionsService,
    private usersService: UsersService,
    private applicationsService: ApplicationsService,
    private contextsService: ContextsService,
    private itemsService: ItemsService,
    private categoriesService: CategoriesService,
    private labelsService: LabelsService,
    private configService: ConfigService,
  ) {}

  async onModuleInit() {
    Logger.log('Initializing database', 'InitDatabaseService');
    await this.createRoles();
    await this.createPermissions();
    await this.linkPermissions();
    await this.createDefaultUsers();
    await this.createApplicationTypes();
    await this.createApplications();
    await this.createDefaultContext();
    await this.createDublinCoreTypes();
    await this.createCategories();
    await this.createDefaultLabels();
    await this.checkApplicationsCatalogs();
    Logger.log('Database initialized', 'InitDatabaseService');
  }

  async createDefaultLabels() {
    const unknownLabel = await this.labelsService.findOne({
      where: { name: 'Unknown' },
    });
    if (!unknownLabel) {
      await this.labelsService.create('unknown', { name: 'Unknown' });
    }
  }

  async checkApplicationsCatalogs() {
    await this.applicationsService.checkApplicationsCatalogs();
  }

  async createRoles() {
    Logger.log('Creating roles', 'InitDatabaseService');
    const rolesCount = await this.rolesService.count();
    const roleNames = Object.keys(RoleNameEnum);
    if (rolesCount === roleNames.length) return;
    // Create missing roles
    for (const roleName of roleNames) {
      const findRole = await this.rolesService.findOne({
        where: { name: RoleNameEnum[roleName] },
      });
      if (findRole) continue;
      await this.rolesService.create({ name: RoleNameEnum[roleName] });
    }
  }

  async createPermissions() {
    Logger.log('Creating permissions', 'InitDatabaseService');
    const permissionsCount = await this.permissionsService.count();
    const permissionNames = Object.keys(PermissionNameEnum);
    if (permissionsCount === permissionNames.length) return;
    // Create missing permissions
    for (const permissionName of permissionNames) {
      const findPermission = await this.permissionsService.findOne({
        where: { name: PermissionNameEnum[permissionName] },
      });
      if (findPermission) continue;
      await this.permissionsService.create({
        name: PermissionNameEnum[permissionName],
      });
    }
  }

  async linkPermissions() {
    Logger.log('Linking permissions to roles', 'InitDatabaseService');
    const roles = await this.rolesService.find();
    for (const role of roles) {
      const permissionNames =
        rolesPermissions.find((p) => p.name === role.name)?.permissions ||
        undefined;
      if (!permissionNames) continue;
      const permissions = [];
      for (const permissionName of permissionNames) {
        const permission = await this.permissionsService.findOne({
          where: { name: permissionName },
        });
        if (!permission) continue;
        permissions.push(permission);
      }
      role.permissions = permissions;
      await this.rolesService.save(role);
    }
  }

  async createDefaultUsers() {
    Logger.log('Creating default users', 'InitDatabaseService');
    const unknownUser = await this.usersService.findOne({
      where: { id: 'unknown' },
    });
    if (!unknownUser) {
      await this.usersService.createUnknownUser();
    }
    for (const adminUserId of adminUserIds) {
      let user = await this.usersService.findOne({
        where: { id: adminUserId },
        relations: { role: true },
      });
      if (!user) {
        user = await this.usersService.createAdmin(adminUserId);
      } else {
        if (user.role.name !== RoleNameEnum.ADMIN) {
          await this.usersService.patchUser('unknown', adminUserId, {
            roleName: RoleNameEnum.ADMIN,
          });
        }
      }
    }
  }

  async createApplicationTypes() {
    Logger.log('Creating application types', 'InitDatabaseService');

    const applicationTypesFile = readFileSync(
      join(__dirname, '../../data/initial-data/application-types.json'),
    );
    const applicationTypes = JSON.parse(
      applicationTypesFile.toString(),
    ).applicationTypes;

    for (const applicationType of applicationTypes) {
      const findApplicationType =
        await this.applicationsService.findOneApplicationType({
          where: {
            name: applicationType.name,
          },
        });
      const md5 = applicationTypeMd5(
        applicationType as CreateApplicationTypeDto,
      );
      if (!findApplicationType) {
        await this.applicationsService.createApplicationType(
          applicationType as CreateApplicationTypeDto,
        );
      } else {
        if (findApplicationType.md5 !== md5) {
          // TODO : update
        }
      }
    }
  }

  async createApplications() {
    Logger.log('Creating applications', 'InitDatabaseService');
    const applicationsFolder = join(__dirname, '../../data/applications');

    for (const name of readdirSync(applicationsFolder)) {
      if (name !== 'olip') continue;
      const applicationPath = join(applicationsFolder, name);

      // Check application files
      const result = checkApplicationFiles(name, applicationPath);
      if (result === undefined) continue;

      const { description, hooks, compose } = result;

      // Get application
      const application = await this.applicationsService.findOne({
        where: { id: description.id },
        relations: { applicationCatalogs: true, applicationTranslations: true },
      });

      // Create application md5 hash
      const md5 = applicationMd5(description, hooks, compose);

      // If application doesn't exists or md5 hash is different
      if (application === null || application.md5 !== md5) {
        await this.applicationsService.createOrUpdate(
          application,
          description,
          hooks,
          compose,
        );
      }
    }

    await this.applicationsService.getCatalogUpdate({
      branch: this.configService.get('MAESTRO_BRANCH'),
    });
  }

  async createDefaultContext() {
    Logger.log('Creating default context', 'InitDatabaseService');
    const context = await this.contextsService.findOne({
      where: { isDefaultContext: true },
    });
    if (context === null) {
      await this.contextsService.create(
        { isOutsideProjectDownloadsAuthorized: true },
        true,
      );
    }
  }

  async createDublinCoreTypes() {
    const dublinCoreTypesFile = readFileSync(
      join(__dirname, '../../data/initial-data/dublin-core-types.json'),
    );
    const dublinCoreTypes = JSON.parse(
      dublinCoreTypesFile.toString(),
    ).dublinCoreTypes;
    for (const dublinCoreType of dublinCoreTypes) {
      const findDublinCoreType = await this.itemsService.findOneDublinCoreType({
        where: { dublinCoreTypeTranslations: { label: dublinCoreType.title } },
      });
      if (findDublinCoreType) continue;
      const createDublinCoreTypeDto: CreateDublinCoreTypeDto = {
        image: '',
      };
      const createDublinCoreTypeTranslationDtos: CreateDublinCoreTypeTranslationDto[] =
        [
          {
            label: dublinCoreType.title,
            description: dublinCoreType.description,
            languageIdentifier: 'eng',
          },
        ];
      await this.itemsService.createDublinCoreType(
        createDublinCoreTypeDto,
        createDublinCoreTypeTranslationDtos,
      );
    }
  }

  async createCategories() {
    const categoriesFile = readFileSync(
      join(__dirname, '../../data/initial-data/categories.json'),
    );
    const categories = JSON.parse(categoriesFile.toString()).categories;
    for (const category of categories) {
      const findCategory = await this.categoriesService.findOne({
        where: { categoryTranslations: { title: category.title } },
      });
      if (findCategory) continue;
      const createCategoryDto: CreateCategoryDto = {
        id: category.id,
        pictogram: '',
        source: ItemSourceEnum.OMEKA,
        categoryTranslations: [
          {
            title: category.title,
            // TODO : languageIdentifier
            languageIdentifier: 'eng',
          },
        ],
      };
      await this.categoriesService.create(createCategoryDto);
    }
  }
}
