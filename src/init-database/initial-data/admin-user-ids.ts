export const adminUserIds = [
  'f5fb092f-904f-456e-baf0-da44349dfce1', // Alexandre
  '76670c75-78d3-4afd-aa7e-073a47baf692', // Audrey
  '668b0ca3-cdda-42fb-9a6b-ab31d8622b6e', // Tom
  'b1d0e7ea-c08a-41c0-879c-81852c3fdc52', // Julien
  'abc7d2e6-8bfa-4e08-b86e-c38918398f5e', // Adrien
  '2c624794-0bdf-4338-9daa-4c8100de9198', // Tests frontend
];
