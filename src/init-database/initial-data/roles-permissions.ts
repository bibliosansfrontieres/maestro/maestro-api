import { PermissionNameEnum } from 'src/permissions/enums/permission-name.enum';
import { RoleNameEnum } from '../../roles/enums/role-name.enum';

export const rolesPermissions: {
  name: RoleNameEnum;
  permissions: PermissionNameEnum[];
}[] = [
  {
    name: RoleNameEnum.ADMIN,
    permissions: [...Object.values(PermissionNameEnum)],
  },
  {
    name: RoleNameEnum.PROJECT_MANAGER,
    permissions: [
      PermissionNameEnum.READ_USER,
      PermissionNameEnum.READ_GROUP,
      PermissionNameEnum.READ_DEFAULT_CONTEXT,
      PermissionNameEnum.READ_CONTEXT,
      PermissionNameEnum.UPDATE_CONTEXT,
      PermissionNameEnum.READ_APPLICATION,
      PermissionNameEnum.READ_VIRTUAL_MACHINE,
      PermissionNameEnum.SAVE_VIRTUAL_MACHINE,
      PermissionNameEnum.CREATE_VIRTUAL_MACHINE,
      PermissionNameEnum.UPDATE_VIRTUAL_MACHINE,
      PermissionNameEnum.READ_DEVICE,
      PermissionNameEnum.BLINK_DEVICE,
      PermissionNameEnum.READ_FQDN,
      PermissionNameEnum.CREATE_DEPLOY,
      PermissionNameEnum.UPDATE_DEPLOY,
      PermissionNameEnum.READ_DEPLOY,
      PermissionNameEnum.READ_PROJECT,
      PermissionNameEnum.UPDATE_PROJECT,
      PermissionNameEnum.CREATE_PROJECT,
      PermissionNameEnum.READ_LABEL,
    ],
  },
  {
    name: RoleNameEnum.USER,
    permissions: [
      PermissionNameEnum.READ_USER,
      PermissionNameEnum.READ_GROUP,
      PermissionNameEnum.READ_DEFAULT_CONTEXT,
      PermissionNameEnum.READ_CONTEXT,
      PermissionNameEnum.READ_APPLICATION,
      PermissionNameEnum.READ_VIRTUAL_MACHINE,
      PermissionNameEnum.READ_DEVICE,
      PermissionNameEnum.READ_FQDN,
      PermissionNameEnum.READ_DEPLOY,
      PermissionNameEnum.READ_PROJECT,
      PermissionNameEnum.READ_LABEL,
    ],
  },
];
