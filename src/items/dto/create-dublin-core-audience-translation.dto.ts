export class CreateDublinCoreAudienceTranslationDto {
  label: string;
  description: string;
  languageIdentifier: string;
}
