export class CreateDublinCoreLicenseTranslationDto {
  label: string;
  description: string;
  languageIdentifier: string;
}
