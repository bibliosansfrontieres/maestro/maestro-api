export class CreateDublinCoreTypeTranslationDto {
  label: string;
  description: string;
  languageIdentifier: string;
}
