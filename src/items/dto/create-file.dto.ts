export class CreateFileDto {
  name: string;
  path: string;
  size: string;
  duration: string;
  pages: string;
  extension: string;
  mimeType: string;
  updatedAt: string;
}
