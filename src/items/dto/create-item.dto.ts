import { ItemSourceEnum } from '../enums/item-source.enum';
import { CreateDublinCoreItemDto } from './create-dublin-core-item.dto';
import { CreateFileDto } from './create-file.dto';
import { CreateDublinCoreItemTranslationDto } from './create-dublin-core-item-translation.dto';

export class CreateItemDto implements CreateDublinCoreItemDto {
  applicationId: string;
  dublinCoreTypeId: number;

  // CreateItemDto
  id: string | number;
  thumbnail: string;
  createdCountry: string;
  version?: string;
  source: ItemSourceEnum;
  dublinCoreItemTranslations: CreateDublinCoreItemTranslationDto[];

  // CreateFileDto
  file: CreateFileDto;
}
