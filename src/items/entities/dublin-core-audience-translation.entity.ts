import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { DublinCoreAudience } from './dublin-core-audience.entity';
import { CreateDublinCoreAudienceTranslationDto } from '../dto/create-dublin-core-audience-translation.dto';

@Entity()
export class DublinCoreAudienceTranslation {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  label: string;

  @Column()
  description: string;

  @Column()
  languageIdentifier: string;

  @ManyToOne(
    () => DublinCoreAudience,
    (dublinCoreAudience) => dublinCoreAudience.dublinCoreAudienceTranslations,
  )
  dublinCoreAudience: DublinCoreAudience;

  constructor(
    createDublinCoreAudienceTranslationDto: CreateDublinCoreAudienceTranslationDto,
    dublinCoreAudience: DublinCoreAudience,
  ) {
    if (!createDublinCoreAudienceTranslationDto) return;
    this.label = createDublinCoreAudienceTranslationDto.label;
    this.description = createDublinCoreAudienceTranslationDto.description;
    this.dublinCoreAudience = dublinCoreAudience;
  }
}
