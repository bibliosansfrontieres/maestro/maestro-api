import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { DublinCoreItem } from './dublin-core-item.entity';
import { CreateDublinCoreItemTranslationDto } from '../dto/create-dublin-core-item-translation.dto';

@Entity()
export class DublinCoreItemTranslation {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  title: string;

  @Column()
  description: string;

  @Column()
  creator: string;

  @Column()
  publisher: string;

  @Column()
  source: string;

  @Column()
  extent: string;

  @Column()
  subject: string;

  @Column()
  dateCreated: string;

  @Column()
  languageIdentifier: string;

  @ManyToOne(
    () => DublinCoreItem,
    (dublinCoreItem) => dublinCoreItem.dublinCoreItemTranslations,
    { onDelete: 'CASCADE' },
  )
  dublinCoreItem: DublinCoreItem;

  constructor(
    createDublinCoreItemTranslationDto: CreateDublinCoreItemTranslationDto,
  ) {
    if (!createDublinCoreItemTranslationDto) return;
    this.title = createDublinCoreItemTranslationDto.title;
    this.description = createDublinCoreItemTranslationDto.description;
    this.creator = createDublinCoreItemTranslationDto.creator;
    this.publisher = createDublinCoreItemTranslationDto.publisher;
    this.source = createDublinCoreItemTranslationDto.source;
    this.extent = createDublinCoreItemTranslationDto.extent;
    this.subject = createDublinCoreItemTranslationDto.subject;
    this.dateCreated = createDublinCoreItemTranslationDto.dateCreated;
    this.languageIdentifier =
      createDublinCoreItemTranslationDto.languageIdentifier;
  }
}
