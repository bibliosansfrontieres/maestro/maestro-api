import {
  Entity,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Item } from 'src/items/entities/item.entity';
import { DublinCoreAudience } from 'src/items/entities/dublin-core-audience.entity';
import { DublinCoreLicense } from 'src/items/entities/dublin-core-license.entity';
import { DublinCoreLanguage } from 'src/items/entities/dublin-core-language.entity';
import { DublinCoreType } from 'src/items/entities/dublin-core-type.entity';
import { DublinCoreItemTranslation } from './dublin-core-item-translation.entity';
import { CreateDublinCoreItemDto } from '../dto/create-dublin-core-item.dto';

@Entity()
export class DublinCoreItem {
  @PrimaryGeneratedColumn()
  id: number;

  @OneToMany(
    () => DublinCoreLanguage,
    (dublinCoreLanguage) => dublinCoreLanguage.dublinCoreItem,
  )
  dublinCoreLanguages: DublinCoreLanguage[];

  @ManyToMany(
    () => DublinCoreLicense,
    (dublinCoreLicense) => dublinCoreLicense.dublinCoreItems,
  )
  @JoinTable()
  dublinCoreLicenses: DublinCoreLicense[];

  @ManyToMany(
    () => DublinCoreAudience,
    (dublinCoreAudience) => dublinCoreAudience.dublinCoreItems,
  )
  @JoinTable()
  dublinCoreAudiences: DublinCoreAudience[];

  @ManyToOne(
    () => DublinCoreType,
    (dublinCoreType) => dublinCoreType.dublinCoreItems,
  )
  dublinCoreType: DublinCoreType;

  @OneToMany(
    () => DublinCoreItemTranslation,
    (dublinCoreItemTranslation) => dublinCoreItemTranslation.dublinCoreItem,
    { cascade: true },
  )
  dublinCoreItemTranslations: DublinCoreItemTranslation[];

  @OneToOne(() => Item, (item) => item.dublinCoreItem, { onDelete: 'CASCADE' })
  item: Item;

  constructor(
    createDublinCoreItemDto: CreateDublinCoreItemDto,
    dublinCoreType: DublinCoreType,
  ) {
    if (!createDublinCoreItemDto) return;
    this.dublinCoreType = dublinCoreType;
  }
}
