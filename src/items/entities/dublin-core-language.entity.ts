import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { DublinCoreItem } from 'src/items/entities/dublin-core-item.entity';
import { DublinCoreLanguageTypeEnum } from '../enums/dublin-core-language-type.enum';

@Entity()
export class DublinCoreLanguage {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  type: DublinCoreLanguageTypeEnum;

  @Column()
  languageIdentifier: string;

  @ManyToOne(
    () => DublinCoreItem,
    (dublinCoreItem) => dublinCoreItem.dublinCoreLanguages,
    { onDelete: 'CASCADE' },
  )
  dublinCoreItem: DublinCoreItem;
  constructor(
    dublinCoreItem: DublinCoreItem,
    type: DublinCoreLanguageTypeEnum,
  ) {
    if (!dublinCoreItem || !type) return;
    this.dublinCoreItem = dublinCoreItem;
    this.type = type;
  }
}
