import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { DublinCoreLicense } from './dublin-core-license.entity';
import { CreateDublinCoreLicenseTranslationDto } from '../dto/create-dublin-core-license-translation.dto';

@Entity()
export class DublinCoreLicenseTranslation {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  label: string;

  @Column()
  description: string;

  @Column()
  languageIdentifier: string;

  @ManyToOne(
    () => DublinCoreLicense,
    (dublinCoreLicense) => dublinCoreLicense.dublinCoreLicenseTranslations,
  )
  dublinCoreLicense: DublinCoreLicense;

  constructor(
    createDublinCoreLicenseTranslationDto: CreateDublinCoreLicenseTranslationDto,
    dublinCoreLicense: DublinCoreLicense,
  ) {
    if (!createDublinCoreLicenseTranslationDto) return;
    this.label = createDublinCoreLicenseTranslationDto.label;
    this.description = createDublinCoreLicenseTranslationDto.description;
    this.dublinCoreLicense = dublinCoreLicense;
  }
}
