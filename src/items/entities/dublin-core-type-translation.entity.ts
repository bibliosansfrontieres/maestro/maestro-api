import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { DublinCoreType } from './dublin-core-type.entity';
import { CreateDublinCoreTypeTranslationDto } from '../dto/create-dublin-core-type-translation.dto';

@Entity()
export class DublinCoreTypeTranslation {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  label: string;

  @Column()
  description: string;

  @Column()
  languageIdentifier: string;

  @ManyToOne(
    () => DublinCoreType,
    (dublinCoreType) => dublinCoreType.dublinCoreTypeTranslations,
  )
  dublinCoreType: DublinCoreType;

  constructor(
    createDublinCoreTypeTranslationDto: CreateDublinCoreTypeTranslationDto,
  ) {
    if (!createDublinCoreTypeTranslationDto) return;
    this.label = createDublinCoreTypeTranslationDto.label;
    this.description = createDublinCoreTypeTranslationDto.description;
    this.languageIdentifier =
      createDublinCoreTypeTranslationDto.languageIdentifier;
  }
}
