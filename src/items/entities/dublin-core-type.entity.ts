import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { DublinCoreTypeTranslation } from './dublin-core-type-translation.entity';
import { DublinCoreItem } from 'src/items/entities/dublin-core-item.entity';
import { CreateDublinCoreTypeDto } from '../dto/create-dublin-core-type.dto';

@Entity()
export class DublinCoreType {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  image: string;

  @OneToMany(
    () => DublinCoreItem,
    (dublinCoreItem) => dublinCoreItem.dublinCoreType,
  )
  dublinCoreItems: DublinCoreItem[];

  @OneToMany(
    () => DublinCoreTypeTranslation,
    (dublinCoreTypeTranslation) => dublinCoreTypeTranslation.dublinCoreType,
    { cascade: true },
  )
  dublinCoreTypeTranslations: DublinCoreTypeTranslation[];

  constructor(createDublinCoreTypeDto: CreateDublinCoreTypeDto) {
    if (!createDublinCoreTypeDto) return;
    this.image = createDublinCoreTypeDto.image;
  }
}
