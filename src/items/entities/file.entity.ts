import { Item } from 'src/items/entities/item.entity';
import { Column, Entity, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { CreateFileDto } from '../dto/create-file.dto';

@Entity()
export class File {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  path: string;

  @Column()
  size: string;

  @Column()
  duration: string;

  @Column()
  pages: string;

  @Column()
  extension: string;

  @Column()
  mimeType: string;

  @Column()
  updatedAt: string;

  @OneToOne(() => Item, (item) => item.file, { onDelete: 'CASCADE' })
  item: Item;

  constructor(createFileDto: CreateFileDto) {
    if (!createFileDto) return;
    this.name = createFileDto.name;
    this.path = createFileDto.path;
    this.size = createFileDto.size;
    this.duration = createFileDto.duration;
    this.pages = createFileDto.pages;
    this.extension = createFileDto.extension;
    this.mimeType = createFileDto.mimeType;
    this.updatedAt = createFileDto.updatedAt;
  }
}
