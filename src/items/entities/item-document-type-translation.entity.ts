import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { ItemDocumentType } from './item-document-type.entity';

@Entity()
export class ItemDocumentTypeTranslation {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  label: string;

  @Column()
  description: string;

  @Column()
  languageIdentifier: string;

  @ManyToOne(
    () => ItemDocumentType,
    (itemDocumentType) => itemDocumentType.itemDocumentTypeTranslations,
  )
  itemDocumentType: ItemDocumentType;
}
