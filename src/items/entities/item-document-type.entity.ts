import { Item } from 'src/items/entities/item.entity';
import { Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { ItemDocumentTypeTranslation } from './item-document-type-translation.entity';

@Entity()
export class ItemDocumentType {
  @PrimaryGeneratedColumn()
  id: number;

  @OneToMany(() => Item, (item) => item.itemDocumentType)
  items: Item[];

  @OneToMany(
    () => ItemDocumentTypeTranslation,
    (itemDocumentTypeTranslation) =>
      itemDocumentTypeTranslation.itemDocumentType,
  )
  itemDocumentTypeTranslations: ItemDocumentTypeTranslation[];
}
