import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { ItemLanguageLevel } from './item-language-level.entity';

@Entity()
export class ItemLanguageLevelTranslation {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  label: string;

  @Column()
  description: string;

  @Column()
  languageIdentifier: string;

  @ManyToOne(
    () => ItemLanguageLevel,
    (itemLanguageLevel) => itemLanguageLevel.itemLanguageLevelTranslations,
  )
  itemLanguageLevel: ItemLanguageLevel;
}
