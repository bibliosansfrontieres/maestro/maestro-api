import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToMany,
  ManyToOne,
  OneToOne,
  PrimaryColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Playlist } from 'src/playlists/entities/playlist.entity';
import { File } from 'src/items/entities/file.entity';
import { Application } from 'src/applications/entities/application.entity';
import { CreateItemDto } from '../dto/create-item.dto';
import { DublinCoreItem } from 'src/items/entities/dublin-core-item.entity';
import { ItemLanguageLevel } from 'src/items/entities/item-language-level.entity';
import { ItemDocumentType } from 'src/items/entities/item-document-type.entity';
import { ItemSourceEnum } from '../enums/item-source.enum';

@Entity()
export class Item {
  @PrimaryColumn()
  id: string;

  @Column({ nullable: true })
  thumbnail: string | null;

  @Column()
  createdCountry: string;

  @Column({ nullable: true })
  version: string | null;

  @Column()
  source: ItemSourceEnum;

  @OneToOne(() => DublinCoreItem, (dublinCoreItem) => dublinCoreItem.item, {
    cascade: true,
  })
  @JoinColumn()
  dublinCoreItem: DublinCoreItem;

  @ManyToOne(
    () => ItemDocumentType,
    (itemDocumentType) => itemDocumentType.items,
  )
  itemDocumentType: ItemDocumentType;

  @ManyToOne(
    () => ItemLanguageLevel,
    (itemLanguageLevel) => itemLanguageLevel.items,
  )
  itemLanguageLevel: ItemLanguageLevel;

  @ManyToMany(() => Playlist, (playlist) => playlist.items)
  playlists: Playlist[];

  @OneToOne(() => File, (file) => file.item, { cascade: true })
  @JoinColumn()
  file: File;

  @ManyToOne(() => Application, (application) => application.items)
  application: Application;

  @CreateDateColumn()
  createdAt: string;

  @UpdateDateColumn()
  updatedAt: string;

  constructor(
    createItemDto: CreateItemDto,
    dublinCoreItem: DublinCoreItem,
    file: File,
    application: Application,
  ) {
    if (!createItemDto) return;
    this.id = `${createItemDto.id}`;
    this.thumbnail = createItemDto.thumbnail;
    this.createdCountry = createItemDto.createdCountry;
    this.version = createItemDto.version;
    this.source = createItemDto.source;
    this.dublinCoreItem = dublinCoreItem;
    this.file = file;
    this.application = application;
  }
}
