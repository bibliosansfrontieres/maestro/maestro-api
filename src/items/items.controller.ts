import { Controller } from '@nestjs/common';
import { ItemsService } from './items.service';
import { ApplicationsService } from 'src/applications/applications.service';

@Controller('items')
export class ItemsController {
  constructor(
    private readonly itemsService: ItemsService,
    private applicationsService: ApplicationsService,
  ) {}
}
