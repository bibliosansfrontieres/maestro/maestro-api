import { Module, forwardRef } from '@nestjs/common';
import { ItemsService } from './items.service';
import { ItemsController } from './items.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Item } from './entities/item.entity';
import { ItemLanguageLevel } from './entities/item-language-level.entity';
import { ItemLanguageLevelTranslation } from './entities/item-language-level-translation.entity';
import { ItemDocumentType } from './entities/item-document-type.entity';
import { ItemDocumentTypeTranslation } from './entities/item-document-type-translation.entity';
import { File } from './entities/file.entity';
import { DublinCoreType } from './entities/dublin-core-type.entity';
import { DublinCoreTypeTranslation } from './entities/dublin-core-type-translation.entity';
import { DublinCoreLicense } from './entities/dublin-core-license.entity';
import { DublinCoreLicenseTranslation } from './entities/dublin-core-license-translation.entity';
import { DublinCoreLanguage } from './entities/dublin-core-language.entity';
import { DublinCoreItem } from './entities/dublin-core-item.entity';
import { DublinCoreItemTranslation } from './entities/dublin-core-item-translation.entity';
import { DublinCoreAudience } from './entities/dublin-core-audience.entity';
import { DublinCoreAudienceTranslation } from './entities/dublin-core-audience-translation.entity';
import { ApplicationsModule } from 'src/applications/applications.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Item,
      ItemLanguageLevel,
      ItemLanguageLevelTranslation,
      ItemDocumentType,
      ItemDocumentTypeTranslation,
      File,
      DublinCoreType,
      DublinCoreTypeTranslation,
      DublinCoreLicense,
      DublinCoreLicenseTranslation,
      DublinCoreLanguage,
      DublinCoreItem,
      DublinCoreItemTranslation,
      DublinCoreAudience,
      DublinCoreAudienceTranslation,
    ]),
    forwardRef(() => ApplicationsModule),
  ],
  controllers: [ItemsController],
  providers: [ItemsService],
  exports: [ItemsService],
})
export class ItemsModule {}
