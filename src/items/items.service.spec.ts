import { Test, TestingModule } from '@nestjs/testing';
import { ItemsService } from './items.service';
import { Repository } from 'typeorm';
import { Item } from './entities/item.entity';
import { DublinCoreItem } from './entities/dublin-core-item.entity';
import { DublinCoreType } from './entities/dublin-core-type.entity';
import { getRepositoryToken } from '@nestjs/typeorm';
import { DublinCoreItemTranslation } from './entities/dublin-core-item-translation.entity';
import { ApplicationsService } from 'src/applications/applications.service';
import { applicationsServiceMock } from 'src/utils/mocks/services/applications-service.mock';
import { File } from './entities/file.entity';

describe('ItemsService', () => {
  let service: ItemsService;
  let itemRepository: Repository<Item>;
  let dublinCoreItemRepository: Repository<DublinCoreItem>;
  let dublinCoreItemTranslationRepository: Repository<DublinCoreItemTranslation>;
  let dublinCoreTypeRepository: Repository<DublinCoreType>;
  let fileRepository: Repository<File>;

  const ITEM_REPOSITORY_TOKEN = getRepositoryToken(Item);
  const DUBLIN_CORE_ITEM_REPOSITORY_TOKEN = getRepositoryToken(DublinCoreItem);
  const DUBLIN_CORE_ITEM_TRANSLATION_REPOSITORY_TOKEN = getRepositoryToken(
    DublinCoreItemTranslation,
  );
  const DUBLIN_CORE_TYPE_REPOSITORY_TOKEN = getRepositoryToken(DublinCoreType);
  const FILE_REPOSITORY_TOKEN = getRepositoryToken(File);

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ItemsService,
        ApplicationsService,
        {
          provide: ITEM_REPOSITORY_TOKEN,
          useClass: Repository<Item>,
        },
        {
          provide: DUBLIN_CORE_ITEM_REPOSITORY_TOKEN,
          useClass: Repository<DublinCoreItem>,
        },
        {
          provide: DUBLIN_CORE_ITEM_TRANSLATION_REPOSITORY_TOKEN,
          useClass: Repository<DublinCoreItemTranslation>,
        },
        {
          provide: DUBLIN_CORE_TYPE_REPOSITORY_TOKEN,
          useClass: Repository<DublinCoreType>,
        },
        {
          provide: FILE_REPOSITORY_TOKEN,
          useClass: Repository<File>,
        },
      ],
    })
      .overrideProvider(ApplicationsService)
      .useValue(applicationsServiceMock)
      .compile();

    service = module.get<ItemsService>(ItemsService);
    itemRepository = module.get<Repository<Item>>(ITEM_REPOSITORY_TOKEN);
    dublinCoreItemRepository = module.get<Repository<DublinCoreItem>>(
      DUBLIN_CORE_ITEM_REPOSITORY_TOKEN,
    );
    dublinCoreItemTranslationRepository = module.get<
      Repository<DublinCoreItemTranslation>
    >(DUBLIN_CORE_ITEM_TRANSLATION_REPOSITORY_TOKEN);
    dublinCoreTypeRepository = module.get<Repository<DublinCoreType>>(
      DUBLIN_CORE_TYPE_REPOSITORY_TOKEN,
    );
    fileRepository = module.get<Repository<File>>(FILE_REPOSITORY_TOKEN);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
    expect(itemRepository).toBeDefined();
    expect(dublinCoreItemRepository).toBeDefined();
    expect(dublinCoreItemTranslationRepository).toBeDefined();
    expect(dublinCoreTypeRepository).toBeDefined();
    expect(fileRepository).toBeDefined();
  });
});
