import { Inject, Injectable, forwardRef } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Item } from './entities/item.entity';
import { FindManyOptions, FindOneOptions, Repository } from 'typeorm';
import { DublinCoreItem } from './entities/dublin-core-item.entity';
import { CreateItemDto } from './dto/create-item.dto';
import { File } from './entities/file.entity';
import { DublinCoreType } from './entities/dublin-core-type.entity';
import { CreateDublinCoreTypeDto } from './dto/create-dublin-core-type.dto';
import { DublinCoreTypeTranslation } from './entities/dublin-core-type-translation.entity';
import { CreateDublinCoreTypeTranslationDto } from './dto/create-dublin-core-type-translation.dto';
import { DublinCoreItemTranslation } from './entities/dublin-core-item-translation.entity';
import { ApplicationsService } from 'src/applications/applications.service';

@Injectable()
export class ItemsService {
  constructor(
    @InjectRepository(Item) private itemRepository: Repository<Item>,
    @InjectRepository(File)
    private fileRepository: Repository<File>,
    @InjectRepository(DublinCoreItemTranslation)
    private dublinCoreItemTranslationRepository: Repository<DublinCoreItemTranslation>,
    @InjectRepository(DublinCoreType)
    private dublinCoreTypeRepository: Repository<DublinCoreType>,
    @Inject(forwardRef(() => ApplicationsService))
    private applicationsService: ApplicationsService,
  ) {}

  count(options?: FindManyOptions<Item>) {
    return this.itemRepository.count(options);
  }

  async create(createItemDto: CreateItemDto) {
    const file = new File(createItemDto.file);
    const dublinCoreType = await this.dublinCoreTypeRepository.findOne({
      where: { id: createItemDto.dublinCoreTypeId },
    });
    const dublinCoreItem = new DublinCoreItem(createItemDto, dublinCoreType);
    dublinCoreItem.dublinCoreItemTranslations = [];
    for (const dublinCoreItemTranslation of createItemDto.dublinCoreItemTranslations) {
      dublinCoreItem.dublinCoreItemTranslations.push(
        new DublinCoreItemTranslation(dublinCoreItemTranslation),
      );
    }
    const application = await this.applicationsService.findOne({
      where: { id: createItemDto.applicationId },
    });
    const item = new Item(createItemDto, dublinCoreItem, file, application);
    return this.itemRepository.save(item);
  }

  async createOrUpdate(createItemDto: CreateItemDto) {
    const item = await this.itemRepository.findOne({
      where: { id: createItemDto.id.toString() },
      relations: {
        file: true,
        dublinCoreItem: { dublinCoreItemTranslations: true },
      },
    });
    if (!item) {
      return this.create(createItemDto);
    } else {
      return this.update(item, createItemDto);
    }
  }

  createQueryBuilder() {
    return this.itemRepository.createQueryBuilder('item');
  }

  async update(item: Item, createItemDto: CreateItemDto) {
    item.file.duration = createItemDto.file.duration;
    item.file.size = createItemDto.file.size;
    item.file.mimeType = createItemDto.file.mimeType;
    item.file.extension = createItemDto.file.extension;
    item.file.pages = createItemDto.file.pages;
    item.file.path = createItemDto.file.path;
    item.file.name = createItemDto.file.name;
    item.file.updatedAt = createItemDto.file.updatedAt;
    item.version = createItemDto.version;

    const dublinCoreType = await this.findOneDublinCoreType({
      where: { id: createItemDto.dublinCoreTypeId },
    });
    item.dublinCoreItem.dublinCoreType = dublinCoreType;

    for (const dublinCoreItemTranslation of item.dublinCoreItem
      .dublinCoreItemTranslations) {
      const dublinCoreItemTranslationDto =
        createItemDto.dublinCoreItemTranslations.find(
          (dublinCoreItemTranslationDto) =>
            dublinCoreItemTranslationDto.languageIdentifier ===
            dublinCoreItemTranslation.languageIdentifier,
        );
      if (!dublinCoreItemTranslationDto) {
        await this.dublinCoreItemTranslationRepository.remove(
          dublinCoreItemTranslation,
        );
        continue;
      }
      dublinCoreItemTranslation.title = dublinCoreItemTranslationDto.title;
      dublinCoreItemTranslation.description =
        dublinCoreItemTranslationDto.description;
      dublinCoreItemTranslation.creator = dublinCoreItemTranslationDto.creator;
      dublinCoreItemTranslation.publisher =
        dublinCoreItemTranslationDto.publisher;
      dublinCoreItemTranslation.source = dublinCoreItemTranslationDto.source;
      dublinCoreItemTranslation.extent = dublinCoreItemTranslationDto.extent;
      dublinCoreItemTranslation.subject = dublinCoreItemTranslationDto.subject;
      dublinCoreItemTranslation.dateCreated =
        dublinCoreItemTranslationDto.dateCreated;
      dublinCoreItemTranslation.languageIdentifier =
        dublinCoreItemTranslationDto.languageIdentifier;
    }

    for (const dublinCoreItemTranslationDto of createItemDto.dublinCoreItemTranslations) {
      const dublinCoreItemTranslation =
        item.dublinCoreItem.dublinCoreItemTranslations.find(
          (dublinCoreItemTranslation) =>
            dublinCoreItemTranslation.languageIdentifier ===
            dublinCoreItemTranslationDto.languageIdentifier,
        );

      if (!dublinCoreItemTranslation) {
        item.dublinCoreItem.dublinCoreItemTranslations.push(
          new DublinCoreItemTranslation(dublinCoreItemTranslationDto),
        );
      }
    }

    const application = await this.applicationsService.findOne({
      where: { id: createItemDto.applicationId },
    });
    item.application = application;

    item.thumbnail = createItemDto.thumbnail;
    item.createdCountry = createItemDto.createdCountry;
    item.version = createItemDto.version;
    item.source = createItemDto.source;

    return this.itemRepository.save(item);
  }

  find(options?: FindManyOptions<Item>) {
    return this.itemRepository.find(options);
  }

  findOneDublinCoreType(options?: FindOneOptions<DublinCoreType>) {
    return this.dublinCoreTypeRepository.findOne(options);
  }

  findDublinCoreType(options?: FindManyOptions<DublinCoreType>) {
    return this.dublinCoreTypeRepository.find(options);
  }

  async createDublinCoreType(
    createDublinCoreTypeDto: CreateDublinCoreTypeDto,
    createDublinCoreTypeTranslationDtos: CreateDublinCoreTypeTranslationDto[],
  ) {
    const dublinCoreType = new DublinCoreType(createDublinCoreTypeDto);
    const dublinCoreTypeTranslations = [];
    for (const createDublinCoreTypeTranslationDto of createDublinCoreTypeTranslationDtos) {
      dublinCoreTypeTranslations.push(
        new DublinCoreTypeTranslation(createDublinCoreTypeTranslationDto),
      );
    }
    dublinCoreType.dublinCoreTypeTranslations = dublinCoreTypeTranslations;
    await this.dublinCoreTypeRepository.save(dublinCoreType);
  }

  save(item: Item) {
    return this.itemRepository.save(item);
  }

  saveFile(file: File) {
    return this.fileRepository.save(file);
  }
}
