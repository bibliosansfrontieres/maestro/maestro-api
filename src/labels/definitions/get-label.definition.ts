import { ApiProperty } from '@nestjs/swagger';
import { Label } from '../entities/label.entity';
import { Deploy } from 'src/deploys/entities/deploy.entity';

export class GetLabelResponse {
  @ApiProperty({ example: 1 })
  id: number;

  @ApiProperty({ example: 'My label' })
  name: string;

  @ApiProperty()
  deploys: Deploy[];

  @ApiProperty({ example: '2022-01-01T00:00:00.000Z' })
  createdAt: string;

  @ApiProperty({ example: '2022-01-01T00:00:00.000Z' })
  updatedAt: string;

  constructor(label: Label) {
    this.id = label.id;
    this.name = label.name;
    this.deploys = label.deploys;
    this.createdAt = label.createdAt;
    this.updatedAt = label.updatedAt;
  }
}
