import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsEnum, IsOptional, IsString } from 'class-validator';
import { PageOptionsRequest } from 'src/utils/pagination/dto/page-options.dto';
import { PageResponse } from 'src/utils/pagination/dto/page-response';
import { Label } from '../entities/label.entity';
import { PageOptions } from 'src/utils/pagination/page-options';
import { LabelOrderByEnum } from '../enums/label-order-by.enum';

export class GetLabelsRequest extends PageOptionsRequest {
  @ApiPropertyOptional({ example: 'search' })
  @IsOptional()
  @IsString()
  query: string;

  @ApiPropertyOptional({ example: '12/30/2021,12/31/2021' })
  @IsOptional()
  @IsString()
  createdDateRange?: string;

  @ApiPropertyOptional({ example: '12/30/2021,12/31/2021' })
  @IsOptional()
  @IsString()
  updatedDateRange?: string;

  @ApiPropertyOptional({
    example: LabelOrderByEnum.ID,
    enum: LabelOrderByEnum,
    default: LabelOrderByEnum.ID,
  })
  @IsOptional()
  @IsEnum(LabelOrderByEnum)
  orderBy?: LabelOrderByEnum;
}

export class GetLabelsResponse extends PageResponse<Label> {
  constructor(data: Label[], count: number, pageOptions: PageOptions) {
    super(data, count, pageOptions);
  }
}
