import { ApiProperty } from '@nestjs/swagger';
import { IsString, MinLength } from 'class-validator';

export class PatchLabelRequest {
  @ApiProperty({ example: 'Mon label' })
  @IsString()
  @MinLength(3)
  name: string;
}
