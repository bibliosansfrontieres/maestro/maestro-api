import { IsString, MinLength } from 'class-validator';

export class PostLabelRequest {
  @IsString()
  @MinLength(3)
  name: string;
}
