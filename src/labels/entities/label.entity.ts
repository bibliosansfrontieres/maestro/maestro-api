import { Deploy } from 'src/deploys/entities/deploy.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { PostLabelRequest } from '../definitions/post-label.definition';

@Entity()
export class Label {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @ManyToMany(() => Deploy, (deploy) => deploy.labels)
  deploys: Deploy[];

  @CreateDateColumn()
  createdAt: string;

  @UpdateDateColumn()
  updatedAt: string;

  constructor(postLabelRequest: PostLabelRequest) {
    if (!postLabelRequest) return;
    this.name = postLabelRequest.name;
  }
}
