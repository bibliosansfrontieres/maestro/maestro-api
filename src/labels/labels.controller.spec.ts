import { Test, TestingModule } from '@nestjs/testing';
import { LabelsController } from './labels.controller';
import { LabelsService } from './labels.service';
import { Repository } from 'typeorm';
import { Label } from './entities/label.entity';
import { getRepositoryToken } from '@nestjs/typeorm';
import { LogsService } from 'src/logs/logs.service';
import { logsServiceMock } from 'src/utils/mocks/services/logs-service.mock';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from 'src/users/users.service';
import { EntraService } from 'src/entra/entra.service';
import { jwtServiceMock } from 'src/utils/mocks/services/jwt-service.mock';
import { usersServiceMock } from 'src/utils/mocks/services/users-service.mock';
import { entraServiceMock } from 'src/utils/mocks/services/entra-service.mock';
import { ConfigService } from '@nestjs/config';
import { configServiceMock } from 'src/utils/mocks/services/config-service.mock';

describe('LabelsController', () => {
  let controller: LabelsController;
  let labelRepository: Repository<Label>;

  const LABEL_REPOSITORY_TOKEN = getRepositoryToken(Label);

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [LabelsController],
      providers: [
        LabelsService,
        LogsService,
        {
          provide: LABEL_REPOSITORY_TOKEN,
          useClass: Repository<Label>,
        },
        JwtService,
        UsersService,
        EntraService,
        ConfigService,
      ],
    })
      .overrideProvider(LogsService)
      .useValue(logsServiceMock)
      .overrideProvider(JwtService)
      .useValue(jwtServiceMock)
      .overrideProvider(UsersService)
      .useValue(usersServiceMock)
      .overrideProvider(EntraService)
      .useValue(entraServiceMock)
      .overrideProvider(ConfigService)
      .useValue(configServiceMock)
      .compile();

    controller = module.get<LabelsController>(LabelsController);
    labelRepository = module.get<Repository<Label>>(LABEL_REPOSITORY_TOKEN);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
    expect(labelRepository).toBeDefined();
  });
});
