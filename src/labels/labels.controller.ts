import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Query,
} from '@nestjs/common';
import { LabelsService } from './labels.service';
import { UserId } from 'src/auth/decorators/user-id.decorator';
import { PostLabelRequest } from './definitions/post-label.definition';
import {
  GetLabelsRequest,
  GetLabelsResponse,
} from './definitions/get-labels.definition';
import {
  ApiBadRequestResponse,
  ApiNotFoundResponse,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { GetLabelResponse } from './definitions/get-label.definition';
import { PatchLabelRequest } from './definitions/patch-label.definition';
import { PermissionNameEnum } from 'src/permissions/enums/permission-name.enum';
import { Auth } from 'src/auth/decorators/auth.decorator';

@ApiTags('labels')
@Controller('labels')
export class LabelsController {
  constructor(private readonly labelsService: LabelsService) {}

  @Post()
  @ApiOperation({ description: 'Create a label' })
  @ApiResponse({ type: GetLabelResponse })
  @ApiBadRequestResponse({ description: 'Invalid request body parameters' })
  postLabel(
    @UserId() userId: string,
    @Body() postLabelRequest: PostLabelRequest,
  ) {
    return this.labelsService.create(userId, postLabelRequest);
  }

  @Get(':id')
  @ApiOperation({ description: 'Get a label' })
  @ApiResponse({ type: GetLabelResponse })
  @ApiNotFoundResponse({ description: 'Label not found' })
  getLabel(@Param('id') id: string) {
    return this.labelsService.getLabel(+id);
  }

  @Get()
  @ApiOperation({ description: 'Get all labels' })
  @ApiResponse({ type: GetLabelsResponse })
  @ApiBadRequestResponse({ description: 'Invalid request body parameters' })
  getLabels(@Query() getLabelsRequest: GetLabelsRequest) {
    return this.labelsService.getLabels(getLabelsRequest);
  }

  @Patch(':id')
  @Auth(PermissionNameEnum.UPDATE_LABEL)
  @ApiOperation({ description: 'Update a label' })
  @ApiResponse({ type: GetLabelResponse })
  @ApiBadRequestResponse({ description: 'Invalid request body parameters' })
  patchLabel(
    @UserId() userId: string,
    @Param('id') id: string,
    @Body() patchLabelRequest: PatchLabelRequest,
  ) {
    return this.labelsService.patchLabel(userId, +id, patchLabelRequest);
  }

  @Delete(':id')
  @Auth(PermissionNameEnum.DELETE_LABEL)
  @ApiOperation({ description: 'Delete a label' })
  @ApiNotFoundResponse({ description: 'label not found' })
  deleteLabel(@UserId() userId: string, @Param('id') id: string) {
    return this.labelsService.deleteLabel(userId, +id);
  }
}
