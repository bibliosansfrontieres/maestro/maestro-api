import { Module } from '@nestjs/common';
import { LabelsService } from './labels.service';
import { LabelsController } from './labels.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Label } from './entities/label.entity';
import { LogsModule } from 'src/logs/logs.module';
import { JwtModule } from '@nestjs/jwt';
import { UsersModule } from 'src/users/users.module';
import { EntraModule } from 'src/entra/entra.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([Label]),
    LogsModule,
    JwtModule,
    UsersModule,
    EntraModule,
  ],
  controllers: [LabelsController],
  providers: [LabelsService],
  exports: [LabelsService],
})
export class LabelsModule {}
