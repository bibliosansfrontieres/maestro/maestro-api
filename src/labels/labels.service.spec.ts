import { Test, TestingModule } from '@nestjs/testing';
import { LabelsService } from './labels.service';
import { Repository } from 'typeorm';
import { Label } from './entities/label.entity';
import { getRepositoryToken } from '@nestjs/typeorm';
import { LogsService } from 'src/logs/logs.service';
import { logsServiceMock } from 'src/utils/mocks/services/logs-service.mock';

describe('LabelsService', () => {
  let service: LabelsService;
  let labelRepository: Repository<Label>;

  const LABEL_REPOSITORY_TOKEN = getRepositoryToken(Label);

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        LabelsService,
        LogsService,
        {
          provide: LABEL_REPOSITORY_TOKEN,
          useClass: Repository<Label>,
        },
      ],
    })
      .overrideProvider(LogsService)
      .useValue(logsServiceMock)
      .compile();

    service = module.get<LabelsService>(LabelsService);
    labelRepository = module.get<Repository<Label>>(LABEL_REPOSITORY_TOKEN);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
    expect(labelRepository).toBeDefined();
  });
});
