import {
  BadRequestException,
  ConflictException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Label } from './entities/label.entity';
import { FindManyOptions, FindOneOptions, Repository } from 'typeorm';
import { LogsService } from 'src/logs/logs.service';
import { LogEntityEnum } from 'src/logs/enums/log-entity.enum';
import { LogActionEnum } from 'src/logs/enums/log-action.enum';
import { PostLabelRequest } from './definitions/post-label.definition';
import {
  GetLabelsRequest,
  GetLabelsResponse,
} from './definitions/get-labels.definition';
import { getManyAndCount } from 'src/utils/get-many-and-count.util';
import { PageOptions } from 'src/utils/pagination/page-options';
import { LabelOrderByEnum } from './enums/label-order-by.enum';
import { QueryHelper } from 'src/utils/query-helpers.util';
import { GetLabelResponse } from './definitions/get-label.definition';
import { PatchLabelRequest } from './definitions/patch-label.definition';

@Injectable()
export class LabelsService {
  constructor(
    @InjectRepository(Label) private labelRepository: Repository<Label>,
    private logsService: LogsService,
  ) {}

  async create(authorId: string, postLabelRequest: PostLabelRequest) {
    let label = new Label(postLabelRequest);
    label = await this.labelRepository.save(label);
    await this.logsService.create({
      authorId,
      action: LogActionEnum.CREATE,
      entity: LogEntityEnum.LABEL,
      entityId: label.id,
      values: postLabelRequest,
    });
    return new GetLabelResponse(label);
  }

  async getLabel(id: number) {
    const label = await this.findOne({
      where: { id },
      relations: { deploys: true },
    });

    if (!label) {
      throw new NotFoundException('label not found');
    }

    return new GetLabelResponse(label);
  }

  async getLabels(getLabelsRequest: GetLabelsRequest) {
    const pageOptions = new PageOptions(getLabelsRequest);
    const labelsQuery = this.labelRepository
      .createQueryBuilder('label')
      .leftJoinAndSelect('label.deploys', 'deploy');

    let { orderBy } = getLabelsRequest;

    orderBy = orderBy ? orderBy : LabelOrderByEnum.UPDATED_AT;

    const queryHelper = new QueryHelper(labelsQuery);
    queryHelper.pagination(pageOptions, orderBy);

    if (getLabelsRequest.createdDateRange) {
      queryHelper.andWhereDateRange(
        'label.createdAt',
        getLabelsRequest.createdDateRange,
      );
    }

    if (getLabelsRequest.updatedDateRange) {
      queryHelper.andWhereDateRange(
        'label.updatedAt',
        getLabelsRequest.updatedDateRange,
      );
    }

    queryHelper.andWhereQuery(getLabelsRequest.query, ['label.name']);

    const [count, labels] = await getManyAndCount<Label>(
      labelsQuery,
      ['label.id'],
      false,
    );

    return new GetLabelsResponse(labels, count, pageOptions);
  }

  async patchLabel(
    authorId: string,
    id: number,
    patchLabelRequest: PatchLabelRequest,
  ) {
    const label = await this.findOne({ where: { id } });
    if (!label) {
      throw new NotFoundException('label not found');
    }
    if (patchLabelRequest?.name) {
      if (patchLabelRequest.name.length < 3) {
        throw new BadRequestException(
          'name must be longer than or equal to 3 characters',
        );
      }

      const labelNameExists = await this.labelRepository
        .createQueryBuilder('label')
        .where('LOWER(label.name) = :name', {
          name: patchLabelRequest.name.toLowerCase(),
        })
        .getOne();

      if (labelNameExists) {
        throw new ConflictException('label name already exists');
      }
    }
    label.name = patchLabelRequest.name;
    await this.labelRepository.save(label);
    await this.logsService.create({
      authorId,
      action: LogActionEnum.UPDATE,
      entity: LogEntityEnum.LABEL,
      entityId: label.id,
      values: patchLabelRequest,
    });
    return new GetLabelResponse(label);
  }

  async deleteLabel(authorId: string, id: number) {
    const label = await this.findOne({ where: { id } });
    if (label === null) {
      throw new NotFoundException('label not found');
    }

    await this.delete(label);
    await this.logsService.create({
      authorId,
      action: LogActionEnum.DELETE,
      entity: LogEntityEnum.LABEL,
      entityId: label.id,
    });

    return;
  }

  findOne(options?: FindOneOptions<Label>) {
    return this.labelRepository.findOne(options);
  }

  find(options?: FindManyOptions<Label>) {
    return this.labelRepository.find(options);
  }

  delete(label: Label) {
    return this.labelRepository.remove(label);
  }
}
