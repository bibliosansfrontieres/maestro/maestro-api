import { IsEnum, IsOptional, IsString } from 'class-validator';
import { PageOptionsRequest } from 'src/utils/pagination/dto/page-options.dto';
import { LogOrderByEnum } from '../enums/log-order-by.enum';
import { PageResponse } from 'src/utils/pagination/dto/page-response';
import { Log } from '../entities/log.entity';
import { PageOptions } from 'src/utils/pagination/page-options';
import { ApiPropertyOptional } from '@nestjs/swagger';
import { LogLevelEnum } from '../enums/log-level.enum';
import { LogEntityEnum } from '../enums/log-entity.enum';
import { LogActionEnum } from '../enums/log-action.enum';

export class GetLogsRequest extends PageOptionsRequest {
  @IsOptional()
  @IsString()
  query?: string;

  @ApiPropertyOptional({
    example: LogOrderByEnum.ID,
    default: LogOrderByEnum.CREATED_AT,
    enum: LogOrderByEnum,
  })
  @IsOptional()
  @IsEnum(LogOrderByEnum)
  orderBy?: LogOrderByEnum;

  @ApiPropertyOptional({
    example: `${LogActionEnum.CREATE},${LogActionEnum.DELETE}`,
  })
  @IsOptional()
  @IsString()
  actions?: string;

  @ApiPropertyOptional({
    example: `${LogEntityEnum.APPLICATION},${LogEntityEnum.DEPLOY}`,
  })
  @IsOptional()
  @IsString()
  entities?: string;

  @ApiPropertyOptional({
    example: `${LogLevelEnum.DEBUG},${LogLevelEnum.ERROR}`,
  })
  @IsOptional()
  @IsString()
  levels?: string;

  @ApiPropertyOptional({
    example:
      '123e4567-e89b-12d3-a456-426655440000,123e4567-e89b-12d3-a456-426655440000',
    default: undefined,
  })
  @IsOptional()
  @IsString()
  userIds?: string;

  @ApiPropertyOptional({ example: '01/25/2024,02/25/2024', default: undefined })
  @IsOptional()
  @IsString()
  dateRange?: string;
}

export class GetLogsResponse extends PageResponse<Log> {
  constructor(data: Log[], count: number, pageOptions: PageOptions) {
    super(data, count, pageOptions);
  }
}
