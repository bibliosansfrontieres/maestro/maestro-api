import { LogActionEnum } from '../enums/log-action.enum';
import { LogEntityEnum } from '../enums/log-entity.enum';
import { LogLevelEnum } from '../enums/log-level.enum';

export class CreateLogDto {
  authorId: string;
  action: LogActionEnum;
  entity?: LogEntityEnum;
  entityId?: string | number;
  values?: object;
  description?: string;
  logLevel?: LogLevelEnum;
}
