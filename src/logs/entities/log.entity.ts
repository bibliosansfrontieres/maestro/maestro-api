import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { LogLevelEnum } from '../enums/log-level.enum';
import { LogActionEnum } from '../enums/log-action.enum';
import { LogEntityEnum } from '../enums/log-entity.enum';
import { CreateLogDto } from '../dto/create-log.dto';
import { User } from 'src/users/entities/user.entity';

@Entity()
export class Log {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  action: LogActionEnum;

  @Column({ nullable: true })
  entity: LogEntityEnum | null;

  @Column({ nullable: true })
  entityId: string | null;

  @Column({ nullable: true })
  values: string | null;

  @Column({ nullable: true })
  description: string | null;

  @Column()
  level: LogLevelEnum;

  @CreateDateColumn()
  createdAt: string;

  @ManyToOne(() => User, (user) => user.logs)
  user: User;

  constructor(createLogDto: CreateLogDto, user: User) {
    if (!createLogDto || !user) return;
    this.user = user;
    this.action = createLogDto.action;
    this.entity = createLogDto.entity;
    if (createLogDto.entityId) {
      if (typeof createLogDto.entityId === 'string') {
        this.entityId = createLogDto.entityId;
      } else {
        this.entityId = createLogDto.entityId.toString();
      }
    }
    if (createLogDto.values) {
      this.values = JSON.stringify(createLogDto.values);
    }
    this.description = createLogDto.description;
    this.level = createLogDto.logLevel || LogLevelEnum.INFO;
  }
}
