import { Test, TestingModule } from '@nestjs/testing';
import { LogsController } from './logs.controller';
import { LogsService } from './logs.service';
import { Log } from './entities/log.entity';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UsersService } from 'src/users/users.service';
import { usersServiceMock } from 'src/utils/mocks/services/users-service.mock';
import { JwtService } from '@nestjs/jwt';
import { EntraService } from 'src/entra/entra.service';
import { jwtServiceMock } from 'src/utils/mocks/services/jwt-service.mock';
import { entraServiceMock } from 'src/utils/mocks/services/entra-service.mock';
import { ConfigService } from '@nestjs/config';
import { configServiceMock } from 'src/utils/mocks/services/config-service.mock';

describe('LogsController', () => {
  let controller: LogsController;
  let logRepository: Repository<Log>;

  const LOG_REPOSITORY_TOKEN = getRepositoryToken(Log);

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [LogsController],
      providers: [
        LogsService,
        UsersService,
        JwtService,
        EntraService,
        ConfigService,
        { provide: LOG_REPOSITORY_TOKEN, useClass: Repository<Log> },
      ],
    })
      .overrideProvider(UsersService)
      .useValue(usersServiceMock)
      .overrideProvider(JwtService)
      .useValue(jwtServiceMock)
      .overrideProvider(EntraService)
      .useValue(entraServiceMock)
      .overrideProvider(ConfigService)
      .useValue(configServiceMock)
      .compile();

    controller = module.get<LogsController>(LogsController);
    logRepository = module.get<Repository<Log>>(LOG_REPOSITORY_TOKEN);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
    expect(logRepository).toBeDefined();
  });
});
