import { Controller, Get, Query } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { LogsService } from './logs.service';
import { GetLogsRequest } from './definitions/get-logs.definition';
import { Auth } from 'src/auth/decorators/auth.decorator';
import { PermissionNameEnum } from 'src/permissions/enums/permission-name.enum';

@Controller('logs')
@ApiTags('logs')
export class LogsController {
  constructor(private readonly logsService: LogsService) {}

  @Get()
  @Auth(PermissionNameEnum.READ_LOGS)
  getLogs(@Query() getLogsRequest: GetLogsRequest) {
    return this.logsService.getLogs(getLogsRequest);
  }
}
