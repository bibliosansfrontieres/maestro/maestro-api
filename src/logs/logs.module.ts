import { Module, forwardRef } from '@nestjs/common';
import { LogsService } from './logs.service';
import { LogsController } from './logs.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Log } from './entities/log.entity';
import { UsersModule } from 'src/users/users.module';
import { JwtModule } from '@nestjs/jwt';
import { EntraModule } from 'src/entra/entra.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([Log]),
    forwardRef(() => UsersModule),
    JwtModule,
    forwardRef(() => EntraModule),
  ],
  controllers: [LogsController],
  providers: [LogsService],
  exports: [LogsService],
})
export class LogsModule {}
