import { Test, TestingModule } from '@nestjs/testing';
import { LogsService } from './logs.service';
import { Repository } from 'typeorm';
import { Log } from './entities/log.entity';
import { getRepositoryToken } from '@nestjs/typeorm';
import { UsersService } from 'src/users/users.service';
import { usersServiceMock } from 'src/utils/mocks/services/users-service.mock';

describe('LogsService', () => {
  let service: LogsService;
  let logRepository: Repository<Log>;

  const LOG_REPOSITORY_TOKEN = getRepositoryToken(Log);

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        LogsService,
        UsersService,
        { provide: LOG_REPOSITORY_TOKEN, useClass: Repository<Log> },
      ],
    })
      .overrideProvider(UsersService)
      .useValue(usersServiceMock)
      .compile();

    service = module.get<LogsService>(LogsService);
    logRepository = module.get<Repository<Log>>(LOG_REPOSITORY_TOKEN);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
    expect(logRepository).toBeDefined();
  });
});
