import {
  Inject,
  Injectable,
  NotFoundException,
  forwardRef,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Log } from './entities/log.entity';
import { QueryRunner, Repository } from 'typeorm';
import { CreateLogDto } from './dto/create-log.dto';
import { UsersService } from 'src/users/users.service';
import {
  GetLogsRequest,
  GetLogsResponse,
} from './definitions/get-logs.definition';
import { PageOptions } from 'src/utils/pagination/page-options';
import { LogOrderByEnum } from './enums/log-order-by.enum';
import { QueryHelper } from 'src/utils/query-helpers.util';
import { getManyAndCount } from 'src/utils/get-many-and-count.util';

@Injectable()
export class LogsService {
  constructor(
    @InjectRepository(Log) private logRepository: Repository<Log>,
    @Inject(forwardRef(() => UsersService)) private usersService: UsersService,
  ) {}

  async create(createLogDto: CreateLogDto) {
    const user = await this.usersService.findOne({
      where: { id: createLogDto.authorId },
    });

    if (!user) {
      throw new NotFoundException('author not found');
    }

    const log = new Log(createLogDto, user);
    return this.logRepository.save(log);
  }

  /*
  @IsOptional()
  @IsString()
  query?: string;

  @ApiPropertyOptional({
    example: LogOrderByEnum.ID,
    default: LogOrderByEnum.CREATED_AT,
    enum: LogOrderByEnum,
  })
  @IsOptional()
  @IsEnum(LogOrderByEnum)
  orderBy?: LogOrderByEnum;

  @ApiPropertyOptional({
    example: `${LogActionEnum.CREATE},${LogActionEnum.DELETE}`,
  })
  @IsOptional()
  @IsString()
  actionIds?: string;

  @ApiPropertyOptional({
    example: `${LogEntityEnum.APPLICATION},${LogEntityEnum.DEPLOY}`,
  })
  @IsOptional()
  @IsString()
  entityIds?: string;

  @ApiPropertyOptional({
    example: `${LogLevelEnum.DEBUG},${LogLevelEnum.ERROR}`,
  })
  @IsOptional()
  @IsString()
  levelIds?: string;

  @ApiPropertyOptional({
    example:
      '123e4567-e89b-12d3-a456-426655440000,123e4567-e89b-12d3-a456-426655440000',
    default: undefined,
  })
  @IsOptional()
  @IsString()
  userIds?: string;
*/
  async getLogs(getLogsRequest: GetLogsRequest) {
    const pageOptions = new PageOptions(getLogsRequest);

    const logsQuery = this.logRepository
      .createQueryBuilder('log')
      .innerJoinAndSelect('log.user', 'user');

    let { orderBy } = getLogsRequest;

    orderBy = orderBy ? orderBy : LogOrderByEnum.CREATED_AT;

    const queryHelper = new QueryHelper(logsQuery);
    queryHelper.pagination(pageOptions, orderBy);

    queryHelper.andWhereQuery(getLogsRequest.query, [
      'log.values',
      'log.description',
    ]);

    if (getLogsRequest.actions) {
      const actions = getLogsRequest.actions.split(',');
      queryHelper.andWhereMultipleEquals('log.action', actions);
    }

    if (getLogsRequest.entities) {
      const entities = getLogsRequest.entities.split(',');
      queryHelper.andWhereMultipleEquals('log.entity', entities);
    }

    if (getLogsRequest.levels) {
      const levels = getLogsRequest.levels.split(',');
      queryHelper.andWhereMultipleEquals('log.level', levels);
    }

    if (getLogsRequest.userIds) {
      const userIds = getLogsRequest.userIds.split(',');
      queryHelper.andWhereMultipleEquals('log.user', userIds);
    }

    queryHelper.andWhereDateRange('log.createdAt', getLogsRequest.dateRange);

    const [count, logs] = await getManyAndCount<Log>(
      logsQuery,
      ['log.id'],
      false,
    );

    return new GetLogsResponse(logs, count, pageOptions);
  }

  getQueryBuilder(alias: string, queryRunner?: QueryRunner) {
    return this.logRepository.createQueryBuilder(alias, queryRunner);
  }
}
