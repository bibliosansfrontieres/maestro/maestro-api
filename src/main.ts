import { NestFactory, Reflector } from '@nestjs/core';
import { AppModule } from './app.module';
import { ConfigService } from '@nestjs/config';
import { ClassSerializerInterceptor, ValidationPipe } from '@nestjs/common';
import {
  FastifyAdapter,
  NestFastifyApplication,
} from '@nestjs/platform-fastify';
import compression from '@fastify/compress';
import helmet from '@fastify/helmet';
import fastifyCsrf from '@fastify/csrf-protection';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

async function bootstrap() {
  const app = await NestFactory.create<NestFastifyApplication>(
    AppModule,
    new FastifyAdapter(),
  );

  // Swagger
  const config = new DocumentBuilder()
    .setTitle('Maestro API')
    .setDescription(
      "Dédicace au chat de ToM, celui d'Alexandre, ceux d'Audrey et bien évidemment le colochat d'Adrien !<br />Et MÊME celui de Julien...",
    )
    .setVersion('1.0')
    .addBearerAuth()
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('swagger', app, document);

  // Validation pipes
  app.useGlobalPipes(new ValidationPipe());
  app.useGlobalInterceptors(new ClassSerializerInterceptor(app.get(Reflector)));

  // Compression
  app.register(compression);

  // Security
  app.enableCors();
  app.register(helmet, { contentSecurityPolicy: false });
  app.register(fastifyCsrf);

  app.enableShutdownHooks();

  // Port configuration
  const configService = app.get(ConfigService);
  const port = configService.get<number>('API_PORT');
  await app.listen(port, '0.0.0.0');
}
bootstrap();
