import { MigrationInterface, QueryRunner } from 'typeorm';

export class RenameCpuSerialId1724771525384 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "device" RENAME COLUMN "cpuSerialId" TO "serialId"`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "device" RENAME COLUMN "serialId" TO "cpuSerialId"`,
    );
  }
}
