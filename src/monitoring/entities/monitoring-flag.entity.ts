import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { MonitoringFlagTypeEnum } from '../enums/monitoring-flag-type.enum';

@Entity()
export class MonitoringFlag {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  type: MonitoringFlagTypeEnum;

  @Column()
  timestamp: number;

  constructor(type: MonitoringFlagTypeEnum) {
    if (!type) return;
    this.type = type;
    this.timestamp = Date.now();
  }
}
