export enum MonitoringFlagTypeEnum {
  MEMORY_USAGE = 'MEMORY_USAGE',
  AVERAGE_LOAD = 'AVERAGE_LOAD',
  DISK_USAGE = 'DISK_USAGE',
}
