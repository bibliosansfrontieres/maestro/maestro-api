import * as si from 'systeminformation';

export interface IMonitoringValues {
  load: si.Systeminformation.CurrentLoadData;
  memory: si.Systeminformation.MemData;
  networkStats: si.Systeminformation.NetworkStatsData[];
  diskLayout: si.Systeminformation.DiskLayoutData[];
  fsSize: si.Systeminformation.FsSizeData[];
  disksIo: si.Systeminformation.DisksIoData;
}
