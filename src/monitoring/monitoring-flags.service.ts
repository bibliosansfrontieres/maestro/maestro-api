import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { MonitoringFlag } from './entities/monitoring-flag.entity';
import { MonitoringFlagTypeEnum } from './enums/monitoring-flag-type.enum';

@Injectable()
export class MonitoringFlagsService {
  constructor(
    @InjectRepository(MonitoringFlag)
    private monitoringFlagRepository: Repository<MonitoringFlag>,
  ) {}

  create(type: MonitoringFlagTypeEnum) {
    const monitoringFlag = new MonitoringFlag(type);
    return this.monitoringFlagRepository.save(monitoringFlag);
  }

  findOne(type: MonitoringFlagTypeEnum) {
    return this.monitoringFlagRepository.findOne({ where: { type } });
  }

  delete(type: MonitoringFlagTypeEnum) {
    return this.monitoringFlagRepository.delete({ type });
  }
}
