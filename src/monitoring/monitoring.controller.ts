import { Controller, Get } from '@nestjs/common';
import { MonitoringService } from './monitoring.service';

@Controller('monitoring')
export class MonitoringController {
  constructor(private readonly monitoringService: MonitoringService) {}

  @Get('load')
  async getLoad() {
    return this.monitoringService.getLoad();
  }

  @Get('memory')
  async getMemory() {
    return this.monitoringService.getMemory();
  }

  @Get('network-stats')
  async getNetworkStats() {
    return this.monitoringService.getNetworkStats();
  }

  @Get('disk-layout')
  async getDiskLayout() {
    return this.monitoringService.getDiskLayout();
  }

  @Get('fs-size')
  async getFsSize() {
    return this.monitoringService.getFsSize();
  }

  @Get('disks-io')
  async getDisksIo() {
    return this.monitoringService.getDisksIo();
  }
}
