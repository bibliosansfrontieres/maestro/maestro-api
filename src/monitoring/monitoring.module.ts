import { Module } from '@nestjs/common';
import { MonitoringService } from './monitoring.service';
import { MonitoringController } from './monitoring.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MonitoringFlag } from './entities/monitoring-flag.entity';
import { MonitoringFlagsService } from './monitoring-flags.service';
import { NotificationsModule } from 'src/notifications/notifications.module';
import { SocketModule } from 'src/socket/socket.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([MonitoringFlag]),
    NotificationsModule,
    SocketModule,
  ],
  controllers: [MonitoringController],
  providers: [MonitoringService, MonitoringFlagsService],
})
export class MonitoringModule {}
