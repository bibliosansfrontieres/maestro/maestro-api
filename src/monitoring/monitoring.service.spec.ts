import { Test, TestingModule } from '@nestjs/testing';
import { MonitoringService } from './monitoring.service';
import { NotificationsService } from 'src/notifications/notifications.service';
import { notificationsServiceMock } from 'src/utils/mocks/services/notifications-service.mock';
import { MonitoringFlagsService } from './monitoring-flags.service';
import { monitoringFlagsServiceMock } from 'src/utils/mocks/services/monitoring-flags-service.mock';
import { ConfigService } from '@nestjs/config';
import { configServiceMock } from 'src/utils/mocks/services/config-service.mock';
import { SocketGateway } from 'src/socket/socket.gateway';
import { socketGatewayMock } from 'src/utils/mocks/gateways/socket-gateway.mock';

describe('MonitoringService', () => {
  let service: MonitoringService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        MonitoringService,
        NotificationsService,
        MonitoringFlagsService,
        ConfigService,
        SocketGateway,
      ],
    })
      .overrideProvider(NotificationsService)
      .useValue(notificationsServiceMock)
      .overrideProvider(MonitoringFlagsService)
      .useValue(monitoringFlagsServiceMock)
      .overrideProvider(ConfigService)
      .useValue(configServiceMock)
      .overrideProvider(SocketGateway)
      .useValue(socketGatewayMock)
      .compile();

    service = module.get<MonitoringService>(MonitoringService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
