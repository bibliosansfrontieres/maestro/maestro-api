import { Injectable } from '@nestjs/common';
import { Interval } from '@nestjs/schedule';
import { NotificationsService } from 'src/notifications/notifications.service';
import * as si from 'systeminformation';
import { MonitoringFlagsService } from './monitoring-flags.service';
import { MonitoringFlagTypeEnum } from './enums/monitoring-flag-type.enum';
import * as prettyMilliseconds from 'pretty-ms';
import { ConfigService } from '@nestjs/config';
import { IMonitoringValues } from './interfaces/monitoring-values.interface';
import { SocketGateway } from 'src/socket/socket.gateway';

@Injectable()
export class MonitoringService {
  private monitoringValues: IMonitoringValues;
  private previousRWaitTime: number;
  private previousWWaitTime: number;
  private previousTWaitTime: number;

  constructor(
    private notificationsService: NotificationsService,
    private monitoringFlagsService: MonitoringFlagsService,
    private configService: ConfigService,
    private socketGateway: SocketGateway,
  ) {}

  @Interval(1000)
  async monitoringInterval() {
    await this.setMonitoringValues();
    await this.monitorLoad();
    await this.monitorMemory();
    await this.monitorDisks();
  }

  private async setMonitoringValues(): Promise<IMonitoringValues> {
    const disksIo = await this.getDisksIo();

    const previousRWaitTime = this.previousRWaitTime || disksIo.rWaitTime;
    const previousWWaitTime = this.previousWWaitTime || disksIo.wWaitTime;
    const previousTWaitTime = this.previousTWaitTime || disksIo.tWaitTime;

    this.previousRWaitTime = disksIo.rWaitTime;
    this.previousWWaitTime = disksIo.wWaitTime;
    this.previousTWaitTime = disksIo.tWaitTime;

    disksIo.rWaitTime = disksIo.rWaitTime - previousRWaitTime;
    disksIo.wWaitTime = disksIo.wWaitTime - previousWWaitTime;
    disksIo.tWaitTime = disksIo.tWaitTime - previousTWaitTime;

    const monitoringValues = {
      load: await this.getLoad(),
      memory: await this.getMemory(),
      networkStats: await this.getNetworkStats(),
      diskLayout: await this.getDiskLayout(),
      fsSize: await this.getFsSize(),
      disksIo,
    };
    this.monitoringValues = monitoringValues;
    this.socketGateway.emitMonitoringValues(monitoringValues);
    return monitoringValues;
  }

  private async monitorDisks() {
    await this.monitor(MonitoringFlagTypeEnum.DISK_USAGE, async () => {
      const maxDiskUsagePercentage = this.configService.get(
        'MONITORING_MAX_DISK_USAGE_PERCENTAGE',
      );
      const fsSize = this.monitoringValues.fsSize;
      const usagePercentage = Math.round(fsSize[0].use);

      const isValid = usagePercentage < maxDiskUsagePercentage;
      const message = `Disk usage: ${usagePercentage}%\nMaximum value: ${maxDiskUsagePercentage}%`;

      return {
        isValid,
        message,
      };
    });
  }

  private async monitorLoad() {
    await this.monitor(MonitoringFlagTypeEnum.AVERAGE_LOAD, async () => {
      const maxAverageLoad = this.configService.get(
        'MONITORING_MAX_AVERAGE_LOAD',
      );
      const { avgLoad } = this.monitoringValues.load;

      const isValid = avgLoad < maxAverageLoad;
      const message = `Average load: ${avgLoad}\nMaximum value: ${maxAverageLoad}`;

      return {
        isValid,
        message,
      };
    });
  }

  private async monitorMemory() {
    await this.monitor(MonitoringFlagTypeEnum.MEMORY_USAGE, async () => {
      const maxMemoryUsagePercentage = this.configService.get(
        'MONITORING_MAX_MEMORY_USAGE_PERCENTAGE',
      );
      const memory = this.monitoringValues.memory;
      const memoryUsagePercentage = Math.round(
        ((memory.total - memory.used) / memory.total) * 100,
      );

      const isValid = memoryUsagePercentage < maxMemoryUsagePercentage;
      const message = `Memory usage: ${memoryUsagePercentage}%\nMaximum value: ${maxMemoryUsagePercentage}%`;

      return {
        isValid,
        message,
      };
    });
  }

  private async monitor(
    type: MonitoringFlagTypeEnum,
    checkValue: () => Promise<{ isValid: boolean; message: string }>,
  ) {
    let flag = await this.monitoringFlagsService.findOne(type);
    const result = await checkValue();

    if (flag) {
      if (result.isValid) {
        await this.monitoringFlagsService.delete(type);
        const duration = Date.now() - flag.timestamp;
        await this.notificationsService.notifyMonitoring(
          `:large_green_circle: *Maestro incident resolved #${flag.id}*\n\nType: *${type}*\nDuration : *${prettyMilliseconds(duration)}*\n\`\`\`${result.message}\`\`\``,
        );
      }
    } else {
      if (!result.isValid) {
        flag = await this.monitoringFlagsService.create(type);
        await this.notificationsService.notifyMonitoring(
          `:red_circle: *Maestro incident started #${flag.id}*\n\nType: *${type}*\nStarted at : *${new Date().toLocaleString()}*\n\`\`\`${result.message}\`\`\``,
        );
      }
    }
  }

  async getLoad(): Promise<si.Systeminformation.CurrentLoadData> {
    return new Promise((resolve) => {
      si.currentLoad((data) => resolve(data));
    });
  }

  async getMemory(): Promise<si.Systeminformation.MemData> {
    return new Promise((resolve) => {
      si.mem((data) => resolve(data));
    });
  }

  async getNetworkStats(): Promise<si.Systeminformation.NetworkStatsData[]> {
    return new Promise((resolve) => {
      si.networkStats(undefined, (data) => resolve(data));
    });
  }

  async getDiskLayout(): Promise<si.Systeminformation.DiskLayoutData[]> {
    return new Promise((resolve) => {
      si.diskLayout((data) => resolve(data));
    });
  }

  async getFsSize(): Promise<si.Systeminformation.FsSizeData[]> {
    return new Promise((resolve) => {
      si.fsSize(undefined, (data) => resolve(data));
    });
  }

  async getDisksIo(): Promise<si.Systeminformation.DisksIoData> {
    return new Promise((resolve) => {
      si.disksIO((data) => resolve(data));
    });
  }
}
