import { PageOptionsRequest } from 'src/utils/pagination/dto/page-options.dto';
import { PageResponse } from 'src/utils/pagination/dto/page-response';
import { Notification } from '../entities/notification.entity';
import { PageOptions } from 'src/utils/pagination/page-options';
import { ApiPropertyOptional } from '@nestjs/swagger';
import { NotificationOrderByEnum } from '../enums/notification-order-by.enum';
import { IsEnum, IsOptional } from 'class-validator';
import { BooleanEnum } from 'src/utils/enums/boolean.enum';

export class GetNotificationsRequest extends PageOptionsRequest {
  @ApiPropertyOptional({
    example: NotificationOrderByEnum.ID,
    enum: NotificationOrderByEnum,
  })
  @IsOptional()
  @IsEnum(NotificationOrderByEnum)
  orderBy?: NotificationOrderByEnum;

  @ApiPropertyOptional({
    example: BooleanEnum.FALSE,
    enum: BooleanEnum,
    default: BooleanEnum.FALSE,
  })
  @IsOptional()
  @IsEnum(BooleanEnum)
  isNotSeenOnly?: BooleanEnum;
}

export class GetNotificationsResponse extends PageResponse<Notification> {
  constructor(
    notifications: Notification[],
    count: number,
    pageOptions: PageOptions,
  ) {
    super(notifications, count, pageOptions);
  }
}
