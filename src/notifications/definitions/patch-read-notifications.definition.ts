import { ApiProperty } from '@nestjs/swagger';
import { ArrayMinSize, IsBoolean, IsNumber } from 'class-validator';

export class PatchReadNotificationsRequest {
  @ApiProperty({ example: [1, 2] })
  @IsNumber({}, { each: true })
  @ArrayMinSize(1)
  notificationIds: number[];

  @ApiProperty({ example: true })
  @IsBoolean()
  isSeen: boolean;
}
