export class CreateNotificationDto {
  message: string;
  values?: object;
}
