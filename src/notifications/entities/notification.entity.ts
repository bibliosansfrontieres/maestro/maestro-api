import { User } from 'src/users/entities/user.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { CreateNotificationDto } from '../dto/create-notification.dto';

@Entity()
export class Notification {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  message: string;

  @Column()
  values: string;

  @Column()
  isSeen: boolean;

  @ManyToOne(() => User, (user) => user.notifications)
  user: User;

  @CreateDateColumn()
  createdAt: string;

  @UpdateDateColumn()
  updatedAt: string;

  constructor(createNotificationDto: CreateNotificationDto, user: User) {
    if (!createNotificationDto || !user) return;
    this.message = createNotificationDto.message;
    this.isSeen = false;
    this.values = JSON.stringify(createNotificationDto.values || {});
    this.user = user;
  }
}
