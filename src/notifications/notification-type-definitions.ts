import { NotificationTypeEnum } from './enums/notification-type.enum';

export interface NotificationTypeDefinition {
  name: string;
  sendInSlackChannel?: boolean;
}

export const notificationTypeDefinitions: {
  [key in keyof typeof NotificationTypeEnum]: NotificationTypeDefinition;
} = {
  CREATE_VIRTUAL_MACHINE: {
    name: 'Create virtual machine',
    sendInSlackChannel: true,
  },
  VIRTUAL_MACHINE_ERROR: {
    name: 'Virtual machine error',
    sendInSlackChannel: true,
  },
  VIRTUAL_MACHINE_READY: {
    name: 'Virtual machine ready',
    sendInSlackChannel: true,
  },
  APPROVE_VIRTUAL_MACHINE: {
    name: 'Approve virtual machine',
    sendInSlackChannel: true,
  },
  DEPLOY: {
    name: 'Deploy started',
    sendInSlackChannel: true,
  },
  DEPLOY_READY: {
    name: 'Deploy finished',
    sendInSlackChannel: true,
  },
  PROJECT_ERROR: {
    name: 'Project error',
    sendInSlackChannel: true,
  },
  PROJECT_READY: {
    name: 'Project ready',
    sendInSlackChannel: true,
  },
};
