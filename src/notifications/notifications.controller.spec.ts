import { Test, TestingModule } from '@nestjs/testing';
import { NotificationsController } from './notifications.controller';
import { NotificationsService } from './notifications.service';
import { ConfigService } from '@nestjs/config';
import { configServiceMock } from 'src/utils/mocks/services/config-service.mock';
import { Repository } from 'typeorm';
import { Notification } from './entities/notification.entity';
import { getRepositoryToken } from '@nestjs/typeorm';
import { SocketGateway } from 'src/socket/socket.gateway';
import { socketGatewayMock } from 'src/utils/mocks/gateways/socket-gateway.mock';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from 'src/users/users.service';
import { EntraService } from 'src/entra/entra.service';
import { entraServiceMock } from 'src/utils/mocks/services/entra-service.mock';
import { usersServiceMock } from 'src/utils/mocks/services/users-service.mock';
import { jwtServiceMock } from 'src/utils/mocks/services/jwt-service.mock';

describe('NotificationsController', () => {
  let controller: NotificationsController;
  let notificationRepository: Repository<Notification>;

  const NOTIFICATION_REPOSITORY_TOKEN = getRepositoryToken(Notification);

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [NotificationsController],
      providers: [
        NotificationsService,
        ConfigService,
        SocketGateway,
        JwtService,
        UsersService,
        ConfigService,
        EntraService,
        {
          provide: NOTIFICATION_REPOSITORY_TOKEN,
          useClass: Repository<Notification>,
        },
      ],
    })
      .overrideProvider(ConfigService)
      .useValue(configServiceMock)
      .overrideProvider(SocketGateway)
      .useValue(socketGatewayMock)
      .overrideProvider(JwtService)
      .useValue(jwtServiceMock)
      .overrideProvider(UsersService)
      .useValue(usersServiceMock)
      .overrideProvider(ConfigService)
      .useValue(configServiceMock)
      .overrideProvider(EntraService)
      .useValue(entraServiceMock)
      .compile();

    controller = module.get<NotificationsController>(NotificationsController);
    notificationRepository = module.get<Repository<Notification>>(
      NOTIFICATION_REPOSITORY_TOKEN,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
    expect(notificationRepository).toBeDefined();
  });
});
