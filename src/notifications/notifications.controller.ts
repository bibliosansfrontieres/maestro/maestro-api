import { Body, Controller, Get, Patch, Query } from '@nestjs/common';
import { NotificationsService } from './notifications.service';
import { Auth } from 'src/auth/decorators/auth.decorator';
import { UserId } from 'src/auth/decorators/user-id.decorator';
import { GetNotificationsRequest } from './definitions/get-notifications.definition';
import { PatchReadNotificationsRequest } from './definitions/patch-read-notifications.definition';
import { ApiOperation, ApiTags } from '@nestjs/swagger';

@Controller('notifications')
@ApiTags('notifications')
export class NotificationsController {
  constructor(private readonly notificationsService: NotificationsService) {}

  @Get()
  @Auth()
  @ApiOperation({ summary: 'Get notifications' })
  getNotifications(
    @UserId() userId: string,
    @Query() getNotificationsRequest: GetNotificationsRequest,
  ) {
    return this.notificationsService.getNotifications(
      userId,
      getNotificationsRequest,
    );
  }

  @Patch()
  @Auth()
  @ApiOperation({ summary: 'Mark specific notifications as read' })
  patchReadNotifications(
    @UserId() userId: string,
    @Body() patchReadNotificationsRequest: PatchReadNotificationsRequest,
  ) {
    return this.notificationsService.patchReadNotifications(
      userId,
      patchReadNotificationsRequest,
    );
  }

  @Patch('all')
  @Auth()
  @ApiOperation({ summary: 'Mark all notifications as read' })
  patchReadAllNotifications(@UserId() userId: string) {
    return this.notificationsService.patchReadAllNotifications(userId);
  }
}
