import { Test, TestingModule } from '@nestjs/testing';
import { NotificationsService } from './notifications.service';
import { ConfigService } from '@nestjs/config';
import { configServiceMock } from 'src/utils/mocks/services/config-service.mock';
import { Notification } from './entities/notification.entity';
import { Repository } from 'typeorm';
import { getRepositoryToken } from '@nestjs/typeorm';
import { SocketGateway } from 'src/socket/socket.gateway';
import { socketGatewayMock } from 'src/utils/mocks/gateways/socket-gateway.mock';
import { UsersService } from 'src/users/users.service';
import { usersServiceMock } from 'src/utils/mocks/services/users-service.mock';

describe('NotificationsService', () => {
  let service: NotificationsService;
  let notificationRepository: Repository<Notification>;

  const NOTIFICATION_REPOSITORY_TOKEN = getRepositoryToken(Notification);

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        NotificationsService,
        ConfigService,
        SocketGateway,
        UsersService,
        {
          provide: NOTIFICATION_REPOSITORY_TOKEN,
          useClass: Repository<Notification>,
        },
      ],
    })
      .overrideProvider(ConfigService)
      .useValue(configServiceMock)
      .overrideProvider(SocketGateway)
      .useValue(socketGatewayMock)
      .overrideProvider(UsersService)
      .useValue(usersServiceMock)
      .compile();

    service = module.get<NotificationsService>(NotificationsService);
    notificationRepository = module.get<Repository<Notification>>(
      NOTIFICATION_REPOSITORY_TOKEN,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
    expect(notificationRepository).toBeDefined();
  });
});
