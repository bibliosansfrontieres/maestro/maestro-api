import { Inject, Injectable, forwardRef } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { InjectRepository } from '@nestjs/typeorm';
import { WebClient } from '@slack/web-api';
import axios from 'axios';
import { User } from 'src/users/entities/user.entity';
import { Notification } from './entities/notification.entity';
import { In, Repository } from 'typeorm';
import { CreateNotificationDto } from './dto/create-notification.dto';
import { NotificationTypeEnum } from './enums/notification-type.enum';
import { Group } from 'src/groups/entities/group.entity';
import { notificationTypeDefinitions } from './notification-type-definitions';
import { SocketGateway } from 'src/socket/socket.gateway';
import {
  GetNotificationsRequest,
  GetNotificationsResponse,
} from './definitions/get-notifications.definition';
import { getManyAndCount } from 'src/utils/get-many-and-count.util';
import { PageOptions } from 'src/utils/pagination/page-options';
import { QueryHelper } from 'src/utils/query-helpers.util';
import { NotificationOrderByEnum } from './enums/notification-order-by.enum';
import { BooleanEnum } from 'src/utils/enums/boolean.enum';
import { PatchReadNotificationsRequest } from './definitions/patch-read-notifications.definition';
import { UsersService } from 'src/users/users.service';

@Injectable()
export class NotificationsService {
  private webClient: WebClient;

  constructor(
    private configService: ConfigService,
    @InjectRepository(Notification)
    private notificationRepository: Repository<Notification>,
    @Inject(forwardRef(() => SocketGateway))
    private socketGateway: SocketGateway,
    private usersService: UsersService,
  ) {
    this.webClient = new WebClient(this.configService.get('SLACK_OAUTH_TOKEN'));
  }

  private create(createNotificationDto: CreateNotificationDto, user: User) {
    const notification = new Notification(createNotificationDto, user);
    return this.notificationRepository.save(notification);
  }

  async patchReadAllNotifications(userId: string) {
    const notifications = await this.notificationRepository.find({
      where: { user: { id: userId } },
    });
    for (const notification of notifications) {
      notification.isSeen = true;
    }
    await this.notificationRepository.save(notifications);
    const user = await this.usersService.findOne({ where: { id: userId } });
    const countNotifications = await this.notificationRepository.find({
      where: { user: { id: userId }, isSeen: false },
    });

    this.socketGateway.emitUserNotification(
      user,
      undefined,
      countNotifications.length,
    );
    return;
  }

  async getNotifications(
    userId: string,
    getNotificationsRequest: GetNotificationsRequest,
  ) {
    const pageOptions = new PageOptions(getNotificationsRequest);
    const notificationsQuery = this.notificationRepository
      .createQueryBuilder('notification')
      .innerJoin('notification.user', 'user')
      .where('user.id = :userId', { userId });

    const queryHelper = new QueryHelper(notificationsQuery);

    let { orderBy } = getNotificationsRequest;
    orderBy = orderBy ? orderBy : NotificationOrderByEnum.ID;
    queryHelper.pagination(pageOptions, orderBy);

    if (getNotificationsRequest.isNotSeenOnly === BooleanEnum.TRUE) {
      queryHelper.andWhereEquals('notification.isSeen', { isSeen: '0' });
    }

    const [count, notifications] = await getManyAndCount<Notification>(
      notificationsQuery,
      ['notification.id'],
      false,
    );

    return new GetNotificationsResponse(notifications, count, pageOptions);
  }

  async patchReadNotifications(
    userId: string,
    patchReadNotificationsRequest: PatchReadNotificationsRequest,
  ) {
    const notifications = await this.notificationRepository.find({
      where: { id: In(patchReadNotificationsRequest.notificationIds) },
    });

    for (const notification of notifications) {
      notification.isSeen = patchReadNotificationsRequest.isSeen;
    }

    await this.notificationRepository.save(notifications);

    const user = await this.usersService.findOne({ where: { id: userId } });
    const countNotifications = await this.notificationRepository.find({
      where: { user: { id: userId }, isSeen: false },
    });

    this.socketGateway.emitUserNotification(
      user,
      undefined,
      countNotifications.length,
    );

    return;
  }

  async notifyMonitoring(message: string) {
    const webhookUrl = this.configService.get(
      'SLACK_WEBHOOK_URL_MONITORING_CHANNEL',
    );
    try {
      await axios.post(webhookUrl, { text: message });
    } catch (e) {
      console.error(e);
    }
  }

  private async notifySlackChannel(message: string) {
    const webhookUrl = this.configService.get(
      'SLACK_WEBHOOK_URL_NOTIFICATIONS_CHANNEL',
    );
    try {
      await axios.post(webhookUrl, { text: message });
    } catch (e) {
      console.error(e);
    }
  }

  async getMention(user: User) {
    const userId = await this.getUserId(user);
    if (!userId) {
      return `<@${user.mail.split('@')[0]}>`;
    }
    return `<@${userId}>`;
  }

  private async getUserId(user: User): Promise<string | undefined> {
    try {
      const result = await this.webClient.users.lookupByEmail({
        email: user.mail,
      });
      return result?.user?.id || undefined;
    } catch (e) {
      console.error(e);
      return undefined;
    }
  }

  private async notifyUser(user: User, message: string) {
    // Maestro notification
    await this.create({ message }, user);

    // Slack user notification
    const userId = await this.getUserId(user);
    if (userId) {
      try {
        this.webClient.chat.postMessage({
          channel: userId,
          text: message,
        });
      } catch (e) {
        console.error(e);
      }
    }
  }

  async notify(
    type: NotificationTypeEnum,
    user: User,
    values: { [key: string]: string },
    notifyOthers?: User[] | Group[],
  ) {
    const { sendInSlackChannel, name } = notificationTypeDefinitions[type];
    // TODO : TRUE here is used to replace user notifications configuration
    if (sendInSlackChannel === true || true) {
      let slackMessage = `${await this.getMention(user)} *${name}*`;
      for (const [key, value] of Object.entries(values)) {
        slackMessage += `\n‣ ${key}: ${value}`;
      }
      if (sendInSlackChannel) {
        await this.notifySlackChannel(slackMessage);
      }
    }

    // TODO: Manage notification channels for user
    const notification = await this.create({ message: name, values }, user);
    const notifications = await this.notificationRepository.find({
      where: { user: { id: user.id }, isSeen: false },
    });

    this.socketGateway.emitUserNotification(
      user,
      notification,
      notifications.length,
    );

    if (notifyOthers && notifyOthers.length > 0) {
      // TODO : Send notification to other users/groups
      // TODO : emit for other groups/users websocket
    }
  }
}
