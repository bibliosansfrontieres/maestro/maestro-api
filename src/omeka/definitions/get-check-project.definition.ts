import { ItemSourceEnum } from 'src/items/enums/item-source.enum';
import { IProject } from '../interfaces/project.interface';

export class GetCheckProjectResponse {
  id: number;
  source: ItemSourceEnum;
  url: string;
  title: string;
  description: string;
  date: string;
  languages: string;
  startDate: string;
  endDate: string;
  partners: string;
  location: string;
  device: string;
  projectManager: string;
  constructor(project: IProject) {
    this.id = project.id;
    this.source = project.source;
    this.url = project.url;
    this.title = project.title;
    this.description = project.description;
    this.date = project.date;
    this.languages = project.languages;
    this.startDate = project.startDate;
    this.endDate = project.endDate;
    this.partners = project.partners;
    this.location = project.location;
    this.device = project.device;
    this.projectManager = project.projectManager;
  }
}
