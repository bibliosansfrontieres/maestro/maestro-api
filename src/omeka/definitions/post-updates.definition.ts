import { IsArray } from 'class-validator';

export class PostUpdatesRequest {
  @IsArray()
  playlists: { id: string; version: string; length: number }[];

  @IsArray()
  items: { id: string; version: string }[];
}
