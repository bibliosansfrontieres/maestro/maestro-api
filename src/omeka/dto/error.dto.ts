export class ErrorDto {
  id: number;
  message: string;
  type: string = 'error';

  constructor(id: number, message: string) {
    this.id = id;
    this.message = message;
  }
}
