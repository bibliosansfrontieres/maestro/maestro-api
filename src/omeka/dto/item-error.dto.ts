import { ErrorDto } from './error.dto';

export class ItemErrorDto extends ErrorDto {
  type = 'item';
}
