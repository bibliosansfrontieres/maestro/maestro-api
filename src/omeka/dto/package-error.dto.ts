import { ErrorDto } from './error.dto';

export class PackageErrorDto extends ErrorDto {
  type = 'package';
}
