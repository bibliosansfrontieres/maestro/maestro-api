import { Application } from 'src/applications/entities/application.entity';
import { Category } from 'src/categories/entities/category.entity';
import { CreateItemDto } from 'src/items/dto/create-item.dto';
import { CreatePlaylistDto } from 'src/playlists/dto/create-playlist.dto';
import { CreateProjectDto } from 'src/projects/dto/create-project.dto';

export interface IMaestroFullProject {
  project: CreateProjectDto;
  applications: Application[];
  categories: Category[];
  playlists: CreatePlaylistDto[];
  items: CreateItemDto[];
}
