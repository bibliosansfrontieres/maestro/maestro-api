export interface IOmekaElementText {
  html: boolean;
  text: string;
  element_set: {
    id: number;
    url: string;
    name: string;
    resource: 'element_sets';
  };
  element: {
    id: number;
    url: string;
    name: string;
    resource: 'elements';
  };
}
