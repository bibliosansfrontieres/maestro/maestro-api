import { IOmekaItemFile } from './omeka-item-file.interface';
import { IOmekaItem } from './omeka-item.interface';
import { IOmekaPackage } from './omeka-package.interface';

export interface IOmekaFullProject {
  project: IOmekaItem;
  packages: IOmekaPackage[];
  itemFiles: IOmekaItemFile[];
}
