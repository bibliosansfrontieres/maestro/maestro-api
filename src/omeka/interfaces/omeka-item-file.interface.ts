import { IOmekaFile } from './omeka-file.interface';
import { IOmekaItem } from './omeka-item.interface';

export interface IOmekaItemFile extends IOmekaItem {
  file: IOmekaFile;
}
