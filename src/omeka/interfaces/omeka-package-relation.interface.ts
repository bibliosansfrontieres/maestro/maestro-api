export interface IOmekaPackageRelation {
  id: number;
  package_id: number;
  item_id: number;
  extended_resources: [];
}
