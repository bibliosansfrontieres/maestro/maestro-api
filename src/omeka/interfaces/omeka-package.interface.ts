export interface IOmekaPackage {
  id: number;
  slug: string;
  description: string;
  goal: string;
  language: string;
  last_exportable_modification: string;
  ideascube_name: string;
  subject: string;
  relations: { item_id: number }[];
  contents: { item_id: number }[];
  extended_resources: [];
}
