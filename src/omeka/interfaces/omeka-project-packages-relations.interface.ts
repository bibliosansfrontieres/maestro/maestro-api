import { IOmekaPackageRelation } from './omeka-package-relation.interface';

export interface IOmekaProjectPackagesRelations {
  projectId: number;
  packagesRelations: IOmekaPackageRelation[];
}
