import { ItemErrorDto } from '../dto/item-error.dto';
import { PackageErrorDto } from '../dto/package-error.dto';
import { ProjectErrorDto } from '../dto/project-error.dto';

export interface IProjectErrors {
  count: number;
  projectErrors: ProjectErrorDto[];
  packagesErrors: PackageErrorDto[];
  itemsErrors: ItemErrorDto[];
}
