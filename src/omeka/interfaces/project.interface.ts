import { ItemSourceEnum } from 'src/items/enums/item-source.enum';

export interface IProject {
  id: number;
  source: ItemSourceEnum;
  url: string;
  title: string;
  description: string;
  date: string;
  languages: string;
  startDate: string;
  endDate: string;
  partners: string;
  location: string;
  device: string;
  projectManager: string;
}
