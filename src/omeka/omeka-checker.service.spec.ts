import { Test, TestingModule } from '@nestjs/testing';
import { OmekaCheckerService } from './omeka-checker.service';

describe('OmekaCheckerService', () => {
  let service: OmekaCheckerService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [OmekaCheckerService],
    }).compile();

    service = module.get<OmekaCheckerService>(OmekaCheckerService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
