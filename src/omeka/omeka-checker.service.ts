import { Injectable } from '@nestjs/common';
import { IOmekaItemFile } from './interfaces/omeka-item-file.interface';
import { findOmekaElement } from 'src/utils/find-omeka-element.util';
import { ItemErrorDto } from './dto/item-error.dto';
import { IOmekaFullProject } from './interfaces/omeka-full-project.interface';
import { IOmekaItem } from './interfaces/omeka-item.interface';
import { ProjectErrorDto } from './dto/project-error.dto';
import { IOmekaPackage } from './interfaces/omeka-package.interface';
import { PackageErrorDto } from './dto/package-error.dto';
import { IProjectErrors } from './interfaces/project-errors.interface';

@Injectable()
export class OmekaCheckerService {
  checkOmekaFullProject(omekaFullProject: IOmekaFullProject): IProjectErrors {
    const projectErrors = this.checkProject(omekaFullProject.project);
    const packagesErrors = this.checkOmekaPackages(omekaFullProject.packages);
    const itemsErrors = this.checkOmekaItemFiles(omekaFullProject.itemFiles);
    const count =
      projectErrors.length + packagesErrors.length + itemsErrors.length;
    return { count, projectErrors, packagesErrors, itemsErrors };
  }

  checkProject(omekaProject: IOmekaItem) {
    const projectErrors: ProjectErrorDto[] = [];
    const id = omekaProject.id;
    const elementTexts = omekaProject.element_texts;

    if (!findOmekaElement(50, elementTexts)) {
      projectErrors.push(new ProjectErrorDto(id, 'title not found'));
    }
    if (!findOmekaElement(41, elementTexts)) {
      projectErrors.push(new ProjectErrorDto(id, 'description not found'));
    }
    /*
    if (!findOmekaElement(39, elementTexts)) {
      projectErrors.push(new ProjectErrorDto(id, 'creator not found'));
    }
    if (!findOmekaElement(44, elementTexts)) {
      projectErrors.push(new ProjectErrorDto(id, 'languages not found'));
    }
    const dateFormatRegex = /(1[0-2]|0[1-9])\/(20|19)([1-9]\d)/;
    const startDate = getOmekaElement(259, elementTexts);
    if (!startDate) {
      projectErrors.push(new ProjectErrorDto(id, 'start date not found'));
    } else if (!dateFormatRegex.test(startDate)) {
      projectErrors.push(
        new ProjectErrorDto(id, 'start date needs to be as MM/YYYY format'),
      );
    }
    const endDate = getOmekaElement(264, elementTexts);
    if (!endDate) {
      projectErrors.push(new ProjectErrorDto(id, 'end date not found'));
    } else if (!dateFormatRegex.test(endDate)) {
      projectErrors.push(
        new ProjectErrorDto(id, 'end date needs to be as MM/YYYY format'),
      );
    }
    if (!findOmekaElement(260, elementTexts)) {
      projectErrors.push(new ProjectErrorDto(id, 'partners not found'));
    }
    if (!findOmekaElement(261, elementTexts)) {
      projectErrors.push(new ProjectErrorDto(id, 'location not found'));
    }
    const device = getOmekaElement(262, elementTexts);
    if (!device) {
      projectErrors.push(new ProjectErrorDto(id, 'tool (outil) not found'));
    } else if (isNaN(parseInt(device))) {
      projectErrors.push(
        new ProjectErrorDto(
          id,
          'tool (outil) should be a number of ideascubes (no text)',
        ),
      );
    }
    if (!findOmekaElement(263, elementTexts)) {
      projectErrors.push(new ProjectErrorDto(id, 'project manager not found'));
    }*/

    return projectErrors;
  }

  checkOmekaPackages(omekaPackages: IOmekaPackage[]) {
    const packagesErrors: PackageErrorDto[] = [];
    for (const omekaPackage of omekaPackages) {
      packagesErrors.push(...this.checkOmekaPackage(omekaPackage));
    }
    return packagesErrors;
  }

  checkOmekaPackage(omekaPackage: IOmekaPackage) {
    const packageErrors: PackageErrorDto[] = [];

    const id = omekaPackage.id;

    if (!omekaPackage.ideascube_name) {
      packageErrors.push(new PackageErrorDto(id, 'ideascube name not found'));
    }
    if (!omekaPackage.description) {
      packageErrors.push(new PackageErrorDto(id, 'description not found'));
    }
    /*
    if (!omekaPackage.goal) {
      packageErrors.push(new PackageErrorDto(id, 'goal not found'));
    }
    if (!omekaPackage.language) {
      packageErrors.push(new PackageErrorDto(id, 'language not found'));
    }*/
    if (!omekaPackage.subject) {
      packageErrors.push(new PackageErrorDto(id, 'subject not found'));
    }

    return packageErrors;
  }

  checkOmekaItemFiles(omekaItemFiles: IOmekaItemFile[]) {
    const itemsErrors: ItemErrorDto[] = [];
    for (const omekaItemFile of omekaItemFiles) {
      itemsErrors.push(...this.checkOmekaItemFile(omekaItemFile));
    }
    return itemsErrors;
  }

  checkOmekaItemFile(omekaItemFile: IOmekaItemFile) {
    const id = omekaItemFile.id;
    const itemErrors: ItemErrorDto[] = [];

    if (!omekaItemFile.item_type || !omekaItemFile.item_type?.id) {
      itemErrors.push(new ItemErrorDto(id, 'item_type not found'));
    }

    if (!omekaItemFile.owner || !omekaItemFile.owner.id) {
      itemErrors.push(new ItemErrorDto(id, 'owner not found'));
    }
    if (!omekaItemFile.file) {
      itemErrors.push(new ItemErrorDto(id, 'file not found'));
    }

    const elementTexts = omekaItemFile.element_texts;
    if (!findOmekaElement(50, elementTexts)) {
      itemErrors.push(new ItemErrorDto(id, 'title not found'));
    }
    if (!findOmekaElement(41, elementTexts)) {
      itemErrors.push(new ItemErrorDto(id, 'description not found'));
    }
    /*
    if (!findOmekaElement(39, elementTexts)) {
      itemErrors.push(new ItemErrorDto(id, 'creator not found'));
    }
    if (!findOmekaElement(51, elementTexts)) {
      itemErrors.push(new ItemErrorDto(id, 'type not found'));
    }
    if (!findOmekaElement(44, elementTexts)) {
      itemErrors.push(new ItemErrorDto(id, 'language not found'));
    }
    if (!findOmekaElement(86, elementTexts)) {
      itemErrors.push(new ItemErrorDto(id, 'audience not found'));
    }
    if (!findOmekaElement(64, elementTexts)) {
      itemErrors.push(new ItemErrorDto(id, 'license not found'));
    }
    if (!omekaItemFile.collection) {
      itemErrors.push(new ItemErrorDto(id, 'collection not found'));
    }
    if (!findOmekaElement(56, elementTexts)) {
      itemErrors.push(new ItemErrorDto(id, 'date created not found'));
    }
    // Item type 3 = Moving Image (video)
    if (omekaItemFile.item_type && omekaItemFile.item_type.id === 3) {
      if (!findOmekaElement(11, elementTexts)) {
        itemErrors.push(new ItemErrorDto(id, 'duration not found'));
      }
    }
    // Item type 5 = Sound (audio)
    if (omekaItemFile.item_type && omekaItemFile.item_type.id === 5) {
      if (!findOmekaElement(11, elementTexts)) {
        itemErrors.push(new ItemErrorDto(id, 'duration not found'));
      }
    }
    // Item type 1 = Text (pdf)
    if (omekaItemFile.item_type && omekaItemFile.item_type.id === 1) {
      if (!findOmekaElement(282, elementTexts)) {
        itemErrors.push(new ItemErrorDto(id, 'pages not found'));
      }
    }
    */
    // TODO : all commented errors should be warnings
    return itemErrors;
  }
}
