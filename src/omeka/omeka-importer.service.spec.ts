import { Test, TestingModule } from '@nestjs/testing';
import { OmekaImporterService } from './omeka-importer.service';
import { OmekaService } from './omeka.service';
import { omekaServiceMock } from 'src/utils/mocks/services/omeka-service.mock';
import { OmekaCheckerService } from './omeka-checker.service';
import { omekaCheckerServiceMock } from 'src/utils/mocks/services/omeka-checker-service.mock';
import { OmekaTransformerService } from './omeka-transformer.service';
import { omekaTransformerServiceMock } from 'src/utils/mocks/services/omeka-transformer-service.mock';
import { ApplicationsService } from 'src/applications/applications.service';
import { applicationsServiceMock } from 'src/utils/mocks/services/applications-service.mock';
import { ItemsService } from 'src/items/items.service';
import { itemsServiceMock } from 'src/utils/mocks/services/items-service.mock';
import { CategoriesService } from 'src/categories/categories.service';
import { categoriesServiceMock } from 'src/utils/mocks/services/categories-service.mock';
import { PlaylistsService } from 'src/playlists/playlists.service';
import { playlistsServiceMock } from 'src/utils/mocks/services/playlists-service.mock';
import { ProjectsService } from 'src/projects/projects.service';
import { projectsServiceMock } from 'src/utils/mocks/services/projects-service.mock';
import { S3Service } from 'src/s3/s3.service';
import { s3ServiceMock } from 'src/utils/mocks/services/s3-service.mock';
import { ConfigService } from '@nestjs/config';
import { configServiceMock } from 'src/utils/mocks/services/config-service.mock';

describe('OmekaImporterService', () => {
  let service: OmekaImporterService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        OmekaImporterService,
        OmekaService,
        OmekaCheckerService,
        OmekaTransformerService,
        ApplicationsService,
        ItemsService,
        CategoriesService,
        PlaylistsService,
        ProjectsService,
        S3Service,
        ConfigService,
      ],
    })
      .overrideProvider(OmekaService)
      .useValue(omekaServiceMock)
      .overrideProvider(OmekaCheckerService)
      .useValue(omekaCheckerServiceMock)
      .overrideProvider(OmekaTransformerService)
      .useValue(omekaTransformerServiceMock)
      .overrideProvider(ApplicationsService)
      .useValue(applicationsServiceMock)
      .overrideProvider(ItemsService)
      .useValue(itemsServiceMock)
      .overrideProvider(CategoriesService)
      .useValue(categoriesServiceMock)
      .overrideProvider(PlaylistsService)
      .useValue(playlistsServiceMock)
      .overrideProvider(ProjectsService)
      .useValue(projectsServiceMock)
      .overrideProvider(S3Service)
      .useValue(s3ServiceMock)
      .overrideProvider(ConfigService)
      .useValue(configServiceMock)
      .compile();

    service = module.get<OmekaImporterService>(OmekaImporterService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
