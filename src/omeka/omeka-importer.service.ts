import { Injectable, Logger } from '@nestjs/common';
import { OmekaService } from './omeka.service';
import { OmekaCheckerService } from './omeka-checker.service';
import { OmekaTransformerService } from './omeka-transformer.service';
import { ApplicationsService } from 'src/applications/applications.service';
import { ItemsService } from 'src/items/items.service';
import { getDublinCoreTypeOmekaMap } from 'src/utils/get-dublin-core-type-omeka-map.util';
import { threadsFunction } from 'src/utils/thread-function.util';
import { CreateItemDto } from 'src/items/dto/create-item.dto';
import { Item } from 'src/items/entities/item.entity';
import { getOmekaCategoriesMap } from 'src/utils/get-omeka-categories-map.util';
import { CategoriesService } from 'src/categories/categories.service';
import { CreatePlaylistDto } from 'src/playlists/dto/create-playlist.dto';
import { Playlist } from 'src/playlists/entities/playlist.entity';
import { PlaylistsService } from 'src/playlists/playlists.service';
import { ProjectsService } from 'src/projects/projects.service';
import { IProjectErrors } from './interfaces/project-errors.interface';
import { IMaestroFullProject } from './interfaces/maestro-full-project.interface';
import { S3Service } from 'src/s3/s3.service';
import { downloadFile } from 'src/utils/download-file.util';
import { join } from 'path';
import { createReadStream, existsSync, readFileSync, rmSync } from 'fs';
import { md5 } from 'src/utils/md5.util';
import { generatePlaylistThumbnail } from 'src/utils/generate-playlist-thumbnail.util';
import { In } from 'typeorm';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class OmekaImporterService {
  constructor(
    private omekaService: OmekaService,
    private omekaCheckerService: OmekaCheckerService,
    private omekaTransformerService: OmekaTransformerService,
    private applicationsService: ApplicationsService,
    private itemsService: ItemsService,
    private categoriesService: CategoriesService,
    private playlistsService: PlaylistsService,
    private projectsService: ProjectsService,
    private s3Service: S3Service,
    private configService: ConfigService,
  ) {}

  async importProject(
    projectId: number,
  ): Promise<IProjectErrors | IMaestroFullProject> {
    const project = await this.omekaService.getFullProject(projectId);
    const projectCheck =
      this.omekaCheckerService.checkOmekaFullProject(project);

    if (projectCheck.count > 0) {
      return projectCheck;
    }

    const application = await this.applicationsService.findOne({
      where: { id: 'olip' },
    });
    const dublinCoreTypesMap = await getDublinCoreTypeOmekaMap(
      await this.itemsService.findDublinCoreType({
        relations: { dublinCoreTypeTranslations: true },
      }),
    );
    const categoriesMap = await getOmekaCategoriesMap(
      await this.categoriesService.find({
        relations: { categoryTranslations: true },
      }),
    );
    const maestroFullProject =
      this.omekaTransformerService.omekaFullProjectToMaestro(
        project,
        application,
        dublinCoreTypesMap,
        categoriesMap,
      );

    const items = await this.importItems(maestroFullProject.items);
    await this.importPlaylists(maestroFullProject.playlists);
    await this.projectsService.createOrUpdate(maestroFullProject.project);

    await this.downloadItems(items);
    const changedItemsThumbnails = await this.downloadItemThumbnails(items);
    await this.processPlaylistThumbnails(changedItemsThumbnails);

    return maestroFullProject;
  }

  async processPlaylistThumbnails(changedItemsThumbnails: Item[]) {
    changedItemsThumbnails = changedItemsThumbnails.filter(
      (item) => item !== null,
    );
    const playlistIds = [];
    for (const item of changedItemsThumbnails) {
      const itemPlaylists = await this.playlistsService.find({
        where: { items: { id: item.id } },
        relations: { items: true },
      });
      for (const itemPlaylist of itemPlaylists) {
        if (!playlistIds.includes(itemPlaylist.id)) {
          playlistIds.push(itemPlaylist.id);
        }
      }
    }
    const playlists = await this.playlistsService.find({
      where: { id: In(playlistIds) },
      relations: { items: true },
    });
    for (const playlist of playlists) {
      Logger.log(
        `Generating playlist thumbnail #${playlist.id}...`,
        'OmekaImporterService',
      );
      const itemImages: string[] = [];
      const itemImageMd5s: string[] = [];
      const maxItemImages = 6;
      for (
        let i = 0;
        i < playlist.items.length && itemImages.length < maxItemImages;
        i++
      ) {
        const item = playlist.items[i];
        if (!item.thumbnail) continue;
        const itemImagePath = join(__dirname, '../../data/s3', item.thumbnail);
        const itemImage = readFileSync(itemImagePath).toString();
        const itemImageMd5 = md5(itemImage);
        if (!itemImageMd5s.includes(itemImageMd5)) {
          itemImages.push(itemImagePath);
          itemImageMd5s.push(itemImageMd5);
        }
      }
      if (itemImages.length !== 0) {
        if (itemImages.length === 1) {
          const thumbnail = await this.s3Service.uploadPlaylistThumbnail(
            playlist,
            readFileSync(itemImages[0]),
          );

          playlist.image = thumbnail.path;
          await this.playlistsService.save(playlist);
        } else {
          if (itemImages.length === 3) {
            itemImages.pop();
          }
          if (itemImages.length === 5) {
            itemImages.pop();
          }
          if (itemImages.length === 2) {
            const patchwork = await generatePlaylistThumbnail(itemImages, {
              canvasWidth: 400,
              canvasHeight: 600,
              imageWidth: 400,
              imageHeight: 300,
            });
            if (patchwork) {
              const thumbnail = await this.s3Service.uploadPlaylistThumbnail(
                playlist,
                readFileSync(
                  join(__dirname, '../../downloads/playlist_thumbnail.png'),
                ),
              );
              playlist.image = thumbnail.path;
              await this.playlistsService.save(playlist);
            }
          }
          if (itemImages.length === 4) {
            const patchwork = await generatePlaylistThumbnail(itemImages, {
              canvasWidth: 400,
              canvasHeight: 600,
              imageWidth: 200,
              imageHeight: 300,
            });
            if (patchwork) {
              const thumbnail = await this.s3Service.uploadPlaylistThumbnail(
                playlist,
                readFileSync(
                  join(__dirname, '../../downloads/playlist_thumbnail.png'),
                ),
              );
              playlist.image = thumbnail.path;
              await this.playlistsService.save(playlist);
            }
          }
          if (itemImages.length === 6) {
            const patchwork = await generatePlaylistThumbnail(itemImages, {
              canvasWidth: 400,
              canvasHeight: 600,
              imageWidth: 200,
              imageHeight: 200,
            });
            if (patchwork) {
              const thumbnail = await this.s3Service.uploadPlaylistThumbnail(
                playlist,
                readFileSync(
                  join(__dirname, '../../downloads/playlist_thumbnail.png'),
                ),
              );
              playlist.image = thumbnail.path;
              await this.playlistsService.save(playlist);
            }
          }
        }
      } else {
        const thumbnail = await this.s3Service.uploadPlaylistThumbnail(
          playlist,
          readFileSync(
            join(
              __dirname,
              '../../data/initial-data/images/placeholder_playlists.svg',
            ),
          ),
        );
        playlist.image = thumbnail.path;
        await this.playlistsService.save(playlist);
      }
    }
  }

  async downloadItemThumbnails(items: Item[]) {
    return threadsFunction<Item, Item>({
      iterationArray: items,
      maxThreads: 10,
      thread: this.downloadItemThumbnail.bind(this),
      options: {
        sleepBeforeRestart: 1000,
        service: 'OmekaImporterService',
        name: 'download items thumbnails',
      },
    });
  }

  async downloadItemThumbnail(item: Item) {
    const s3File = await this.s3Service.getItemThumbnail(`${item.id}`);
    if (s3File) {
      const fileSourceVersion = new Date(item.file.updatedAt).toISOString();
      const s3FileSourceVersion = new Date(s3File.sourceVersion).toISOString();
      if (s3FileSourceVersion === fileSourceVersion) {
        item.thumbnail = s3File.path;
        await this.itemsService.save(item);
        Logger.debug(
          `Item thumbnail #${item.id} is up to date, no upload to S3 needed`,
          'OmekaImporterService',
        );
        return null;
      }
      Logger.debug(
        `Item thumbnail #${item.id} needs update, uploading to S3`,
        'OmekaImporterService',
      );
    }

    const path = item.thumbnail;
    if (!path) {
      if (s3File) {
        Logger.debug(
          `Item thumbnail #${item.id} is up to date, no upload to S3 needed`,
          'OmekaImporterService',
        );
      } else {
        if (item.dublinCoreItem.dublinCoreType.id === 3) {
          // video
          const placeholderVideoPath = join(
            __dirname,
            '../../data/initial-data/images/placeholder_item_video.png',
          );
          item.thumbnail = 'placeholder_item_video.png';
          const uploadResult = await this.s3Service.uploadItemThumbnail(
            item,
            createReadStream(placeholderVideoPath),
          );
          if (!uploadResult) return undefined;
          return item;
        } else if (item.dublinCoreItem.dublinCoreType.id === 5) {
          // audio
          const placeholderAudioPath = join(
            __dirname,
            '../../data/initial-data/images/placeholder_item_audio.png',
          );
          item.thumbnail = 'placeholder_item_audio.png';
          const uploadResult = await this.s3Service.uploadItemThumbnail(
            item,
            createReadStream(placeholderAudioPath),
          );
          if (!uploadResult) return undefined;
          return item;
        } else {
          // 1 text
          const placeholderTextPath = join(
            __dirname,
            '../../data/initial-data/images/placeholder_item_text.png',
          );
          item.thumbnail = 'placeholder_item_text.png';
          const uploadResult = await this.s3Service.uploadItemThumbnail(
            item,
            createReadStream(placeholderTextPath),
          );
          if (!uploadResult) return undefined;
          return item;
        }
      }
      return null;
    }

    const pathSplit = path.split('.');
    const extension = pathSplit[pathSplit.length - 1];

    const downloadedThumbailPath = join(
      __dirname,
      `../../downloads/${item.id}.${extension}`,
    );

    Logger.log(
      `Downloading item thumbnail #${item.id}...`,
      'OmekaImporterService',
    );
    const isFileDownloaded = await downloadFile(path, downloadedThumbailPath);
    if (!isFileDownloaded) {
      Logger.error(
        `Error downloading item thumbnail #${item.id}`,
        'OmekaImporterService',
      );
      return undefined;
    }
    Logger.log(
      `Item thumbnail #${item.id} downloaded !`,
      'OmekaImporterService',
    );

    const uploadResult = await this.s3Service.uploadItemThumbnail(
      item,
      createReadStream(downloadedThumbailPath),
    );

    if (!uploadResult) {
      if (existsSync(downloadedThumbailPath)) {
        rmSync(downloadedThumbailPath);
      }
      return undefined;
    }

    if (existsSync(downloadedThumbailPath)) {
      rmSync(downloadedThumbailPath);
    }

    return item;
  }

  async downloadItems(items: Item[]) {
    return threadsFunction<Item, Item>({
      iterationArray: items,
      maxThreads: 1,
      thread: this.downloadItem.bind(this),
      options: {
        sleepBeforeRestart: 1000,
        service: 'OmekaImporterService',
        name: 'download items',
      },
    });
  }

  async downloadItem(item: Item) {
    const { path, extension } = item.file;
    if (!path || !extension) {
      Logger.error(
        `Item #${item.id} without file url/extension`,
        'OmekaImporterService',
      );
      return undefined;
    }
    const s3File = await this.s3Service.getItem(`${item.id}`);
    if (s3File) {
      const fileSourceVersion = new Date(item.file.updatedAt).toISOString();
      const s3FileSourceVersion = new Date(s3File.sourceVersion).toISOString();
      if (s3FileSourceVersion === fileSourceVersion) {
        item.file.path = s3File.path;
        await this.itemsService.saveFile(item.file);
        Logger.debug(
          `Item #${item.id} is up to date, no upload to S3 needed`,
          'OmekaImporterService',
        );
        return item;
      }
      Logger.debug(
        `Item #${item.id} needs update, uploading to S3`,
        'OmekaImporterService',
      );
    }
    const downloadedFilePath = join(
      __dirname,
      `../../downloads/${item.id}.${extension}`,
    );

    Logger.log(`Downloading item #${item.id}...`, 'OmekaImporterService');
    const isFileDownloaded = await downloadFile(path, downloadedFilePath);
    if (!isFileDownloaded) {
      Logger.error(
        `Error downloading item #${item.id}`,
        'OmekaImporterService',
      );
      return undefined;
    }
    Logger.log(`Item #${item.id} downloaded !`, 'OmekaImporterService');

    const uploadResult = await this.s3Service.uploadItem(
      item,
      createReadStream(downloadedFilePath),
    );

    if (!uploadResult) {
      if (existsSync(downloadedFilePath)) {
        rmSync(downloadedFilePath);
      }
      return undefined;
    }

    if (existsSync(downloadedFilePath)) {
      rmSync(downloadedFilePath);
    }

    return item;
  }

  async importPlaylists(createPlaylistDtos: CreatePlaylistDto[]) {
    return threadsFunction<CreatePlaylistDto, Playlist>({
      iterationArray: createPlaylistDtos,
      maxThreads: 1,
      thread: this.importPlaylist.bind(this),
      options: {
        sleepBeforeRestart: 1000,
        service: 'OmekaImporterService',
        name: 'import playlists',
      },
    });
  }

  async importPlaylist(createPlaylistDto: CreatePlaylistDto) {
    return this.playlistsService.createOrUpdate(createPlaylistDto);
  }

  async importItems(createItemDtos: CreateItemDto[]) {
    return threadsFunction<CreateItemDto, Item>({
      iterationArray: createItemDtos,
      maxThreads: 10,
      thread: this.importItem.bind(this),
      options: {
        sleepBeforeRestart: 1000,
        service: 'OmekaImporterService',
        name: 'import items',
      },
    });
  }

  async importItem(createItemDto: CreateItemDto) {
    return this.itemsService.createOrUpdate(createItemDto);
  }
}
