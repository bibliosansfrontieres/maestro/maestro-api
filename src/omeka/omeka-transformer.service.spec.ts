import { Test, TestingModule } from '@nestjs/testing';
import { OmekaTransformerService } from './omeka-transformer.service';
import { ItemsService } from 'src/items/items.service';
import { itemsServiceMock } from 'src/utils/mocks/services/items-service.mock';

describe('OmekaTransformerService', () => {
  let service: OmekaTransformerService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [OmekaTransformerService, ItemsService],
    })
      .overrideProvider(ItemsService)
      .useValue(itemsServiceMock)
      .compile();

    service = module.get<OmekaTransformerService>(OmekaTransformerService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
