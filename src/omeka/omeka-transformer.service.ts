import { Injectable } from '@nestjs/common';
import { IOmekaItemFile } from './interfaces/omeka-item-file.interface';
import { CreateItemDto } from 'src/items/dto/create-item.dto';
import { md5 } from 'src/utils/md5.util';
import { ItemSourceEnum } from 'src/items/enums/item-source.enum';
import { Application } from 'src/applications/entities/application.entity';
import { ItemsService } from 'src/items/items.service';
import { DublinCoreType } from 'src/items/entities/dublin-core-type.entity';
import {
  getOmekaElement,
  getOmekaElements,
} from 'src/utils/get-omeka-element.util';
import { getDublinCoreType } from 'src/utils/get-dublin-core-type-omeka-map.util';
import { IOmekaFullProject } from './interfaces/omeka-full-project.interface';
import { IOmekaPackage } from './interfaces/omeka-package.interface';
import { CreatePlaylistDto } from 'src/playlists/dto/create-playlist.dto';
import { IOmekaItem } from './interfaces/omeka-item.interface';
import { CreateProjectDto } from 'src/projects/dto/create-project.dto';
import { getCategory } from 'src/utils/get-omeka-categories-map.util';
import { Category } from 'src/categories/entities/category.entity';
import { IMaestroFullProject } from './interfaces/maestro-full-project.interface';
import { IProject } from './interfaces/project.interface';
import { Project } from 'src/projects/entities/project.entity';
import { Playlist } from 'src/playlists/entities/playlist.entity';
import { Item } from 'src/items/entities/item.entity';

@Injectable()
export class OmekaTransformerService {
  constructor(private itemsService: ItemsService) {}

  omekaFullProjectToMaestro(
    omekaFullProject: IOmekaFullProject,
    application: Application,
    dublinCoreTypesMap: Map<string, DublinCoreType>,
    categoriesMap: Map<string, Category>,
  ): IMaestroFullProject {
    const items = this.omekaItemFilesToMaestro(
      omekaFullProject.itemFiles,
      application,
      dublinCoreTypesMap,
    );
    const { playlists, categories } = this.omekaPackagesToMaestro(
      omekaFullProject.packages,
      application,
      items,
      categoriesMap,
    );
    const project = this.omekaProjectToMaestro(
      omekaFullProject.project,
      playlists,
    );
    const applications = [application];
    return { project, applications, categories, playlists, items };
  }

  omekaProject(omekaProject: IOmekaItem) {
    const elementTexts = omekaProject.element_texts;
    const project: IProject = {
      id: omekaProject.id,
      source: ItemSourceEnum.OMEKA,
      url: omekaProject.url,
      title: getOmekaElement(50, elementTexts),
      description: getOmekaElement(41, elementTexts),
      date: getOmekaElement(40, elementTexts),
      languages: getOmekaElements(44, elementTexts).join(','),
      startDate: getOmekaElement(259, elementTexts),
      endDate: getOmekaElement(264, elementTexts),
      partners: getOmekaElement(260, elementTexts),
      location: getOmekaElement(261, elementTexts),
      device: getOmekaElement(262, elementTexts),
      projectManager: getOmekaElement(263, elementTexts),
    };
    return project;
  }

  omekaProjectToMaestro(
    omekaProject: IOmekaItem,
    createPlaylistDtos: CreatePlaylistDto[],
  ) {
    const elementTexts = omekaProject.element_texts;

    const createProjectDto: CreateProjectDto = {
      id: omekaProject.id,
      source: ItemSourceEnum.OMEKA,
      url: omekaProject.url,
      title: getOmekaElement(50, elementTexts),
      description: getOmekaElement(41, elementTexts),
      date: getOmekaElement(40, elementTexts),
      languages: getOmekaElements(44, elementTexts).join(','),
      startDate: getOmekaElement(259, elementTexts),
      endDate: getOmekaElement(264, elementTexts),
      partners: getOmekaElement(260, elementTexts),
      location: getOmekaElement(261, elementTexts),
      device: getOmekaElement(262, elementTexts),
      projectManager: getOmekaElement(263, elementTexts),
      playlistIds: createPlaylistDtos.map((playlist) => playlist.id),
      version: omekaProject.modified,
    };

    return createProjectDto;
  }

  projectToMaestro(project: Project): CreateProjectDto {
    return {
      id: project.id,
      source: project.source,
      url: project.url,
      title: project.title,
      description: project.description,
      date: project.date,
      languages: project.languages,
      startDate: project.startDate,
      endDate: project.endDate,
      partners: project.partners,
      location: project.location,
      device: project.device,
      projectManager: project.projectManager,
      playlistIds: project.playlists.map((playlist) => playlist.id.toString()),
      version: project.version,
    };
  }

  playlistsToMaestro(playlists: Playlist[]): CreatePlaylistDto[] {
    const maestroPlaylists: CreatePlaylistDto[] = [];
    for (const playlist of playlists) {
      maestroPlaylists.push({
        id: playlist.id,
        source: playlist.source,
        image: playlist.image,
        version: playlist.version,
        applicationId: playlist.application.id,
        categoryId: playlist.categories[0].id,
        size: playlist.size,
        itemIds: playlist.items.map((item) => item.id),
        playlistTranslations: playlist.playlistTranslations,
      });
    }
    return maestroPlaylists;
  }

  itemsToMaestro(items: Item[]): CreateItemDto[] {
    const maestroItems: CreateItemDto[] = [];
    for (const item of items) {
      maestroItems.push({
        applicationId: item.application.id,
        dublinCoreTypeId: item.dublinCoreItem.dublinCoreType.id,
        id: item.id,
        thumbnail: item.thumbnail,
        createdCountry: item.createdCountry,
        version: item.version,
        source: item.source,
        dublinCoreItemTranslations:
          item.dublinCoreItem.dublinCoreItemTranslations,
        file: item.file,
      });
    }

    return maestroItems;
  }

  omekaPackagesToMaestro(
    omekaPackages: IOmekaPackage[],
    application: Application,
    items: CreateItemDto[],
    categoriesMap: Map<string, Category>,
  ) {
    const categories: Category[] = [];
    const playlists: CreatePlaylistDto[] = [];
    for (const omekaPackage of omekaPackages) {
      const { category, playlist } = this.omekaPackageToMaestro(
        omekaPackage,
        application,
        items,
        categoriesMap,
      );
      playlists.push(playlist);
      if (!categories.includes(category)) {
        categories.push(category);
      }
    }
    return { playlists, categories };
  }

  omekaPackageToMaestro(
    omekaPackage: IOmekaPackage,
    application: Application,
    items: CreateItemDto[],
    categoriesMap: Map<string, Category>,
  ) {
    const itemIds = omekaPackage.contents.map((content) =>
      content.item_id.toString(),
    );
    const playlistItems = items.filter((item) =>
      itemIds.includes(item.id.toString()),
    );
    const itemsSizes = playlistItems.map((item) => parseInt(item.file.size));
    const size = itemsSizes.reduce((total, size) => total + size, 0);
    const category = getCategory(omekaPackage, categoriesMap);
    const createPlaylistDto: CreatePlaylistDto = {
      id: omekaPackage.id.toString(),
      source: ItemSourceEnum.OMEKA,
      image: '',
      applicationId: application.id,
      categoryId: category.id,
      size,
      itemIds,
      version: md5(omekaPackage.last_exportable_modification),
      playlistTranslations: [
        {
          title: omekaPackage.ideascube_name,
          description: omekaPackage.description,
          // TODO : languageIdentifier
          languageIdentifier: 'fra',
        },
      ],
    };
    return { playlist: createPlaylistDto, category };
  }

  omekaItemFilesToMaestro(
    omekaItemFiles: IOmekaItemFile[],
    application: Application,
    dublinCoreTypesMap: Map<string, DublinCoreType>,
  ) {
    const items: CreateItemDto[] = [];
    for (const omekaItemFile of omekaItemFiles) {
      const item = this.omekaItemFileToMaestro(
        omekaItemFile,
        application,
        dublinCoreTypesMap,
      );
      items.push(item);
    }
    return items;
  }

  omekaItemFileToMaestro(
    omekaItemFile: IOmekaItemFile,
    application: Application,
    dublinCoreTypesMap: Map<string, DublinCoreType>,
  ) {
    // TODO: dublin core type
    const dublinCoreType = getDublinCoreType(omekaItemFile, dublinCoreTypesMap);
    const fileNameSplit = omekaItemFile.file.filename.split('.');
    const extension = fileNameSplit[fileNameSplit.length - 1];
    const createItemDto: CreateItemDto = {
      id: omekaItemFile.id,
      thumbnail: omekaItemFile?.file?.file_urls?.thumbnail || undefined,
      createdCountry: '',
      version: md5(omekaItemFile.modified),
      source: ItemSourceEnum.OMEKA,
      applicationId: application.id,
      dublinCoreTypeId: dublinCoreType.id,
      file: {
        updatedAt: omekaItemFile.file.modified,
        name: omekaItemFile.file.filename,
        // TODO: path
        path: omekaItemFile?.file?.file_urls?.original || undefined,
        size: omekaItemFile.file.size.toString(),
        // TODO : duration
        duration: '',
        // TODO : pages
        pages: '',
        extension,
        mimeType: omekaItemFile.file.mime_type,
      },
      dublinCoreItemTranslations: [
        {
          title: getOmekaElement(50, omekaItemFile.element_texts),
          description: getOmekaElement(41, omekaItemFile.element_texts),
          creator: getOmekaElement(39, omekaItemFile.element_texts),
          publisher: getOmekaElement(45, omekaItemFile.element_texts),
          subject: getOmekaElement(49, omekaItemFile.element_texts),
          source: getOmekaElement(48, omekaItemFile.element_texts),
          // TODO : extent doesn't exists in Omeka
          extent: '',
          dateCreated: getOmekaElement(56, omekaItemFile.element_texts),
          // TODO : languageIdentifier
          languageIdentifier: 'eng',
        },
      ],
    };
    return createItemDto;
  }
}
