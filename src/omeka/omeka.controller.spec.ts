import { Test, TestingModule } from '@nestjs/testing';
import { OmekaController } from './omeka.controller';
import { OmekaService } from './omeka.service';
import { ConfigService } from '@nestjs/config';
import { configServiceMock } from 'src/utils/mocks/services/config-service.mock';
import { OmekaImporterService } from './omeka-importer.service';
import { omekaImporterServiceMock } from 'src/utils/mocks/services/omeka-importer-service.mock';
import { OmekaTransformerService } from './omeka-transformer.service';
import { omekaTransformerServiceMock } from 'src/utils/mocks/services/omeka-transformer-service.mock';
import { PlaylistsService } from 'src/playlists/playlists.service';
import { playlistsServiceMock } from 'src/utils/mocks/services/playlists-service.mock';

describe('OmekaController', () => {
  let controller: OmekaController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [OmekaController],
      providers: [
        OmekaService,
        ConfigService,
        OmekaImporterService,
        OmekaTransformerService,
        PlaylistsService,
      ],
    })
      .overrideProvider(ConfigService)
      .useValue(configServiceMock)
      .overrideProvider(OmekaImporterService)
      .useValue(omekaImporterServiceMock)
      .overrideProvider(OmekaTransformerService)
      .useValue(omekaTransformerServiceMock)
      .overrideProvider(PlaylistsService)
      .useValue(playlistsServiceMock)
      .compile();

    controller = module.get<OmekaController>(OmekaController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
