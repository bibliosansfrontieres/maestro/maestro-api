import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { OmekaService } from './omeka.service';
import { OmekaImporterService } from './omeka-importer.service';
import {
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { GetCheckProjectResponse } from './definitions/get-check-project.definition';
import { PostUpdatesRequest } from './definitions/post-updates.definition';

@Controller('omeka')
@ApiTags('omeka')
export class OmekaController {
  constructor(
    private readonly omekaService: OmekaService,
    private omekaImporterService: OmekaImporterService,
  ) {}

  @Get('check-project/:projectId')
  @ApiOperation({
    summary: 'Check if a project exists in Omeka',
  })
  @ApiOkResponse({ type: GetCheckProjectResponse })
  @ApiNotFoundResponse({ description: 'project not found' })
  async getCheckProject(@Param('projectId') projectId: string) {
    return this.omekaService.getCheckProject(+projectId);
  }

  @Get('project/:projectId')
  async getProject(@Param('projectId') projectId: string) {
    return this.omekaImporterService.importProject(+projectId);
  }

  @Post('updates')
  async postUpdates(@Body() postUpdatesRequest: PostUpdatesRequest) {
    return this.omekaService.postUpdates(postUpdatesRequest);
  }

  @Get('scrap')
  getScrap() {
    return this.omekaService.getAllOmeka();
  }
}
