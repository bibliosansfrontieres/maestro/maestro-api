import { Module } from '@nestjs/common';
import { OmekaService } from './omeka.service';
import { OmekaController } from './omeka.controller';
import { OmekaTransformerService } from './omeka-transformer.service';
import { ItemsModule } from 'src/items/items.module';
import { OmekaCheckerService } from './omeka-checker.service';
import { ApplicationsModule } from 'src/applications/applications.module';
import { OmekaImporterService } from './omeka-importer.service';
import { CategoriesModule } from 'src/categories/categories.module';
import { PlaylistsModule } from 'src/playlists/playlists.module';
import { ProjectsModule } from 'src/projects/projects.module';
import { S3Module } from 'src/s3/s3.module';

@Module({
  imports: [
    ItemsModule,
    ApplicationsModule,
    CategoriesModule,
    PlaylistsModule,
    ProjectsModule,
    S3Module,
  ],
  controllers: [OmekaController],
  providers: [
    OmekaService,
    OmekaTransformerService,
    OmekaCheckerService,
    OmekaImporterService,
  ],
  exports: [OmekaService, OmekaImporterService, OmekaTransformerService],
})
export class OmekaModule {}
