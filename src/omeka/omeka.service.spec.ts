import { Test, TestingModule } from '@nestjs/testing';
import { OmekaService } from './omeka.service';
import { ConfigService } from '@nestjs/config';
import { configServiceMock } from 'src/utils/mocks/services/config-service.mock';
import { OmekaTransformerService } from './omeka-transformer.service';
import { omekaTransformerServiceMock } from 'src/utils/mocks/services/omeka-transformer-service.mock';
import { PlaylistsService } from 'src/playlists/playlists.service';
import { playlistsServiceMock } from 'src/utils/mocks/services/playlists-service.mock';

describe('OmekaService', () => {
  let service: OmekaService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        OmekaService,
        ConfigService,
        OmekaTransformerService,
        PlaylistsService,
      ],
    })
      .overrideProvider(ConfigService)
      .useValue(configServiceMock)
      .overrideProvider(OmekaTransformerService)
      .useValue(omekaTransformerServiceMock)
      .overrideProvider(PlaylistsService)
      .useValue(playlistsServiceMock)
      .compile();

    service = module.get<OmekaService>(OmekaService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
