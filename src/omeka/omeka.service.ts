import { Injectable, Logger, NotFoundException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { IOmekaItem } from './interfaces/omeka-item.interface';
import { IOmekaPackageRelation } from './interfaces/omeka-package-relation.interface';
import { IOmekaPackage } from './interfaces/omeka-package.interface';
import { threadsFunction } from 'src/utils/thread-function.util';
import { IOmekaFile } from './interfaces/omeka-file.interface';
import { IOmekaItemFile } from './interfaces/omeka-item-file.interface';
import { IOmekaFullProject } from './interfaces/omeka-full-project.interface';
import { IOmekaProjectPackagesRelations } from './interfaces/omeka-project-packages-relations.interface';
import axios from 'axios';
import { OmekaTransformerService } from './omeka-transformer.service';
import { GetCheckProjectResponse } from './definitions/get-check-project.definition';
import { PostUpdatesRequest } from './definitions/post-updates.definition';
import { PlaylistsService } from 'src/playlists/playlists.service';
import { In } from 'typeorm';

@Injectable()
export class OmekaService {
  private apiUrl: string;
  private apiKey: string;
  private apiFetchingThreads: number;

  constructor(
    private configService: ConfigService,
    private omekaTransformerService: OmekaTransformerService,
    private playlistsService: PlaylistsService,
  ) {
    this.apiUrl = this.configService.get('OMEKA_API_URL');
    this.apiKey = this.configService.get('OMEKA_API_KEY');
    this.apiFetchingThreads = this.configService.get(
      'OMEKA_API_FETCHING_THREADS',
    );
  }

  async postUpdates(postUpdatesRequest: PostUpdatesRequest) {
    const playlists = await this.playlistsService.find({
      where: {
        id: In(postUpdatesRequest.playlists.map((playlist) => playlist.id)),
      },
      relations: { items: true },
    });
    const playlistsToUpdate: string[] = [];
    for (const playlist of playlists) {
      const olipPlaylist = postUpdatesRequest.playlists.find(
        (p) => p.id === playlist.id,
      );
      if (!olipPlaylist) continue;
      if (
        olipPlaylist.version !== playlist.version ||
        olipPlaylist.length !== playlist.items.length
      ) {
        playlistsToUpdate.push(playlist.id);
      }
      for (const item of playlist.items) {
        const olipItem = postUpdatesRequest.items.find((i) => i.id === item.id);
        if (!olipItem) continue;
        if (olipItem.version !== item.version) {
          if (!playlistsToUpdate.includes(playlist.id)) {
            playlistsToUpdate.push(playlist.id);
          }
        }
      }
    }

    return {
      playlistIds: playlistsToUpdate,
    };
  }

  async api<T>(endpoint: string, query?: string[]): Promise<T | undefined> {
    try {
      query = !query ? [] : query;
      query.push(`key=${this.apiKey}`);
      const result = await axios.get(
        `${this.apiUrl}/${endpoint}?${query.join('&')}`,
        {},
      );
      return result.data;
    } catch (error) {
      console.error(error);
      return undefined;
    }
  }

  async getCheckProject(omekaProjectId: number) {
    const omekaProject = await this.getProject(omekaProjectId);
    const project = this.omekaTransformerService.omekaProject(omekaProject);
    return new GetCheckProjectResponse(project);
  }

  ////////////////////
  // Omeka scraping //
  ////////////////////

  async getAllOmeka() {
    Logger.log('Fetching all Omeka data...', 'OmekaService');

    // Get all Omeka projects
    Logger.log('Fetching all Omeka projects...', 'OmekaService');
    const projects = await this.getProjects();

    // Get only project ids
    const projectsIds = this.getProjectsIds(projects);

    // Get all projects -> packages relations
    Logger.log(
      `Fetching ${projectsIds.length} Omeka projects packages relations...`,
      'OmekaService',
    );
    const projectsPackagesRelations =
      await this.getProjectsPackagesRelations(projectsIds);

    // Get unique packages ids
    const packagesIds = this.getUniquePackagesIds(projectsPackagesRelations);

    // Get all packages
    Logger.log(
      `Fetching ${packagesIds.length} Omeka packages...`,
      'OmekaService',
    );
    const packages = await this.getPackages(packagesIds);

    // Get unique items ids
    const itemsIds = this.getUniqueItemsIds(packages);

    // Get all items and files
    Logger.log(
      `Fetching ${itemsIds.length} Omeka items and files...`,
      'OmekaService',
    );
    const itemFiles = await this.getOmekaItemFiles(itemsIds);

    Logger.log('All Omeka data fetched', 'OmekaService');

    return {
      projects,
      packages,
      itemFiles,
    };
  }

  getUniquePackagesIds(
    projectsPackagesRelations: IOmekaProjectPackagesRelations[],
  ) {
    const packagesIds: number[] = [];
    for (const projectPackagesRelations of projectsPackagesRelations) {
      for (const packageRelation of projectPackagesRelations.packagesRelations) {
        if (packagesIds.includes(packageRelation.package_id)) continue;
        packagesIds.push(packageRelation.package_id);
      }
    }
    return packagesIds;
  }

  getProjectsIds(projects: IOmekaItem[]) {
    return projects.map((project) => project.id);
  }

  async getProjectsPackagesRelations(projectsIds: number[]) {
    return threadsFunction<number, IOmekaProjectPackagesRelations>({
      iterationArray: projectsIds,
      maxThreads: this.apiFetchingThreads,
      thread: this.getProjectPackagesRelations.bind(this),
      options: {
        sleepBeforeRestart: 1000,
        service: 'OmekaService',
        name: 'get Omeka projects packages relations',
      },
    });
  }

  async getPackageRelationsPage(itemId: number, page?: number) {
    if (page <= 0 || page === undefined) {
      page = 1;
    }

    const query = [];
    query.push(`item_id=${itemId}`);
    query.push(`page=${page}`);

    return (await this.api(
      `package_relations`,
      query,
    )) as IOmekaPackageRelation[];
  }

  async getProjectPackagesRelations(projectId: number) {
    const packageRelations: IOmekaPackageRelation[] = [];
    let page = 1;
    let pageResults = [];
    do {
      pageResults = await this.getPackageRelationsPage(projectId, page);
      if (pageResults.length > 0) {
        packageRelations.push(...pageResults);
      }
      page++;
    } while (pageResults.length !== 0);
    Logger.debug(`Made ${page - 1} calls to get package relations`);
    return { projectId, packageRelations };
  }

  async getProjects() {
    const projects: IOmekaItem[] = [];
    let page = 1;
    let pageResults = [];
    do {
      pageResults = await this.getItemsPage(page, 19);
      if (pageResults.length > 0) {
        projects.push(...pageResults);
      }
      page++;
    } while (pageResults.length !== 0);
    Logger.debug(`Made ${page - 1} calls to get projects`);

    const filterProjects = [];
    for (let i = 0; i < projects.length / 2; i++) {
      filterProjects.push(projects[i]);
    }
    return filterProjects;
  }

  async getItemsPage(page?: number, itemTypeId?: number) {
    if (page <= 0 || page === undefined) {
      page = 1;
    }
    const query = [];
    if (itemTypeId) {
      query.push(`item_type=${itemTypeId}`);
    }
    query.push(`page=${page}`);
    return (await this.api(`items`, query)) as IOmekaItem[];
  }

  ////////////////////
  // Omeka projects //
  ////////////////////

  async getProject(omekaProjectId: number): Promise<IOmekaItem> {
    const omekaProject = await this.api<IOmekaItem>(`items/${omekaProjectId}`);

    if (!omekaProject || omekaProject.item_type.id !== 19) {
      throw new NotFoundException('project not found');
    }

    return omekaProject;
  }

  async getFullProject(omekaProjectId: number): Promise<IOmekaFullProject> {
    const omekaProject = await this.getProject(omekaProjectId);
    const projectPackagesIds = await this.getProjectPackagesIds(omekaProjectId);
    const projectPackages = await this.getPackages(projectPackagesIds);
    const itemsIds = this.getUniqueItemsIds(projectPackages);
    const omekaItemFiles = await this.getOmekaItemFiles(itemsIds);
    return {
      project: omekaProject,
      packages: projectPackages,
      itemFiles: omekaItemFiles,
    };
  }

  /////////////////
  // Omeka items //
  /////////////////

  async getOmekaItemFiles(itemsIds: number[]) {
    return threadsFunction<number, IOmekaItemFile>({
      iterationArray: itemsIds,
      maxThreads: this.apiFetchingThreads,
      thread: this.getOmekaItemFile.bind(this),
      options: {
        sleepBeforeRestart: 1000,
        service: 'OmekaService',
        name: 'get Omeka items and files',
      },
    });
  }

  async getOmekaItemFile(itemId: number): Promise<IOmekaItemFile> {
    try {
      const omekaItem = await this.api<IOmekaItem>(`items/${itemId}`);
      if (!omekaItem) return undefined;
      const fileQuery = [`item=${itemId}`];
      const omekaFiles = await this.api<IOmekaFile[]>(`files`, fileQuery);
      if (!omekaFiles) return undefined;
      const omekaFile = omekaFiles[0];
      return { ...omekaItem, file: omekaFile };
    } catch (error) {
      console.error(error);
      return undefined;
    }
  }

  ////////////////////
  // Omeka packages //
  ////////////////////

  async getPackages(packagesIds: number[]) {
    return threadsFunction<number, IOmekaPackage>({
      iterationArray: packagesIds,
      maxThreads: this.apiFetchingThreads,
      thread: this.getPackage.bind(this),
      options: {
        sleepBeforeRestart: 1000,
        service: 'OmekaService',
        name: 'get Omeka packages',
      },
    });
  }

  async getPackage(packageId: number): Promise<IOmekaPackage> {
    try {
      const omekaPackage = await this.api<IOmekaPackage>(
        `packages/${packageId}`,
      );
      if (!omekaPackage) return undefined;
      return omekaPackage;
    } catch (error) {
      console.error(error);
      return undefined;
    }
  }

  async getProjectPackagesIds(omekaProjectId: number): Promise<number[]> {
    const packageRelations: IOmekaPackageRelation[] = [];
    let page = 1;
    let pageResults = [];
    do {
      pageResults = await this.getPackageRelationsPage(omekaProjectId, page);
      if (pageResults.length > 0) {
        packageRelations.push(...pageResults);
      }
      page++;
    } while (pageResults.length !== 0);
    Logger.debug(`Made ${page - 1} calls to get package relations`);
    return packageRelations.map(
      (packageRelation) => packageRelation.package_id,
    );
  }

  getUniqueItemsIds(omekaPackages: IOmekaPackage[]) {
    const itemsIds: number[] = [];
    for (const omekaPackage of omekaPackages) {
      for (const omekaItem of omekaPackage.contents) {
        if (itemsIds.includes(omekaItem.item_id)) continue;
        itemsIds.push(omekaItem.item_id);
      }
    }
    return itemsIds;
  }
}
