import { PermissionNameEnum } from '../enums/permission-name.enum';

export class CreatePermissionDto {
  name: PermissionNameEnum;
}
