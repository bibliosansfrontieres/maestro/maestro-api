import { Column, Entity, ManyToMany, PrimaryGeneratedColumn } from 'typeorm';
import { PermissionNameEnum } from '../enums/permission-name.enum';
import { Role } from 'src/roles/entities/role.entity';
import { CreatePermissionDto } from '../dto/create-permission.dto';

@Entity()
export class Permission {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: PermissionNameEnum;

  @ManyToMany(() => Role, (role) => role.permissions)
  roles: Role[];

  constructor(createPermissionDto: CreatePermissionDto) {
    if (!createPermissionDto) return;
    this.name = createPermissionDto.name;
  }
}
