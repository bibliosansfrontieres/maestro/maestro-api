import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Permission } from './entities/permission.entity';
import { FindManyOptions, FindOneOptions, Repository } from 'typeorm';
import { CreatePermissionDto } from './dto/create-permission.dto';

@Injectable()
export class PermissionsService {
  constructor(
    @InjectRepository(Permission)
    private permissionRepository: Repository<Permission>,
  ) {}

  create(createPermissionDto: CreatePermissionDto) {
    const permission = new Permission(createPermissionDto);
    return this.permissionRepository.save(permission);
  }

  count(options?: FindManyOptions<Permission>) {
    return this.permissionRepository.count(options);
  }

  findOne(options?: FindOneOptions<Permission>) {
    return this.permissionRepository.findOne(options);
  }
}
