import { CreateItemDto } from 'src/items/dto/create-item.dto';
import { Item } from 'src/items/entities/item.entity';

export class GetPlaylistItemsResponse {
  items: CreateItemDto[];

  constructor(items: Item[]) {
    this.items = [];
    for (const item of items) {
      const createItemDto: CreateItemDto = {
        id: item.id,
        thumbnail: item.thumbnail,
        createdCountry: item.createdCountry,
        version: item.version,
        source: item.source,
        dublinCoreItemTranslations:
          item.dublinCoreItem.dublinCoreItemTranslations,
        file: {
          updatedAt: item.file.updatedAt,
          duration: item.file.duration,
          size: item.file.size,
          mimeType: item.file.mimeType,
          extension: item.file.extension,
          pages: item.file.pages,
          path: item.file.path,
          name: item.file.name,
        },
        applicationId: item.application.id,
        dublinCoreTypeId: item?.dublinCoreItem?.dublinCoreType?.id || undefined,
      };
      this.items.push(createItemDto);
    }
  }
}
