import { CreatePlaylistTranslationDto } from '../dto/create-playlist-translation.dto';
import { CreatePlaylistDto } from '../dto/create-playlist.dto';
import { Playlist } from '../entities/playlist.entity';
import { PlaylistSourceEnum } from '../enums/playlist-source.enum';

export class GetPlaylistResponse implements CreatePlaylistDto {
  id: string;
  source: PlaylistSourceEnum;
  image: string;
  version?: string;
  applicationId: string;
  categoryId?: number;
  size: number;
  itemIds: string[];
  playlistTranslations: CreatePlaylistTranslationDto[];

  constructor(playlist: Playlist) {
    this.id = playlist.id;
    this.source = playlist.source;
    this.image = playlist.image;
    this.version = playlist.version;
    this.applicationId = playlist.application.id;
    this.categoryId = playlist?.categories[0]?.id || undefined;
    this.size = playlist.size;
    this.itemIds = playlist.items.map((item) => item.id.toString());
    this.playlistTranslations = playlist.playlistTranslations;
  }
}
