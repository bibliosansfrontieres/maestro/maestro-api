import { PageOptionsRequest } from 'src/utils/pagination/dto/page-options.dto';
import { PageResponse } from 'src/utils/pagination/dto/page-response';
import { PageOptions } from 'src/utils/pagination/page-options';
import { Playlist } from '../entities/playlist.entity';
import { IsOptional, IsString } from 'class-validator';

export class GetUninstalledPlaylistsRequest extends PageOptionsRequest {
  @IsString()
  @IsOptional()
  playlistIds?: string;

  @IsString()
  @IsOptional()
  applicationIds?: string;

  @IsString()
  @IsOptional()
  query?: string;

  @IsOptional()
  @IsString()
  languageIdentifier?: string;
}

export class GetUninstalledPlaylistsResponse extends PageResponse<Playlist> {
  constructor(groups: Playlist[], count: number, pageOptions: PageOptions) {
    super(groups, count, pageOptions);
  }
}
