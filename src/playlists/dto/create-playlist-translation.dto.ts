export class CreatePlaylistTranslationDto {
  title: string;
  description: string;
  languageIdentifier: string;
}
