import { PlaylistSourceEnum } from '../enums/playlist-source.enum';
import { CreatePlaylistTranslationDto } from './create-playlist-translation.dto';

export class CreatePlaylistDto {
  id: string;
  source: PlaylistSourceEnum;
  image: string;
  version?: string;
  applicationId: string;
  categoryId?: number;
  size: number;
  itemIds: string[];
  playlistTranslations: CreatePlaylistTranslationDto[];
}
