import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Playlist } from './playlist.entity';
import { CreatePlaylistTranslationDto } from '../dto/create-playlist-translation.dto';

@Entity()
export class PlaylistTranslation {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  title: string;

  @Column()
  description: string;

  @Column()
  languageIdentifier: string;

  @ManyToOne(() => Playlist, (playlist) => playlist.playlistTranslations, {
    onDelete: 'CASCADE',
  })
  playlist: Playlist;

  constructor(createPlaylistTranslationDto: CreatePlaylistTranslationDto) {
    if (!createPlaylistTranslationDto) return;
    this.title = createPlaylistTranslationDto.title;
    this.description = createPlaylistTranslationDto.description;
    this.languageIdentifier = createPlaylistTranslationDto.languageIdentifier;
  }
}
