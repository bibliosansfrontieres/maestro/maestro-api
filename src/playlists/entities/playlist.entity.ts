import { Item } from 'src/items/entities/item.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  PrimaryColumn,
  UpdateDateColumn,
} from 'typeorm';
import { PlaylistTranslation } from './playlist-translation.entity';
import { Category } from 'src/categories/entities/category.entity';
import { CreatePlaylistDto } from '../dto/create-playlist.dto';
import { Application } from 'src/applications/entities/application.entity';
import { PlaylistSourceEnum } from '../enums/playlist-source.enum';
import { Project } from 'src/projects/entities/project.entity';

@Entity()
export class Playlist {
  @PrimaryColumn()
  id: string;

  @Column()
  image: string;

  @Column()
  source: PlaylistSourceEnum;

  @Column({ nullable: true })
  version: string | null;

  @Column()
  size: number;

  @ManyToOne(() => Application, (application) => application.playlists)
  application: Application;

  @ManyToMany(() => Item, (item) => item.playlists)
  @JoinTable()
  items: Item[];

  @OneToMany(
    () => PlaylistTranslation,
    (playlistTranslation) => playlistTranslation.playlist,
    { cascade: true },
  )
  playlistTranslations: PlaylistTranslation[];

  @ManyToMany(() => Category, (category) => category.playlists)
  @JoinTable()
  categories: Category[];

  @ManyToMany(() => Project, (project) => project.playlists)
  projects: Project[];

  @CreateDateColumn()
  createdAt: string;

  @UpdateDateColumn()
  updatedAt: string;

  constructor(createPlaylistDto: CreatePlaylistDto, application: Application) {
    if (!createPlaylistDto) return;
    this.id = createPlaylistDto.id;
    this.application = application;
    this.image = createPlaylistDto.image;
    this.items = [];
    this.version = createPlaylistDto.version || null;
    this.source = createPlaylistDto.source;
    this.size = createPlaylistDto.size;
  }
}
