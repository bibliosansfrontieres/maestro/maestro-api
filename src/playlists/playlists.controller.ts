import { Controller, Get, Param, Query } from '@nestjs/common';
import { PlaylistsService } from './playlists.service';
import { GetUninstalledPlaylistsRequest } from './definitions/get-uninstalled-playlists.definition';
import { ApiTags } from '@nestjs/swagger';

@Controller('playlists')
@ApiTags('playlists')
export class PlaylistsController {
  constructor(private readonly playlistsService: PlaylistsService) {}

  @Get('uninstalled')
  getUninstalledPlaylists(
    @Query() getUninstalledPlaylistsRequest: GetUninstalledPlaylistsRequest,
  ) {
    return this.playlistsService.getUninstalledPlaylists(
      getUninstalledPlaylistsRequest,
    );
  }

  @Get(':id/items')
  getPlaylistItems(@Param('id') id: string) {
    return this.playlistsService.getPlaylistItems(id);
  }

  @Get(':id')
  getPlaylist(@Param('id') id: string) {
    return this.playlistsService.getPlaylist(id);
  }
}
