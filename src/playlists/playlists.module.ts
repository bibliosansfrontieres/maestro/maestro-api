import { Module, forwardRef } from '@nestjs/common';
import { PlaylistsService } from './playlists.service';
import { PlaylistsController } from './playlists.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Playlist } from './entities/playlist.entity';
import { PlaylistTranslation } from './entities/playlist-translation.entity';
import { ApplicationsModule } from 'src/applications/applications.module';
import { CategoriesModule } from 'src/categories/categories.module';
import { ItemsModule } from 'src/items/items.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([Playlist, PlaylistTranslation]),
    forwardRef(() => ApplicationsModule),
    CategoriesModule,
    ItemsModule,
  ],
  controllers: [PlaylistsController],
  providers: [PlaylistsService],
  exports: [PlaylistsService],
})
export class PlaylistsModule {}
