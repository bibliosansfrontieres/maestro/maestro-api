import { Test, TestingModule } from '@nestjs/testing';
import { PlaylistsService } from './playlists.service';
import { Repository } from 'typeorm';
import { Playlist } from './entities/playlist.entity';
import { getRepositoryToken } from '@nestjs/typeorm';
import { ApplicationsService } from 'src/applications/applications.service';
import { applicationsServiceMock } from 'src/utils/mocks/services/applications-service.mock';
import { CategoriesService } from 'src/categories/categories.service';
import { categoriesServiceMock } from 'src/utils/mocks/services/categories-service.mock';
import { ItemsService } from 'src/items/items.service';
import { itemsServiceMock } from 'src/utils/mocks/services/items-service.mock';

describe('PlaylistsService', () => {
  let service: PlaylistsService;
  let playlistRepository: Repository<Playlist>;

  const PLAYLIST_REPOSITORY_TOKEN = getRepositoryToken(Playlist);

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        PlaylistsService,
        ApplicationsService,
        CategoriesService,
        ItemsService,
        { provide: PLAYLIST_REPOSITORY_TOKEN, useClass: Repository<Playlist> },
      ],
    })
      .overrideProvider(ApplicationsService)
      .useValue(applicationsServiceMock)
      .overrideProvider(CategoriesService)
      .useValue(categoriesServiceMock)
      .overrideProvider(ItemsService)
      .useValue(itemsServiceMock)
      .compile();

    service = module.get<PlaylistsService>(PlaylistsService);
    playlistRepository = module.get<Repository<Playlist>>(
      PLAYLIST_REPOSITORY_TOKEN,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
    expect(playlistRepository).toBeDefined();
  });
});
