import {
  Inject,
  Injectable,
  NotFoundException,
  forwardRef,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Playlist } from './entities/playlist.entity';
import { FindManyOptions, FindOneOptions, In, Repository } from 'typeorm';
import { CreatePlaylistDto } from './dto/create-playlist.dto';
import { ApplicationsService } from 'src/applications/applications.service';
import { CategoriesService } from 'src/categories/categories.service';
import { PlaylistTranslation } from './entities/playlist-translation.entity';
import { ItemsService } from 'src/items/items.service';
import { getManyAndCount } from 'src/utils/get-many-and-count.util';
import { PageOptions } from 'src/utils/pagination/page-options';
import {
  GetUninstalledPlaylistsRequest,
  GetUninstalledPlaylistsResponse,
} from './definitions/get-uninstalled-playlists.definition';
import { GetPlaylistResponse } from './definitions/get-playlist.definition';
import { GetPlaylistItemsResponse } from './definitions/get-playlist-items.definition';
import { QueryHelper } from 'src/utils/query-helpers.util';

@Injectable()
export class PlaylistsService {
  constructor(
    @InjectRepository(Playlist)
    private playlistRepository: Repository<Playlist>,
    @Inject(forwardRef(() => ApplicationsService))
    private applicationsService: ApplicationsService,
    private categoriesService: CategoriesService,
    private itemsService: ItemsService,
  ) {}

  count(options?: FindManyOptions<Playlist>) {
    return this.playlistRepository.count(options);
  }

  async getPlaylist(id: string) {
    const playlist = await this.findOne({
      where: { id },
      relations: {
        categories: true,
        application: true,
        playlistTranslations: true,
        items: true,
      },
    });

    if (!playlist) {
      throw new NotFoundException('playlist not found');
    }

    return new GetPlaylistResponse(playlist);
  }

  async getPlaylistItems(id: string) {
    const playlist = await this.findOne({
      where: { id },
      relations: {
        items: {
          application: true,
          file: true,
          dublinCoreItem: {
            dublinCoreType: true,
            dublinCoreItemTranslations: true,
          },
        },
      },
    });

    if (!playlist) {
      throw new NotFoundException('playlist not found');
    }

    return new GetPlaylistItemsResponse(playlist.items);
  }

  async getUninstalledPlaylists(
    getUninstalledRequest: GetUninstalledPlaylistsRequest,
  ) {
    const playlistIds = [];
    if (getUninstalledRequest.playlistIds) {
      playlistIds.push(...getUninstalledRequest.playlistIds.split(','));
    }
    const pageOptions = new PageOptions(getUninstalledRequest);

    const playlistsQueryBuilder = this.playlistRepository
      .createQueryBuilder('playlist')
      .innerJoinAndSelect('playlist.application', 'application')
      .innerJoinAndSelect('playlist.items', 'item')
      .innerJoinAndSelect(
        'playlist.playlistTranslations',
        'playlistTranslation',
      )
      .andWhere('playlist.id NOT IN (:...playlistIds)', {
        playlistIds,
      });

    if (getUninstalledRequest.applicationIds) {
      playlistsQueryBuilder.andWhere('application.id IN (:...applicationIds)', {
        applicationIds: getUninstalledRequest.applicationIds.split(','),
      });
    }
    const queryHelper = new QueryHelper(playlistsQueryBuilder);
    queryHelper.pagination(pageOptions, 'playlist.id');

    if (getUninstalledRequest.query) {
      queryHelper.andWhereQuery(getUninstalledRequest.query, [
        'playlistTranslation.title',
        'playlistTranslation.description',
      ]);
    }

    const [count, playlists] = await getManyAndCount<Playlist>(
      playlistsQueryBuilder,
      ['playlist.id'],
    );

    // TODO : manage languageIdentifier for translations

    return new GetUninstalledPlaylistsResponse(playlists, count, pageOptions);
  }

  async create(createPlaylistDto: CreatePlaylistDto) {
    const application = await this.applicationsService.findOne({
      where: { id: createPlaylistDto.applicationId },
    });
    const items = await this.itemsService.find({
      where: { id: In(createPlaylistDto.itemIds) },
    });

    const playlistTranslations = [];
    for (const playlistTranslationDto of createPlaylistDto.playlistTranslations) {
      const playlistTranslation = new PlaylistTranslation(
        playlistTranslationDto,
      );
      playlistTranslations.push(playlistTranslation);
    }

    const playlist = new Playlist(createPlaylistDto, application);
    if (createPlaylistDto.categoryId) {
      const category = await this.categoriesService.findOne({
        where: { id: createPlaylistDto.categoryId },
      });
      playlist.categories = [category];
    }
    playlist.playlistTranslations = playlistTranslations;
    playlist.items = items;
    return this.playlistRepository.save(playlist);
  }

  async update(playlist: Playlist, createPlaylistDto: CreatePlaylistDto) {
    const category = await this.categoriesService.findOne({
      where: { id: createPlaylistDto.categoryId },
    });
    const application = await this.applicationsService.findOne({
      where: { id: createPlaylistDto.applicationId },
    });
    const items = await this.itemsService.find({
      where: { id: In(createPlaylistDto.itemIds) },
    });

    playlist.categories = [category];
    playlist.application = application;
    playlist.items = items;
    playlist.version = createPlaylistDto.version;

    for (const playlistTranslation of playlist.playlistTranslations) {
      const findPlaylistTranslationDto =
        createPlaylistDto.playlistTranslations.find(
          (playlistTranslationDto) =>
            playlistTranslationDto.languageIdentifier ===
            playlistTranslation.languageIdentifier,
        );

      // TODO : delete playlist translation
      /*if (!findPlaylistTranslationDto) {
        await this.playlistRepository.delete(playlistTranslation);
        continue;
      }*/

      playlistTranslation.description = findPlaylistTranslationDto.description;
      playlistTranslation.title = findPlaylistTranslationDto.title;
    }

    for (const playlistTranslationDto of createPlaylistDto.playlistTranslations) {
      const findPlaylistTranslation = playlist.playlistTranslations.find(
        (playlistTranslation) =>
          playlistTranslation.languageIdentifier ===
          playlistTranslationDto.languageIdentifier,
      );
      if (findPlaylistTranslation) continue;
      const playlistTranslation = new PlaylistTranslation(
        playlistTranslationDto,
      );
      playlist.playlistTranslations.push(playlistTranslation);
    }

    playlist.image =
      createPlaylistDto.image !== ''
        ? createPlaylistDto.image || playlist.image
        : playlist.image;
    playlist.source = createPlaylistDto.source;
    playlist.version = createPlaylistDto.version;
    playlist.size = createPlaylistDto.size;

    return this.playlistRepository.save(playlist);
  }

  async createOrUpdate(createPlaylistDto: CreatePlaylistDto) {
    const playlist = await this.findOne({
      where: { id: createPlaylistDto.id },
      relations: { playlistTranslations: true },
    });
    if (!playlist) {
      return this.create(createPlaylistDto);
    } else {
      return this.update(playlist, createPlaylistDto);
    }
  }

  find(options?: FindManyOptions<Playlist>) {
    return this.playlistRepository.find(options);
  }

  findOne(options?: FindOneOptions<Playlist>) {
    return this.playlistRepository.findOne(options);
  }

  save(playlist: Playlist) {
    return this.playlistRepository.save(playlist);
  }
}
