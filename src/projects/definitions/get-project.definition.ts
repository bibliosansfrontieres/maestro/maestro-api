import { Project } from '../entities/project.entity';
import { ApiProperty } from '@nestjs/swagger';
import { ItemSourceEnum } from 'src/items/enums/item-source.enum';
import { VirtualMachine } from 'src/virtual-machines/entities/virtual-machine.entity';
import { ProjectError } from '../entities/project-error.entity';

export class GetProjectResponse {
  @ApiProperty({ example: 1 })
  id: number;

  @ApiProperty({ example: ItemSourceEnum.OMEKA, enum: ItemSourceEnum })
  source: ItemSourceEnum;

  @ApiProperty({ example: 'http://omeka.tm.bsf-intranet.org/items/show/1' })
  url: string;

  @ApiProperty({ example: 'Project title' })
  title: string;

  @ApiProperty({ example: 'Project description' })
  description: string;

  @ApiProperty({ example: '01/01/2024' })
  date: string;

  @ApiProperty({ example: 'fra' })
  languages: string;

  @ApiProperty({ example: '01/2025' })
  startDate: string;

  @ApiProperty({ example: '01/2027' })
  endDate: string;

  @ApiProperty({ example: 'A partner' })
  partners: string;

  @ApiProperty({ example: 'Somewhere... , over the rainbow' })
  location: string;

  @ApiProperty({ example: '20 ideascubes' })
  device: string;

  @ApiProperty({ example: 'Manon Tanguy' })
  projectManager: string;

  @ApiProperty()
  virtualMachines: VirtualMachine[];

  @ApiProperty()
  projectErrors: ProjectError[];

  constructor(project: Project) {
    this.id = project.id;
    this.source = project.source;
    this.url = project.url;
    this.title = project.title;
    this.description = project.description;
    this.date = project.date;
    this.languages = project.languages;
    this.startDate = project.startDate;
    this.endDate = project.endDate;
    this.partners = project.partners;
    this.location = project.location;
    this.device = project.device;
    this.projectManager = project.projectManager;
    this.virtualMachines = project.virtualMachines;
    this.projectErrors = project.projectErrors;
  }
}
