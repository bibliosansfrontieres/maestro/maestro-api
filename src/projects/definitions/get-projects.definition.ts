import { PageOptionsRequest } from 'src/utils/pagination/dto/page-options.dto';
import { PageResponse } from 'src/utils/pagination/dto/page-response';
import { PageOptions } from 'src/utils/pagination/page-options';
import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsEnum, IsOptional, IsString } from 'class-validator';
import { Project } from '../entities/project.entity';
import { ProjectOrderByEnum } from '../enums/project-order-by.enum';
import { ProjectStatusEnum } from '../enums/project-status.enum';

export class GetProjectsRequest extends PageOptionsRequest {
  @ApiPropertyOptional({ example: '12/30/2021,12/31/2021' })
  @IsOptional()
  @IsString()
  updatedDateRange?: string;

  @ApiPropertyOptional({ example: 'search' })
  @IsOptional()
  @IsString()
  query?: string;

  @ApiPropertyOptional({
    example: ProjectOrderByEnum.ID,
    enum: ProjectOrderByEnum,
    default: ProjectOrderByEnum.ID,
  })
  @IsOptional()
  @IsEnum(ProjectOrderByEnum)
  orderBy?: ProjectOrderByEnum;

  @ApiPropertyOptional({
    example: ProjectStatusEnum.READY,
    enum: ProjectStatusEnum,
  })
  @IsOptional()
  @IsString()
  statuses?: ProjectStatusEnum;
}

export interface IProjectResponse extends Project {
  playlistsCount: number;
  itemsCount: number;
}

export class GetProjectsResponse extends PageResponse<IProjectResponse> {
  constructor(
    data: IProjectResponse[],
    count: number,
    pageOptions: PageOptions,
  ) {
    super(data, count, pageOptions);
  }
}
