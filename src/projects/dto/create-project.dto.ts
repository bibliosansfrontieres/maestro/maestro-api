import { ProjectSourceEnum } from '../enums/project-source.enum';

export class CreateProjectDto {
  id: number;
  source: ProjectSourceEnum;
  url: string;
  title: string;
  description: string;
  date: string;
  languages: string;
  startDate: string;
  endDate: string;
  partners: string;
  location: string;
  device: string;
  projectManager: string;
  playlistIds: string[];
  version: string;
}
