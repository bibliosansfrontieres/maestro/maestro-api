import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { ErrorDto } from 'src/omeka/dto/error.dto';
import { Project } from './project.entity';
import { VirtualMachineErrorEntityEnum } from 'src/virtual-machines/enums/virtual-machine-error-entity.enum';

@Entity()
export class ProjectError {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  entity: VirtualMachineErrorEntityEnum;

  @Column()
  entityId: number;

  @Column()
  message: string;

  @ManyToOne(() => Project, (project) => project.projectErrors)
  project: Project;

  constructor(errorDto: ErrorDto, project: Project) {
    if (!errorDto || !project) return;
    this.entity = errorDto.type.toUpperCase() as VirtualMachineErrorEntityEnum;
    this.entityId = errorDto.id;
    this.message = errorDto.message;
    this.project = project;
  }
}
