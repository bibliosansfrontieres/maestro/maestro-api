import {
  Column,
  CreateDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  OneToMany,
  PrimaryColumn,
  UpdateDateColumn,
} from 'typeorm';
import { ProjectSourceEnum } from '../enums/project-source.enum';
import { CreateProjectDto } from '../dto/create-project.dto';
import { Playlist } from 'src/playlists/entities/playlist.entity';
import { ProjectStatusEnum } from '../enums/project-status.enum';
import { VirtualMachine } from 'src/virtual-machines/entities/virtual-machine.entity';
import { ProjectError } from './project-error.entity';

@Entity()
export class Project {
  @PrimaryColumn()
  id: number;

  @Column()
  source: ProjectSourceEnum;

  @Column()
  status: ProjectStatusEnum;

  @Column()
  url: string;

  @Column()
  title: string;

  @Column()
  description: string;

  @Column()
  date: string;

  @Column()
  languages: string;

  @Column()
  startDate: string;

  @Column()
  endDate: string;

  @Column()
  partners: string;

  @Column()
  location: string;

  @Column()
  device: string;

  @Column()
  projectManager: string;

  @Column()
  version: string;

  @ManyToMany(() => Playlist, (playlist) => playlist.projects)
  @JoinTable()
  playlists: Playlist[];

  @OneToMany(() => VirtualMachine, (virtualMachine) => virtualMachine.project)
  virtualMachines: VirtualMachine[];

  @OneToMany(() => ProjectError, (projectError) => projectError.project)
  projectErrors: ProjectError[];

  @CreateDateColumn()
  createdAt: string;

  @UpdateDateColumn()
  updatedAt: string;

  constructor(createProjectDto: CreateProjectDto) {
    if (!createProjectDto) return;
    this.id = createProjectDto.id;
    this.source = createProjectDto.source;
    this.url = createProjectDto.url;
    this.title = createProjectDto.title;
    this.description = createProjectDto.description;
    this.date = createProjectDto.date;
    this.languages = createProjectDto.languages;
    this.startDate = createProjectDto.startDate;
    this.endDate = createProjectDto.endDate;
    this.partners = createProjectDto.partners;
    this.location = createProjectDto.location;
    this.device = createProjectDto.device;
    this.projectManager = createProjectDto.projectManager;
    this.version = createProjectDto.version;
    this.status = ProjectStatusEnum.CHECKING;
  }
}
