export enum ProjectStatusEnum {
  READY = 'READY',
  ERROR = 'ERROR',
  CHECKING = 'CHECKING',
  DOWNLOADING = 'DOWNLOADING',
}
