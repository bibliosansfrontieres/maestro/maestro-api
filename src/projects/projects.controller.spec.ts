import { Test, TestingModule } from '@nestjs/testing';
import { ProjectsController } from './projects.controller';
import { ProjectsService } from './projects.service';
import { Repository } from 'typeorm';
import { Project } from './entities/project.entity';
import { getRepositoryToken } from '@nestjs/typeorm';
import { PlaylistsService } from 'src/playlists/playlists.service';
import { playlistsServiceMock } from 'src/utils/mocks/services/playlists-service.mock';
import { OmekaTransformerService } from 'src/omeka/omeka-transformer.service';
import { omekaTransformerServiceMock } from 'src/utils/mocks/services/omeka-transformer-service.mock';
import { ItemsService } from 'src/items/items.service';
import { itemsServiceMock } from 'src/utils/mocks/services/items-service.mock';
import { ApplicationsService } from 'src/applications/applications.service';
import { CategoriesService } from 'src/categories/categories.service';
import { applicationsServiceMock } from 'src/utils/mocks/services/applications-service.mock';
import { categoriesServiceMock } from 'src/utils/mocks/services/categories-service.mock';
import { OmekaImporterService } from 'src/omeka/omeka-importer.service';
import { omekaImporterServiceMock } from 'src/utils/mocks/services/omeka-importer-service.mock';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from 'src/users/users.service';
import { EntraService } from 'src/entra/entra.service';
import { ConfigService } from '@nestjs/config';
import { jwtServiceMock } from 'src/utils/mocks/services/jwt-service.mock';
import { usersServiceMock } from 'src/utils/mocks/services/users-service.mock';
import { entraServiceMock } from 'src/utils/mocks/services/entra-service.mock';
import { configServiceMock } from 'src/utils/mocks/services/config-service.mock';
import { NotificationsService } from 'src/notifications/notifications.service';
import { notificationsServiceMock } from 'src/utils/mocks/services/notifications-service.mock';
import { ProjectError } from './entities/project-error.entity';
import { OmekaService } from 'src/omeka/omeka.service';
import { omekaServiceMock } from 'src/utils/mocks/services/omeka-service.mock';

describe('ProjectsController', () => {
  let controller: ProjectsController;
  let projectRepository: Repository<Project>;
  let projectErrorRepository: Repository<ProjectError>;
  const PROJECT_REPOSITORY_TOKEN = getRepositoryToken(Project);
  const PROJECT_ERROR_REPOSITORY_TOKEN = getRepositoryToken(ProjectError);

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ProjectsController],
      providers: [
        ProjectsService,
        PlaylistsService,
        OmekaTransformerService,
        OmekaImporterService,
        ItemsService,
        ApplicationsService,
        CategoriesService,
        JwtService,
        UsersService,
        EntraService,
        ConfigService,
        {
          provide: PROJECT_REPOSITORY_TOKEN,
          useClass: Repository<Project>,
        },
        NotificationsService,
        UsersService,
        {
          provide: PROJECT_ERROR_REPOSITORY_TOKEN,
          useClass: Repository<ProjectError>,
        },
        OmekaService,
      ],
    })
      .overrideProvider(PlaylistsService)
      .useValue(playlistsServiceMock)
      .overrideProvider(OmekaTransformerService)
      .useValue(omekaTransformerServiceMock)
      .overrideProvider(ItemsService)
      .useValue(itemsServiceMock)
      .overrideProvider(ApplicationsService)
      .useValue(applicationsServiceMock)
      .overrideProvider(CategoriesService)
      .useValue(categoriesServiceMock)
      .overrideProvider(OmekaImporterService)
      .useValue(omekaImporterServiceMock)
      .overrideProvider(JwtService)
      .useValue(jwtServiceMock)
      .overrideProvider(UsersService)
      .useValue(usersServiceMock)
      .overrideProvider(EntraService)
      .useValue(entraServiceMock)
      .overrideProvider(ConfigService)
      .useValue(configServiceMock)
      .overrideProvider(NotificationsService)
      .useValue(notificationsServiceMock)
      .overrideProvider(UsersService)
      .useValue(usersServiceMock)
      .overrideProvider(OmekaService)
      .useValue(omekaServiceMock)
      .compile();

    controller = module.get<ProjectsController>(ProjectsController);
    projectRepository = module.get<Repository<Project>>(
      PROJECT_REPOSITORY_TOKEN,
    );
    projectErrorRepository = module.get<Repository<ProjectError>>(
      PROJECT_ERROR_REPOSITORY_TOKEN,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
    expect(projectRepository).toBeDefined();
    expect(projectErrorRepository).toBeDefined();
  });
});
