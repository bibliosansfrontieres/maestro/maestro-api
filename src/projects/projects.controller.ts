import { Controller, Get, Param, Patch, Post, Query } from '@nestjs/common';
import { ProjectsService } from './projects.service';
import { ApiNotFoundResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { GetProjectsRequest } from './definitions/get-projects.definition';
import { Auth } from 'src/auth/decorators/auth.decorator';
import { PermissionNameEnum } from 'src/permissions/enums/permission-name.enum';
import { UserId } from 'src/auth/decorators/user-id.decorator';

@Controller('projects')
@ApiTags('projects')
export class ProjectsController {
  constructor(private readonly projectsService: ProjectsService) {}

  @Get(':id')
  @Auth(PermissionNameEnum.READ_PROJECT)
  @ApiOperation({ summary: 'Get project by id' })
  @ApiNotFoundResponse({ description: 'Project not found' })
  getProject(@Param('id') id: string) {
    return this.projectsService.getProject(+id);
  }

  @Get('full/:id')
  getFullProject(@Param('id') id: string) {
    return this.projectsService.getFullProject(+id);
  }

  @Get()
  @Auth(PermissionNameEnum.READ_PROJECT)
  getProjects(@Query() getProjectsRequest: GetProjectsRequest) {
    return this.projectsService.getProjects(getProjectsRequest);
  }

  @Patch(':id')
  @Auth(PermissionNameEnum.UPDATE_PROJECT)
  updateProject(@Param('id') id: string, @UserId() userId: string) {
    return this.projectsService.updateProject(+id, userId);
  }

  @Post(':id')
  @Auth(PermissionNameEnum.CREATE_PROJECT)
  createProject(@Param('id') id: string, @UserId() userId: string) {
    return this.projectsService.createProject(+id, userId);
  }
}
