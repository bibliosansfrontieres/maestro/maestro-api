import { Module, forwardRef } from '@nestjs/common';
import { ProjectsService } from './projects.service';
import { ProjectsController } from './projects.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Project } from './entities/project.entity';
import { PlaylistsModule } from 'src/playlists/playlists.module';
import { OmekaModule } from 'src/omeka/omeka.module';
import { ItemsModule } from 'src/items/items.module';
import { ApplicationsModule } from 'src/applications/applications.module';
import { CategoriesModule } from 'src/categories/categories.module';
import { JwtModule } from '@nestjs/jwt';
import { UsersModule } from 'src/users/users.module';
import { EntraModule } from 'src/entra/entra.module';
import { ProjectError } from './entities/project-error.entity';
import { NotificationsModule } from 'src/notifications/notifications.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([Project, ProjectError]),
    PlaylistsModule,
    forwardRef(() => OmekaModule),
    ItemsModule,
    ApplicationsModule,
    CategoriesModule,
    JwtModule,
    UsersModule,
    EntraModule,
    NotificationsModule,
  ],
  controllers: [ProjectsController],
  providers: [ProjectsService],
  exports: [ProjectsService],
})
export class ProjectsModule {}
