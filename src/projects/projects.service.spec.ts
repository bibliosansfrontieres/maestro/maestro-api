import { Test, TestingModule } from '@nestjs/testing';
import { ProjectsService } from './projects.service';
import { Project } from './entities/project.entity';
import { Repository, SelectQueryBuilder } from 'typeorm';
import { getRepositoryToken } from '@nestjs/typeorm';
import { PlaylistsService } from 'src/playlists/playlists.service';
import { playlistsServiceMock } from 'src/utils/mocks/services/playlists-service.mock';
import { OmekaTransformerService } from 'src/omeka/omeka-transformer.service';
import { omekaTransformerServiceMock } from 'src/utils/mocks/services/omeka-transformer-service.mock';
import { ItemsService } from 'src/items/items.service';
import { CategoriesService } from 'src/categories/categories.service';
import { ApplicationsService } from 'src/applications/applications.service';
import { categoriesServiceMock } from 'src/utils/mocks/services/categories-service.mock';
import { applicationsServiceMock } from 'src/utils/mocks/services/applications-service.mock';
import { itemsServiceMock } from 'src/utils/mocks/services/items-service.mock';
import { OmekaImporterService } from 'src/omeka/omeka-importer.service';
import { omekaImporterServiceMock } from 'src/utils/mocks/services/omeka-importer-service.mock';
import { NotificationsService } from 'src/notifications/notifications.service';
import { UsersService } from 'src/users/users.service';
import { ProjectError } from './entities/project-error.entity';
import { usersServiceMock } from 'src/utils/mocks/services/users-service.mock';
import { notificationsServiceMock } from 'src/utils/mocks/services/notifications-service.mock';
import { ConfigService } from '@nestjs/config';
import { configServiceMock } from 'src/utils/mocks/services/config-service.mock';
import { OmekaService } from 'src/omeka/omeka.service';
import { omekaServiceMock } from 'src/utils/mocks/services/omeka-service.mock';
import { ProjectOrderByEnum } from './enums/project-order-by.enum';
import { ProjectStatusEnum } from './enums/project-status.enum';
import * as getManyAndCountUtil from 'src/utils/get-many-and-count.util';
import { projectMock } from 'src/utils/mocks/objects/project.mock';
import { GetProjectsResponse } from './definitions/get-projects.definition';
import { NotFoundException } from '@nestjs/common';
import { GetProjectResponse } from './definitions/get-project.definition';

describe('ProjectsService', () => {
  let service: ProjectsService;
  let playlistsService: PlaylistsService;
  let itemsService: ItemsService;
  let projectRepository: Repository<Project>;
  let projectErrorRepository: Repository<ProjectError>;

  const PROJECT_REPOSITORY_TOKEN = getRepositoryToken(Project);
  const PROJECT_ERROR_REPOSITORY_TOKEN = getRepositoryToken(ProjectError);

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ProjectsService,
        PlaylistsService,
        OmekaTransformerService,
        ItemsService,
        CategoriesService,
        ApplicationsService,
        OmekaImporterService,
        {
          provide: PROJECT_REPOSITORY_TOKEN,
          useClass: Repository<Project>,
        },
        NotificationsService,
        UsersService,
        {
          provide: PROJECT_ERROR_REPOSITORY_TOKEN,
          useClass: Repository<ProjectError>,
        },
        ConfigService,
        OmekaService,
      ],
    })
      .overrideProvider(PlaylistsService)
      .useValue(playlistsServiceMock)
      .overrideProvider(OmekaTransformerService)
      .useValue(omekaTransformerServiceMock)
      .overrideProvider(ItemsService)
      .useValue(itemsServiceMock)
      .overrideProvider(ApplicationsService)
      .useValue(applicationsServiceMock)
      .overrideProvider(CategoriesService)
      .useValue(categoriesServiceMock)
      .overrideProvider(OmekaImporterService)
      .useValue(omekaImporterServiceMock)
      .overrideProvider(NotificationsService)
      .useValue(notificationsServiceMock)
      .overrideProvider(UsersService)
      .useValue(usersServiceMock)
      .overrideProvider(ConfigService)
      .useValue(configServiceMock)
      .overrideProvider(OmekaService)
      .useValue(omekaServiceMock)
      .compile();

    service = module.get<ProjectsService>(ProjectsService);
    projectRepository = module.get<Repository<Project>>(
      PROJECT_REPOSITORY_TOKEN,
    );
    projectErrorRepository = module.get<Repository<ProjectError>>(
      PROJECT_ERROR_REPOSITORY_TOKEN,
    );
    playlistsService = module.get<PlaylistsService>(PlaylistsService);
    itemsService = module.get<ItemsService>(ItemsService);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
    expect(projectRepository).toBeDefined();
    expect(projectErrorRepository).toBeDefined();
  });

  describe('getProjects', () => {
    const mockQueryBuilder = {
      leftJoinAndSelect: jest.fn().mockReturnThis(),
      leftJoin: jest.fn().mockReturnThis(),
      where: jest.fn().mockReturnThis(),
      andWhere: jest.fn().mockReturnThis(),
      addSelect: jest.fn().mockReturnThis(),
      loadRelationCountAndMap: jest.fn().mockReturnThis(),
      orWhere: jest.fn().mockReturnThis(),
      orderBy: jest.fn().mockReturnThis(),
      take: jest.fn().mockReturnThis(),
      skip: jest.fn().mockReturnThis(),
    } as unknown as SelectQueryBuilder<Project>;
    const getProjectsRequest = {
      updatedDateRange: '12/30/2021,12/31/2021',
      query: 'search',
      orderBy: ProjectOrderByEnum.ID,
      statuses: ProjectStatusEnum.READY,
    };
    const mockProject = projectMock();
    const mockProjects = [mockProject];

    beforeEach(() => {
      jest
        .spyOn(projectRepository, 'createQueryBuilder')
        .mockReturnValue(mockQueryBuilder);
      jest
        .spyOn(getManyAndCountUtil, 'getManyAndCount')
        .mockResolvedValue([1, mockProjects]);
      jest
        .spyOn(playlistsService, 'count')
        .mockResolvedValue(mockProject.playlists.length);
      const itemsCount = mockProject.playlists.length
        ? mockProject.playlists
            .map((playlist) => playlist.items.length)
            .reduce((acc, playlistItemsLength) => {
              acc += playlistItemsLength;
              return acc;
            })
        : 0;
      jest.spyOn(itemsService, 'count').mockResolvedValue(itemsCount);
    });

    it('should join needed relations', async () => {
      await service.getProjects(getProjectsRequest);
      expect(mockQueryBuilder.leftJoinAndSelect).toHaveBeenCalledWith(
        'project.virtualMachines',
        'virtualMachine',
      );
      expect(mockQueryBuilder.leftJoinAndSelect).toHaveBeenCalledWith(
        'virtualMachine.user',
        'user',
      );
      expect(mockQueryBuilder.leftJoinAndSelect).toHaveBeenCalledWith(
        'virtualMachine.virtualMachineSaves',
        'virtualMachineSave',
      );
      expect(mockQueryBuilder.leftJoinAndSelect).toHaveBeenCalledWith(
        'virtualMachineSave.deploys',
        'deploy',
        'NOT deploy.status = "ARCHIVED"',
      );
      expect(mockQueryBuilder.leftJoinAndSelect).toHaveBeenCalledWith(
        'deploy.device',
        'device',
      );
    });

    it.skip('should paginate query builder', async () => {
      //TODO test query helper object
    });

    it('should call getManyAndCount', async () => {
      await service.getProjects(getProjectsRequest);
      expect(getManyAndCountUtil.getManyAndCount).toHaveBeenCalledWith(
        mockQueryBuilder,
        ['project.id'],
        false,
      );
    });

    it('should count project playlists', async () => {
      const playlistCount = Date.now();
      jest.spyOn(playlistsService, 'count').mockResolvedValue(playlistCount);

      await service.getProjects(getProjectsRequest);

      for (const projectMocked of mockProjects) {
        expect(projectMocked['playlistsCount']).toEqual(playlistCount);
      }
      expect(playlistsService.count).toHaveBeenCalledTimes(mockProjects.length);
    });

    it('should count project items', async () => {
      const itemsCount = Date.now();
      jest.spyOn(itemsService, 'count').mockResolvedValue(itemsCount);

      await service.getProjects(getProjectsRequest);

      for (const projectMocked of mockProjects) {
        expect(projectMocked['itemsCount']).toEqual(itemsCount);
      }
      expect(itemsService.count).toHaveBeenCalledTimes(mockProjects.length);
    });

    it('should return GetProjectsResponse', async () => {
      const result = await service.getProjects(getProjectsRequest);
      expect(result).toBeInstanceOf(GetProjectsResponse);
    });
  });

  describe('getProject', () => {
    const projectId = Date.now();
    const mockProject = projectMock({ id: projectId });
    const mockQueryBuilder = {
      leftJoinAndSelect: jest.fn().mockReturnThis(),
      where: jest.fn().mockReturnThis(),
      getOne: jest.fn().mockResolvedValue(mockProject),
    } as unknown as SelectQueryBuilder<Project>;

    beforeEach(() => {
      jest
        .spyOn(projectRepository, 'createQueryBuilder')
        .mockReturnValue(mockQueryBuilder);
    });

    it('should join needed relations', async () => {
      await service.getProject(projectId);
      expect(mockQueryBuilder.leftJoinAndSelect).toHaveBeenCalledWith(
        'project.virtualMachines',
        'virtualMachine',
      );
      expect(mockQueryBuilder.leftJoinAndSelect).toHaveBeenCalledWith(
        'virtualMachine.user',
        'user',
      );
      expect(mockQueryBuilder.leftJoinAndSelect).toHaveBeenCalledWith(
        'virtualMachine.virtualMachineSaves',
        'virtualMachineSave',
      );
      expect(mockQueryBuilder.leftJoinAndSelect).toHaveBeenCalledWith(
        'virtualMachineSave.deploys',
        'deploy',
        'NOT deploy.status = "ARCHIVED"',
      );
      expect(mockQueryBuilder.leftJoinAndSelect).toHaveBeenCalledWith(
        'deploy.device',
        'device',
      );
      expect(mockQueryBuilder.leftJoinAndSelect).toHaveBeenCalledWith(
        'project.projectErrors',
        'projectError',
      );
    });

    it('should call where', async () => {
      await service.getProject(projectId);
      expect(mockQueryBuilder.where).toHaveBeenCalledWith('project.id = :id', {
        id: projectId,
      });
    });

    it('should call getOne', async () => {
      await service.getProject(projectId);
      expect(mockQueryBuilder.getOne).toHaveBeenCalledTimes(1);
    });

    it('should throw NotFoundException if project not found', () => {
      jest.spyOn(mockQueryBuilder, 'getOne').mockResolvedValue(null);
      const result = service.getProject(Date.now());
      expect(result).rejects.toThrow(NotFoundException);
    });

    it('should return GetProjectResponse', async () => {
      jest.spyOn(mockQueryBuilder, 'getOne').mockResolvedValue(mockProject);
      const result = await service.getProject(projectId);
      expect(result).toBeInstanceOf(GetProjectResponse);
    });
  });
});
