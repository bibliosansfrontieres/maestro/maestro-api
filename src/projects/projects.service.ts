import {
  ConflictException,
  Inject,
  Injectable,
  Logger,
  NotFoundException,
  forwardRef,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Project } from './entities/project.entity';
import { FindOneOptions, In, Repository } from 'typeorm';
import { CreateProjectDto } from './dto/create-project.dto';
import { PlaylistsService } from 'src/playlists/playlists.service';
import { GetProjectResponse } from './definitions/get-project.definition';
import { IMaestroFullProject } from 'src/omeka/interfaces/maestro-full-project.interface';
import { OmekaTransformerService } from 'src/omeka/omeka-transformer.service';
import { ItemsService } from 'src/items/items.service';
import { ApplicationsService } from 'src/applications/applications.service';
import { CategoriesService } from 'src/categories/categories.service';
import { Cron } from '@nestjs/schedule';
import { OmekaImporterService } from 'src/omeka/omeka-importer.service';
import {
  GetProjectsRequest,
  GetProjectsResponse,
  IProjectResponse,
} from './definitions/get-projects.definition';
import { PageOptions } from 'src/utils/pagination/page-options';
import { ProjectOrderByEnum } from './enums/project-order-by.enum';
import { QueryHelper } from 'src/utils/query-helpers.util';
import { getManyAndCount } from 'src/utils/get-many-and-count.util';
import { ProjectStatusEnum } from './enums/project-status.enum';
import { ProjectError } from './entities/project-error.entity';
import { IProjectErrors } from 'src/omeka/interfaces/project-errors.interface';
import { UsersService } from 'src/users/users.service';
import { NotificationsService } from 'src/notifications/notifications.service';
import { NotificationTypeEnum } from 'src/notifications/enums/notification-type.enum';
import { ConfigService } from '@nestjs/config';
import { OmekaService } from 'src/omeka/omeka.service';

@Injectable()
export class ProjectsService {
  constructor(
    @InjectRepository(Project) private projectRepository: Repository<Project>,
    private playlistsService: PlaylistsService,
    @Inject(forwardRef(() => OmekaTransformerService))
    private omekaTransformerService: OmekaTransformerService,
    @Inject(forwardRef(() => OmekaImporterService))
    private omekaImporterService: OmekaImporterService,
    private itemsService: ItemsService,
    private applicationsService: ApplicationsService,
    private categoriesService: CategoriesService,
    @InjectRepository(ProjectError)
    private projectErrorRepository: Repository<ProjectError>,
    private notificationsService: NotificationsService,
    private usersService: UsersService,
    private configService: ConfigService,
    private omekaService: OmekaService,
  ) {}

  @Cron('0 5 * * 0-5')
  async updateAllProjects() {
    Logger.log('Updating all projects...', 'ProjectsService');
    const projects = await this.projectRepository.find();
    for (const project of projects) {
      try {
        Logger.log(`Updating project #${project.id}...`, 'ProjectsService');
        await this.omekaImporterService.importProject(project.id);
        Logger.log(
          `Project #${project.id} successfully updated`,
          'ProjectsService',
        );
      } catch (e) {
        console.error(e);
        Logger.error(`Could not update project #${project.id}`);
      }
    }
    Logger.log('All projects successfully updated !', 'ProjectsService');
  }

  async create(createProjectDto: CreateProjectDto) {
    const playlists = await this.playlistsService.find({
      where: { id: In(createProjectDto.playlistIds) },
    });
    const project = new Project(createProjectDto);
    project.playlists = playlists;
    return this.projectRepository.save(project);
  }

  async update(project: Project, createProjectDto: CreateProjectDto) {
    const playlists = await this.playlistsService.find({
      where: { id: In(createProjectDto.playlistIds) },
    });
    project.playlists = playlists;

    project.source = createProjectDto.source;
    project.url = createProjectDto.url;
    project.title = createProjectDto.title;
    project.description = createProjectDto.description;
    project.date = createProjectDto.date;
    project.languages = createProjectDto.languages;
    project.startDate = createProjectDto.startDate;
    project.endDate = createProjectDto.endDate;
    project.partners = createProjectDto.partners;
    project.location = createProjectDto.location;
    project.device = createProjectDto.device;
    project.projectManager = createProjectDto.projectManager;

    return this.projectRepository.save(project);
  }

  async createOrUpdate(createProjectDto: CreateProjectDto) {
    const project = await this.findOne({ where: { id: createProjectDto.id } });
    if (!project) {
      return this.create(createProjectDto);
    } else {
      return this.update(project, createProjectDto);
    }
  }

  async setStatus(project: Project, status: ProjectStatusEnum) {
    project.status = status;
    return this.projectRepository.save(project);
  }

  async getProject(id: number) {
    const project = await this.projectRepository
      .createQueryBuilder('project')
      .leftJoinAndSelect('project.virtualMachines', 'virtualMachine')
      .leftJoinAndSelect('virtualMachine.user', 'user')
      .leftJoinAndSelect(
        'virtualMachine.virtualMachineSaves',
        'virtualMachineSave',
      )
      .leftJoinAndSelect(
        'virtualMachineSave.deploys',
        'deploy',
        'NOT deploy.status = "ARCHIVED"',
      )
      .leftJoinAndSelect('deploy.device', 'device')
      .leftJoinAndSelect('project.projectErrors', 'projectError')
      .where('project.id = :id', { id })
      .getOne();

    if (!project) {
      throw new NotFoundException('project not found');
    }
    return new GetProjectResponse(project);
  }

  async getFullProject(id: number): Promise<IMaestroFullProject> {
    const project = await this.projectRepository.findOne({
      where: { id },
      relations: { playlists: true },
    });
    const playlists = await this.playlistsService.find({
      where: { id: In(project.playlists.map((playlist) => playlist.id)) },
      relations: {
        categories: true,
        application: true,
        items: true,
        playlistTranslations: true,
      },
    });
    const itemIds = [];
    for (const playlist of playlists) {
      for (const item of playlist.items) {
        if (!itemIds.includes(item.id)) {
          itemIds.push(item.id);
        }
      }
    }
    const items = await this.itemsService.find({
      where: { id: In(itemIds) },
      relations: {
        application: true,
        dublinCoreItem: {
          dublinCoreType: true,
          dublinCoreItemTranslations: true,
        },
        file: true,
      },
    });
    const applicationIds = [];
    for (const item of items) {
      if (!applicationIds.includes(item.application.id)) {
        applicationIds.push(item.application.id);
      }
    }
    const applications = await this.applicationsService.find({
      where: { id: In(applicationIds) },
    });
    const categoryIds = [];
    for (const playlist of playlists) {
      for (const category of playlist.categories) {
        if (!categoryIds.includes(category.id)) {
          categoryIds.push(category.id);
        }
      }
    }
    const categories = await this.categoriesService.find({
      where: { id: In(categoryIds) },
      relations: { categoryTranslations: true },
    });
    return {
      project: this.omekaTransformerService.projectToMaestro(project),
      applications,
      categories,
      playlists: this.omekaTransformerService.playlistsToMaestro(playlists),
      items: this.omekaTransformerService.itemsToMaestro(items),
    };
  }

  async getProjects(getProjectsRequest: GetProjectsRequest) {
    const pageOptions = new PageOptions(getProjectsRequest);
    const projectsQuery = this.projectRepository
      .createQueryBuilder('project')
      .leftJoinAndSelect('project.virtualMachines', 'virtualMachine')
      .leftJoinAndSelect('virtualMachine.user', 'user')
      .leftJoinAndSelect(
        'virtualMachine.virtualMachineSaves',
        'virtualMachineSave',
      )
      .leftJoinAndSelect(
        'virtualMachineSave.deploys',
        'deploy',
        'NOT deploy.status = "ARCHIVED"',
      )
      .leftJoinAndSelect('deploy.device', 'device');

    let { orderBy } = getProjectsRequest;

    orderBy = orderBy ? orderBy : ProjectOrderByEnum.UPDATED_AT;

    const queryHelper = new QueryHelper(projectsQuery);
    queryHelper.pagination(pageOptions, orderBy);
    if (getProjectsRequest.updatedDateRange) {
      queryHelper.andWhereDateRange(
        'project.updatedAt',
        getProjectsRequest.updatedDateRange,
      );
    }

    if (getProjectsRequest.statuses) {
      const statuses = getProjectsRequest.statuses.split(',');
      queryHelper.andWhereMultipleEquals('project.status', statuses);
    }

    queryHelper.andWhereQuery(getProjectsRequest.query, [
      'project.title',
      'project.description',
      'project.id',
    ]);

    const [count, projects] = await getManyAndCount<Project>(
      projectsQuery,
      ['project.id'],
      false,
    );

    for (const project of projects) {
      const playlistsCount = await this.playlistsService.count({
        where: { projects: { id: project.id } },
      });
      project['playlistsCount'] = playlistsCount;

      const itemsCount = await this.itemsService.count({
        where: { playlists: { projects: { id: project.id } } },
      });
      project['itemsCount'] = itemsCount;
    }

    return new GetProjectsResponse(
      projects as IProjectResponse[],
      count,
      pageOptions,
    );
  }

  async deleteAllErrors(project: Project) {
    await this.projectErrorRepository.delete({ project });
  }

  async saveErrors(project: Project, projectErrors: IProjectErrors) {
    await this.deleteAllErrors(project);
    for (const projectError of projectErrors.projectErrors) {
      const error = new ProjectError(projectError, project);
      await this.projectErrorRepository.save(error);
    }

    for (const packageError of projectErrors.packagesErrors) {
      const error = new ProjectError(packageError, project);
      await this.projectErrorRepository.save(error);
    }

    for (const itemError of projectErrors.itemsErrors) {
      const error = new ProjectError(itemError, project);
      await this.projectErrorRepository.save(error);
    }
  }

  async updateProject(id: number, userId: string) {
    const project = await this.findOne({
      where: { id },
      relations: {
        virtualMachines: {
          virtualMachineSaves: { deploys: { device: true } },
          user: true,
        },
      },
    });
    const user = await this.usersService.findOne({ where: { id: userId } });
    if (!user) {
      throw new NotFoundException('user not found');
    }
    if (!project) {
      throw new NotFoundException('project not found');
    }
    if (project.status === ProjectStatusEnum.CHECKING) {
      throw new ConflictException('project is already being checked');
    }
    await this.setStatus(project, ProjectStatusEnum.CHECKING);
    await this.deleteAllErrors(project);
    const importResult = await this.omekaImporterService.importProject(id);
    // if errors in projects
    if ('count' in importResult) {
      await this.saveErrors(project, importResult);
      await this.setStatus(project, ProjectStatusEnum.ERROR);
      await this.notificationsService.notify(
        NotificationTypeEnum.PROJECT_ERROR,
        user,
        {
          Reason: 'Project errors detected',
          ID: id.toString(),
          URL: `http://${this.configService.get('MAESTRO_HOST')}/projects/${id}`,
          'Omeka project': `https://omeka.tm.bsf-intranet.org/admin/items/show/${id}`,
        },
      );
      return importResult;
    } else {
      await this.setStatus(project, ProjectStatusEnum.READY);
      await this.notificationsService.notify(
        NotificationTypeEnum.PROJECT_READY,
        user,
        {
          Reason: 'Project updated and ready to be used',
          ID: id.toString(),
          Title: project.title,
          URL: `http://${this.configService.get('MAESTRO_HOST')}/projects/${id}`,
          'Omeka project': `https://omeka.tm.bsf-intranet.org/admin/items/show/${id}`,
        },
      );
    }
    return project;
  }

  async createProject(id: number, userId: string) {
    const user = await this.usersService.findOne({ where: { id: userId } });
    if (!user) {
      throw new NotFoundException('user not found');
    }

    const omekaProject = await this.omekaService.getProject(id);
    const projectMetadatas = this.omekaTransformerService.omekaProjectToMaestro(
      omekaProject,
      [],
    );

    let project = await this.findOne({
      where: { id: id },
    });
    if (!project) {
      await this.create(projectMetadatas);
      const importResult = await this.omekaImporterService.importProject(id);
      project = await this.findOne({
        where: { id: id },
        relations: {
          playlists: { items: true },
        },
      });
      // if errors in projects
      if ('count' in importResult) {
        await this.saveErrors(project, importResult);
        await this.setStatus(project, ProjectStatusEnum.ERROR);
        await this.notificationsService.notify(
          NotificationTypeEnum.PROJECT_ERROR,
          user,
          {
            Reason: 'Project errors detected',
            ID: id.toString(),
            URL: `http://${this.configService.get('MAESTRO_HOST')}/projects/${id}`,
            'Omeka project': `https://omeka.tm.bsf-intranet.org/admin/items/show/${id}`,
          },
        );
        return importResult;
      } else {
        await this.setStatus(project, ProjectStatusEnum.READY);
        await this.notificationsService.notify(
          NotificationTypeEnum.PROJECT_READY,
          user,
          {
            Reason: 'Project updated and ready to be used',
            ID: id.toString(),
            Title: project.title,
            URL: `http://${this.configService.get('MAESTRO_HOST')}/projects/${id}`,
            'Omeka project': `https://omeka.tm.bsf-intranet.org/admin/items/show/${id}`,
          },
        );
      }
      return project;
    } else {
      throw new ConflictException('project already exist');
    }
  }

  findOne(options?: FindOneOptions<Project>): Promise<Project> {
    return this.projectRepository.findOne(options);
  }
}
