import { RoleNameEnum } from '../enums/role-name.enum';

export class CreateRoleDto {
  name: RoleNameEnum;
}
