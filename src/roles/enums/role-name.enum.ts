export enum RoleNameEnum {
  USER = 'USER',
  PROJECT_MANAGER = 'PROJECT_MANAGER',
  ADMIN = 'ADMIN',
}
