import { Injectable } from '@nestjs/common';
import { FindManyOptions, FindOneOptions, Repository } from 'typeorm';
import { Role } from './entities/role.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateRoleDto } from './dto/create-role.dto';

@Injectable()
export class RolesService {
  constructor(
    @InjectRepository(Role) private roleRepository: Repository<Role>,
  ) {}

  create(createRoleDto: CreateRoleDto) {
    const role = new Role(createRoleDto);
    return this.roleRepository.save(role);
  }

  findOne(options?: FindOneOptions<Role>) {
    return this.roleRepository.findOne(options);
  }

  find(options?: FindManyOptions<Role>) {
    return this.roleRepository.find(options);
  }

  count(options?: FindManyOptions<Role>) {
    return this.roleRepository.count(options);
  }

  save(role: Role) {
    return this.roleRepository.save(role);
  }

  async removeAllPermissions() {
    const roles = await this.find();
    for (const role of roles) {
      role.permissions = [];
      await this.roleRepository.save(role);
    }
  }
}
