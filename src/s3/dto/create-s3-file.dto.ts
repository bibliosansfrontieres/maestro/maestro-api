import { S3FileTypeEnum } from '../enums/s3-file-type.enum';

export class CreateS3FileDto {
  name: string;
  extension: string;
  type: S3FileTypeEnum;
  sourceVersion?: string;
  sourceId?: string;
}
