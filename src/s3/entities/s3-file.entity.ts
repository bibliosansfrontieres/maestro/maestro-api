import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { S3FileTypeEnum } from '../enums/s3-file-type.enum';
import { CreateS3FileDto } from '../dto/create-s3-file.dto';

@Entity()
export class S3File {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  extension: string;

  @Column()
  path: string;

  @Column()
  type: S3FileTypeEnum;

  @Column({ nullable: true })
  sourceVersion: string;

  @Column({ nullable: true })
  sourceId: string;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  constructor(createS3FileDto: CreateS3FileDto) {
    if (!createS3FileDto) return;
    this.name = createS3FileDto.name;
    this.extension = createS3FileDto.extension;
    this.path = `${createS3FileDto.type}/${createS3FileDto.name}.${createS3FileDto.extension}`;
    this.type = createS3FileDto.type;
    this.sourceVersion = createS3FileDto.sourceVersion;
    this.sourceId = createS3FileDto.sourceId;
  }
}
