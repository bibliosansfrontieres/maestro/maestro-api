export enum S3FileTypeEnum {
  ITEMS = 'items',
  ITEM_THUMBNAILS = 'item_thumbnails',
  PLAYLIST_THUMBNAILS = 'playlist_thumbnails',
}
