import { Module } from '@nestjs/common';
import { S3Service } from './s3.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { S3File } from './entities/s3-file.entity';
import { ItemsModule } from 'src/items/items.module';
import { PlaylistsModule } from 'src/playlists/playlists.module';

@Module({
  imports: [TypeOrmModule.forFeature([S3File]), ItemsModule, PlaylistsModule],
  providers: [S3Service],
  exports: [S3Service],
})
export class S3Module {}
