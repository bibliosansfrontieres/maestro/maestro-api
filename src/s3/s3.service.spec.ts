import { Test, TestingModule } from '@nestjs/testing';
import { S3Service } from './s3.service';
import { Repository } from 'typeorm';
import { S3File } from './entities/s3-file.entity';
import { getRepositoryToken } from '@nestjs/typeorm';
import { ConfigService } from '@nestjs/config';
import { configServiceMock } from 'src/utils/mocks/services/config-service.mock';
import { ItemsService } from 'src/items/items.service';
import { PlaylistsService } from 'src/playlists/playlists.service';
import { itemsServiceMock } from 'src/utils/mocks/services/items-service.mock';
import { playlistsServiceMock } from 'src/utils/mocks/services/playlists-service.mock';

describe('S3Service', () => {
  let service: S3Service;
  let s3FileRepository: Repository<S3File>;

  const S3_FILE_REPOSITORY_TOKEN = getRepositoryToken(S3File);

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        { provide: S3_FILE_REPOSITORY_TOKEN, useClass: Repository<S3File> },
        S3Service,
        ConfigService,
        ItemsService,
        PlaylistsService,
      ],
    })
      .overrideProvider(ConfigService)
      .useValue(configServiceMock)
      .overrideProvider(ItemsService)
      .useValue(itemsServiceMock)
      .overrideProvider(PlaylistsService)
      .useValue(playlistsServiceMock)
      .compile();

    service = module.get<S3Service>(S3Service);
    s3FileRepository = module.get<Repository<S3File>>(S3_FILE_REPOSITORY_TOKEN);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
    expect(s3FileRepository).toBeDefined();
  });
});
