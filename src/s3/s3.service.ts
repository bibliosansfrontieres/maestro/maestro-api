import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { PutObjectCommand, S3Client } from '@aws-sdk/client-s3';
import { Readable } from 'stream';
import { S3FileTypeEnum } from './enums/s3-file-type.enum';
import { CreateS3FileDto } from './dto/create-s3-file.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { S3File } from './entities/s3-file.entity';
import { Repository } from 'typeorm';
import { Item } from 'src/items/entities/item.entity';
import { ItemsService } from 'src/items/items.service';
import { Playlist } from 'src/playlists/entities/playlist.entity';
import { PlaylistsService } from 'src/playlists/playlists.service';

@Injectable()
export class S3Service {
  private s3: S3Client;

  constructor(
    @InjectRepository(S3File) private s3FileRepository: Repository<S3File>,
    private configService: ConfigService,
    private itemsService: ItemsService,
    private playlistsService: PlaylistsService,
  ) {}

  async onModuleInit() {
    this.setS3();
  }

  private setS3() {
    this.s3 = new S3Client({
      credentials: {
        accessKeyId: this.configService.get('S3_ACCESS_KEY'),
        secretAccessKey: this.configService.get('S3_SECRET_KEY'),
      },
      region: this.configService.get('S3_REGION'),
      endpoint: this.configService.get('S3_ENDPOINT'),
    });
  }

  private async uploadFile(
    type: S3FileTypeEnum,
    fileName: string,
    extension: string,
    body: string | Uint8Array | Buffer | Readable,
  ) {
    const command = new PutObjectCommand({
      Bucket: this.configService.get('S3_BUCKET'),
      Key: `${type}/${fileName}.${extension}`,
      Body: body,
    });
    try {
      const uploadResult = await this.s3.send(command);
      return uploadResult;
    } catch (e) {
      console.error(e);
      return undefined;
    }
  }

  private create(createS3FileDto: CreateS3FileDto) {
    const s3File = new S3File(createS3FileDto);
    return this.s3FileRepository.save(s3File);
  }

  async uploadItem(item: Item, body: string | Uint8Array | Buffer | Readable) {
    Logger.log(`Uploading item #${item.id} to S3...`, 'S3Service');
    const name = `${item.id}`;
    const extension = item.file.extension;
    const sourceId = name;
    const sourceVersion = new Date(item.file.updatedAt).toISOString();
    const uploadResult = await this.uploadFile(
      S3FileTypeEnum.ITEMS,
      name,
      extension,
      body,
    );
    if (!uploadResult || uploadResult.$metadata.httpStatusCode !== 200) {
      Logger.error(`Error uploading item #${item.id} to S3`, 'S3Service');
      return undefined;
    }
    let s3File = await this.getItem(sourceId);
    if (!s3File) {
      s3File = await this.create({
        name,
        extension,
        type: S3FileTypeEnum.ITEMS,
        sourceId,
        sourceVersion,
      });
    } else {
      s3File.sourceVersion = sourceVersion;
      await this.s3FileRepository.save(s3File);
    }
    item.file.path = s3File.path;
    await this.itemsService.saveFile(item.file);
    Logger.log(`Item #${item.id} uploaded to S3 !`, 'S3Service');
    return s3File;
  }

  async uploadItemThumbnail(
    item: Item,
    body: string | Uint8Array | Buffer | Readable,
  ) {
    Logger.log(`Uploading item thumbnail #${item.id} to S3...`, 'S3Service');
    const name = `${item.id}`;
    const pathSplit = item.thumbnail.split('.');
    const extension = pathSplit[pathSplit.length - 1];
    const sourceId = name;
    const sourceVersion = new Date(item.file.updatedAt).toISOString();
    const uploadResult = await this.uploadFile(
      S3FileTypeEnum.ITEM_THUMBNAILS,
      name,
      extension,
      body,
    );
    if (!uploadResult || uploadResult.$metadata.httpStatusCode !== 200) {
      Logger.error(
        `Error uploading item thumbnail #${item.id} to S3`,
        'S3Service',
      );
      return undefined;
    }
    let s3File = await this.getItemThumbnail(sourceId);
    if (!s3File) {
      s3File = await this.create({
        name,
        extension,
        type: S3FileTypeEnum.ITEM_THUMBNAILS,
        sourceId,
        sourceVersion,
      });
    } else {
      s3File.sourceVersion = sourceVersion;
      await this.s3FileRepository.save(s3File);
    }
    item.thumbnail = s3File.path;
    await this.itemsService.save(item);
    Logger.log(`Item thumbnail #${item.id} uploaded to S3 !`, 'S3Service');
    return s3File;
  }

  async uploadPlaylistThumbnail(
    playlist: Playlist,
    body: string | Uint8Array | Buffer | Readable,
  ) {
    Logger.log(
      `Uploading playlist thumbnail #${playlist.id} to S3...`,
      'S3Service',
    );
    const name = `${playlist.id}`;
    const extension = 'png';
    const sourceId = name;
    const uploadResult = await this.uploadFile(
      S3FileTypeEnum.PLAYLIST_THUMBNAILS,
      name,
      extension,
      body,
    );
    if (!uploadResult || uploadResult.$metadata.httpStatusCode !== 200) {
      Logger.error(
        `Error uploading playlist thumbnail #${playlist.id} to S3`,
        'S3Service',
      );
      return undefined;
    }
    let s3File = await this.getPlaylistThumbnail(sourceId);
    if (!s3File) {
      s3File = await this.create({
        name,
        extension,
        type: S3FileTypeEnum.PLAYLIST_THUMBNAILS,
        sourceId,
      });
    }
    playlist.image = s3File.path;
    await this.playlistsService.save(playlist);
    Logger.log(
      `Playlist thumbnail #${playlist.id} uploaded to S3 !`,
      'S3Service',
    );
    return s3File;
  }

  async getItem(sourceId: string) {
    const s3File = await this.s3FileRepository.findOne({
      where: { type: S3FileTypeEnum.ITEMS, sourceId },
    });
    return s3File;
  }

  async getItemThumbnail(sourceId: string) {
    const s3File = await this.s3FileRepository.findOne({
      where: { type: S3FileTypeEnum.ITEM_THUMBNAILS, sourceId },
    });
    return s3File;
  }

  async getPlaylistThumbnail(sourceId: string) {
    const s3File = await this.s3FileRepository.findOne({
      where: { type: S3FileTypeEnum.PLAYLIST_THUMBNAILS, sourceId },
    });
    return s3File;
  }
}
