import { DeviceStatusEnum } from 'src/devices/enums/device-status.enum';

export interface IDeviceIdentity {
  status: DeviceStatusEnum;
  publicIpv4?: string;
  privateIpv4?: string;
  ipv4FqdnName?: string;
  ipv4FqdnArpa?: string;
  publicIpv6?: string;
  ipv6FqdnName?: string;
  ipv6FqdnArpa?: string;
  macAddress?: string;
  serialId?: string;
  cpuSerialId?: string;
  deployId?: string;
  virtualMachineSaveId?: string;
}
