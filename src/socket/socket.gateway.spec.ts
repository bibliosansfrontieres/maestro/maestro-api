import { Test, TestingModule } from '@nestjs/testing';
import { SocketGateway } from './socket.gateway';
import { DevicesService } from 'src/devices/devices.service';
import { devicesServiceMock } from 'src/utils/mocks/services/devices-service.mock';
import { DeploysService } from 'src/deploys/deploys.service';
import { deploysServiceMock } from 'src/utils/mocks/services/deploys-service.mock';
import { ConfigService } from '@nestjs/config';
import { NotificationsService } from 'src/notifications/notifications.service';
import { configServiceMock } from 'src/utils/mocks/services/config-service.mock';
import { notificationsServiceMock } from 'src/utils/mocks/services/notifications-service.mock';

describe('SocketGateway', () => {
  let gateway: SocketGateway;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        SocketGateway,
        DevicesService,
        DeploysService,
        ConfigService,
        NotificationsService,
      ],
    })
      .overrideProvider(DevicesService)
      .useValue(devicesServiceMock)
      .overrideProvider(DeploysService)
      .useValue(deploysServiceMock)
      .overrideProvider(ConfigService)
      .useValue(configServiceMock)
      .overrideProvider(NotificationsService)
      .useValue(notificationsServiceMock)
      .compile();

    gateway = module.get<SocketGateway>(SocketGateway);
  });

  it('should be defined', () => {
    expect(gateway).toBeDefined();
  });
});
