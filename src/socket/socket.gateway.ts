import {
  OnGatewayConnection,
  OnGatewayDisconnect,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';
import { VirtualMachine } from 'src/virtual-machines/entities/virtual-machine.entity';
import { IDeviceIdentity } from './interfaces/device-identity.interface';
import { DevicesService } from 'src/devices/devices.service';
import { isDeviceSocketAuth } from 'src/utils/is-device-socket-auth.util';
import { Device } from 'src/devices/entities/device.entity';
import { Deploy } from 'src/deploys/entities/deploy.entity';
import { DeployStatusEnum } from 'src/deploys/enums/deploy-status.enum';
import { DeploysService } from 'src/deploys/deploys.service';
import { DeviceStatusEnum } from 'src/devices/enums/device-status.enum';
import { NotificationTypeEnum } from 'src/notifications/enums/notification-type.enum';
import { NotificationsService } from 'src/notifications/notifications.service';
import { ConfigService } from '@nestjs/config';
import { User } from 'src/users/entities/user.entity';
import { Notification } from 'src/notifications/entities/notification.entity';
import { Inject, NotFoundException, forwardRef } from '@nestjs/common';
import { IMonitoringValues } from 'src/monitoring/interfaces/monitoring-values.interface';
import { readFileSync } from 'fs';
import { join } from 'path';
import { createHash } from 'crypto';

@WebSocketGateway({ cors: { origin: '*' } })
export class SocketGateway implements OnGatewayConnection, OnGatewayDisconnect {
  @WebSocketServer() server: Server;

  constructor(
    private devicesService: DevicesService,
    private deploysService: DeploysService,
    @Inject(forwardRef(() => NotificationsService))
    private notificationsService: NotificationsService,
    private configService: ConfigService,
  ) {}

  async handleConnection(client: Socket) {
    if (isDeviceSocketAuth(client)) {
      const identity = client.handshake.auth as IDeviceIdentity;
      const device = await this.devicesService.createOrUpdate(identity, true);
      await this.devicesService.checkDeploy(device, identity);
    }
  }

  async handleDisconnect(client: Socket) {
    if (isDeviceSocketAuth(client)) {
      await this.devicesService.createOrUpdate(
        client.handshake.auth as IDeviceIdentity,
        false,
      );
    }
  }

  emitMonitoringValues(monitoringValues: IMonitoringValues) {
    this.server.emit(`monitoring.values`, {
      monitoringValues,
    });
  }

  emitVirtualMachineStatus(virtualMachine: VirtualMachine) {
    this.server.emit(`virtualMachine.${virtualMachine.id}.status`, {
      status: virtualMachine.status,
    });
  }

  emitVirtualMachineIsArchived(virtualMachine: VirtualMachine) {
    this.server.emit(`virtualMachine.${virtualMachine.id}.isArchived`, {
      isArchived: virtualMachine.isArchived,
    });
  }

  emitVirtualMachineApproval(virtualMachine: VirtualMachine) {
    this.server.emit(`virtualMachine.${virtualMachine.id}.approval`, {
      approval: virtualMachine.approval,
    });
  }

  async emitDeviceBlink(device: Device, blinking: boolean = true) {
    const blinkingDurationInSeconds = 20;
    if (blinking) {
      this.server.emit(`device.${device.macAddress}.blink`, {
        duration: blinkingDurationInSeconds,
      });
      setTimeout(
        this.emitDeviceBlink.bind(this, device, false),
        blinkingDurationInSeconds * 1000,
      );
    }
    await this.devicesService.setBlinking(device, blinking);
    this.server.emit('device.blinking', { id: device.id, blinking });
  }

  emitDeviceReset(device: Device) {
    this.server.emit(`device.${device.macAddress}.reset`);
  }

  emitDeviceDeploy(deploy: Deploy) {
    const olipServicesTag =
      this.configService.get('MAESTRO_BRANCH') === 'main'
        ? 'latest'
        : `${this.configService.get('MAESTRO_BRANCH')}`;
    const maestroApiUrl =
      olipServicesTag === 'latest'
        ? 'https://api.maestro.bsf-intranet.org'
        : 'https://api.staging.maestro.bsf-intranet.org';

    this.server.emit(`device.${deploy.device.macAddress}.deploy`, {
      deployId: deploy.id,
      virtualMachineSaveId: deploy.virtualMachineSave.id,
      defaultLanguageIdentifier:
        deploy.virtualMachineSave.virtualMachine.defaultLanguageIdentifier,
      context: deploy.virtualMachineSave.context,
      variables: {
        APPS_IMAGES_TAG: olipServicesTag,
        OLIP_SERVICES_TAG: olipServicesTag,
        DOCKER_PRIVATE_APPS_REPOSITORY_TOKEN: this.configService.get(
          'DOCKER_PRIVATE_APPS_REPOSITORY_TOKEN',
        ),
        MAESTRO_API_URL: maestroApiUrl,
        MAESTRO_API_TOKEN: deploy.virtualMachineSave.virtualMachine.token,
      },
    });
  }

  emitUserNotification(
    user: User,
    notification: Notification | undefined,
    count: number,
  ) {
    this.server.emit(`user.${user.id}.notification`, {
      notification,
      count,
    });
  }

  @SubscribeMessage('device.deploy.status')
  async handleDeviceDeployStatus(
    client: Socket,
    payload: { deployId: number; status: DeployStatusEnum },
  ) {
    if (isDeviceSocketAuth(client)) {
      const device = await this.devicesService.createOrUpdate(
        client.handshake.auth as IDeviceIdentity,
        true,
      );
      this.deploysService.setDeployStatus(payload?.deployId, payload?.status);
      if (payload.status === DeployStatusEnum.DEPLOYING) {
        this.devicesService.setStatus(device, DeviceStatusEnum.DEPLOYING);
      }
    }
  }

  @SubscribeMessage('device.blinking')
  async handleDeviceBlinking(client: Socket, payload: { blinking: boolean }) {
    if (isDeviceSocketAuth(client)) {
      const device = await this.devicesService.createOrUpdate(
        client.handshake.auth as IDeviceIdentity,
        true,
      );
      this.emitDeviceBlink(device, payload.blinking);
    }
  }

  @SubscribeMessage('device.deploy')
  async handleDeviceDeploy(
    client: Socket,
    payload: { status: DeployStatusEnum; virtualMachineSaveId: string },
  ) {
    // TODO : mhhh deploys ?
    if (
      isDeviceSocketAuth(client) &&
      payload.status &&
      payload.virtualMachineSaveId
    ) {
      const device = await this.devicesService.createOrUpdate(
        client.handshake.auth as IDeviceIdentity,
        true,
      );
      let deploy = await this.deploysService.findOne({
        where: {
          device: { id: device.id },
          user: true,
          virtualMachineSave: { virtualMachine: true },
          labels: true,
        },
        order: { id: 'DESC' },
      });
      if (!deploy) {
        deploy = await this.deploysService.createFromUnknown(
          device,
          payload.virtualMachineSaveId,
        );
      }
      deploy = await this.deploysService.findOne({
        where: {
          device: { id: device.id },
          user: true,
          virtualMachineSave: { virtualMachine: true },
          labels: true,
        },
        order: { id: 'DESC' },
      });
      this.deploysService.setStatus(deploy, payload.status);
      if (payload.status === DeployStatusEnum.DEPLOYED) {
        this.devicesService.setStatus(device, DeviceStatusEnum.DEPLOYED);
        await this.notificationsService.notify(
          NotificationTypeEnum.DEPLOY_READY,
          deploy.user,
          {
            'VM ID': deploy.virtualMachineSave.virtualMachine.id,
            'VM name': deploy.virtualMachineSave.virtualMachine.title,
            'VM URL': `http://${this.configService.get('MAESTRO_HOST')}/virtual-machines/${deploy.virtualMachineSave.virtualMachine.id}`,
            'VM approved save': deploy.virtualMachineSave.name,
            Labels: deploy.labels
              .map((label) => `\`${label.name}\``)
              .join(', '),
          },
        );
      }
    }
  }

  @SubscribeMessage('certificate.version')
  async handleCertificateVersion(client: Socket) {
    if (!isDeviceSocketAuth(client)) return;

    const certificate = readFileSync(
      join(__dirname, '../../data/certificates/_.ideascube.io.crt'),
    ).toString('base64');

    const certificateMd5 = createHash('md5').update(certificate).digest('hex');

    client.emit('certificate.version', {
      version: certificateMd5,
    });
  }

  @SubscribeMessage('certificate')
  async handleCertificate(client: Socket) {
    if (!isDeviceSocketAuth(client)) return;

    const certificate = readFileSync(
      join(__dirname, '../../data/certificates/_.ideascube.io.crt'),
    ).toString('base64');
    const key = readFileSync(
      join(__dirname, '../../data/certificates/ideascube.io.key'),
    ).toString('base64');

    client.emit('certificate', {
      certificate,
      key,
    });
  }

  @SubscribeMessage('device.status')
  async handleDeviceStatus(
    client: Socket,
    payload: { status: DeviceStatusEnum },
  ) {
    if (isDeviceSocketAuth(client)) {
      const device = await this.devicesService.createOrUpdate(
        client.handshake.auth as IDeviceIdentity,
        true,
      );
      this.devicesService.setStatus(device, payload?.status);
    }
  }

  @SubscribeMessage('device.reset')
  async handleDeviceReset(client: Socket) {
    if (isDeviceSocketAuth(client)) {
      const device = await this.devicesService.createOrUpdate(
        client.handshake.auth as IDeviceIdentity,
        true,
      );
      this.devicesService.setStatus(device, DeviceStatusEnum.READY);
    }
  }

  @SubscribeMessage('device.exec.request')
  async handleDeviceExecRequest(
    client: Socket,
    payload: {
      deviceId: string;
      request: string;
    },
  ) {
    const device = await this.devicesService.findOne({
      where: { id: payload.deviceId },
    });
    if (!device) {
      throw new NotFoundException('device not found');
    }
    this.server.emit(`device.${device.macAddress}.exec.request`, {
      request: payload.request,
    });
  }

  @SubscribeMessage('device.exec.response')
  async handleDeviceExecResponse(
    client: Socket,
    payload: {
      stdout?: string;
      stderr?: string;
      error?: string;
    },
  ) {
    const macAddress = client.handshake.auth.macAddress;
    const device = await this.devicesService.findOne({
      where: { macAddress },
    });
    if (!device) {
      throw new NotFoundException('device not found');
    }
    console.log(payload);
    this.server.emit(`device.${device.id}.exec.response`, {
      response: payload,
    });
  }
}
