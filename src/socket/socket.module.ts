import { Module, Global, forwardRef } from '@nestjs/common';
import { SocketGateway } from './socket.gateway';
import { DevicesModule } from 'src/devices/devices.module';
import { DeploysModule } from 'src/deploys/deploys.module';
import { NotificationsModule } from 'src/notifications/notifications.module';

@Global()
@Module({
  imports: [
    forwardRef(() => DevicesModule),
    forwardRef(() => DeploysModule),
    forwardRef(() => NotificationsModule),
  ],
  providers: [SocketGateway],
  exports: [SocketGateway],
})
export class SocketModule {}
