import { User } from '../entities/user.entity';
import { GetProfileResponse } from './get-profile.definition';

export class DeleteUserGroupsDeleteResponse extends GetProfileResponse {
  constructor(user: User) {
    super(user);
  }
}
