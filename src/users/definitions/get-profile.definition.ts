import { Role } from 'src/roles/entities/role.entity';
import { User } from '../entities/user.entity';
import { ApiProperty } from '@nestjs/swagger';
import { Group } from 'src/groups/entities/group.entity';

export class GetProfileResponse {
  @ApiProperty({ example: '00000-aaaaa-00000-aaaaa' })
  id: string;

  @ApiProperty({ example: 'mail@domain.tld' })
  mail: string;

  @ApiProperty({ example: 'Julien Morali' })
  name: string;

  @ApiProperty({
    example:
      'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAIAQMAAAD+wSzIAAAABlBMVEX///+/v7+jQ3Y5AAAADklEQVQI12P4AIX8EAgALgAD/aNpbtEAAAAASUVORK5CYII',
  })
  avatar: string;

  @ApiProperty({
    example: {
      id: 1,
      name: 'PROJECT_MANAGER',
      permissions: [
        {
          id: 1,
          name: 'READ_USER',
        },
      ],
    },
  })
  role: Role;

  @ApiProperty({
    example: [
      {
        id: 1,
        name: 'Group name',
        description: 'My group description',
        isArchived: false,
      },
    ],
    description: 'Will only return not archived groups',
  })
  groups: Group[];

  constructor(user: User) {
    this.id = user.id;
    this.mail = user.mail;
    this.name = user.name;
    this.avatar = user.avatar;
    this.role = user.role;
    this.groups = user.groups;
  }
}
