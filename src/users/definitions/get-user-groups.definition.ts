import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsEnum, IsOptional, IsString } from 'class-validator';
import { GetGroupsResponse } from 'src/groups/definitions/get-groups.definition';
import { BooleanEnum } from 'src/utils/enums/boolean.enum';
import { PageOptionsRequest } from 'src/utils/pagination/dto/page-options.dto';

export class GetUserGroupsRequest extends PageOptionsRequest {
  @ApiPropertyOptional({
    enum: BooleanEnum,
    example: BooleanEnum.TRUE,
    default: BooleanEnum.TRUE,
  })
  @IsEnum(BooleanEnum)
  @IsOptional()
  isUserInGroup?: BooleanEnum;

  @ApiPropertyOptional({
    description: 'Search for group names',
  })
  @IsString()
  @IsOptional()
  query?: string;
}

export class GetUserGroupsResponse extends GetGroupsResponse {}
