import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsEnum, IsOptional, IsString } from 'class-validator';
import { User } from '../entities/user.entity';
import { PageOptions } from 'src/utils/pagination/page-options';
import { RoleNameEnum } from 'src/roles/enums/role-name.enum';
import { PageOptionsRequest } from 'src/utils/pagination/dto/page-options.dto';
import { PageResponse } from 'src/utils/pagination/dto/page-response';
import { GetUsersOrderFieldsEnum } from '../enums/get-users-order-fields.enum';

export class GetUsersRequest extends PageOptionsRequest {
  @ApiPropertyOptional({ example: 'julien' })
  @IsString()
  @IsOptional()
  query?: string;

  @ApiPropertyOptional({ example: RoleNameEnum.ADMIN, enum: RoleNameEnum })
  @IsEnum(RoleNameEnum)
  @IsOptional()
  roleName?: RoleNameEnum;

  @ApiPropertyOptional({
    example: GetUsersOrderFieldsEnum.ID,
    enum: GetUsersOrderFieldsEnum,
  })
  @IsEnum(GetUsersOrderFieldsEnum)
  @IsOptional()
  orderField?: GetUsersOrderFieldsEnum;
}

export class GetUsersResponse extends PageResponse<User> {
  constructor(data: User[], count: number, pageOptions: PageOptions) {
    super(data, count, pageOptions);
  }
}
