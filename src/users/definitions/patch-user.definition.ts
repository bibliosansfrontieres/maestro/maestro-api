import { RoleNameEnum } from 'src/roles/enums/role-name.enum';
import { User } from '../entities/user.entity';
import { GetProfileResponse } from './get-profile.definition';
import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsOptional } from 'class-validator';

export class PatchUserRequest {
  @ApiProperty({
    example: RoleNameEnum.ADMIN,
    enum: RoleNameEnum,
  })
  @IsEnum(RoleNameEnum)
  @IsOptional()
  roleName?: RoleNameEnum;
}

export class PatchUserResponse extends GetProfileResponse {
  constructor(user: User) {
    super(user);
  }
}
