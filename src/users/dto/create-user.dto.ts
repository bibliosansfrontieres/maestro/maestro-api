import { IEntraIdToken } from 'src/entra/interfaces/entra/entra-id-token.interface';
import { IEntraTokenResponse } from 'src/entra/interfaces/entra/entra-token-response.interface';

export class CreateUserDto {
  id: string;
  accessToken: string;
  expiresAt: number;
  refreshToken: string;
  mail: string;
  name: string;
  avatar?: string;

  constructor(
    entraToken: IEntraTokenResponse,
    entraId: IEntraIdToken,
    avatar?: string,
  ) {
    this.id = entraId.oid;
    this.accessToken = entraToken.access_token;
    // We remove 1 minute from expiration to be safe
    entraToken.expires_in = (entraToken.expires_in - 1) * 1000;
    this.expiresAt = Date.now() + entraToken.expires_in;
    this.refreshToken = entraToken.refresh_token;
    this.mail = entraId.email;
    this.name = entraId.name;
    this.avatar = avatar;
  }
}
