import {
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  PrimaryColumn,
} from 'typeorm';
import { CreateUserDto } from '../dto/create-user.dto';
import { Exclude } from 'class-transformer';
import { Role } from 'src/roles/entities/role.entity';
import { ApiProperty } from '@nestjs/swagger';
import { Group } from 'src/groups/entities/group.entity';
import { VirtualMachine } from 'src/virtual-machines/entities/virtual-machine.entity';
import { Log } from 'src/logs/entities/log.entity';
import { VirtualMachineSave } from 'src/virtual-machine-saves/entities/virtual-machine-save.entity';
import { Deploy } from 'src/deploys/entities/deploy.entity';
import { Notification } from 'src/notifications/entities/notification.entity';

@Entity()
export class User {
  @PrimaryColumn()
  @ApiProperty()
  id: string;

  @Exclude({ toPlainOnly: true })
  @Column({ nullable: true })
  accessToken: string | null;

  @Exclude({ toPlainOnly: true })
  @Column({ nullable: true })
  expiresAt: number | null;

  @Exclude({ toPlainOnly: true })
  @Column({ nullable: true })
  refreshToken: string | null;

  @Column()
  @ApiProperty()
  mail: string;

  @Column()
  @ApiProperty()
  name: string;

  @Column()
  @ApiProperty()
  avatar: string;

  @ManyToOne(() => Role, (role) => role.users)
  role: Role;

  @ManyToMany(() => Group, (group) => group.users)
  @JoinTable()
  groups: Group[];

  @OneToMany(() => VirtualMachine, (virtualMachine) => virtualMachine.user)
  virtualMachines: VirtualMachine[];

  @OneToMany(
    () => VirtualMachineSave,
    (virtualMachineSave) => virtualMachineSave.user,
  )
  virtualMachineSaves: VirtualMachineSave[];

  @OneToMany(() => Log, (log) => log.user)
  logs: Log[];

  @OneToMany(() => Deploy, (deploy) => deploy.user)
  deploys: Deploy[];

  @OneToMany(() => Notification, (notification) => notification.user)
  notifications: Notification[];

  constructor(createUserDto: CreateUserDto, role: Role) {
    if (!createUserDto) return;
    this.id = createUserDto.id;
    this.accessToken = createUserDto.accessToken;
    this.expiresAt = createUserDto.expiresAt;
    this.refreshToken = createUserDto.refreshToken;
    this.mail = createUserDto.mail;
    this.name = createUserDto.name;
    this.avatar = createUserDto.avatar;
    this.role = role;
  }
}
