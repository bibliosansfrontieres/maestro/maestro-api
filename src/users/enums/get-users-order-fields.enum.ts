export enum GetUsersOrderFieldsEnum {
  ID = 'ID',
  NAME = 'NAME',
  MAIL = 'MAIL',
  ROLE = 'ROLE',
}
