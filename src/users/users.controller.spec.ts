import { Test, TestingModule } from '@nestjs/testing';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { User } from './entities/user.entity';
import { Repository } from 'typeorm';
import { getRepositoryToken } from '@nestjs/typeorm';
import { JwtService } from '@nestjs/jwt';
import { jwtServiceMock } from 'src/utils/mocks/services/jwt-service.mock';
import { EntraService } from 'src/entra/entra.service';
import { entraServiceMock } from 'src/utils/mocks/services/entra-service.mock';
import { ConfigService } from '@nestjs/config';
import { configServiceMock } from 'src/utils/mocks/services/config-service.mock';
import { RolesService } from 'src/roles/roles.service';
import { rolesServiceMock } from 'src/utils/mocks/services/roles-service.mock';
import { LogsService } from 'src/logs/logs.service';
import { logsServiceMock } from 'src/utils/mocks/services/logs-service.mock';
import { GroupsService } from 'src/groups/groups.service';
import { groupsServiceMock } from 'src/utils/mocks/services/groups-service.mock';

describe('UsersController', () => {
  let controller: UsersController;
  let userRepository: Repository<User>;

  const USER_REPOSITORY_TOKEN = getRepositoryToken(User);

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UsersController],
      providers: [
        UsersService,
        JwtService,
        ConfigService,
        EntraService,
        RolesService,
        LogsService,
        GroupsService,
        { provide: USER_REPOSITORY_TOKEN, useClass: Repository<User> },
      ],
    })
      .overrideProvider(JwtService)
      .useValue(jwtServiceMock)
      .overrideProvider(ConfigService)
      .useValue(configServiceMock)
      .overrideProvider(EntraService)
      .useValue(entraServiceMock)
      .overrideProvider(RolesService)
      .useValue(rolesServiceMock)
      .overrideProvider(LogsService)
      .useValue(logsServiceMock)
      .overrideProvider(GroupsService)
      .useValue(groupsServiceMock)
      .compile();

    controller = module.get<UsersController>(UsersController);
    userRepository = module.get<Repository<User>>(USER_REPOSITORY_TOKEN);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
    expect(userRepository).toBeDefined();
  });
});
