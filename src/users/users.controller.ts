import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Query,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { Auth } from 'src/auth/decorators/auth.decorator';
import { UserId } from 'src/auth/decorators/user-id.decorator';
import {
  ApiBadRequestResponse,
  ApiBody,
  ApiConflictResponse,
  ApiCreatedResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import {
  PatchUserRequest,
  PatchUserResponse,
} from './definitions/patch-user.definition';
import { GetProfileResponse } from './definitions/get-profile.definition';
import { PermissionNameEnum } from 'src/permissions/enums/permission-name.enum';
import {
  GetUsersRequest,
  GetUsersResponse,
} from './definitions/get-users.definition';
import { PostUserGroupsAddResponse } from './definitions/post-user-groups-add.definition';
import { DeleteUserGroupsDeleteResponse } from './definitions/delete-user-groups-delete.definition';
import {
  GetUserGroupsRequest,
  GetUserGroupsResponse,
} from './definitions/get-user-groups.definition';

@Controller('users')
@ApiTags('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Get('profile')
  @Auth()
  @ApiOperation({ summary: 'Get self user profile' })
  @ApiOkResponse({ type: GetProfileResponse })
  @ApiNotFoundResponse({ description: 'User not found' })
  getProfile(@UserId() id: string) {
    return this.usersService.getProfile(id);
  }

  @Get(':id/groups')
  @Auth()
  @ApiOperation({ summary: 'Get groups of a user' })
  @ApiOkResponse({ type: GetUserGroupsResponse })
  getUserGroups(
    @Param('id') id: string,
    @Query() getUserGroupsRequest: GetUserGroupsRequest,
  ) {
    return this.usersService.getUserGroups(id, getUserGroupsRequest);
  }

  @Patch(':id')
  @Auth(PermissionNameEnum.UPDATE_USER)
  @ApiOperation({ summary: 'Update a user (role)' })
  @ApiBody({ type: PatchUserRequest })
  @ApiOkResponse({ type: PatchUserResponse })
  @ApiNotFoundResponse({ description: 'User not found' })
  @ApiBadRequestResponse({ description: 'Invalid request body parameters' })
  patchUser(
    @UserId() userId: string,
    @Param('id') id: string,
    @Body() patchUserRequest: PatchUserRequest,
  ) {
    return this.usersService.patchUser(userId, id, patchUserRequest);
  }

  @Get(':id')
  @Auth(PermissionNameEnum.READ_USER)
  @ApiOperation({ summary: 'Get user by id' })
  @ApiOkResponse({ type: GetProfileResponse })
  @ApiNotFoundResponse({ description: 'User not found' })
  getUser(@Param('id') id: string) {
    return this.usersService.getUser(id);
  }

  @Get()
  @Auth(PermissionNameEnum.READ_USER)
  @ApiOperation({ summary: 'Get all users' })
  @ApiOkResponse({ type: GetUsersResponse })
  @ApiBadRequestResponse({ description: 'Invalid request query parameters' })
  getUsers(@Query() getUsersRequest: GetUsersRequest) {
    return this.usersService.getUsers(getUsersRequest);
  }

  @Post(':userId/groups/add/:groupId')
  @Auth(PermissionNameEnum.UPDATE_USER)
  @ApiOperation({ summary: 'Add user to group' })
  @ApiCreatedResponse({ type: PostUserGroupsAddResponse })
  @ApiNotFoundResponse({ description: 'Group or user not found' })
  @ApiConflictResponse({ description: 'Group is already assigned to user' })
  @ApiBadRequestResponse({ description: 'Group is archived' })
  postUserGroupsAdd(
    @UserId() userId: string,
    @Param('userId') paramUserId: string,
    @Param('groupId') groupId: string,
  ) {
    return this.usersService.postUserGroupsAdd(userId, paramUserId, +groupId);
  }

  @Delete(':userId/groups/delete/:groupId')
  @Auth(PermissionNameEnum.UPDATE_USER)
  @ApiOperation({ summary: 'Remove user from group' })
  @ApiOkResponse({ type: DeleteUserGroupsDeleteResponse })
  @ApiNotFoundResponse({ description: 'Group or user not found' })
  @ApiBadRequestResponse({
    description: 'Group is archived or user not in group',
  })
  deleteUserGroupsDelete(
    @UserId() userId: string,
    @Param('userId') paramUserId: string,
    @Param('groupId') groupId: string,
  ) {
    return this.usersService.deleteUserGroupsDelete(
      userId,
      paramUserId,
      +groupId,
    );
  }
}
