import {
  BadRequestException,
  ConflictException,
  Inject,
  Injectable,
  NotFoundException,
  UnauthorizedException,
  forwardRef,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './entities/user.entity';
import { FindOneOptions, Repository } from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';
import { RoleNameEnum } from 'src/roles/enums/role-name.enum';
import { RolesService } from 'src/roles/roles.service';
import {
  PatchUserRequest,
  PatchUserResponse,
} from './definitions/patch-user.definition';
import { GetProfileResponse } from './definitions/get-profile.definition';
import { LogsService } from 'src/logs/logs.service';
import {
  GetUsersRequest,
  GetUsersResponse,
} from './definitions/get-users.definition';
import { PageOptions } from 'src/utils/pagination/page-options';
import { getManyAndCount } from 'src/utils/get-many-and-count.util';
import { GroupsService } from 'src/groups/groups.service';
import { PostUserGroupsAddResponse } from './definitions/post-user-groups-add.definition';
import { DeleteUserGroupsDeleteResponse } from './definitions/delete-user-groups-delete.definition';
import { LogActionEnum } from 'src/logs/enums/log-action.enum';
import { LogEntityEnum } from 'src/logs/enums/log-entity.enum';
import { EntraService } from 'src/entra/entra.service';
import { GetUsersOrderFieldsEnum } from './enums/get-users-order-fields.enum';
import { GetUserGroupsRequest } from './definitions/get-user-groups.definition';
import { PageOrderEnum } from 'src/utils/pagination/enums/page-order.enum';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User) private userRepository: Repository<User>,
    private rolesService: RolesService,
    private logsService: LogsService,
    private groupsService: GroupsService,
    @Inject(forwardRef(() => EntraService))
    private entraService: EntraService,
  ) {}

  async createUnknownUser() {
    const role = await this.rolesService.findOne({
      where: { name: RoleNameEnum.ADMIN },
    });
    if (!role) throw new NotFoundException('role not found');
    const avatar = `https://ui-avatars.com/api/?name=Unknown`;

    const user = new User(
      {
        id: 'unknown',
        avatar,
        name: 'Unknown',
        mail: 'unknown@robot',
      } as CreateUserDto,
      role,
    );
    return this.userRepository.save(user);
  }

  async createAdmin(id: string) {
    const role = await this.rolesService.findOne({
      where: { name: RoleNameEnum.ADMIN },
    });
    if (!role) throw new NotFoundException('role not found');
    const entraUser = await this.entraService.getUser(id);
    const avatar = await this.entraService.getUserProfilePicture(
      id,
      entraUser.mail,
    );

    const user = new User(
      {
        id,
        avatar,
        name: entraUser.displayName,
        mail: entraUser.mail,
      } as CreateUserDto,
      role,
    );
    return this.userRepository.save(user);
  }

  async create(
    createUserDto: CreateUserDto,
    roleName: RoleNameEnum = RoleNameEnum.USER,
  ): Promise<User> {
    const role = await this.rolesService.findOne({ where: { name: roleName } });
    const user = new User(createUserDto, role);
    return this.userRepository.save(user);
  }

  async save(user: User): Promise<User> {
    return this.userRepository.save(user);
  }

  async createOrUpdate(createUserDto: CreateUserDto): Promise<User> {
    const user = await this.findOne({ where: { id: createUserDto.id } });
    if (!user) {
      return this.create(createUserDto);
    } else {
      return this.update(createUserDto, user);
    }
  }

  async update(createUserDto: CreateUserDto, user?: User): Promise<User> {
    if (!user) {
      user = await this.findOne({ where: { id: createUserDto.id } });
      if (!user) {
        throw new NotFoundException('user not found');
      }
    }

    user.accessToken = createUserDto.accessToken;
    user.expiresAt = createUserDto.expiresAt;
    user.refreshToken = createUserDto.refreshToken;
    user.mail = createUserDto.mail;
    user.name = createUserDto.name;
    user.avatar = createUserDto.avatar || user.avatar;
    user = await this.userRepository.save(user);

    return user;
  }

  findOne(options?: FindOneOptions<User>): Promise<User> {
    return this.userRepository.findOne(options);
  }

  async getProfile(id: string): Promise<GetProfileResponse> {
    const user = await this.findOne({
      where: { id },
      relations: { role: { permissions: true }, groups: true },
    });
    if (!user) {
      throw new NotFoundException('user not found');
    }

    // Remove archived groups
    user.groups = user.groups.filter((group) => !group.isArchived);

    return new GetProfileResponse(user);
  }

  async getUserGroups(id: string, getUserGroupsRequest: GetUserGroupsRequest) {
    return this.groupsService.getUserGroups(id, getUserGroupsRequest);
  }

  async patchUser(
    authorId: string,
    id: string,
    patchUserRequest: PatchUserRequest,
  ): Promise<PatchUserResponse> {
    let user = await this.findOne({ where: { id }, relations: { role: true } });
    if (user === null) {
      throw new NotFoundException('user not found');
    }

    if (patchUserRequest.roleName) {
      if (user.id === authorId) {
        throw new UnauthorizedException('you cannot change your own role');
      }
      const role = await this.rolesService.findOne({
        where: { name: patchUserRequest.roleName },
      });
      if (role === null) {
        throw new NotFoundException('role not found');
      }
      user.role = role;

      await this.logsService.create({
        authorId,
        action: LogActionEnum.UPDATE,
        entity: LogEntityEnum.USER,
        entityId: user.id,
        values: {
          role: role.id,
        },
      });
    }

    await this.save(user);

    user = await this.findOne({
      where: { id },
      relations: { role: { permissions: true } },
    });

    return new PatchUserResponse(user);
  }

  async getUser(id: string) {
    const user = await this.findOne({ where: { id } });
    if (!user) {
      throw new NotFoundException('user not found');
    }
    return user;
  }

  async getUsers(getUsersRequest: GetUsersRequest): Promise<GetUsersResponse> {
    const orderField =
      getUsersRequest?.orderField || GetUsersOrderFieldsEnum.NAME;
    const pageOptions = new PageOptions(getUsersRequest, {
      order:
        orderField === GetUsersOrderFieldsEnum.NAME
          ? PageOrderEnum.ASC
          : undefined,
    });
    let usersQuery = this.userRepository
      .createQueryBuilder('user')
      .select('user')
      .innerJoinAndSelect('user.role', 'role')
      .skip(pageOptions.skip)
      .take(pageOptions.take)
      .orderBy(`user.${orderField.toLowerCase()}`, pageOptions.order);

    if (getUsersRequest.query) {
      usersQuery = usersQuery.where(
        'LOWER(user.name) LIKE :query OR LOWER(SUBSTR(user.mail, 0, INSTR(user.mail, "@"))) LIKE :query',
        {
          query: `%${getUsersRequest.query.toLowerCase()}%`,
        },
      );
    }

    const [count, users] = await getManyAndCount<User>(usersQuery, ['user.id']);

    return new GetUsersResponse(users, count, pageOptions);
  }

  async postUserGroupsAdd(authorId: string, userId: string, groupId: number) {
    const user = await this.findOne({
      where: { id: userId },
      relations: { groups: true, role: { permissions: true } },
    });
    if (user === null) {
      throw new NotFoundException('user not found');
    }

    const group = await this.groupsService.findOne({ where: { id: groupId } });
    if (group === null) {
      throw new NotFoundException('group not found');
    }

    if (group.isArchived) {
      throw new BadRequestException('group is archived');
    }

    const findGroup = user.groups.find((group) => group.id === groupId);
    if (findGroup) {
      throw new ConflictException('user already in group');
    }

    user.groups = [...user.groups, group];
    await this.save(user);

    await this.logsService.create({
      authorId,
      action: LogActionEnum.UPDATE,
      entity: LogEntityEnum.USER,
      entityId: user.id,
      description: `added to group ${group.id}`,
    });

    return new PostUserGroupsAddResponse(user);
  }

  async deleteUserGroupsDelete(
    authorId: string,
    userId: string,
    groupId: number,
  ) {
    const user = await this.findOne({
      where: { id: userId },
      relations: { groups: true, role: { permissions: true } },
    });
    if (user === null) {
      throw new NotFoundException('user not found');
    }

    const group = await this.groupsService.findOne({ where: { id: groupId } });
    if (group === null) {
      throw new NotFoundException('group not found');
    }

    if (group.isArchived) {
      throw new BadRequestException('group is archived');
    }

    const findGroup = user.groups.find((group) => group.id === groupId);
    if (!findGroup) {
      throw new BadRequestException('user not in group');
    }

    user.groups = [...user.groups.filter((group) => group.id !== groupId)];
    await this.save(user);

    await this.logsService.create({
      authorId,
      action: LogActionEnum.UPDATE,
      entity: LogEntityEnum.USER,
      entityId: user.id,
      description: `deleted from group ${group.id}`,
    });

    return new DeleteUserGroupsDeleteResponse(user);
  }
}
