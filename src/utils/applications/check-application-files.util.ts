import { checkFile } from './check-file.util';
import {
  DescriptionType,
  TDescription,
} from './validation/types/description.type';
import { HooksType, THooks } from './validation/types/hooks.type';
import { validateFile } from './validation/validate-file';

export function checkApplicationFiles(
  applicationName: string,
  applicationPath: string,
) {
  let description = checkFile(
    applicationName,
    applicationPath,
    'description.json',
  );
  let hooks = checkFile(applicationName, applicationPath, 'hooks.json');
  const compose = checkFile(applicationName, applicationPath, 'compose.yml');

  if (!description || !hooks || !compose) {
    return undefined;
  }

  description = JSON.parse(description);
  hooks = JSON.parse(hooks);

  const isDescriptionValid = validateFile(
    applicationName,
    DescriptionType,
    description,
  );
  const isHooksValid = validateFile(applicationName, HooksType, hooks);

  if (!isDescriptionValid || !isHooksValid) {
    return undefined;
  }

  return {
    description,
    compose,
    hooks,
  } as unknown as { description: TDescription; hooks: THooks; compose: string };
}
