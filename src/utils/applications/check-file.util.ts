import { Logger } from '@nestjs/common';
import { existsSync, readFileSync } from 'fs';
import { join } from 'path';

export function checkFile(
  applicationName: string,
  applicationPath: string,
  fileName: string,
) {
  const filePath = join(applicationPath, fileName);
  if (!existsSync(filePath)) {
    Logger.warn(
      `${fileName} file not found for application ${applicationName}`,
      'InitDatabaseService',
    );
    return undefined;
  }

  return readFileSync(filePath, 'utf-8');
}
