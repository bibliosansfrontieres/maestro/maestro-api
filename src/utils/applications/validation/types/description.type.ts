import * as t from 'io-ts';
import { ApplicationCatalogTypeEnum } from 'src/applications/enums/application-catalog-type.enum';
import { ApplicationTypeNameEnum } from 'src/applications/enums/application-type-name.enum';

const DescriptionTranslationType = t.type({
  shortDescription: t.string,
  longDescription: t.string,
  languageIdentifier: t.string,
});

const DescriptionCatalogType = t.type({
  type: t.keyof(
    Object.fromEntries(
      Object.keys(ApplicationCatalogTypeEnum).map((key) => [key, null]),
    ),
  ),
  url: t.string,
});

export const ExposeServiceType = t.type({
  name: t.string,
  port: t.string,
});

export const DescriptionType = t.type({
  id: t.string,
  name: t.string,
  displayName: t.string,
  port: t.string,
  version: t.string,
  exposeServices: t.union([t.array(ExposeServiceType), t.undefined]),
  catalogFunction: t.union([t.string, t.undefined]),
  applicationTranslations: t.array(DescriptionTranslationType),
  type: t.keyof(
    Object.fromEntries(
      Object.keys(ApplicationTypeNameEnum).map((key) => [key, null]),
    ),
  ),
  size: t.number,
  catalog: t.union([DescriptionCatalogType, t.undefined]),
  logo: t.string,
  image: t.string,
});

export type TDescription = t.TypeOf<typeof DescriptionType>;
