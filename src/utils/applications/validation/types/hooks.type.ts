import * as t from 'io-ts';
import { ApplicationHookNameEnum } from 'src/applications/enums/application-hook-name.enum';

const HookType = t.type({
  name: t.keyof(
    Object.fromEntries(
      Object.keys(ApplicationHookNameEnum).map((key) => [key, null]),
    ),
  ),
  exec: t.union([t.undefined, t.string]),
  value: t.union([t.undefined, t.string]),
});

const HookWithExecOrValueType = t.refinement(
  HookType,
  (hook) => hook.exec !== undefined || hook.value !== undefined,
  'Hook needs exec or value',
);

export const HooksType = t.type({
  hooks: t.array(HookWithExecOrValueType),
});

export type THooks = t.TypeOf<typeof HooksType>;
