import { Logger } from '@nestjs/common';
import * as t from 'io-ts';
import { PathReporter } from 'io-ts/lib/PathReporter';

export function validateFile(
  applicationName: string,
  Type: t.Type<any>,
  toValidate: unknown,
) {
  let isValid = true;
  const descriptionResult = Type.decode(toValidate);
  if (descriptionResult._tag !== 'Right') {
    Logger.warn(
      `Invalid description.json file for application ${applicationName}`,
      'InitDatabaseService',
    );
    console.error(PathReporter.report(descriptionResult).join('\n'));
    isValid = false;
  }
  return isValid;
}
