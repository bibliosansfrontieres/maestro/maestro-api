import { createWriteStream, existsSync, rmSync } from 'fs';
import axios from 'axios';

export async function downloadFile(
  url: string,
  savePath: string,
): Promise<boolean> {
  try {
    const response = await axios({
      method: 'get',
      url: url,
      responseType: 'stream', // Specify the response type as a stream
    });

    // Create a writable stream to save the file
    const fileStream = createWriteStream(savePath);

    // Pipe the response data stream to the file stream
    response.data.pipe(fileStream);

    // Return a promise to handle completion
    return new Promise<boolean>((resolve, reject) => {
      fileStream.on('finish', () => {
        fileStream.close();
        resolve(true); // Return true upon successful download
      });

      fileStream.on('error', (error) => {
        console.error('Error writing to file:', error.message);
        reject(error);
      });
    });
  } catch (error) {
    console.error('Error downloading file:', error.message);
    console.error('url of error file : ', error.config.url);
    if (existsSync(savePath)) {
      rmSync(savePath);
    }
    return false; // Return false if any error occurs
  }
}
