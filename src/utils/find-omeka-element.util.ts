import { IOmekaElementText } from 'src/omeka/interfaces/omeka-element-text.interface';

export function findOmekaElement(
  id: number,
  element_texts: IOmekaElementText[],
) {
  const foundElement = element_texts.find(
    (element_text) => element_text.element.id === id,
  );
  return (
    foundElement !== undefined &&
    foundElement.text !== null &&
    foundElement.text !== undefined &&
    foundElement.text !== ''
  );
}
