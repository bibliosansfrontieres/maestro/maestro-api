import { createCanvas, loadImage } from 'canvas';
import { writeFileSync } from 'fs';
import { join } from 'path';

interface PatchworkOptions {
  canvasWidth: number;
  canvasHeight: number;
  imageWidth: number;
  imageHeight: number;
}

export async function generatePlaylistThumbnail(
  images: string[],
  options: PatchworkOptions,
): Promise<boolean> {
  try {
    const { canvasWidth, canvasHeight, imageWidth, imageHeight } = options;

    const canvas = createCanvas(canvasWidth, canvasHeight);
    const ctx = canvas.getContext('2d');

    // Loop through each image and draw it on the canvas
    let currentX = 0;
    let currentY = 0;

    for (const imagePath of images) {
      const image = await loadImage(imagePath);

      // Resize image if needed (using canvas instead of sharp)
      ctx.drawImage(image, currentX, currentY, imageWidth, imageHeight);

      // Move to the next position
      currentX += imageWidth;

      // If the next image exceeds the canvas width, move to the next row
      if (currentX + imageWidth > canvasWidth) {
        currentX = 0;
        currentY += imageHeight;
      }

      // If the next row exceeds the canvas height, stop adding images
      if (currentY + imageHeight > canvasHeight) {
        break;
      }
    }

    const buffer = canvas.toBuffer('image/png');
    writeFileSync(
      join(__dirname, '../../downloads/playlist_thumbnail.png'),
      buffer,
    );
    return true;
  } catch (e) {
    console.error(e);
    return false;
  }
}
