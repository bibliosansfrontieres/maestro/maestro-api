import { DublinCoreType } from 'src/items/entities/dublin-core-type.entity';
import { IOmekaItemFile } from 'src/omeka/interfaces/omeka-item-file.interface';

export async function getDublinCoreTypeOmekaMap(
  dublinCoreTypes: DublinCoreType[],
) {
  const dublinCoreTypeMap = new Map<string, DublinCoreType>();
  for (const dublinCoreType of dublinCoreTypes) {
    for (const dublinCoreTypeTranslation of dublinCoreType.dublinCoreTypeTranslations) {
      // TODO : set omeka dublin core type ids
      if (dublinCoreTypeTranslation.label === 'Text') {
        dublinCoreTypeMap.set('text', dublinCoreType);
        break;
      } else if (dublinCoreTypeTranslation.label === 'Moving Image') {
        dublinCoreTypeMap.set('video', dublinCoreType);
        break;
      } else if (dublinCoreTypeTranslation.label === 'Sound') {
        dublinCoreTypeMap.set('sound', dublinCoreType);
        break;
      } else if (dublinCoreTypeTranslation.label === 'Still Image') {
        dublinCoreTypeMap.set('image', dublinCoreType);
        break;
      } else if (dublinCoreTypeTranslation.label === 'Other') {
        dublinCoreTypeMap.set('other', dublinCoreType);
        break;
      }
    }
  }
  return dublinCoreTypeMap;
}

export function getDublinCoreType(
  omekaItemFile: IOmekaItemFile,
  dublinCoreTypeOmekaMap: Map<string, DublinCoreType>,
) {
  const mimeType = omekaItemFile.file.mime_type;
  if (mimeType.includes('pdf')) {
    return dublinCoreTypeOmekaMap.get('text');
  } else if (mimeType.includes('audio/')) {
    return dublinCoreTypeOmekaMap.get('sound');
  } else if (mimeType.includes('video/')) {
    return dublinCoreTypeOmekaMap.get('video');
  } else if (mimeType.includes('image/')) {
    return dublinCoreTypeOmekaMap.get('image');
  } else {
    return dublinCoreTypeOmekaMap.get('other');
  }
}
