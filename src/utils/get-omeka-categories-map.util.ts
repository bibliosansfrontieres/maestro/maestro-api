import { Category } from 'src/categories/entities/category.entity';
import { IOmekaPackage } from 'src/omeka/interfaces/omeka-package.interface';

export async function getOmekaCategoriesMap(categories: Category[]) {
  const categoriesMap = new Map<string, Category>();
  for (const category of categories) {
    for (const categoryTranslation of category.categoryTranslations) {
      categoriesMap.set(categoryTranslation.title.toLowerCase(), category);
    }
  }
  return categoriesMap;
}

export function getCategory(
  omekaPackage: IOmekaPackage,
  categoriesMap: Map<string, Category>,
) {
  return categoriesMap.get(omekaPackage.subject.toLowerCase());
}
