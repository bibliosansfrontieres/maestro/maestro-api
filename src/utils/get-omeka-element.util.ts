import { IOmekaElementText } from 'src/omeka/interfaces/omeka-element-text.interface';

export function getOmekaElement(
  elementId: number,
  elementTexts: IOmekaElementText[],
): string {
  const elementText = elementTexts.find(
    (elementText) => elementText.element.id === elementId,
  );
  return elementText?.text || '';
}
export function getOmekaElements(
  elementId: number,
  elementTexts: IOmekaElementText[],
): string[] {
  const matchingElements = elementTexts.filter(
    (elementText) => elementText.element.id === elementId,
  );
  return matchingElements.map((elementText) => elementText.text);
}
