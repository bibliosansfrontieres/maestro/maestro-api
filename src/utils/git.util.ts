import { exec } from 'child_process';

export class Git {
  private workdir: string = '.';
  private logging: boolean = false;

  constructor(options?: { workdir?: string; logging?: boolean }) {
    if (!options) {
      return;
    }

    if (options.workdir) {
      this.workdir = options.workdir;
    }

    if (options.logging) {
      this.logging = options.logging;
    }
  }

  private log(message: any, ...optionalParams: any[]) {
    if (this.logging) {
      console.log(message, ...optionalParams);
    }
  }

  clone(url: string, options?: { branch?: string }) {
    const branch = options?.branch ?? 'main';

    return new Promise<void>((resolve, reject) => {
      exec(
        `git clone --branch ${branch} ${url} ${this.workdir}`,
        (error, stdout, stderr) => {
          this.log('git clone stdout', stdout);
          this.log('git clone stderr', stderr);
          if (error) {
            this.log(error);
            reject(error);
          } else {
            resolve();
          }
        },
      );
    });
  }

  checkout(branch: string) {
    return new Promise<void>((resolve, reject) => {
      exec(
        `cd ${this.workdir} && git checkout ${branch}`,
        (error, stdout, stderr) => {
          this.log('git clone stdout', stdout);
          this.log('git clone stderr', stderr);
          if (error) {
            this.log(error);
            reject(error);
          } else {
            resolve();
          }
        },
      );
    });
  }
}
