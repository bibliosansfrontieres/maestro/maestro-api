function createIdGenerator() {
  let id = 0;
  return function () {
    return id++;
  };
}

// Create an instance of the ID generator
export const generateRuntimeId = createIdGenerator();
