import * as t from 'io-ts';
import { Socket } from 'socket.io';

const DeviceSocketAuthType = t.type({
  macAddress: t.string,
});

export function isDeviceSocketAuth(client: Socket) {
  const deviceAuthResult = DeviceSocketAuthType.decode(client.handshake.auth);
  return deviceAuthResult._tag === 'Right';
}
