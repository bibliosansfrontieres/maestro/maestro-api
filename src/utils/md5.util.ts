import * as crypto from 'crypto';
import { TDescription } from './applications/validation/types/description.type';
import { THooks } from './applications/validation/types/hooks.type';
import { CreateApplicationTypeDto } from 'src/applications/dto/create-application-type.dto';

export function md5(string: string) {
  return crypto.createHash('md5').update(string).digest('hex');
}

export function applicationMd5(
  description: TDescription,
  hooks: THooks,
  compose: string,
) {
  return md5(`${JSON.stringify({ description, hooks })}${compose}`);
}

export function applicationTypeMd5(
  createApplicationTypeDto: CreateApplicationTypeDto,
) {
  return md5(JSON.stringify(createApplicationTypeDto));
}
