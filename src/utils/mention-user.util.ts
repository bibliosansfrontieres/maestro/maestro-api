import { User } from 'src/users/entities/user.entity';

export function mentionUser(user: User) {
  const username = user.mail.split('@')[0];
  return `<@${username}>`;
}
