import { ApplicationCatalog } from 'src/applications/entities/application-catalog.entity';
import { ApplicationTranslation } from 'src/applications/entities/application-translation.entity';
import { ApplicationType } from 'src/applications/entities/application-type.entity';
import { Application } from 'src/applications/entities/application.entity';
import { Context } from 'src/contexts/entities/context.entity';
import { Item } from 'src/items/entities/item.entity';
import { Playlist } from 'src/playlists/entities/playlist.entity';

export interface IApplicationMockOptions {
  id?: string;
  name?: string;
  displayName?: string;
  exposeServices?: string | null;
  port?: string;
  size?: number;
  version?: string;
  logo?: string;
  image?: string;
  compose?: string;
  hooks?: string;
  md5?: string;
  catalogFunction?: string | null;
  applicationType?: ApplicationType;
  applicationCatalogs?: ApplicationCatalog[];
  applicationTranslations?: ApplicationTranslation[];
  contexts?: Context[];
  items?: Item[];
  playlists?: Playlist[];
}

export function applicationMock(
  options?: IApplicationMockOptions,
): Application {
  return {
    id: options?.id || Date.now().toString(),
    name: options?.name || 'My application name',
    displayName: options?.displayName || 'My application display name',
    exposeServices: options?.exposeServices || null,
    port: options?.port || '80',
    size: options?.size || 1000000,
    version: options?.version || '1.0.0',
    logo: options?.logo || 'https://example.com/logo.png',
    image: options?.image || 'https://example.com/image.png',
    compose: options?.compose || 'docker-compose.yml',
    hooks: options?.hooks || '{}',
    md5: options?.md5 || 'ed076287532e86365e841e92bfc50d8c',
    catalogFunction: options?.catalogFunction || null,
    applicationType: options?.applicationType,
    applicationCatalogs: options?.applicationCatalogs || [],
    applicationTranslations: options?.applicationTranslations || [],
    contexts: options?.contexts || [],
    items: options?.items || [],
    playlists: options?.playlists || [],
  };
}
