import { Application } from 'src/applications/entities/application.entity';
import { Context } from 'src/contexts/entities/context.entity';
import { VirtualMachineSave } from 'src/virtual-machine-saves/entities/virtual-machine-save.entity';
import { VirtualMachine } from 'src/virtual-machines/entities/virtual-machine.entity';

export interface IContextMockOptions {
  id?: number;
  isOutsideProjectDownloadsAuthorized?: boolean;
  isDefaultContext?: boolean;
  originalContext?: string | null;
  applications?: Application[];
  virtualMachine?: VirtualMachine;
  virtualMachineSave?: VirtualMachineSave;
}

export function contextMock(options?: IContextMockOptions): Context {
  return {
    id: options?.id || 1,
    isOutsideProjectDownloadsAuthorized:
      options?.isOutsideProjectDownloadsAuthorized || false,
    isDefaultContext: options?.isDefaultContext || false,
    originalContext: options?.originalContext || null,
    applications: options?.applications || [],
    virtualMachine: options?.virtualMachine || null,
    virtualMachineSave: options?.virtualMachineSave || null,
  };
}
