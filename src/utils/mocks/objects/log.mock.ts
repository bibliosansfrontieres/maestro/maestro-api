import { Log } from 'src/logs/entities/log.entity';
import { LogActionEnum } from 'src/logs/enums/log-action.enum';
import { LogEntityEnum } from 'src/logs/enums/log-entity.enum';
import { LogLevelEnum } from 'src/logs/enums/log-level.enum';
import { User } from 'src/users/entities/user.entity';

export interface ILogMockOptions {
  id?: number;
  action?: LogActionEnum;
  entity?: LogEntityEnum | null;
  entityId?: string | null;
  values?: string | null;
  description?: string | null;
  level?: LogLevelEnum;
  createdAt?: string;
  user?: User;
}

export function logMock(options?: ILogMockOptions): Log {
  return {
    id: options?.id || Date.now(),
    action: options?.action || LogActionEnum.CREATE,
    entity: options?.entity || LogEntityEnum.VIRTUAL_MACHINE,
    entityId: options?.entityId || Date.now().toString(),
    values: options?.values || 'My log values',
    description: options?.description || 'My log description',
    level: options?.level || LogLevelEnum.INFO,
    createdAt: options?.createdAt || '2000-10-31T01:30:00.000-05:00',
    user: options?.user || undefined,
  };
}
