import { ItemSourceEnum } from 'src/items/enums/item-source.enum';
import { Playlist } from 'src/playlists/entities/playlist.entity';
import { ProjectError } from 'src/projects/entities/project-error.entity';
import { Project } from 'src/projects/entities/project.entity';
import { ProjectSourceEnum } from 'src/projects/enums/project-source.enum';
import { ProjectStatusEnum } from 'src/projects/enums/project-status.enum';
import { VirtualMachine } from 'src/virtual-machines/entities/virtual-machine.entity';

export interface IProjectMockOptions {
  id?: number;
  source?: ProjectSourceEnum;
  status?: ProjectStatusEnum;
  url?: string;
  title?: string;
  description?: string;
  date?: string;
  languages?: string;
  startDate?: string;
  endDate?: string;
  partners?: string;
  location?: string;
  device?: string;
  projectManager?: string;
  version?: string;
  playlists?: Playlist[];
  virtualMachines?: VirtualMachine[];
  projectErrors?: ProjectError[];
  createdAt?: string;
  updatedAt?: string;
}

export function projectMock(options?: IProjectMockOptions): Project {
  return {
    id: options?.id || Date.now(),
    source: options?.source || ItemSourceEnum.OMEKA,
    status: options?.status || ProjectStatusEnum.READY,
    url: options?.url || 'http://omeka.fr',
    title: options?.title || 'My project title',
    description: options?.description || 'My project description',
    date: options?.date || '13/01/2025',
    languages: options?.languages || 'fra',
    startDate: options?.startDate || '13/01/2025',
    endDate: options?.endDate || '13/01/2025',
    partners: options?.partners || 'particuliers',
    location: options?.location || 'Montreuil',
    device: options?.device || 'idc',
    projectManager: options?.projectManager || 'Jérémy Lachal',
    version: options?.version || Date.now().toString(),
    playlists: options?.playlists || [],
    virtualMachines: options?.virtualMachines || [],
    projectErrors: options?.projectErrors || [],
    createdAt: options?.createdAt || '2000-10-31T01:30:00.000-05:00',
    updatedAt: options?.updatedAt || '2000-10-31T01:30:00.000-05:00',
  };
}
