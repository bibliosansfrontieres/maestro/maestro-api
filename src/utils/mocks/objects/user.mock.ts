import { Deploy } from 'src/deploys/entities/deploy.entity';
import { Group } from 'src/groups/entities/group.entity';
import { Log } from 'src/logs/entities/log.entity';
import { Notification } from 'src/notifications/entities/notification.entity';
import { Role } from 'src/roles/entities/role.entity';
import { User } from 'src/users/entities/user.entity';
import { VirtualMachineSave } from 'src/virtual-machine-saves/entities/virtual-machine-save.entity';
import { VirtualMachine } from 'src/virtual-machines/entities/virtual-machine.entity';

export interface IUserMockOptions {
  id?: string;
  accessToken?: string | null;
  expiresAt?: number | null;
  refreshToken?: string | null;
  mail?: string;
  name?: string;
  avatar?: string;
  role?: Role;
  groups?: Group[];
  virtualMachines?: VirtualMachine[];
  virtualMachineSaves?: VirtualMachineSave[];
  logs?: Log[];
  deploys?: Deploy[];
  notifications?: Notification[];
}

export function userMock(options?: IUserMockOptions): User {
  return {
    id: options?.id || Date.now().toString(),
    accessToken: options?.accessToken || null,
    expiresAt: options?.expiresAt || null,
    refreshToken: options?.refreshToken || null,
    mail: options?.mail || 'a@a.com',
    name: options?.name || 'John Doe',
    avatar: options?.avatar || 'https://example.com/avatar.jpg',
    role: options?.role || undefined,
    groups: options?.groups || [],
    virtualMachines: options?.virtualMachines || [],
    virtualMachineSaves: options?.virtualMachineSaves || [],
    logs: options?.logs || [],
    deploys: options?.deploys || [],
    notifications: options?.notifications || [],
  };
}
