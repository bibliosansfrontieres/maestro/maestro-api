import { Context } from 'src/contexts/entities/context.entity';
import { Deploy } from 'src/deploys/entities/deploy.entity';
import { User } from 'src/users/entities/user.entity';
import { VirtualMachineSave } from 'src/virtual-machine-saves/entities/virtual-machine-save.entity';
import { VirtualMachineSaveTypeEnum } from 'src/virtual-machine-saves/enums/virtual-machine-save-type.enum';
import { VirtualMachine } from 'src/virtual-machines/entities/virtual-machine.entity';

export interface IVirtualMachineSaveMockOptions {
  id?: string;
  name?: string;
  description?: string | null;
  type?: VirtualMachineSaveTypeEnum;
  virtualMachine?: VirtualMachine;
  createdVirtualMachines?: VirtualMachine[];
  user?: User;
  createdAt?: number;
  context?: Context;
  deploys?: Deploy[];
}

export function virtualMachineSaveMock(
  options?: IVirtualMachineSaveMockOptions,
): VirtualMachineSave {
  return {
    id: options?.id || Date.now().toString(),
    name: options?.name || 'My virtual machine save name',
    description: options?.description || 'My virtual machine save description',
    type: options?.type || VirtualMachineSaveTypeEnum.AUTOMATIC,
    virtualMachine: options?.virtualMachine,
    createdVirtualMachines: options?.createdVirtualMachines || [],
    user: options?.user,
    createdAt: options?.createdAt || Date.now(),
    context: options?.context,
    deploys: options?.deploys || [],
  };
}
