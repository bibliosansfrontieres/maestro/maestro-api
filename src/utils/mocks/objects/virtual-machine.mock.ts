import { Context } from 'src/contexts/entities/context.entity';
import { Group } from 'src/groups/entities/group.entity';
import { Project } from 'src/projects/entities/project.entity';
import { User } from 'src/users/entities/user.entity';
import { VirtualMachineSave } from 'src/virtual-machine-saves/entities/virtual-machine-save.entity';
import { VirtualMachineError } from 'src/virtual-machines/entities/virtual-machine-error.entity';
import { VirtualMachine } from 'src/virtual-machines/entities/virtual-machine.entity';
import { VirtualMachineApprovalEnum } from 'src/virtual-machines/enums/virtual-machine-approval.enum';
import { VirtualMachineStatusEnum } from 'src/virtual-machines/enums/virtual-machine-status.enum';

export interface IVirtualMachineMockOptions {
  id?: string;
  title?: string;
  description?: string | null;
  referenceUrl?: string | null;
  defaultLanguageIdentifier?: string;
  status?: VirtualMachineStatusEnum;
  approval?: VirtualMachineApprovalEnum;
  approvalSaveId?: string | null;
  projectVersion?: string;
  isUpdateAvailable?: boolean;
  token?: string;
  isArchived?: boolean;
  user?: User;
  groups?: Group[];
  context?: Context;
  virtualMachineErrors?: VirtualMachineError[];
  virtualMachineSaves?: VirtualMachineSave[];
  originVirtualMachineSave?: VirtualMachineSave;
  project?: Project;
  createdAt?: string;
  updatedAt?: string;
}

export function virtualMachineMock(
  options?: IVirtualMachineMockOptions,
): VirtualMachine {
  return {
    id: options?.id || Date.now().toString(),
    title: options?.title || 'My virtual machine title',
    description: options?.description || 'My virtual machine description',
    referenceUrl: options?.referenceUrl || 'https://example.com',
    defaultLanguageIdentifier: options?.defaultLanguageIdentifier || 'en',
    status: options?.status || VirtualMachineStatusEnum.CHECKING_PROJECT,
    approval: options?.approval || VirtualMachineApprovalEnum.NONE,
    approvalSaveId: options?.approvalSaveId || null,
    projectVersion: options?.projectVersion || '1.0.0',
    isUpdateAvailable: options?.isUpdateAvailable || false,
    token: options?.token || 'token',
    isArchived: options?.isArchived || false,
    user: options?.user,
    groups: options?.groups || [],
    context: options?.context,
    virtualMachineErrors: options?.virtualMachineErrors || [],
    virtualMachineSaves: options?.virtualMachineSaves || [],
    originVirtualMachineSave: options?.originVirtualMachineSave,
    project: options?.project,
    createdAt: options?.createdAt || '2000-10-31T01:30:00.000-05:00',
    updatedAt: options?.updatedAt || '2000-10-31T01:30:00.000-05:00',
  };
}
