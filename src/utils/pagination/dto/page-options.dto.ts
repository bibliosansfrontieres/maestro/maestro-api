import { ApiPropertyOptional } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsEnum, IsInt, IsOptional, Min } from 'class-validator';
import { PageOrderEnum } from '../enums/page-order.enum';

export class PageOptionsRequest {
  @ApiPropertyOptional({ enum: PageOrderEnum, default: PageOrderEnum.DESC })
  @IsEnum(PageOrderEnum)
  @IsOptional()
  order?: PageOrderEnum;

  @ApiPropertyOptional({
    minimum: 1,
    default: 1,
  })
  @Type(() => Number)
  @IsInt()
  @Min(1)
  @IsOptional()
  page?: number;

  @ApiPropertyOptional({
    minimum: 1,
    default: 10,
  })
  @Type(() => Number)
  @IsInt()
  @Min(1)
  @IsOptional()
  take?: number;
}
