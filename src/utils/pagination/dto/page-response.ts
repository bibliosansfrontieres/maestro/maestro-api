import { ApiProperty } from '@nestjs/swagger';
import { IsArray } from 'class-validator';
import { PageMetaDto } from './page-meta.dto';
import { PageOptions } from '../page-options';

export class PageResponse<T> {
  @IsArray()
  @ApiProperty({ isArray: true })
  data: T[];

  @ApiProperty({ type: () => PageMetaDto })
  meta: PageMetaDto;

  constructor(data: T[], count: number, pageOptions: PageOptions) {
    const meta = new PageMetaDto({ count, pageOptions });
    this.data = data;
    this.meta = meta;
  }
}
