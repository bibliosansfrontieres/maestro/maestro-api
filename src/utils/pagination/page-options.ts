import { PageOptionsRequest } from './dto/page-options.dto';
import { PageOrderEnum } from './enums/page-order.enum';

class PageOptionsDefaultOptions {
  order?: PageOrderEnum;
}
export class PageOptions {
  order: PageOrderEnum;
  take: number;
  page: number;
  skip: number;

  constructor(
    pageOptionsRequest: PageOptionsRequest,
    options?: PageOptionsDefaultOptions,
  ) {
    this.order =
      pageOptionsRequest.order || options?.order || PageOrderEnum.DESC;
    this.take = +pageOptionsRequest.take || 10;
    this.page = +pageOptionsRequest.page || 1;
    this.skip = (this.page - 1) * this.take;
  }
}
