import { SelectQueryBuilder } from 'typeorm';
import { PageOptions } from './pagination/page-options';
import { BadRequestException } from '@nestjs/common';

export class QueryHelper<T> {
  private queryBuilder: SelectQueryBuilder<T>;
  constructor(queryBuilder: SelectQueryBuilder<T>) {
    this.queryBuilder = queryBuilder;
  }

  andWhereQuery(query: string | undefined, keys: string | string[]) {
    if (!query) {
      return;
    }
    const { sql, parameters } = this.query(query, keys);
    this.queryBuilder.andWhere(sql, parameters);
  }

  private query(query: string, keys: string | string[]) {
    if (typeof keys === 'string') {
      return {
        sql: `LOWER(${keys}) LIKE :query`,
        parameters: { query: `%${query.toLowerCase()}%` },
      };
    } else {
      let sql = '';
      const parameters = {};
      for (let i = 0; i < keys.length; i++) {
        const key = keys[i];
        if (i === 0) {
          sql += `(LOWER(${key}) LIKE :query${i}`;
          parameters[`query${i}`] = `%${query.toLowerCase()}%`;
        } else {
          sql += ` OR LOWER(${key}) LIKE :query${i}`;
          parameters[`query${i}`] = `%${query.toLowerCase()}%`;
        }
        if (i === keys.length - 1) {
          sql += ')';
        }
      }
      return { sql, parameters };
    }
  }

  andWhereDateRange(key: string, dateRange?: string) {
    if (!dateRange) {
      return;
    }
    const { sql, parameters } = this.dateRange(key, dateRange);
    this.queryBuilder.andWhere(sql, parameters);
  }

  private dateRange(key: string, dateRange: string) {
    const [startDateString, endDateString] = dateRange.split(',');

    if (!startDateString || !endDateString) {
      throw new BadRequestException(
        'dateRange must be in format YYYY-MM-DD,YYYY-MM-DD',
      );
    }

    const parameters = {};
    const formattedKey = key.replaceAll('.', '');

    parameters[`startDate${formattedKey}`] = this.dateRangeStartDate(dateRange);
    parameters[`endDate${formattedKey}`] = this.dateRangeEndDate(dateRange);

    return {
      sql: `${key} >= :startDate${formattedKey} AND ${key} <= :endDate${formattedKey}`,
      parameters,
    };
  }

  private dateRangeStartDate(dateRange: string) {
    const range = dateRange.split(',');
    const startDateString = range[0];
    const startDateParts = startDateString.split('/');
    const startDate = new Date(
      parseInt(startDateParts[2]),
      parseInt(startDateParts[0]) - 1,
      parseInt(startDateParts[1]),
      0,
      0,
      0,
    );
    return startDate.toISOString().split('T')[0];
  }

  private dateRangeEndDate(dateRange: string) {
    const endDateString = dateRange.split(',')[1];
    const endDateParts = endDateString.split('/');
    const endDate = new Date(
      parseInt(endDateParts[2]),
      parseInt(endDateParts[0]) - 1,
      parseInt(endDateParts[1]),
      23,
      59,
      59,
      999,
    );
    return endDate.toISOString();
  }

  pagination(pageOptions: PageOptions, orderBy: any) {
    this.queryBuilder
      .take(pageOptions.take)
      .skip(pageOptions.skip)
      .orderBy(`${orderBy}`, pageOptions.order);
  }

  andWhereOrAndObjects(objects: object[]) {
    const { query, parameters } = this.orAndObjects(objects);
    this.queryBuilder.andWhere(query, parameters);
  }

  private orAndObjects(objects: object[]) {
    let query = '';
    const parameters = {};
    for (let i = 0; i < objects.length; i++) {
      const object = objects[i];
      let objectQuery = '';
      const keys = Object.keys(object);
      for (let j = 0; j < keys.length; j++) {
        const key = keys[j];
        const line = `${key} = :${key}${i}`;
        parameters[`${key}${i}`] = object[key];
        if (j === 0) {
          objectQuery = `(${line}`;
        } else {
          objectQuery += ` AND ${line}`;
        }
        if (j === keys.length - 1) {
          objectQuery += ')';
        }
      }
      if (i === 0) {
        query = `(${objectQuery}`;
      } else {
        query += ' OR ' + objectQuery;
      }

      if (i === objects.length - 1) {
        query += ')';
      }
    }
    return { query, parameters };
  }

  andWhereMultipleEquals(
    key: string,
    values: string[],
    operator: 'OR' | 'AND' = 'OR',
  ) {
    let sql = '(';
    const parameters = {};
    for (let i = 0; i < values.length; i++) {
      const value = values[i];
      const formattedKey = key.replaceAll('.', '');
      if (i !== values.length - 1) {
        sql += `${key} = :${formattedKey}${i} ${operator} `;
      } else {
        sql += `${key} = :${formattedKey}${i}`;
      }
      parameters[`${formattedKey}${i}`] = value;
    }
    sql += ')';
    this.queryBuilder.andWhere(sql, parameters);
  }

  andWhereMultipleIn(
    key: string,
    values: string[],
    operator: 'OR' | 'AND' = 'OR',
  ) {
    let sql = '(';
    const parameters = {};
    for (let i = 0; i < values.length; i++) {
      const value = values[i];
      const formattedKey = key.replaceAll('.', '');
      if (i !== values.length - 1) {
        sql += `${key} IN (:...${formattedKey}${i}) ${operator} `;
      } else {
        sql += `${key} IN (:...${formattedKey}${i})`;
      }
      parameters[`${formattedKey}${i}`] = value;
    }
    sql += ')';
    this.queryBuilder.andWhere(sql, parameters);
  }

  andWhereEquals(key: string, objectWithValue?: object) {
    const objectKey = Object.keys(objectWithValue)[0];
    if (!objectKey) {
      return;
    }
    const objectValue = objectWithValue[objectKey];
    if (!objectValue) {
      return;
    }
    this.queryBuilder.andWhere(`${key} = :${objectKey}`, {
      [objectKey]: objectValue,
    });
  }
}
