import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { FastifyRequest } from 'fastify';
import { IOlipAuthPayload } from 'src/auth/interfaces/olip-auth-payload.interface';
import { VirtualMachinesService } from 'src/virtual-machines/virtual-machines.service';

export async function storeOlipVmInRequest(
  jwtService: JwtService,
  configService: ConfigService,
  virtualMachinesService: VirtualMachinesService,
  request: FastifyRequest['raw'],
  token?: string,
): Promise<{ isAuthorized: boolean; reason: string }> {
  try {
    // Check validity of token retrieved
    const payload = (await jwtService.verifyAsync(token, {
      secret: configService.get<string>('JWT_SECRET'),
    })) as IOlipAuthPayload;

    const virtualMachine = await virtualMachinesService.findOne({
      where: { id: payload.virtualMachineId },
    });

    // Check if virtualMachine exists
    if (virtualMachine === null) {
      return { isAuthorized: false, reason: 'invalid token' };
    }

    // Store payload in request
    request['payload'] = payload;
    return { isAuthorized: true, reason: '' };
  } catch {
    return { isAuthorized: false, reason: 'invalid token' };
  }
}
