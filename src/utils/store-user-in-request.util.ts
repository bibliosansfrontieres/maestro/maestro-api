import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { FastifyRequest } from 'fastify';
import { IAuthPayload } from 'src/auth/interfaces/auth-payload.interface';
import { EntraService } from 'src/entra/entra.service';
import { PermissionNameEnum } from 'src/permissions/enums/permission-name.enum';
import { CreateUserDto } from 'src/users/dto/create-user.dto';
import { UsersService } from 'src/users/users.service';

export async function storeUserInRequest(
  jwtService: JwtService,
  configService: ConfigService,
  entraService: EntraService,
  usersService: UsersService,
  request: FastifyRequest['raw'],
  token?: string,
  permissionName?: PermissionNameEnum,
): Promise<{ isAuthorized: boolean; reason: string }> {
  // TODO remove dev key
  if (token === 'dev') {
    request['payload'] = { userId: 'f5fb092f-904f-456e-baf0-da44349dfce1' };
    return { isAuthorized: true, reason: '' };
  }
  try {
    let isAuthorized = true;

    // Check validity of token retrieved
    const payload = (await jwtService.verifyAsync(token, {
      secret: configService.get<string>('JWT_SECRET'),
    })) as IAuthPayload;

    const user = await usersService.findOne({
      where: { id: payload.userId },
      relations: { role: { permissions: true } },
    });

    // Check if user exists
    if (user === null) {
      return { isAuthorized: false, reason: 'invalid token' };
    }

    if (permissionName !== undefined) {
      isAuthorized = false;
      const permissionFound = user.role.permissions.find(
        (permission) => permission.name === permissionName,
      );
      isAuthorized = permissionFound !== undefined;
    }

    // If access token expired, create a new one with refresh token
    if (user.expiresAt < Date.now()) {
      const { entraToken, entraId } = await entraService.getToken({
        refreshToken: user.refreshToken,
      });
      await usersService.createOrUpdate(new CreateUserDto(entraToken, entraId));
    }

    // Store payload in request
    request['payload'] = payload;
    return { isAuthorized, reason: 'permission not granted' };
  } catch {
    return { isAuthorized: false, reason: 'invalid token' };
  }
}
