import { Logger } from '@nestjs/common';
import { sleep } from './sleep.util';
import { capitalize } from './capitalize.util';
import { generateRuntimeId } from './id-runtime-generator.util';
export interface IThreadsFunctionParams<T, U> {
  iterationArray: T[];
  maxThreads: number;
  thread: (value: T) => Promise<U | undefined>;
  options?: IThreadsFunctionOptions;
  threadsResults?: U[];
  lastPercentage?: number;
  startTimestamp?: number;
  threadsId?: number;
  iterationArrayCopy?: T[];
  missingIterationArray?: T[];
}

export interface IThreadsFunctionOptions {
  sleepBeforeRestart?: number;
  service?: string;
  name?: string;
}

export async function threadsFunction<T, U>(
  params: IThreadsFunctionParams<T, U>,
): Promise<U[]> {
  const { maxThreads } = params;

  initThreadsParams(params);
  const maxThreadsLength = params.iterationArrayCopy.length;

  // Start threads
  const threads = [];
  for (let i = 0; i < maxThreads && i < maxThreadsLength; i++) {
    threads.push(threadWrapper(params));
  }
  await Promise.all(threads);
  // Threads finished

  if (params.missingIterationArray.length > 0) {
    return restartThreads<T, U>(params);
  }

  logEndingThreads(params);
  return params.threadsResults;
}

async function threadWrapper<T, U>(params: IThreadsFunctionParams<T, U>) {
  const { threadsResults, thread } = params;
  while (params.iterationArrayCopy.length > 0) {
    const value = params.iterationArrayCopy.shift();
    const result = await thread(value);
    logPercentage(params);
    if (result === undefined) {
      params.missingIterationArray.push(value);
      continue;
    }
    threadsResults.push(result);
  }
}

async function restartThreads<T, U>(params: IThreadsFunctionParams<T, U>) {
  // Filter results to keep only missing elements

  if (params?.options?.sleepBeforeRestart) {
    await sleep(params.options.sleepBeforeRestart);
  }

  return threadsFunction<T, U>(params);
}

function getThreadsName(params: IThreadsFunctionParams<any, any>) {
  return params?.options?.name ? params?.options.name + ' ' : '';
}

function logStartingThreads(params: IThreadsFunctionParams<any, any>) {
  if (params.missingIterationArray.length !== 0) {
    log(
      `Restarting ${getThreadsName(params)}threads (missing ${params.missingIterationArray.length} element${params.missingIterationArray.length > 1 ? 's' : ''}, ${params.maxThreads} thread${params.maxThreads > 1 ? 's' : ''})`,
      params,
    );
  } else {
    log(
      `Starting ${getThreadsName(params)}threads (${params.iterationArray.length} element${params.iterationArray.length > 1 ? 's' : ''}, ${params.maxThreads} thread${params.maxThreads > 1 ? 's' : ''})`,
      params,
    );
  }
}

function logEndingThreads(params: IThreadsFunctionParams<any, any>) {
  const took = ((Date.now() - params.startTimestamp) / 1000).toFixed(2);
  log(`Finished ${getThreadsName(params)}threads (took ${took} s)`, params);
}

function log(message: string, params: IThreadsFunctionParams<any, any>) {
  const threadsGroupId = ` - TGID ${params.threadsId}`;
  Logger.log(
    `${message}`,
    params.options?.service
      ? `${params.options.service}${threadsGroupId}`
      : `ThreadFunctionUtil${threadsGroupId}`,
  );
}

function isPercentageLogged(
  params: IThreadsFunctionParams<any, any>,
  percentage: number,
) {
  return (
    params.lastPercentage <= percentage - 10 || params.lastPercentage === -1
  );
}

function logPercentage(params: IThreadsFunctionParams<any, any>) {
  const { threadsResults, iterationArray, options } = params;
  const percentage = Math.round(
    (threadsResults.length / iterationArray.length) * 100,
  );
  if (isPercentageLogged(params, percentage)) {
    params.lastPercentage = percentage;
    log(
      `${capitalize(options?.name ? options.name + ' threads' : 'Threads')} - Percentage : ${percentage}% (${threadsResults.length}/${iterationArray.length})`,
      params,
    );
  }
}

function initThreadsParams(params: IThreadsFunctionParams<any, any>) {
  params.threadsId = params.threadsId ? params.threadsId : generateRuntimeId();

  // Set last percentage
  params.lastPercentage = -1;

  params.startTimestamp = params.startTimestamp
    ? params.startTimestamp
    : Date.now();

  // Set missingIterationArray to empty if not defined
  params.missingIterationArray = params?.missingIterationArray
    ? params.missingIterationArray
    : [];

  // Set threadsResults to empty if not defined
  params.threadsResults = params.threadsResults ? params.threadsResults : [];

  logStartingThreads(params);

  // Copy iterationArray if filteredIterationArray is empty
  params.iterationArrayCopy =
    params?.missingIterationArray?.length > 0
      ? [...params.missingIterationArray]
      : [...params.iterationArray];

  params.missingIterationArray = [];
}
