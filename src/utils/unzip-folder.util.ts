import { createReadStream } from 'fs';
import * as unzipper from 'unzipper';

export async function unzipFolder(
  zipFilePath: string,
  outputFolderPath: string,
) {
  return new Promise<void>((resolve, reject) => {
    createReadStream(zipFilePath)
      .pipe(unzipper.Extract({ path: outputFolderPath }))
      .on('close', () => {
        console.log('Unzipping complete');
        resolve();
      })
      .on('error', (err) => {
        console.error('An error occurred:', err);
        reject(err);
      });
  });
}
