import { RoleNameEnum } from 'src/roles/enums/role-name.enum';
import { User } from 'src/users/entities/user.entity';
import { VirtualMachine } from 'src/virtual-machines/entities/virtual-machine.entity';

export function userCanPatchVm(
  user: User,
  virtualMachine: VirtualMachine,
  onlyCreator = false,
): boolean {
  if (virtualMachine.user.id === user.id) {
    return true;
  }

  if (user.role.name === RoleNameEnum.ADMIN) {
    return true;
  }

  if (onlyCreator === true) {
    return false;
  }

  let isAuthorized = false;
  for (const group of user.groups) {
    if (group.virtualMachines.includes(virtualMachine)) {
      isAuthorized = true;
      break;
    }
  }

  return isAuthorized;
}
