import * as archiver from 'archiver';
import { createWriteStream } from 'fs';

export async function zipFolder(folderPath: string, outputPath: string) {
  // Create a file to stream archive data to
  const output = createWriteStream(outputPath);
  const archive = archiver('zip', {
    zlib: { level: 9 }, // Sets the compression level
  });

  // Listen for all archive data to be written
  output.on('close', () => {
    console.log(
      `Archive has been finalized. Total size: ${archive.pointer()} bytes`,
    );
  });

  // Good practice to catch warnings (ie stat failures and other non-blocking errors)
  archive.on('warning', (err) => {
    if (err.code !== 'ENOENT') {
      throw err;
    }
    console.warn(err);
  });

  // Good practice to catch this error explicitly
  archive.on('error', (err) => {
    throw err;
  });

  // Pipe archive data to the file
  archive.pipe(output);

  // Append files from a directory
  archive.directory(folderPath, false);

  // Finalize the archive (i.e., we are done appending files but streams have to finish yet)
  await archive.finalize();
}
