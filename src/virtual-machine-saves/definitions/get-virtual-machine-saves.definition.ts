import { ApiPropertyOptional } from '@nestjs/swagger';
import { PageOptionsRequest } from 'src/utils/pagination/dto/page-options.dto';
import { VirtualMachineSaveOrderByEnum } from '../enums/virtual-machine-save-order-by.enum';
import { IsEnum, IsOptional, IsString, Matches } from 'class-validator';
import { VirtualMachineSaveTypeEnum } from '../enums/virtual-machine-save-type.enum';
import { PageResponse } from 'src/utils/pagination/dto/page-response';
import { VirtualMachineSave } from '../entities/virtual-machine-save.entity';
import { PageOptions } from 'src/utils/pagination/page-options';

export class GetVirtualMachineSavesRequest extends PageOptionsRequest {
  @ApiPropertyOptional({
    enum: VirtualMachineSaveOrderByEnum,
    default: VirtualMachineSaveOrderByEnum.ID,
    example: VirtualMachineSaveOrderByEnum.ID,
  })
  @IsOptional()
  @IsEnum(VirtualMachineSaveOrderByEnum)
  orderBy?: VirtualMachineSaveOrderByEnum;

  @ApiPropertyOptional({
    example:
      '123e4567-e89b-12d3-a456-426655440000,123e4567-e89b-12d3-a456-426655440000',
  })
  @IsOptional()
  @IsString()
  userIds?: string;

  @ApiPropertyOptional({ example: '01/28/2024,01/29/2024' })
  @IsOptional()
  @IsString()
  @Matches(
    /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2},(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/,
  )
  dateRange?: string;

  @ApiPropertyOptional({
    example: `${VirtualMachineSaveTypeEnum.AUTOMATIC},${VirtualMachineSaveTypeEnum.MANUAL}`,
  })
  @IsOptional()
  @IsString()
  types?: string;

  @ApiPropertyOptional({ example: 'search', description: 'Search by name' })
  @IsOptional()
  @IsString()
  query?: string;
}

export class GetVirtualMachineSavesResponse extends PageResponse<VirtualMachineSave> {
  constructor(
    data: VirtualMachineSave[],
    count: number,
    pageOptions: PageOptions,
  ) {
    super(data, count, pageOptions);
  }
}
