import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsString, MinLength } from 'class-validator';

export class PatchVirtualMachineSaveRequest {
  @ApiProperty({ example: "Save's name" })
  @IsString()
  @MinLength(3)
  @IsOptional()
  name?: string;

  @ApiProperty({ example: "Save's description" })
  @IsString()
  @IsOptional()
  description?: string;
}
