import { VirtualMachine } from 'src/virtual-machines/entities/virtual-machine.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
} from 'typeorm';
import { User } from 'src/users/entities/user.entity';
import { PostVirtualMachineSaveRequest } from '../definitions/post-virtual-machine-save.definition';
import { VirtualMachineSaveTypeEnum } from '../enums/virtual-machine-save-type.enum';
import { Context } from 'src/contexts/entities/context.entity';
import { Deploy } from 'src/deploys/entities/deploy.entity';

@Entity()
export class VirtualMachineSave {
  @PrimaryColumn()
  id: string;

  @Column()
  name: string;

  @Column({ nullable: true })
  description: string | null;

  @Column()
  type: VirtualMachineSaveTypeEnum;

  @ManyToOne(
    () => VirtualMachine,
    (virtualMachine) => virtualMachine.virtualMachineSaves,
  )
  virtualMachine: VirtualMachine;

  @OneToMany(
    () => VirtualMachine,
    (virtualMachine) => virtualMachine.originVirtualMachineSave,
    { nullable: true },
  )
  createdVirtualMachines: VirtualMachine[];

  @ManyToOne(() => User, (user) => user.virtualMachineSaves)
  user: User;

  @CreateDateColumn()
  createdAt: number;

  @OneToOne(() => Context, (context) => context.virtualMachineSave)
  @JoinColumn()
  context: Context;

  @OneToMany(() => Deploy, (deploy) => deploy.virtualMachineSave)
  deploys: Deploy[];

  constructor(
    postVirtualMachineSaveRequest: PostVirtualMachineSaveRequest,
    id: string,
    virtualMachine: VirtualMachine,
    user: User,
    context: Context,
    type?: VirtualMachineSaveTypeEnum,
  ) {
    if (!postVirtualMachineSaveRequest || !id || !virtualMachine || !user)
      return;
    this.name = postVirtualMachineSaveRequest.name;
    this.description = postVirtualMachineSaveRequest.description;
    this.id = id;
    this.virtualMachine = virtualMachine;
    this.user = user;
    this.type = type || VirtualMachineSaveTypeEnum.AUTOMATIC;
    this.context = context;
  }
}
