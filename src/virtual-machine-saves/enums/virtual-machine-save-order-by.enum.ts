export enum VirtualMachineSaveOrderByEnum {
  ID = 'virtualMachineSave.id',
  NAME = 'virtualMachineSave.name',
  CREATED_AT = 'virtualMachineSave.createdAt',
  USER_ID = 'user.id',
}
