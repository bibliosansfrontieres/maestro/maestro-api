export enum VirtualMachineSaveTypeEnum {
  MANUAL = 'MANUAL',
  AUTOMATIC = 'AUTOMATIC',
  APPROVE = 'APPROVE',
}
