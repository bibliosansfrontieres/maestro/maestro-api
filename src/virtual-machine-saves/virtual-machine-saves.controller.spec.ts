import { Test, TestingModule } from '@nestjs/testing';
import { VirtualMachineSavesController } from './virtual-machine-saves.controller';
import { VirtualMachineSavesService } from './virtual-machine-saves.service';
import { Repository } from 'typeorm';
import { VirtualMachineSave } from './entities/virtual-machine-save.entity';
import { getRepositoryToken } from '@nestjs/typeorm';
import { UsersService } from 'src/users/users.service';
import { usersServiceMock } from 'src/utils/mocks/services/users-service.mock';
import { VirtualMachinesService } from 'src/virtual-machines/virtual-machines.service';
import { virtualMachinesServiceMock } from 'src/utils/mocks/services/virtual-machines-service.mock';
import { LogsService } from 'src/logs/logs.service';
import { logsServiceMock } from 'src/utils/mocks/services/logs-service.mock';
import { JwtService } from '@nestjs/jwt';
import { EntraService } from 'src/entra/entra.service';
import { jwtServiceMock } from 'src/utils/mocks/services/jwt-service.mock';
import { entraServiceMock } from 'src/utils/mocks/services/entra-service.mock';
import { ConfigService } from '@nestjs/config';
import { configServiceMock } from 'src/utils/mocks/services/config-service.mock';
import { ContextsService } from 'src/contexts/contexts.service';
import { contextsServiceMock } from 'src/utils/mocks/services/contexts-service.mock';

describe('VirtualMachineSavesController', () => {
  let controller: VirtualMachineSavesController;
  let virtualMachineSaveRepository: Repository<VirtualMachineSave>;

  const VIRTUAL_MACHINE_SAVE_REPOSITORY_TOKEN =
    getRepositoryToken(VirtualMachineSave);

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [VirtualMachineSavesController],
      providers: [
        VirtualMachineSavesService,
        UsersService,
        LogsService,
        JwtService,
        EntraService,
        ConfigService,
        VirtualMachinesService,
        ContextsService,
        {
          provide: VIRTUAL_MACHINE_SAVE_REPOSITORY_TOKEN,
          useClass: Repository<VirtualMachineSave>,
        },
      ],
    })
      .overrideProvider(UsersService)
      .useValue(usersServiceMock)
      .overrideProvider(VirtualMachinesService)
      .useValue(virtualMachinesServiceMock)
      .overrideProvider(LogsService)
      .useValue(logsServiceMock)
      .overrideProvider(JwtService)
      .useValue(jwtServiceMock)
      .overrideProvider(EntraService)
      .useValue(entraServiceMock)
      .overrideProvider(ConfigService)
      .useValue(configServiceMock)
      .overrideProvider(ContextsService)
      .useValue(contextsServiceMock)
      .compile();

    controller = module.get<VirtualMachineSavesController>(
      VirtualMachineSavesController,
    );
    virtualMachineSaveRepository = module.get<Repository<VirtualMachineSave>>(
      VIRTUAL_MACHINE_SAVE_REPOSITORY_TOKEN,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
    expect(virtualMachineSaveRepository).toBeDefined();
  });
});
