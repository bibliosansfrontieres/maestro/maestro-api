import {
  Body,
  Controller,
  Get,
  Param,
  Patch,
  Post,
  Query,
  Res,
} from '@nestjs/common';
import { VirtualMachineSavesService } from './virtual-machine-saves.service';
import { PostVirtualMachineSaveRequest } from './definitions/post-virtual-machine-save.definition';
import { Auth } from 'src/auth/decorators/auth.decorator';
import { PermissionNameEnum } from 'src/permissions/enums/permission-name.enum';
import { UserId } from 'src/auth/decorators/user-id.decorator';
import {
  ApiBadRequestResponse,
  ApiInternalServerErrorResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import {
  GetVirtualMachineSavesRequest,
  GetVirtualMachineSavesResponse,
} from './definitions/get-virtual-machine-saves.definition';
import { FastifyReply } from 'fastify';
import { PatchVirtualMachineSaveRequest } from './definitions/patch-virtual-machine-save.definition';

@Controller('virtual-machine-saves')
@ApiTags('virtual-machine-saves')
export class VirtualMachineSavesController {
  constructor(
    private readonly virtualMachineSavesService: VirtualMachineSavesService,
  ) {}

  @Post(':vmId')
  @Auth(PermissionNameEnum.SAVE_VIRTUAL_MACHINE)
  @ApiOperation({ summary: 'Save a virtual machine' })
  @ApiNotFoundResponse({ description: 'Virtual machine not found' })
  @ApiInternalServerErrorResponse({
    description: 'Failed to save virtual machine',
  })
  @ApiBadRequestResponse({ description: 'Invalid body request parameters' })
  postVirtualMachineSave(
    @Param('vmId') vmId: string,
    @Body() postVirtualMachineSaveRequest: PostVirtualMachineSaveRequest,
    @UserId() userId: string,
  ) {
    return this.virtualMachineSavesService.postVirtualMachineSave(
      vmId,
      postVirtualMachineSaveRequest,
      userId,
    );
  }

  @Patch(':saveId')
  @Auth(PermissionNameEnum.SAVE_VIRTUAL_MACHINE)
  @ApiOperation({ summary: 'Update a virtual machine save' })
  @ApiNotFoundResponse({ description: 'Virtual machine save not found' })
  @ApiInternalServerErrorResponse({
    description: 'Failed to update virtual machine save',
  })
  @ApiBadRequestResponse({ description: 'Invalid body request parameters' })
  patchVirtualMachineSave(
    @Param('saveId') saveId: string,
    @Body() patchVirtualMachineSaveRequest: PatchVirtualMachineSaveRequest,
    @UserId() userId: string,
  ) {
    return this.virtualMachineSavesService.patchVirtualMachineSave(
      saveId,
      patchVirtualMachineSaveRequest,
      userId,
    );
  }

  @Get(':id/database')
  getVirtualMachineSaveDatabase(
    @Param('id') id: string,
    @Res() res: FastifyReply,
  ) {
    return this.virtualMachineSavesService.getVirtualMachineSaveDatabase(
      id,
      res,
    );
  }

  @Get(':id/images')
  getVirtualMachineSaveImages(
    @Param('id') id: string,
    @Res() res: FastifyReply,
  ) {
    return this.virtualMachineSavesService.getVirtualMachineSaveImages(id, res);
  }

  @Get(':id/context')
  getVirtualMachineSaveContext(@Param('id') id: string) {
    return this.virtualMachineSavesService.getVirtualMachineSaveContext(id);
  }

  @Get(':vmId')
  @ApiOperation({ summary: 'Get virtual machine saves' })
  @ApiOkResponse({ type: GetVirtualMachineSavesResponse })
  @ApiNotFoundResponse({ description: 'Virtual machine not found' })
  @ApiBadRequestResponse({ description: 'Invalid body request parameters' })
  getVirtualMachineSaves(
    @Param('vmId') vmId: string,
    @Query() getVirtualMachineSavesRequest: GetVirtualMachineSavesRequest,
  ) {
    return this.virtualMachineSavesService.getVirtualMachineSaves(
      vmId,
      getVirtualMachineSavesRequest,
    );
  }
}
