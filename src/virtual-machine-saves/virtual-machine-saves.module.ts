import { Module, forwardRef } from '@nestjs/common';
import { VirtualMachineSavesService } from './virtual-machine-saves.service';
import { VirtualMachineSavesController } from './virtual-machine-saves.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { VirtualMachineSave } from './entities/virtual-machine-save.entity';
import { UsersModule } from 'src/users/users.module';
import { VirtualMachinesModule } from 'src/virtual-machines/virtual-machines.module';
import { JwtModule } from '@nestjs/jwt';
import { EntraModule } from 'src/entra/entra.module';
import { LogsModule } from 'src/logs/logs.module';
import { ContextsModule } from 'src/contexts/contexts.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([VirtualMachineSave]),
    UsersModule,
    forwardRef(() => VirtualMachinesModule),
    JwtModule,
    EntraModule,
    LogsModule,
    forwardRef(() => ContextsModule),
  ],
  controllers: [VirtualMachineSavesController],
  providers: [VirtualMachineSavesService],
  exports: [VirtualMachineSavesService],
})
export class VirtualMachineSavesModule {}
