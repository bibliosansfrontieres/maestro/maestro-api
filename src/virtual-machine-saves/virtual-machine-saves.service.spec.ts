import { Test, TestingModule } from '@nestjs/testing';
import { VirtualMachineSavesService } from './virtual-machine-saves.service';
import { VirtualMachineSave } from './entities/virtual-machine-save.entity';
import { Repository } from 'typeorm';
import { getRepositoryToken } from '@nestjs/typeorm';
import { UsersService } from 'src/users/users.service';
import { EntraService } from 'src/entra/entra.service';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import { usersServiceMock } from 'src/utils/mocks/services/users-service.mock';
import { configServiceMock } from 'src/utils/mocks/services/config-service.mock';
import { jwtServiceMock } from 'src/utils/mocks/services/jwt-service.mock';
import { entraServiceMock } from 'src/utils/mocks/services/entra-service.mock';
import { VirtualMachinesService } from 'src/virtual-machines/virtual-machines.service';
import { virtualMachinesServiceMock } from 'src/utils/mocks/services/virtual-machines-service.mock';
import { LogsService } from 'src/logs/logs.service';
import { logsServiceMock } from 'src/utils/mocks/services/logs-service.mock';
import { ContextsService } from 'src/contexts/contexts.service';
import { contextsServiceMock } from 'src/utils/mocks/services/contexts-service.mock';
import { NotFoundException } from '@nestjs/common';
import { virtualMachineSaveMock } from 'src/utils/mocks/objects/virtual-machine-save.mock';

describe('VirtualMachineSavesService', () => {
  let service: VirtualMachineSavesService;
  let virtualMachineSaveRepository: Repository<VirtualMachineSave>;

  const VIRTUAL_MACHINE_SAVE_REPOSITORY_TOKEN =
    getRepositoryToken(VirtualMachineSave);

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        VirtualMachineSavesService,
        UsersService,
        EntraService,
        JwtService,
        ConfigService,
        VirtualMachinesService,
        LogsService,
        ContextsService,
        {
          provide: VIRTUAL_MACHINE_SAVE_REPOSITORY_TOKEN,
          useClass: Repository<VirtualMachineSave>,
        },
      ],
    })
      .overrideProvider(UsersService)
      .useValue(usersServiceMock)
      .overrideProvider(EntraService)
      .useValue(entraServiceMock)
      .overrideProvider(JwtService)
      .useValue(jwtServiceMock)
      .overrideProvider(ConfigService)
      .useValue(configServiceMock)
      .overrideProvider(VirtualMachinesService)
      .useValue(virtualMachinesServiceMock)
      .overrideProvider(LogsService)
      .useValue(logsServiceMock)
      .overrideProvider(ContextsService)
      .useValue(contextsServiceMock)
      .compile();

    service = module.get<VirtualMachineSavesService>(
      VirtualMachineSavesService,
    );
    virtualMachineSaveRepository = module.get<Repository<VirtualMachineSave>>(
      VIRTUAL_MACHINE_SAVE_REPOSITORY_TOKEN,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
    expect(virtualMachineSaveRepository).toBeDefined();
  });

  describe('getVirtualMachineSaveContext', () => {
    const id = '1';
    const mockVirtualMachineSave = virtualMachineSaveMock({ id });

    it('should be defined', () => {
      expect(service.getVirtualMachineSaveContext).toBeDefined();
    });

    it('should call findOne with right arguments', async () => {
      const findOneSpy = jest
        .spyOn(virtualMachineSaveRepository, 'findOne')
        .mockResolvedValue(mockVirtualMachineSave);
      await service.getVirtualMachineSaveContext(`${id}`);
      expect(findOneSpy).toHaveBeenCalledWith({
        where: { id: id },
        relations: { context: { applications: true } },
      });
    });

    it('should throw NotFoundException if virtual machine save not found', async () => {
      jest
        .spyOn(virtualMachineSaveRepository, 'findOne')
        .mockResolvedValue(null);

      await expect(
        service.getVirtualMachineSaveContext(`${id}`),
      ).rejects.toThrow(
        new NotFoundException('virtual machine save not found'),
      );
    });

    it('should return context', async () => {
      jest.spyOn(virtualMachineSaveRepository, 'findOne').mockResolvedValue({
        context: { id: 1 },
      } as VirtualMachineSave);
      const result = await service.getVirtualMachineSaveContext(`${id}`);
      expect(result).toEqual({ id: 1 });
    });
  });
});
