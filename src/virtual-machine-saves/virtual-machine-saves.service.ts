import {
  Inject,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
  forwardRef,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { VirtualMachineSave } from './entities/virtual-machine-save.entity';
import { FindOneOptions, Repository } from 'typeorm';
import { VirtualMachine } from 'src/virtual-machines/entities/virtual-machine.entity';
import { UsersService } from 'src/users/users.service';
import { join } from 'path';
import { v4 as uuidv4 } from 'uuid';
import {
  copyFileSync,
  createReadStream,
  existsSync,
  mkdirSync,
  rmSync,
} from 'fs';
import { VirtualMachinesService } from 'src/virtual-machines/virtual-machines.service';
import { PostVirtualMachineSaveRequest } from './definitions/post-virtual-machine-save.definition';
import { LogsService } from 'src/logs/logs.service';
import { LogActionEnum } from 'src/logs/enums/log-action.enum';
import { LogEntityEnum } from 'src/logs/enums/log-entity.enum';
import {
  GetVirtualMachineSavesRequest,
  GetVirtualMachineSavesResponse,
} from './definitions/get-virtual-machine-saves.definition';
import { QueryHelper } from 'src/utils/query-helpers.util';
import { PageOptions } from 'src/utils/pagination/page-options';
import { VirtualMachineSaveOrderByEnum } from './enums/virtual-machine-save-order-by.enum';
import { VirtualMachineSaveTypeEnum } from './enums/virtual-machine-save-type.enum';
import { getManyAndCount } from 'src/utils/get-many-and-count.util';
import { VirtualMachineStatusEnum } from 'src/virtual-machines/enums/virtual-machine-status.enum';
import { ContextsService } from 'src/contexts/contexts.service';
import { FastifyReply } from 'fastify';
import { zipFolder } from 'src/utils/zip-folder.util';
import { unzipFolder } from 'src/utils/unzip-folder.util';
import { PatchVirtualMachineSaveRequest } from './definitions/patch-virtual-machine-save.definition';

@Injectable()
export class VirtualMachineSavesService {
  private virtualMachinesPath = join(__dirname, '../../data/virtual_machines');

  constructor(
    @InjectRepository(VirtualMachineSave)
    private virtualMachineSaveRepository: Repository<VirtualMachineSave>,
    private usersService: UsersService,
    @Inject(forwardRef(() => VirtualMachinesService))
    private virtualMachinesService: VirtualMachinesService,
    private logsService: LogsService,
    @Inject(forwardRef(() => ContextsService))
    private contextsService: ContextsService,
  ) {}

  async create(
    postVirtualMachineSaveRequest: PostVirtualMachineSaveRequest,
    virtualMachine: VirtualMachine,
    userId: string,
    type?: VirtualMachineSaveTypeEnum,
  ) {
    const user = await this.usersService.findOne({ where: { id: userId } });
    if (!user) {
      throw new NotFoundException('user not found');
    }

    const id = uuidv4();

    const context = await this.contextsService.cloneContext(
      virtualMachine.context.id,
    );

    const virtualMachineSave = new VirtualMachineSave(
      postVirtualMachineSaveRequest,
      id,
      virtualMachine,
      user,
      context,
      type,
    );

    const saveResult = await this.saveVirtualMachine(virtualMachine, id);
    if (!saveResult) {
      throw new InternalServerErrorException('failed to save virtual machine');
    }

    await this.logsService.create({
      authorId: user.id,
      action: LogActionEnum.CREATE,
      entity: LogEntityEnum.VIRTUAL_MACHINE_SAVE,
      entityId: id,
      values: postVirtualMachineSaveRequest,
    });

    return this.virtualMachineSaveRepository.save(virtualMachineSave);
  }

  async saveVirtualMachine(virtualMachine: VirtualMachine, id: string) {
    try {
      const virtualMachinePath = `${this.virtualMachinesPath}/${virtualMachine.id}`;
      const databasePath = `${virtualMachinePath}/data/databases/olip-api-v2.db`;
      const savesPath = `${virtualMachinePath}/saves`;

      if (!existsSync(savesPath)) {
        mkdirSync(savesPath);
      }

      copyFileSync(databasePath, `${savesPath}/${id}.db`);

      const imagesPath = `${virtualMachinePath}/data/static/images`;
      const imagesSavePath = `${savesPath}/${id}-images.zip`;
      await zipFolder(imagesPath, imagesSavePath);
      return true;
    } catch (e) {
      console.error(e);
      return false;
    }
  }

  async postVirtualMachineSave(
    vmId: string,
    postVirtualMachineSaveRequest: PostVirtualMachineSaveRequest,
    userId: string,
  ) {
    const virtualMachine = await this.virtualMachinesService.findOne({
      where: { id: vmId },
      relations: { context: true },
    });
    if (!virtualMachine) {
      throw new NotFoundException('virtual machine not found');
    }

    if (
      virtualMachine.status !== VirtualMachineStatusEnum.ONLINE &&
      virtualMachine.status !== VirtualMachineStatusEnum.OFFLINE
    ) {
      throw new NotFoundException('virtual machine not online/offline');
    }

    await this.create(
      postVirtualMachineSaveRequest,
      virtualMachine,
      userId,
      VirtualMachineSaveTypeEnum.MANUAL,
    );
  }

  async patchVirtualMachineSave(
    saveId: string,
    patchVirtualMachineSaveRequest: PatchVirtualMachineSaveRequest,
    userId: string,
  ) {
    const save = await this.findOne({
      where: { id: saveId },
    });
    if (!save) {
      throw new NotFoundException('virtual machine save not found');
    }

    save.name = patchVirtualMachineSaveRequest.name || save.name;
    save.description =
      patchVirtualMachineSaveRequest.description || save.description;

    await this.virtualMachineSaveRepository.save(save);

    await this.logsService.create({
      authorId: userId,
      action: LogActionEnum.UPDATE,
      entity: LogEntityEnum.VIRTUAL_MACHINE_SAVE,
      entityId: saveId,
      values: patchVirtualMachineSaveRequest,
    });
  }

  async getVirtualMachineSaves(
    vmId: string,
    getVirtualMachineSavesRequest: GetVirtualMachineSavesRequest,
  ) {
    const virtualMachine = await this.virtualMachinesService.findOne({
      where: { id: vmId },
    });
    if (!virtualMachine) {
      throw new NotFoundException('virtual machine not found');
    }

    const pageOptions = new PageOptions(getVirtualMachineSavesRequest);

    const { query, dateRange } = getVirtualMachineSavesRequest;
    let { orderBy } = getVirtualMachineSavesRequest;

    orderBy = orderBy ? orderBy : VirtualMachineSaveOrderByEnum.CREATED_AT;

    const savesQueryBuilder = this.virtualMachineSaveRepository
      .createQueryBuilder('virtualMachineSave')
      .innerJoinAndSelect('virtualMachineSave.user', 'user')
      .innerJoin('virtualMachineSave.virtualMachine', 'virtualMachine')
      .leftJoinAndSelect(
        'virtualMachineSave.createdVirtualMachines',
        'createdVirtualMachine',
      )
      .leftJoinAndSelect('createdVirtualMachine.user', 'virtualMachineUser')
      .where('virtualMachine.id = :vmId', {
        vmId,
      });

    const queryHelper = new QueryHelper(savesQueryBuilder);
    queryHelper.pagination(pageOptions, orderBy);

    if (getVirtualMachineSavesRequest.userIds) {
      const userIds = getVirtualMachineSavesRequest.userIds.split(',');
      queryHelper.andWhereMultipleEquals('user.id', userIds);
    }

    if (getVirtualMachineSavesRequest.types) {
      const types = getVirtualMachineSavesRequest.types.split(',');
      queryHelper.andWhereMultipleEquals('virtualMachineSave.type', types);
    }

    queryHelper.andWhereDateRange('virtualMachineSave.createdAt', dateRange);
    queryHelper.andWhereQuery(query, 'virtualMachineSave.name');

    const [count, virtualMachineSaves] =
      await getManyAndCount<VirtualMachineSave>(savesQueryBuilder, [
        'virtualMachineSave.id',
      ]);

    return new GetVirtualMachineSavesResponse(
      virtualMachineSaves,
      count,
      pageOptions,
    );
  }

  async getVirtualMachineSaveDatabase(id: string, res: FastifyReply) {
    const virtualMachineSave = await this.virtualMachineSaveRepository.findOne({
      where: { id },
      relations: { virtualMachine: true },
    });
    if (!virtualMachineSave) {
      throw new NotFoundException('virtual machine save not found');
    }
    const databasePath = `${this.virtualMachinesPath}/${virtualMachineSave.virtualMachine.id}/saves/${virtualMachineSave.id}.db`;
    if (!existsSync(databasePath)) {
      throw new NotFoundException('database file not found');
    }

    const file = createReadStream(databasePath);
    res.raw.setHeader('Content-Type', 'application/octet-stream');
    res.raw.setHeader(
      'Content-Disposition',
      `attachment; filename="${virtualMachineSave.id}.db"`,
    );
    file.pipe(res.raw);
  }

  async getVirtualMachineSaveImages(id: string, res: FastifyReply) {
    const virtualMachineSave = await this.virtualMachineSaveRepository.findOne({
      where: { id },
      relations: { virtualMachine: true },
    });
    if (!virtualMachineSave) {
      throw new NotFoundException('virtual machine save not found');
    }
    const imagesPath = `${this.virtualMachinesPath}/${virtualMachineSave.virtualMachine.id}/saves/${virtualMachineSave.id}-images.zip`;
    if (!existsSync(imagesPath)) {
      throw new NotFoundException('images zip file not found');
    }

    const file = createReadStream(imagesPath);
    res.raw.setHeader('Content-Type', 'application/octet-stream');
    res.raw.setHeader(
      'Content-Disposition',
      `attachment; filename="${virtualMachineSave.id}-images.zip"`,
    );
    file.pipe(res.raw);
  }

  async getVirtualMachineSaveContext(id: string) {
    const virtualMachineSave = await this.virtualMachineSaveRepository.findOne({
      where: { id },
      relations: { context: { applications: true } },
    });
    if (!virtualMachineSave) {
      throw new NotFoundException('virtual machine save not found');
    }
    return virtualMachineSave.context;
  }

  async importDatabase(
    virtualMachine: VirtualMachine,
    virtualMachineSave: VirtualMachineSave,
  ) {
    try {
      const databasePath = `${this.virtualMachinesPath}/${virtualMachineSave.virtualMachine.id}/saves/${virtualMachineSave.id}.db`;
      const newDatabasePath = `${this.virtualMachinesPath}/${virtualMachine.id}/data/databases`;
      if (!existsSync(newDatabasePath)) {
        mkdirSync(newDatabasePath, { recursive: true });
      }
      copyFileSync(databasePath, `${newDatabasePath}/olip-api-v2.db`);
      const imagesPath = `${this.virtualMachinesPath}/${virtualMachineSave.virtualMachine.id}/data/static/images`;
      if (existsSync(imagesPath)) {
        rmSync(imagesPath, { recursive: true });
      }
      mkdirSync(imagesPath);
      await unzipFolder(
        `${this.virtualMachinesPath}/${virtualMachineSave.virtualMachine.id}/saves/${virtualMachineSave.id}-images.zip`,
        imagesPath,
      );
      return true;
    } catch (e) {
      console.error(e);
      return false;
    }
  }

  findOne(options?: FindOneOptions<VirtualMachineSave>) {
    return this.virtualMachineSaveRepository.findOne(options);
  }
}
