import { ApiPropertyOptional } from '@nestjs/swagger';
import { Deploy } from 'src/deploys/entities/deploy.entity';
import { PageOptionsRequest } from 'src/utils/pagination/dto/page-options.dto';
import { PageResponse } from 'src/utils/pagination/dto/page-response';
import { PageOptions } from 'src/utils/pagination/page-options';
import { VirtualMachineDeployOrderByEnum } from '../enums/virtual-machine-deploy-order-by.enum';
import { IsEnum, IsOptional, IsString } from 'class-validator';
import { DeployStatusEnum } from 'src/deploys/enums/deploy-status.enum';

export class GetVirtualMachineDeploysRequest extends PageOptionsRequest {
  @ApiPropertyOptional({
    example: `${DeployStatusEnum.DEPLOYED},${DeployStatusEnum.ORDER_SENT}`,
  })
  @IsString()
  @IsOptional()
  statuses?: string;

  @ApiPropertyOptional({ example: '1,2' })
  @IsString()
  @IsOptional()
  userIds?: string;

  @ApiPropertyOptional({ example: '1,2' })
  @IsString()
  @IsOptional()
  labelIds?: string;

  @ApiPropertyOptional({ example: '12/30/2021,12/31/2021' })
  @IsOptional()
  @IsString()
  createdDateRange?: string;

  @ApiPropertyOptional({ example: 'search for labels' })
  @IsOptional()
  @IsString()
  query?: string;

  @ApiPropertyOptional({
    example: VirtualMachineDeployOrderByEnum.ID,
    enum: VirtualMachineDeployOrderByEnum,
    default: VirtualMachineDeployOrderByEnum.ID,
  })
  @IsEnum(VirtualMachineDeployOrderByEnum)
  @IsOptional()
  orderBy?: VirtualMachineDeployOrderByEnum;
}

export class GetVirtualMachineDeploysResponse extends PageResponse<Deploy> {
  constructor(data: Deploy[], count: number, pageOptions: PageOptions) {
    super(data, count, pageOptions);
  }
}
