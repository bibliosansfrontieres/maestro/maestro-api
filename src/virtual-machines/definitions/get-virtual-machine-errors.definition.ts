import { PageOptionsRequest } from 'src/utils/pagination/dto/page-options.dto';
import { PageResponse } from 'src/utils/pagination/dto/page-response';
import { PageOptions } from 'src/utils/pagination/page-options';
import { VirtualMachineError } from '../entities/virtual-machine-error.entity';

export class GetVirtualMachineErrorsRequest extends PageOptionsRequest {}

export class GetVirtualMachineErrorsResponse extends PageResponse<VirtualMachineError> {
  constructor(
    data: VirtualMachineError[],
    count: number,
    pageOptions: PageOptions,
  ) {
    super(data, count, pageOptions);
  }
}
