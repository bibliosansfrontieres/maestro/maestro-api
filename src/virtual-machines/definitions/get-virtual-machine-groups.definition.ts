import { Group } from 'src/groups/entities/group.entity';
import { PageOptionsRequest } from 'src/utils/pagination/dto/page-options.dto';
import { PageResponse } from 'src/utils/pagination/dto/page-response';
import { PageOptions } from 'src/utils/pagination/page-options';
import { VirtualMachineGroupOrderByEnum } from '../enums/virtual-machine-group-order-by.enum';

export class GetVirtualMachineGroupsRequest extends PageOptionsRequest {
  orderBy?: VirtualMachineGroupOrderByEnum;
}

export class GetVirtualMachineGroupsResponse extends PageResponse<Group> {
  constructor(data: Group[], count: number, pageOptions: PageOptions) {
    super(data, count, pageOptions);
  }
}
