import { ApiPropertyOptional } from '@nestjs/swagger';
import { LogActionEnum } from 'src/logs/enums/log-action.enum';
import { LogLevelEnum } from 'src/logs/enums/log-level.enum';
import { LogOrderByEnum } from 'src/logs/enums/log-order-by.enum';
import { PageOptionsRequest } from 'src/utils/pagination/dto/page-options.dto';
import { VirtualMachineHistoryEntity } from '../enums/virtual-machine-history-entity.enum';
import { PageResponse } from 'src/utils/pagination/dto/page-response';
import { Log } from 'src/logs/entities/log.entity';
import { PageOptions } from 'src/utils/pagination/page-options';

export class GetVirtualMachineHistoryRequest extends PageOptionsRequest {
  @ApiPropertyOptional({ example: '01/25/2024,02/25/2024', default: undefined })
  dateRange?: string;

  @ApiPropertyOptional({
    example:
      'f5fb092f-904f-456e-baf0-a0d0a0d0e0,f5fb092f-904f-456e-baf0-a0d0a0d0e0',
  })
  userIds?: string;

  @ApiPropertyOptional({
    example: `${LogActionEnum.CREATE},${LogActionEnum.UPDATE}`,
    enum: LogActionEnum,
  })
  actions?: string;

  @ApiPropertyOptional({
    example: `${VirtualMachineHistoryEntity.VIRTUAL_MACHINE},${VirtualMachineHistoryEntity.CONTEXT}`,
  })
  entities?: string;

  @ApiPropertyOptional({
    example: `${LogLevelEnum.INFO},${LogLevelEnum.DEBUG}`,
  })
  levels?: string;

  @ApiPropertyOptional({
    example: LogOrderByEnum.ENTITY,
    default: LogOrderByEnum.ID,
    enum: LogOrderByEnum,
  })
  orderBy?: LogOrderByEnum;

  @ApiPropertyOptional({
    example: 'search',
    description: 'Query all fields',
  })
  query?: string;
}

export class GetVirtualMachineHistoryResponse extends PageResponse<Log> {
  constructor(data: Log[], count: number, pageOptions: PageOptions) {
    super(data, count, pageOptions);
  }
}
