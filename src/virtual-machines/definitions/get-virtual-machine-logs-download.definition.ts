import { ApiProperty } from '@nestjs/swagger';
import { VirtualMachineContainerEnum } from '../enums/virtual-machine-container.enum';

export class GetVirtualMachineLogsDownloadRequest {
  @ApiProperty({
    example: VirtualMachineContainerEnum.OLIP_API,
    enum: VirtualMachineContainerEnum,
  })
  container: VirtualMachineContainerEnum;
}
