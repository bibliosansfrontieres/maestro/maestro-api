import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { VirtualMachineContainerEnum } from '../enums/virtual-machine-container.enum';

export class GetVirtualMachineLogsRequest {
  @ApiProperty({
    example: VirtualMachineContainerEnum.OLIP_API,
    enum: VirtualMachineContainerEnum,
  })
  container: VirtualMachineContainerEnum;

  @ApiPropertyOptional({ example: '100', default: '100' })
  maxLines?: string;
}

export class GetVirtualMachineLogsResponse {
  html: string;
  constructor(html: string) {
    this.html = html;
  }
}
