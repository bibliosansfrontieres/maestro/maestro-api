import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { VirtualMachine } from '../entities/virtual-machine.entity';
import { Context } from 'src/contexts/entities/context.entity';
import { GetContextResponse } from 'src/contexts/definitions/get-context.definition';
import { GetGroupResponse } from 'src/groups/definitions/get-group.definition';
import { Group } from 'src/groups/entities/group.entity';
import { User } from 'src/users/entities/user.entity';
import { GetProfileResponse } from 'src/users/definitions/get-profile.definition';
import { VirtualMachineApprovalEnum } from '../enums/virtual-machine-approval.enum';
import { VirtualMachineStatusEnum } from '../enums/virtual-machine-status.enum';
import { VirtualMachineSave } from 'src/virtual-machine-saves/entities/virtual-machine-save.entity';
import { Project } from 'src/projects/entities/project.entity';

export class GetVirtualMachineResponse {
  @ApiProperty({ example: 'sxg49' })
  id: string;

  @ApiProperty({ example: 'fra' })
  defaultLanguageIdentifier: string;

  @ApiProperty({
    example: VirtualMachineStatusEnum.CHECKING_PROJECT,
    enum: VirtualMachineStatusEnum,
  })
  status: VirtualMachineStatusEnum;

  @ApiProperty({
    example: VirtualMachineApprovalEnum.NONE,
    enum: VirtualMachineApprovalEnum,
  })
  approval: VirtualMachineApprovalEnum;

  @ApiProperty({ example: Date.now().toString() })
  projectVersion: string;

  @ApiProperty({ example: false })
  isUpdateAvailable: boolean;

  @ApiProperty({ example: false })
  isArchived: boolean;

  @ApiProperty({ example: 'Virtual machine title' })
  title: string;

  @ApiProperty({ example: 'Description' })
  description: string | null;

  @ApiProperty({ example: 'https://google.com' })
  referenceUrl: string | null;

  @ApiProperty({ type: GetProfileResponse })
  user: User;

  @ApiProperty()
  project: Project;

  @ApiProperty({ type: GetGroupResponse, isArray: true })
  groups: Group[];

  @ApiProperty({ type: GetContextResponse })
  context: Context;

  @ApiProperty({ example: Date.now().toString() })
  createdAt: string;

  @ApiProperty({ example: Date.now().toString() })
  updatedAt: string;

  @ApiPropertyOptional()
  originVirtualMachineSave: VirtualMachineSave | null;

  constructor(virtualMachine: VirtualMachine) {
    this.id = virtualMachine.id;
    this.defaultLanguageIdentifier = virtualMachine.defaultLanguageIdentifier;
    this.status = virtualMachine.status;
    this.approval = virtualMachine.approval;
    this.projectVersion = virtualMachine.projectVersion;
    this.isUpdateAvailable = virtualMachine.isUpdateAvailable;
    this.user = virtualMachine.user;
    this.groups = virtualMachine.groups;
    this.context = virtualMachine.context;
    this.createdAt = virtualMachine.createdAt;
    this.updatedAt = virtualMachine.updatedAt;
    this.isArchived = virtualMachine.isArchived;
    this.title = virtualMachine.title;
    this.description = virtualMachine.description;
    this.referenceUrl = virtualMachine.referenceUrl;
    this.originVirtualMachineSave = virtualMachine.originVirtualMachineSave;
    this.project = virtualMachine.project;
  }
}
