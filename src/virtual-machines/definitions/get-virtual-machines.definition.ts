import { PageOptionsRequest } from 'src/utils/pagination/dto/page-options.dto';
import { PageResponse } from 'src/utils/pagination/dto/page-response';
import { VirtualMachine } from '../entities/virtual-machine.entity';
import { PageOptions } from 'src/utils/pagination/page-options';
import { IsEnum, IsOptional, IsString } from 'class-validator';
import { BooleanEnum } from 'src/utils/enums/boolean.enum';
import { ApiPropertyOptional } from '@nestjs/swagger';
import { VirtualMachineOrderByEnum } from '../enums/virtual-machine-order-by.enum';

export class GetVirtualMachinesRequest extends PageOptionsRequest {
  @ApiPropertyOptional({ example: 'TRUE', enum: BooleanEnum })
  @IsOptional()
  @IsEnum(BooleanEnum)
  isUserVmsFiltered?: BooleanEnum;

  @ApiPropertyOptional({
    example: 'ONLINE,OFFLINE',
    description: 'One or multiple VirtualMachineStatusEnum separated by comma',
  })
  @IsOptional()
  @IsString()
  statuses?: string;

  @ApiPropertyOptional({
    example:
      '4f1181be-f81f-47c5-9e97-5a5d0222cc53,2323fef4-0f23-47af-b144-03d1285e2a73',
    description: 'One or multiple user ids separated by comma',
  })
  @IsOptional()
  @IsString()
  users?: string;

  @ApiPropertyOptional({
    example: 'NONE,APPROVED',
    description:
      'One or multiple VirtualMachineApprovalEnum separated by comma',
  })
  @IsOptional()
  @IsString()
  approvals?: string;

  @ApiPropertyOptional({
    example: '1,2',
    description: 'One or multiple group ids separated by comma',
  })
  @IsOptional()
  @IsString()
  groups?: string;

  @ApiPropertyOptional({ example: 'search' })
  @IsOptional()
  @IsString()
  query?: string;

  @ApiPropertyOptional({ example: '12/30/2021,12/31/2021' })
  @IsOptional()
  @IsString()
  createdDateRange?: string;

  @ApiPropertyOptional({ example: '12/30/2021,12/31/2021' })
  @IsOptional()
  @IsString()
  updatedDateRange?: string;

  @ApiPropertyOptional({
    example: VirtualMachineOrderByEnum.ID,
    enum: VirtualMachineOrderByEnum,
    default: VirtualMachineOrderByEnum.ID,
  })
  @IsOptional()
  @IsEnum(VirtualMachineOrderByEnum)
  orderBy?: VirtualMachineOrderByEnum;
}

export class GetVirtualMachinesResponse extends PageResponse<VirtualMachine> {
  constructor(data: VirtualMachine[], count: number, pageOptions: PageOptions) {
    super(data, count, pageOptions);
  }
}
