import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class PatchVirtualMachineApprovalApproveRequest {
  @ApiProperty({ example: 'My approved save' })
  @IsString()
  name: string;
}
