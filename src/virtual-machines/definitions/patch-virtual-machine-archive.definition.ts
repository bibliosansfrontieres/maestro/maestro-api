import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean } from 'class-validator';

export class PatchVirtualMachineArchiveRequest {
  @ApiProperty({ example: true })
  @IsBoolean()
  isArchived: boolean;
}
