import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean } from 'class-validator';

export class PatchVirtualMachineDeployStatusRequest {
  @ApiProperty({ example: true })
  @IsBoolean()
  isDeployed: boolean;
}
