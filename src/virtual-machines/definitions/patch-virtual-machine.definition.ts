import { IsOptional, IsString } from 'class-validator';
import { VirtualMachine } from '../entities/virtual-machine.entity';
import { GetVirtualMachineResponse } from './get-virtual-machine.definition';

export class PatchVirtualMachineRequest {
  @IsOptional()
  @IsString()
  title?: string;

  @IsOptional()
  @IsString()
  description?: string;

  @IsOptional()
  @IsString()
  referenceUrl?: string;

  @IsOptional()
  @IsString()
  defaultLanguageIdentifier?: string;
}

export class PatchVirtualMachineResponse extends GetVirtualMachineResponse {
  constructor(virtualMachine: VirtualMachine) {
    super(virtualMachine);
  }
}
