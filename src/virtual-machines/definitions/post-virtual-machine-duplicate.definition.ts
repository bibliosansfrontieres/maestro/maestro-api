import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';
import { VirtualMachine } from '../entities/virtual-machine.entity';
import { GetVirtualMachineResponse } from './get-virtual-machine.definition';

export class PostVirtualMachineDuplicateRequest {
  @ApiProperty({ example: 'My title' })
  @IsString()
  title: string;

  @ApiPropertyOptional({ example: 'My description' })
  @IsOptional()
  @IsString()
  description?: string;

  @ApiPropertyOptional({ example: 'https://example.com' })
  @IsOptional()
  @IsString()
  referenceUrl?: string;
}

export class PostVirtualMachineDuplicateResponse extends GetVirtualMachineResponse {
  constructor(virtualMachine: VirtualMachine) {
    super(virtualMachine);
  }
}
