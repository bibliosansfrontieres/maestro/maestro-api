import {
  PostVirtualMachineDuplicateRequest,
  PostVirtualMachineDuplicateResponse,
} from './post-virtual-machine-duplicate.definition';

export class PostVirtualMachineFromSaveRequest extends PostVirtualMachineDuplicateRequest {}

export class PostVirtualMachineFromSaveResponse extends PostVirtualMachineDuplicateResponse {}
