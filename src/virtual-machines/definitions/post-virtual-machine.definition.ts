import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsNumber, IsOptional, IsString } from 'class-validator';
import { VirtualMachine } from '../entities/virtual-machine.entity';
import { GetVirtualMachineResponse } from './get-virtual-machine.definition';

export class PostVirtualMachineRequest {
  @ApiProperty({ example: 88040 })
  @IsNumber()
  projectId: number;

  @ApiProperty({ example: 'My title' })
  @IsString()
  title: string;

  @ApiProperty({ example: 'fra' })
  @IsString()
  defaultLanguageIdentifier: string;

  @ApiPropertyOptional({ example: 'My description' })
  @IsOptional()
  @IsString()
  description?: string;

  @ApiPropertyOptional({ example: 'https://example.com' })
  @IsOptional()
  @IsString()
  referenceUrl?: string;
}

export class PostVirtualMachineResponse extends GetVirtualMachineResponse {
  constructor(virtualMachine: VirtualMachine) {
    super(virtualMachine);
  }
}
