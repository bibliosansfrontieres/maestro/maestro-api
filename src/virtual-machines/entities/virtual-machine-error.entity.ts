import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { VirtualMachineErrorEntityEnum } from '../enums/virtual-machine-error-entity.enum';
import { ErrorDto } from 'src/omeka/dto/error.dto';
import { VirtualMachine } from './virtual-machine.entity';

@Entity()
export class VirtualMachineError {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  entity: VirtualMachineErrorEntityEnum;

  @Column()
  entityId: number;

  @Column()
  message: string;

  @ManyToOne(
    () => VirtualMachine,
    (virtualMachine) => virtualMachine.virtualMachineErrors,
  )
  virtualMachine: VirtualMachine;

  constructor(errorDto: ErrorDto, virtualMachine: VirtualMachine) {
    if (!errorDto || !virtualMachine) return;
    this.entity = errorDto.type.toUpperCase() as VirtualMachineErrorEntityEnum;
    this.entityId = errorDto.id;
    this.message = errorDto.message;
    this.virtualMachine = virtualMachine;
  }
}
