import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  UpdateDateColumn,
} from 'typeorm';
import { VirtualMachineStatusEnum } from '../enums/virtual-machine-status.enum';
import { VirtualMachineApprovalEnum } from '../enums/virtual-machine-approval.enum';
import { User } from 'src/users/entities/user.entity';
import { Group } from 'src/groups/entities/group.entity';
import { Context } from 'src/contexts/entities/context.entity';
import { PostVirtualMachineRequest } from '../definitions/post-virtual-machine.definition';
import { VirtualMachineError } from './virtual-machine-error.entity';
import { VirtualMachineSave } from 'src/virtual-machine-saves/entities/virtual-machine-save.entity';
import { Project } from 'src/projects/entities/project.entity';

@Entity()
export class VirtualMachine {
  @PrimaryColumn()
  id: string;

  @Column()
  title: string;

  @Column({ nullable: true })
  description: string | null;

  @Column({ nullable: true })
  referenceUrl: string | null;

  @Column()
  defaultLanguageIdentifier: string;

  @Column()
  status: VirtualMachineStatusEnum;

  @Column()
  approval: VirtualMachineApprovalEnum;

  @Column({ nullable: true })
  approvalSaveId: string | null;

  @Column()
  projectVersion: string;

  @Column()
  isUpdateAvailable: boolean;

  @Column()
  token: string;

  @Column()
  isArchived: boolean;

  @ManyToOne(() => User, (user) => user.virtualMachines)
  user: User;

  @ManyToMany(() => Group, (group) => group.virtualMachines)
  @JoinTable()
  groups: Group[];

  @OneToOne(() => Context, (context) => context.virtualMachine, {
    cascade: true,
  })
  @JoinColumn()
  context: Context;

  @OneToMany(
    () => VirtualMachineError,
    (virtualMachineError) => virtualMachineError.virtualMachine,
  )
  virtualMachineErrors: VirtualMachineError[];

  @OneToMany(
    () => VirtualMachineSave,
    (virtualMachineSave) => virtualMachineSave.virtualMachine,
  )
  virtualMachineSaves: VirtualMachineSave[];

  @ManyToOne(
    () => VirtualMachineSave,
    (virtualMachineSave) => virtualMachineSave.createdVirtualMachines,
    { nullable: true },
  )
  originVirtualMachineSave: VirtualMachineSave;

  @ManyToOne(() => Project, (project) => project.virtualMachines)
  project: Project;

  @CreateDateColumn()
  createdAt: string;

  @UpdateDateColumn()
  updatedAt: string;

  constructor(
    id: string,
    postVirtualMachineRequest: PostVirtualMachineRequest,
    user: User,
    context: Context,
    token: string,
    project: Project,
    save?: VirtualMachineSave,
  ) {
    if (!id || !postVirtualMachineRequest || !user || !context) {
      return;
    }
    this.token = token;
    this.id = id;
    this.defaultLanguageIdentifier =
      postVirtualMachineRequest.defaultLanguageIdentifier;
    this.status = VirtualMachineStatusEnum.CHECKING_PROJECT;
    this.approval = VirtualMachineApprovalEnum.NONE;
    this.isUpdateAvailable = false;
    this.project = project;
    // TODO : project version
    this.projectVersion = Date.now().toString();
    this.user = user;
    this.context = context;
    this.isArchived = false;
    this.title = postVirtualMachineRequest.title;
    this.description = postVirtualMachineRequest.description;
    this.referenceUrl = postVirtualMachineRequest.referenceUrl;
    this.originVirtualMachineSave = save;
  }
}
