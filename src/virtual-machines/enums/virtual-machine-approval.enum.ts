export enum VirtualMachineApprovalEnum {
  NONE = 'NONE',
  IN_PROGRESS = 'IN_PROGRESS',
  APPROVED = 'APPROVED',
}
