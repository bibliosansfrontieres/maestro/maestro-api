export enum VirtualMachineGroupOrderByEnum {
  ID = 'group.id',
  NAME = 'group.name',
}
