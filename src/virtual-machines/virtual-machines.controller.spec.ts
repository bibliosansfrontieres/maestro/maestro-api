import { Test, TestingModule } from '@nestjs/testing';
import { VirtualMachinesController } from './virtual-machines.controller';
import { VirtualMachinesService } from './virtual-machines.service';
import { VirtualMachine } from './entities/virtual-machine.entity';
import { Repository } from 'typeorm';
import { getRepositoryToken } from '@nestjs/typeorm';
import { UsersService } from 'src/users/users.service';
import { usersServiceMock } from 'src/utils/mocks/services/users-service.mock';
import { GroupsService } from 'src/groups/groups.service';
import { groupsServiceMock } from 'src/utils/mocks/services/groups-service.mock';
import { ContextsService } from 'src/contexts/contexts.service';
import { contextsServiceMock } from 'src/utils/mocks/services/contexts-service.mock';
import { ApplicationsService } from 'src/applications/applications.service';
import { applicationsServiceMock } from 'src/utils/mocks/services/applications-service.mock';
import { JwtService } from '@nestjs/jwt';
import { EntraService } from 'src/entra/entra.service';
import { ConfigService } from '@nestjs/config';
import { configServiceMock } from 'src/utils/mocks/services/config-service.mock';
import { entraServiceMock } from 'src/utils/mocks/services/entra-service.mock';
import { jwtServiceMock } from 'src/utils/mocks/services/jwt-service.mock';
import { OmekaImporterService } from 'src/omeka/omeka-importer.service';
import { omekaImporterServiceMock } from 'src/utils/mocks/services/omeka-importer-service.mock';
import { DockerService } from 'src/docker/docker.service';
import { dockerServiceMock } from 'src/utils/mocks/services/docker-service.mock';
import { VirtualMachineError } from './entities/virtual-machine-error.entity';
import { OmekaService } from 'src/omeka/omeka.service';
import { omekaServiceMock } from 'src/utils/mocks/services/omeka-service.mock';
import { LogsService } from 'src/logs/logs.service';
import { logsServiceMock } from 'src/utils/mocks/services/logs-service.mock';
import { SocketGateway } from 'src/socket/socket.gateway';
import { socketGatewayMock } from 'src/utils/mocks/gateways/socket-gateway.mock';
import { VirtualMachineSavesService } from 'src/virtual-machine-saves/virtual-machine-saves.service';
import { virtualMachineSavesServiceMock } from 'src/utils/mocks/services/virtual-machine-saves-service.mock';
import { DeploysService } from 'src/deploys/deploys.service';
import { deploysServiceMock } from 'src/utils/mocks/services/deploys-service.mock';
import { NotificationsService } from 'src/notifications/notifications.service';
import { notificationsServiceMock } from 'src/utils/mocks/services/notifications-service.mock';
import { LabelsService } from 'src/labels/labels.service';
import { labelsServiceMock } from 'src/utils/mocks/services/labels-service.mock';
import { ProjectsService } from 'src/projects/projects.service';
import { projectsServiceMock } from 'src/utils/mocks/services/projects-service.mock';
import { OmekaTransformerService } from 'src/omeka/omeka-transformer.service';
import { omekaTransformerServiceMock } from 'src/utils/mocks/services/omeka-transformer-service.mock';

describe('VirtualMachinesController', () => {
  let controller: VirtualMachinesController;
  let virtualMachineRepository: Repository<VirtualMachine>;
  let virtualMachineErrorRepository: Repository<VirtualMachineError>;

  const VIRTUAL_MACHINE_REPOSITORY_TOKEN = getRepositoryToken(VirtualMachine);
  const VIRTUAL_MACHINE_ERROR_REPOSITORY_TOKEN =
    getRepositoryToken(VirtualMachineError);

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [VirtualMachinesController],
      providers: [
        VirtualMachinesService,
        UsersService,
        GroupsService,
        ContextsService,
        ApplicationsService,
        JwtService,
        EntraService,
        ConfigService,
        OmekaImporterService,
        DockerService,
        OmekaService,
        LogsService,
        SocketGateway,
        VirtualMachineSavesService,
        DeploysService,
        NotificationsService,
        LabelsService,
        ProjectsService,
        OmekaTransformerService,
        {
          provide: VIRTUAL_MACHINE_REPOSITORY_TOKEN,
          useClass: Repository<VirtualMachine>,
        },
        {
          provide: VIRTUAL_MACHINE_ERROR_REPOSITORY_TOKEN,
          useClass: Repository<VirtualMachineError>,
        },
      ],
    })
      .overrideProvider(UsersService)
      .useValue(usersServiceMock)
      .overrideProvider(GroupsService)
      .useValue(groupsServiceMock)
      .overrideProvider(ContextsService)
      .useValue(contextsServiceMock)
      .overrideProvider(ApplicationsService)
      .useValue(applicationsServiceMock)
      .overrideProvider(JwtService)
      .useValue(jwtServiceMock)
      .overrideProvider(EntraService)
      .useValue(entraServiceMock)
      .overrideProvider(ConfigService)
      .useValue(configServiceMock)
      .overrideProvider(OmekaImporterService)
      .useValue(omekaImporterServiceMock)
      .overrideProvider(DockerService)
      .useValue(dockerServiceMock)
      .overrideProvider(OmekaService)
      .useValue(omekaServiceMock)
      .overrideProvider(LogsService)
      .useValue(logsServiceMock)
      .overrideProvider(SocketGateway)
      .useValue(socketGatewayMock)
      .overrideProvider(VirtualMachineSavesService)
      .useValue(virtualMachineSavesServiceMock)
      .overrideProvider(DeploysService)
      .useValue(deploysServiceMock)
      .overrideProvider(NotificationsService)
      .useValue(notificationsServiceMock)
      .overrideProvider(LabelsService)
      .useValue(labelsServiceMock)
      .overrideProvider(ProjectsService)
      .useValue(projectsServiceMock)
      .overrideProvider(OmekaTransformerService)
      .useValue(omekaTransformerServiceMock)
      .compile();

    controller = module.get<VirtualMachinesController>(
      VirtualMachinesController,
    );
    virtualMachineRepository = module.get<Repository<VirtualMachine>>(
      VIRTUAL_MACHINE_REPOSITORY_TOKEN,
    );
    virtualMachineErrorRepository = module.get<Repository<VirtualMachineError>>(
      VIRTUAL_MACHINE_ERROR_REPOSITORY_TOKEN,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
    expect(virtualMachineRepository).toBeDefined();
    expect(virtualMachineErrorRepository).toBeDefined();
  });
});
