import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Query,
} from '@nestjs/common';
import { VirtualMachinesService } from './virtual-machines.service';
import {
  PostVirtualMachineRequest,
  PostVirtualMachineResponse,
} from './definitions/post-virtual-machine.definition';
import { UserId } from 'src/auth/decorators/user-id.decorator';
import { Auth } from 'src/auth/decorators/auth.decorator';
import { PermissionNameEnum } from 'src/permissions/enums/permission-name.enum';
import {
  ApiBadRequestResponse,
  ApiBody,
  ApiCreatedResponse,
  ApiInternalServerErrorResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { GetVirtualMachineResponse } from './definitions/get-virtual-machine.definition';
import { GetVirtualMachinesRequest } from './definitions/get-virtual-machines.definition';
import { OlipAuth } from 'src/auth/decorators/olip-auth.decorator';
import { PatchVirtualMachineDeployStatusRequest } from './definitions/patch-virtual-machine-deploy-status.definition';
import { VmId } from 'src/auth/decorators/vm-id.decorator';
import {
  GetVirtualMachineLogsRequest,
  GetVirtualMachineLogsResponse,
} from './definitions/get-virtual-machine-logs.definition';
import { GetVirtualMachineLogsDownloadRequest } from './definitions/get-virtual-machine-logs-download.definition';
import {
  GetVirtualMachineHistoryRequest,
  GetVirtualMachineHistoryResponse,
} from './definitions/get-virtual-machine-history.definition';
import { PatchVirtualMachineArchiveRequest } from './definitions/patch-virtual-machine-archive.definition';
import {
  GetVirtualMachineGroupsRequest,
  GetVirtualMachineGroupsResponse,
} from './definitions/get-virtual-machine-groups.definition';
import {
  GetVirtualMachineErrorsRequest,
  GetVirtualMachineErrorsResponse,
} from './definitions/get-virtual-machine-errors.definition';
import {
  PatchVirtualMachineRequest,
  PatchVirtualMachineResponse,
} from './definitions/patch-virtual-machine.definition';
import { PostVirtualMachineDuplicateRequest } from './definitions/post-virtual-machine-duplicate.definition';
import { PostVirtualMachineFromSaveRequest } from './definitions/post-virtual-machine-from-save.definition';
import {
  GetVirtualMachineDeploysRequest,
  GetVirtualMachineDeploysResponse,
} from './definitions/get-virtual-machine-deploys.definitions';
import { PatchVirtualMachineApprovalApproveRequest } from './definitions/patch-virtual-machine-approval-approve.definition';

@Controller('virtual-machines')
@ApiTags('virtual-machines')
export class VirtualMachinesController {
  constructor(
    private readonly virtualMachinesService: VirtualMachinesService,
  ) {}

  @Patch(':id')
  @Auth(PermissionNameEnum.UPDATE_VIRTUAL_MACHINE)
  @ApiOkResponse({ type: PatchVirtualMachineResponse })
  patchVirtualMachine(
    @Param('id') id: string,
    @Body() patchVirtualMachineRequest: PatchVirtualMachineRequest,
    @UserId() userId: string,
  ) {
    return this.virtualMachinesService.patchVirtualMachine(
      id,
      patchVirtualMachineRequest,
      userId,
    );
  }

  @Patch(':id/stop')
  @Auth(PermissionNameEnum.UPDATE_VIRTUAL_MACHINE)
  @ApiNotFoundResponse({ description: 'Virtual machine not found' })
  patchVirtualMachineStop(@Param('id') id: string, @UserId() userId: string) {
    return this.virtualMachinesService.patchVirtualMachineStop(id, userId);
  }

  @Patch(':id/start')
  @Auth(PermissionNameEnum.UPDATE_VIRTUAL_MACHINE)
  @ApiNotFoundResponse({ description: 'Virtual machine not found' })
  patchVirtualMachineStart(@Param('id') id: string, @UserId() userId: string) {
    return this.virtualMachinesService.patchVirtualMachineStart(id, userId);
  }

  @Patch(':id/archive')
  @Auth(PermissionNameEnum.UPDATE_VIRTUAL_MACHINE)
  @ApiNotFoundResponse({ description: 'Virtual machine not found' })
  patchVirtualMachineArchive(
    @Param('id') id: string,
    @UserId() userId: string,
    @Body()
    patchVirtualMachineArchiveRequest: PatchVirtualMachineArchiveRequest,
  ) {
    return this.virtualMachinesService.patchVirtualMachineArchive(
      id,
      userId,
      patchVirtualMachineArchiveRequest,
    );
  }

  @Patch('deploy-status')
  @OlipAuth()
  patchVirtualMachineDeployStatus(
    @VmId() vmId: string,
    @Body()
    patchVirtualMachineDeployStatusRequest: PatchVirtualMachineDeployStatusRequest,
  ) {
    return this.virtualMachinesService.patchVirtualMachineDeployStatus(
      vmId,
      patchVirtualMachineDeployStatusRequest,
    );
  }

  @Patch(':id/import/:saveId')
  @Auth()
  patchVirtualMachineImport(
    @UserId() userId: string,
    @Param('id') id: string,
    @Param('saveId') saveId: string,
  ) {
    return this.virtualMachinesService.patchVirtualMachineImport(
      id,
      saveId,
      userId,
    );
  }

  @Patch(':id/approval')
  @Auth(PermissionNameEnum.UPDATE_VIRTUAL_MACHINE)
  patchVirtualMachineApproval(
    @UserId() userId: string,
    @Param('id') id: string,
  ) {
    return this.virtualMachinesService.patchVirtualMachineApproval(id, userId);
  }

  @Patch(':id/approval/approve')
  @Auth(PermissionNameEnum.UPDATE_VIRTUAL_MACHINE)
  patchVirtualMachineApprovalApprove(
    @UserId() userId: string,
    @Param('id') id: string,
    @Body()
    patchVirtualMachineApprovalApproveRequest: PatchVirtualMachineApprovalApproveRequest,
  ) {
    return this.virtualMachinesService.patchVirtualMachineApprovalApprove(
      id,
      userId,
      patchVirtualMachineApprovalApproveRequest,
    );
  }

  @Patch(':id/approval/reject')
  @Auth(PermissionNameEnum.UPDATE_VIRTUAL_MACHINE)
  patchVirtualMachineApprovalReject(
    @UserId() userId: string,
    @Param('id') id: string,
  ) {
    return this.virtualMachinesService.patchVirtualMachineApprovalReject(
      id,
      userId,
    );
  }

  @Patch(':id/refresh-project')
  @Auth(PermissionNameEnum.UPDATE_VIRTUAL_MACHINE)
  patchVirtualMachineRefreshProject(
    @UserId() userId: string,
    @Param('id') id: string,
  ) {
    return this.virtualMachinesService.patchVirtualMachineRefreshProject(
      id,
      userId,
    );
  }

  @Post(':saveId')
  @Auth(PermissionNameEnum.CREATE_VIRTUAL_MACHINE)
  @ApiOperation({ summary: 'Create a virtual machine from save' })
  @ApiBody({ type: PostVirtualMachineRequest })
  @ApiCreatedResponse({ type: PostVirtualMachineFromSaveRequest })
  @ApiNotFoundResponse({ description: 'Default context/user/save not found' })
  @ApiBadRequestResponse({ description: 'Invalid body request parameters' })
  postVirtualMachineFromSave(
    @Param('saveId') saveId: string,
    @UserId() userId: string,
    @Body()
    postVirtualMachineFromSaveRequest: PostVirtualMachineFromSaveRequest,
  ) {
    return this.virtualMachinesService.postVirtualMachineFromSave(
      userId,
      postVirtualMachineFromSaveRequest,
      saveId,
    );
  }

  @Post()
  @Auth(PermissionNameEnum.CREATE_VIRTUAL_MACHINE)
  @ApiOperation({ summary: 'Create a virtual machine' })
  @ApiBody({ type: PostVirtualMachineRequest })
  @ApiCreatedResponse({ type: PostVirtualMachineResponse })
  @ApiNotFoundResponse({ description: 'Default context/user not found' })
  @ApiBadRequestResponse({ description: 'Invalid body request parameters' })
  postVirtualMachine(
    @UserId() userId: string,
    @Body() postVirtualMachineRequest: PostVirtualMachineRequest,
  ) {
    return this.virtualMachinesService.create(
      userId,
      postVirtualMachineRequest,
    );
  }

  @Post(':id/duplicate')
  @Auth(PermissionNameEnum.CREATE_VIRTUAL_MACHINE)
  @ApiOperation({ summary: 'Duplicate a virtual machine' })
  @ApiCreatedResponse({ type: PostVirtualMachineResponse })
  @ApiNotFoundResponse({ description: 'Virtual machine not found' })
  postVirtualMachineDuplication(
    @UserId() userId: string,
    @Param('id') id: string,
    @Body()
    postVirtualMachineDuplicateRequest: PostVirtualMachineDuplicateRequest,
  ) {
    return this.virtualMachinesService.postVirtualMachineDuplicate(
      id,
      userId,
      postVirtualMachineDuplicateRequest,
    );
  }

  @Post(':id/groups/:groupId')
  @Auth(PermissionNameEnum.UPDATE_VIRTUAL_MACHINE)
  @ApiOperation({ summary: 'Add a group to a virtual machine' })
  @ApiNotFoundResponse({ description: 'Virtual machine/user/group not found' })
  postVirtualMachineGroup(
    @Param('id') id: string,
    @Param('groupId') groupId: string,
    @UserId() userId: string,
  ) {
    return this.virtualMachinesService.postVirtualMachineGroup(
      id,
      +groupId,
      userId,
    );
  }

  @Get(':id/logs')
  @Auth(PermissionNameEnum.READ_VIRTUAL_MACHINE)
  @ApiOperation({ summary: 'Get virtual machine logs' })
  @ApiOkResponse({ type: GetVirtualMachineLogsResponse })
  @ApiNotFoundResponse({ description: 'Virtual machine not found' })
  @ApiInternalServerErrorResponse({ description: 'Failed to get logs' })
  @ApiBadRequestResponse({ description: 'Invalid query parameters' })
  getVirtualMachineLogs(
    @Param('id') id: string,
    @Query() getVirtualMachineLogsRequest: GetVirtualMachineLogsRequest,
  ) {
    return this.virtualMachinesService.getVirtualMachineLogs(
      id,
      getVirtualMachineLogsRequest,
    );
  }

  @Get(':id/deploys')
  @Auth(PermissionNameEnum.READ_VIRTUAL_MACHINE)
  @ApiOperation({ summary: 'Get virtual machine deploys' })
  @ApiOkResponse({ type: GetVirtualMachineDeploysResponse })
  @ApiNotFoundResponse({ description: 'Virtual machine not found' })
  @ApiBadRequestResponse({ description: 'Invalid query parameters' })
  getVirtualMachineDeploys(
    @Param('id') id: string,
    @Query() getVirtualMachineDeploysRequest: GetVirtualMachineDeploysRequest,
  ) {
    return this.virtualMachinesService.getVirtualMachineDeploys(
      id,
      getVirtualMachineDeploysRequest,
    );
  }

  @Get(':id/logs/download')
  @Auth(PermissionNameEnum.READ_VIRTUAL_MACHINE)
  @ApiOperation({ summary: 'Get virtual machine logs download' })
  @ApiNotFoundResponse({ description: 'Virtual machine not found' })
  @ApiBadRequestResponse({ description: 'Invalid query parameters' })
  @ApiInternalServerErrorResponse({ description: 'Failed to get logs' })
  async getVirtualMachineLogsDownload(
    @Param('id') id: string,
    @Query()
    getVirtualMachineLogsDownloadRequest: GetVirtualMachineLogsDownloadRequest,
  ) {
    return this.virtualMachinesService.getVirtualMachineLogsDownload(
      id,
      getVirtualMachineLogsDownloadRequest,
    );
  }

  @Get(':id/history')
  @Auth(PermissionNameEnum.READ_VIRTUAL_MACHINE)
  @ApiOperation({ summary: 'Get virtual machine history' })
  @ApiOkResponse({ type: GetVirtualMachineHistoryResponse })
  @ApiNotFoundResponse({ description: 'Virtual machine not found' })
  @ApiBadRequestResponse({ description: 'Invalid query parameters' })
  async getVirtualMachineHistory(
    @Param('id') id: string,
    @Query() getVirtualMachineHistoryRequest: GetVirtualMachineHistoryRequest,
  ) {
    return this.virtualMachinesService.getVirtualMachineHistory(
      id,
      getVirtualMachineHistoryRequest,
    );
  }

  @Get(':id/groups')
  @Auth(PermissionNameEnum.READ_VIRTUAL_MACHINE)
  @ApiOperation({ summary: 'Get virtual machine groups' })
  @ApiOkResponse({ type: GetVirtualMachineGroupsResponse })
  @ApiNotFoundResponse({ description: 'Virtual machine not found' })
  @ApiBadRequestResponse({ description: 'Invalid query parameters' })
  async getVirtualMachineGroups(
    @Param('id') id: string,
    @Query() getVirtualMachineGroupsRequest: GetVirtualMachineGroupsRequest,
  ) {
    return this.virtualMachinesService.getVirtualMachineGroups(
      id,
      getVirtualMachineGroupsRequest,
    );
  }

  @Get(':id/errors')
  @Auth(PermissionNameEnum.READ_VIRTUAL_MACHINE)
  @ApiOperation({ summary: 'Get virtual machine errors' })
  @ApiOkResponse({ type: GetVirtualMachineErrorsResponse })
  @ApiNotFoundResponse({ description: 'Virtual machine not found' })
  @ApiBadRequestResponse({ description: 'Invalid query parameters' })
  async getVirtualMachineErrors(
    @Param('id') id: string,
    @Query() getVirtualMachineErrorsRequest: GetVirtualMachineErrorsRequest,
  ) {
    return this.virtualMachinesService.getVirtualMachineErrors(
      id,
      getVirtualMachineErrorsRequest,
    );
  }

  @Get(':id/:container')
  @Auth(PermissionNameEnum.READ_VIRTUAL_MACHINE)
  async getVirtualMachineContainerInspect(
    @Param('id') id: string,
    @Param('container') container: string,
  ) {
    return this.virtualMachinesService.getVirtualMachineContainerInspect(
      id,
      container,
    );
  }

  @Get(':id/:container/port')
  @Auth(PermissionNameEnum.READ_VIRTUAL_MACHINE)
  async getVirtualMachineContainerExposedPort(
    @Param('id') id: string,
    @Param('container') container: string,
  ) {
    return this.virtualMachinesService.getVirtualMachineContainerExposedPort(
      id,
      container,
    );
  }

  @Get(':id')
  @Auth(PermissionNameEnum.READ_VIRTUAL_MACHINE)
  @ApiOperation({ summary: 'Get a virtual machine by ID' })
  @ApiOkResponse({ type: GetVirtualMachineResponse })
  @ApiNotFoundResponse({ description: 'Virtual machine not found' })
  getVirtualMachine(@Param('id') id: string) {
    return this.virtualMachinesService.getVirtualMachine(id);
  }

  @Get()
  @Auth()
  getVirtualMachines(
    @UserId() userId: string,
    @Query() getVirtualMachinesRequest: GetVirtualMachinesRequest,
  ) {
    return this.virtualMachinesService.getVirtualMachines(
      userId,
      getVirtualMachinesRequest,
    );
  }

  @Delete(':id/groups/:groupId')
  @Auth(PermissionNameEnum.UPDATE_VIRTUAL_MACHINE)
  @ApiOperation({ summary: 'Delete a virtual machine group' })
  @ApiNotFoundResponse({ description: 'Virtual machine/group/user not found' })
  deleteVirtualMachineGroup(
    @Param('id') id: string,
    @Param('groupId') groupId: string,
    @UserId() userId: string,
  ) {
    return this.virtualMachinesService.deleteVirtualMachineGroup(
      id,
      +groupId,
      userId,
    );
  }
}
