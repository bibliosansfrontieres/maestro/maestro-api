import { forwardRef, Module } from '@nestjs/common';
import { VirtualMachinesService } from './virtual-machines.service';
import { VirtualMachinesController } from './virtual-machines.controller';
import { ApplicationsModule } from 'src/applications/applications.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { VirtualMachine } from './entities/virtual-machine.entity';
import { GroupsModule } from 'src/groups/groups.module';
import { UsersModule } from 'src/users/users.module';
import { ContextsModule } from 'src/contexts/contexts.module';
import { JwtModule } from '@nestjs/jwt';
import { EntraModule } from 'src/entra/entra.module';
import { OmekaModule } from 'src/omeka/omeka.module';
import { DockerModule } from 'src/docker/docker.module';
import { LogsModule } from 'src/logs/logs.module';
import { VirtualMachineError } from './entities/virtual-machine-error.entity';
import { VirtualMachineSavesModule } from 'src/virtual-machine-saves/virtual-machine-saves.module';
import { DeploysModule } from 'src/deploys/deploys.module';
import { NotificationsModule } from 'src/notifications/notifications.module';
import { LabelsModule } from 'src/labels/labels.module';
import { ProjectsModule } from 'src/projects/projects.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([VirtualMachine, VirtualMachineError]),
    ApplicationsModule,
    GroupsModule,
    UsersModule,
    forwardRef(() => ContextsModule),
    JwtModule,
    EntraModule,
    OmekaModule,
    DockerModule,
    LogsModule,
    VirtualMachineSavesModule,
    DeploysModule,
    NotificationsModule,
    LabelsModule,
    ProjectsModule,
  ],
  controllers: [VirtualMachinesController],
  providers: [VirtualMachinesService],
  exports: [VirtualMachinesService],
})
export class VirtualMachinesModule {}
