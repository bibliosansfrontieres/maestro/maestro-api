import { Test, TestingModule } from '@nestjs/testing';
import { VirtualMachinesService } from './virtual-machines.service';
import { VirtualMachine } from './entities/virtual-machine.entity';
import { Repository, SelectQueryBuilder } from 'typeorm';
import { getRepositoryToken } from '@nestjs/typeorm';
import { UsersService } from 'src/users/users.service';
import { usersServiceMock } from 'src/utils/mocks/services/users-service.mock';
import { GroupsService } from 'src/groups/groups.service';
import { ContextsService } from 'src/contexts/contexts.service';
import { ApplicationsService } from 'src/applications/applications.service';
import { applicationsServiceMock } from 'src/utils/mocks/services/applications-service.mock';
import { contextsServiceMock } from 'src/utils/mocks/services/contexts-service.mock';
import { groupsServiceMock } from 'src/utils/mocks/services/groups-service.mock';
import { OmekaImporterService } from 'src/omeka/omeka-importer.service';
import { omekaImporterServiceMock } from 'src/utils/mocks/services/omeka-importer-service.mock';
import { DockerService } from 'src/docker/docker.service';
import { dockerServiceMock } from 'src/utils/mocks/services/docker-service.mock';
import { ConfigService } from '@nestjs/config';
import { configServiceMock } from 'src/utils/mocks/services/config-service.mock';
import { VirtualMachineError } from './entities/virtual-machine-error.entity';
import { OmekaService } from 'src/omeka/omeka.service';
import { omekaServiceMock } from 'src/utils/mocks/services/omeka-service.mock';
import { LogsService } from 'src/logs/logs.service';
import { logsServiceMock } from 'src/utils/mocks/services/logs-service.mock';
import { JwtService } from '@nestjs/jwt';
import { jwtServiceMock } from 'src/utils/mocks/services/jwt-service.mock';
import { SocketGateway } from 'src/socket/socket.gateway';
import { socketGatewayMock } from 'src/utils/mocks/gateways/socket-gateway.mock';
import { VirtualMachineSavesService } from 'src/virtual-machine-saves/virtual-machine-saves.service';
import { virtualMachineSavesServiceMock } from 'src/utils/mocks/services/virtual-machine-saves-service.mock';
import { DeploysService } from 'src/deploys/deploys.service';
import { deploysServiceMock } from 'src/utils/mocks/services/deploys-service.mock';
import { NotificationsService } from 'src/notifications/notifications.service';
import { notificationsServiceMock } from 'src/utils/mocks/services/notifications-service.mock';
import { LabelsService } from 'src/labels/labels.service';
import { labelsServiceMock } from 'src/utils/mocks/services/labels-service.mock';
import { ProjectsService } from 'src/projects/projects.service';
import { projectsServiceMock } from 'src/utils/mocks/services/projects-service.mock';
import { OmekaTransformerService } from 'src/omeka/omeka-transformer.service';
import { omekaTransformerServiceMock } from 'src/utils/mocks/services/omeka-transformer-service.mock';
import { Log } from 'src/logs/entities/log.entity';
import { GetVirtualMachineHistoryResponse } from './definitions/get-virtual-machine-history.definition';
import { LogActionEnum } from 'src/logs/enums/log-action.enum';
import { VirtualMachineHistoryEntity } from './enums/virtual-machine-history-entity.enum';
import { LogLevelEnum } from 'src/logs/enums/log-level.enum';
import { LogOrderByEnum } from 'src/logs/enums/log-order-by.enum';
import * as getManyAndCountUtil from 'src/utils/get-many-and-count.util';
import { logMock } from 'src/utils/mocks/objects/log.mock';
import { virtualMachineMock } from 'src/utils/mocks/objects/virtual-machine.mock';
import { contextMock } from 'src/utils/mocks/objects/context.mock';
import { NotFoundException } from '@nestjs/common';
import { BooleanEnum } from 'src/utils/enums/boolean.enum';
import { VirtualMachineStatusEnum } from './enums/virtual-machine-status.enum';
import { VirtualMachineApprovalEnum } from './enums/virtual-machine-approval.enum';
import { VirtualMachineOrderByEnum } from './enums/virtual-machine-order-by.enum';
import { GetVirtualMachinesResponse } from './definitions/get-virtual-machines.definition';
import { virtualMachineSaveMock } from 'src/utils/mocks/objects/virtual-machine-save.mock';
import { VirtualMachineSaveTypeEnum } from 'src/virtual-machine-saves/enums/virtual-machine-save-type.enum';
import * as fs from 'fs';

describe('VirtualMachinesService', () => {
  let service: VirtualMachinesService;
  let virtualMachineRepository: Repository<VirtualMachine>;
  let virtualMachineErrorRepository: Repository<VirtualMachineError>;
  let virtualMachineSavesService: VirtualMachineSavesService;
  let logsService: LogsService;
  let contextsService: ContextsService;

  const VIRTUAL_MACHINE_REPOSITORY_TOKEN = getRepositoryToken(VirtualMachine);
  const VIRTUAL_MACHINE_ERROR_REPOSITORY_TOKEN =
    getRepositoryToken(VirtualMachineError);

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        VirtualMachinesService,
        UsersService,
        GroupsService,
        ContextsService,
        ApplicationsService,
        OmekaImporterService,
        DockerService,
        ConfigService,
        OmekaService,
        LogsService,
        JwtService,
        SocketGateway,
        VirtualMachineSavesService,
        DeploysService,
        NotificationsService,
        LabelsService,
        ProjectsService,
        OmekaTransformerService,
        {
          provide: VIRTUAL_MACHINE_REPOSITORY_TOKEN,
          useClass: Repository<VirtualMachine>,
        },
        {
          provide: VIRTUAL_MACHINE_ERROR_REPOSITORY_TOKEN,
          useClass: Repository<VirtualMachineError>,
        },
      ],
    })
      .overrideProvider(UsersService)
      .useValue(usersServiceMock)
      .overrideProvider(GroupsService)
      .useValue(groupsServiceMock)
      .overrideProvider(ContextsService)
      .useValue(contextsServiceMock)
      .overrideProvider(ApplicationsService)
      .useValue(applicationsServiceMock)
      .overrideProvider(OmekaImporterService)
      .useValue(omekaImporterServiceMock)
      .overrideProvider(DockerService)
      .useValue(dockerServiceMock)
      .overrideProvider(ConfigService)
      .useValue(configServiceMock)
      .overrideProvider(OmekaService)
      .useValue(omekaServiceMock)
      .overrideProvider(LogsService)
      .useValue(logsServiceMock)
      .overrideProvider(JwtService)
      .useValue(jwtServiceMock)
      .overrideProvider(SocketGateway)
      .useValue(socketGatewayMock)
      .overrideProvider(VirtualMachineSavesService)
      .useValue(virtualMachineSavesServiceMock)
      .overrideProvider(DeploysService)
      .useValue(deploysServiceMock)
      .overrideProvider(NotificationsService)
      .useValue(notificationsServiceMock)
      .overrideProvider(LabelsService)
      .useValue(labelsServiceMock)
      .overrideProvider(ProjectsService)
      .useValue(projectsServiceMock)
      .overrideProvider(OmekaTransformerService)
      .useValue(omekaTransformerServiceMock)
      .compile();

    service = module.get<VirtualMachinesService>(VirtualMachinesService);
    virtualMachineRepository = module.get<Repository<VirtualMachine>>(
      VIRTUAL_MACHINE_REPOSITORY_TOKEN,
    );
    virtualMachineErrorRepository = module.get<Repository<VirtualMachineError>>(
      VIRTUAL_MACHINE_ERROR_REPOSITORY_TOKEN,
    );
    logsService = module.get<LogsService>(LogsService);
    virtualMachineSavesService = module.get<VirtualMachineSavesService>(
      VirtualMachineSavesService,
    );
    contextsService = module.get<ContextsService>(ContextsService);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
    expect(virtualMachineRepository).toBeDefined();
    expect(virtualMachineErrorRepository).toBeDefined();
    expect(logsService).toBeDefined();
  });

  describe('getVirtualMachines', () => {
    const userId = Date.now().toString();
    const getVirtualMachinesRequest = {
      isUserVmsFiltered: BooleanEnum.TRUE,
      statuses: VirtualMachineStatusEnum.ONLINE,
      users:
        '4f1181be-f81f-47c5-9e97-5a5d0222cc53,2323fef4-0f23-47af-b144-03d1285e2a73',
      approvals: VirtualMachineApprovalEnum.APPROVED,
      groups: '1',
      query: 'search',
      createdDateRange: '12/30/2021,12/31/2021',
      updatedDateRange: '12/30/2021,12/31/2021',
      orderBy: VirtualMachineOrderByEnum.ID,
    };

    const mockQueryBuilder = {
      innerJoinAndSelect: jest.fn().mockReturnThis(),
      leftJoinAndSelect: jest.fn().mockReturnThis(),
      andWhere: jest.fn().mockReturnThis(),
      take: jest.fn().mockReturnThis(),
      skip: jest.fn().mockReturnThis(),
      orderBy: jest.fn().mockReturnThis(),
    } as unknown as SelectQueryBuilder<VirtualMachine>;

    const mockVm = virtualMachineMock();
    const mockVms = [mockVm];

    beforeEach(() => {
      jest
        .spyOn(virtualMachineRepository, 'createQueryBuilder')
        .mockReturnValue(mockQueryBuilder);
      jest
        .spyOn(getManyAndCountUtil, 'getManyAndCount')
        .mockResolvedValue([1, mockVms]);
    });

    it('should join needed relations', async () => {
      await service.getVirtualMachines(userId, getVirtualMachinesRequest);
      expect(mockQueryBuilder.leftJoinAndSelect).toHaveBeenCalledWith(
        'virtualMachine.user',
        'user',
      );
      expect(mockQueryBuilder.leftJoinAndSelect).toHaveBeenCalledWith(
        'virtualMachine.groups',
        'group',
      );
      expect(mockQueryBuilder.innerJoinAndSelect).toHaveBeenCalledWith(
        'virtualMachine.project',
        'project',
      );
    });

    it('should call getManyAndCount', async () => {
      await service.getVirtualMachines(userId, getVirtualMachinesRequest);
      expect(getManyAndCountUtil.getManyAndCount).toHaveBeenCalledWith(
        mockQueryBuilder,
        ['virtualMachine.id'],
        false,
      );
    });

    it('should return GetVirtualMachinesResponse', async () => {
      const result = await service.getVirtualMachines(
        userId,
        getVirtualMachinesRequest,
      );
      expect(result).toBeInstanceOf(GetVirtualMachinesResponse);
    });
  });

  describe('getVirtualMachineHistory', () => {
    const id = Date.now().toString();
    const getVirtualMachineHistoryRequest = {
      dateRange: '12/30/2021,12/31/2021',
      userIds:
        'f5fb092f-904f-456e-baf0-a0d0a0d0e0,f5fb092f-904f-456e-baf0-a0d0a0d0e0',
      actions: `${LogActionEnum.CREATE},${LogActionEnum.UPDATE}`,
      entities: `${VirtualMachineHistoryEntity.VIRTUAL_MACHINE},${VirtualMachineHistoryEntity.CONTEXT}`,
      levels: `${LogLevelEnum.INFO},${LogLevelEnum.DEBUG}`,
      orderBy: LogOrderByEnum.ENTITY,
      query: 'search',
    };

    const mockQueryBuilder = {
      innerJoinAndSelect: jest.fn().mockReturnThis(),
      andWhere: jest.fn().mockReturnThis(),
      take: jest.fn().mockReturnThis(),
      skip: jest.fn().mockReturnThis(),
      orderBy: jest.fn().mockReturnThis(),
    } as unknown as SelectQueryBuilder<Log>;

    const mockLog = logMock();
    const mockLogs = [mockLog];

    beforeEach(() => {
      jest
        .spyOn(logsService, 'getQueryBuilder')
        .mockReturnValue(mockQueryBuilder);
      jest
        .spyOn(service, 'findOne')
        .mockResolvedValue(virtualMachineMock({ context: contextMock() }));
      jest
        .spyOn(getManyAndCountUtil, 'getManyAndCount')
        .mockResolvedValue([1, mockLogs]);
    });

    it('should call findOne with right relations', async () => {
      await service.getVirtualMachineHistory(
        id,
        getVirtualMachineHistoryRequest,
      );
      expect(service.findOne).toHaveBeenCalledWith({
        where: { id },
        relations: { context: true, virtualMachineSaves: { deploys: true } },
      });
    });

    it('should throw if no virtual machine', async () => {
      jest.spyOn(service, 'findOne').mockResolvedValue(null);
      expect(
        service.getVirtualMachineHistory(id, getVirtualMachineHistoryRequest),
      ).rejects.toThrow(NotFoundException);
    });

    it('should join needed relations', async () => {
      await service.getVirtualMachineHistory(
        id,
        getVirtualMachineHistoryRequest,
      );
      expect(mockQueryBuilder.innerJoinAndSelect).toHaveBeenCalledWith(
        'log.user',
        'user',
      );
    });

    it('should call getManyAndCount', async () => {
      await service.getVirtualMachineHistory(
        id,
        getVirtualMachineHistoryRequest,
      );
      expect(getManyAndCountUtil.getManyAndCount).toHaveBeenCalledWith(
        expect.anything(),
        ['log.id'],
      );
    });

    it('should return GetVirtualMachineHistoryResponse', async () => {
      const result = await service.getVirtualMachineHistory(
        id,
        getVirtualMachineHistoryRequest,
      );
      expect(result).toBeInstanceOf(GetVirtualMachineHistoryResponse);
    });
  });

  describe('postVirtualMachineDuplicate', () => {
    const id = Date.now().toString();
    const userId = Date.now().toString();
    const postVirtualMachineDuplicateRequest = {
      title: 'My title',
      description: 'My description',
      referenceUrl: 'https://example.com',
    };

    const mockVirtualMachine = virtualMachineMock({
      status: VirtualMachineStatusEnum.ONLINE,
    });
    const mockVirtualMachineSave = virtualMachineSaveMock({
      virtualMachine: mockVirtualMachine,
    });

    beforeEach(() => {
      jest.spyOn(service, 'findOne').mockResolvedValue(mockVirtualMachine);
      jest.spyOn(service, 'create').mockResolvedValue(mockVirtualMachine);
      jest
        .spyOn(virtualMachineSavesService, 'create')
        .mockResolvedValue(mockVirtualMachineSave);
      jest
        .spyOn(service, 'postVirtualMachineFromSave')
        .mockResolvedValue(mockVirtualMachine);
    });

    it('should call findOne with right arguments', async () => {
      await service.postVirtualMachineDuplicate(
        id,
        userId,
        postVirtualMachineDuplicateRequest,
      );
      expect(service.findOne).toHaveBeenCalledWith({
        where: { id },
        relations: { context: true },
      });
    });

    it('should throw if no virtual machine', async () => {
      jest.spyOn(service, 'findOne').mockResolvedValue(null);
      expect(
        service.postVirtualMachineDuplicate(
          id,
          userId,
          postVirtualMachineDuplicateRequest,
        ),
      ).rejects.toThrow(NotFoundException);
    });

    it('should throw if virtual machine status is not online/offline', async () => {
      jest.spyOn(service, 'findOne').mockResolvedValue(
        virtualMachineMock({
          status: VirtualMachineStatusEnum.PROJECT_ERROR,
        }),
      );

      expect(
        service.postVirtualMachineDuplicate(
          id,
          userId,
          postVirtualMachineDuplicateRequest,
        ),
      ).rejects.toThrow(NotFoundException);
    });

    it('should call virtualMachineSavesService.create with the right arguments', async () => {
      await service.postVirtualMachineDuplicate(
        id,
        userId,
        postVirtualMachineDuplicateRequest,
      );

      expect(virtualMachineSavesService.create).toHaveBeenCalledWith(
        { name: 'Duplicate', description: 'Virtual machine duplication' },
        mockVirtualMachine,
        userId,
        VirtualMachineSaveTypeEnum.AUTOMATIC,
      );
    });

    it('should postVirtualMachineFromSave with the right arguments', async () => {
      await service.postVirtualMachineDuplicate(
        id,
        userId,
        postVirtualMachineDuplicateRequest,
      );

      expect(service.postVirtualMachineFromSave).toHaveBeenCalledWith(
        userId,
        postVirtualMachineDuplicateRequest,
        mockVirtualMachineSave.id,
      );
    });

    it('should return PostVirtualMachineResponse', async () => {
      const result = await service.postVirtualMachineDuplicate(
        id,
        userId,
        postVirtualMachineDuplicateRequest,
      );
      expect(result).toEqual(mockVirtualMachine);
    });
  });

  describe('createOlipFilesPath', () => {
    const mockVirtualMachine = virtualMachineMock();

    beforeEach(() => {
      jest
        .spyOn(service, 'getVirtualMachineDeployPath')
        .mockReturnValue(
          `../../data/virtual_machines/${mockVirtualMachine.id}`,
        );
      jest.spyOn(fs, 'mkdirSync').mockReturnValue('');
    });
    afterEach(() => {
      jest.clearAllMocks();
    });

    it('should be defined', () => {
      expect(service.createOlipFilesPath).toBeDefined();
    });

    it('should call getVirtualMachineDeployPath', () => {
      service.createOlipFilesPath(mockVirtualMachine);
      expect(service.getVirtualMachineDeployPath).toHaveBeenCalledWith(
        mockVirtualMachine,
      );
    });

    it('should call mkdirSync if path does not exists', () => {
      jest.spyOn(fs, 'existsSync').mockReturnValue(false);
      service.createOlipFilesPath(mockVirtualMachine);
      // expect use mkdirsync
      expect(fs.mkdirSync).toHaveBeenCalled();
    });

    it('should not call mkdirSync if path does exists', () => {
      jest.spyOn(fs, 'existsSync').mockReturnValue(true);
      service.createOlipFilesPath(mockVirtualMachine);
      // expect use mkdirsync
      expect(fs.mkdirSync).not.toHaveBeenCalled();
    });
  });

  describe('createInternetFlagFile', () => {
    const mockVirtualMachine = virtualMachineMock();

    beforeEach(() => {
      jest
        .spyOn(service, 'getVirtualMachineDeployPath')
        .mockReturnValue(
          `../../data/virtual_machines/${mockVirtualMachine.id}`,
        );
      jest.spyOn(fs, 'mkdirSync').mockReturnValue('');
      jest.spyOn(fs, 'writeFileSync').mockReturnValue();
    });
    afterEach(() => {
      jest.clearAllMocks();
    });

    it('should be defined', () => {
      expect(service.createInternetFlagFile).toBeDefined();
    });

    it('should call getVirtualMachineDeployPath', () => {
      service.createInternetFlagFile(mockVirtualMachine);
      expect(service.getVirtualMachineDeployPath).toHaveBeenCalledWith(
        mockVirtualMachine,
      );
    });

    it('should call mkdirSync if path does not exists', () => {
      jest.spyOn(fs, 'existsSync').mockReturnValue(false);
      service.createInternetFlagFile(mockVirtualMachine);
      // expect use mkdirsync
      expect(fs.mkdirSync).toHaveBeenCalled();
    });

    it('should not call mkdirSync if path does exists', () => {
      jest.spyOn(fs, 'existsSync').mockReturnValue(true);
      service.createInternetFlagFile(mockVirtualMachine);
      // expect use mkdirsync
      expect(fs.mkdirSync).not.toHaveBeenCalled();
    });

    it('should call writeFileSync if path does not exists', () => {
      jest.spyOn(fs, 'existsSync').mockReturnValue(false);
      service.createInternetFlagFile(mockVirtualMachine);
      // expect use writeFileSync
      expect(fs.writeFileSync).toHaveBeenCalled();
    });

    it('should not call writeFileSync if path does exists', () => {
      jest.spyOn(fs, 'existsSync').mockReturnValue(true);
      service.createInternetFlagFile(mockVirtualMachine);
      // expect use writeFileSync
      expect(fs.writeFileSync).not.toHaveBeenCalled();
    });
  });

  describe('createOrUpdateDeployContextFile', () => {
    const mockVirtualMachine = virtualMachineMock();

    beforeEach(() => {
      jest
        .spyOn(service, 'getVirtualMachineDeployPath')
        .mockReturnValue(
          `../../data/virtual_machines/${mockVirtualMachine.id}`,
        );
      jest.spyOn(fs, 'mkdirSync').mockReturnValue('');
      jest.spyOn(fs, 'rmSync').mockReturnValue();
      jest.spyOn(fs, 'writeFileSync').mockReturnValue();
      jest.spyOn(service, 'getStringifyDeployContext').mockResolvedValue('');
    });

    it('should be defined', () => {
      expect(service.createOrUpdateDeployContextFile).toBeDefined();
    });

    it('should call getVirtualMachineDeployPath', async () => {
      await service.createOrUpdateDeployContextFile(mockVirtualMachine);
      expect(service.getVirtualMachineDeployPath).toHaveBeenCalledWith(
        mockVirtualMachine,
      );
    });

    it('should call mkdirSync if path does not exists', async () => {
      jest.spyOn(fs, 'existsSync').mockReturnValue(false);
      await service.createOrUpdateDeployContextFile(mockVirtualMachine);
      // expect use mkdirsync
      expect(fs.mkdirSync).toHaveBeenCalled();
    });

    it('should not call mkdirSync if path does exists', async () => {
      jest.spyOn(fs, 'existsSync').mockReturnValue(true);
      await service.createOrUpdateDeployContextFile(mockVirtualMachine);
      // expect use mkdirsync
      expect(fs.mkdirSync).not.toHaveBeenCalled();
    });

    it('should call rmSync if path does exists and update', async () => {
      jest.spyOn(fs, 'existsSync').mockReturnValue(true);
      await service.createOrUpdateDeployContextFile(mockVirtualMachine, true);
      // expect use rmsync
      expect(fs.rmSync).toHaveBeenCalled();
    });

    it('should not call rmSync if path does not exists and update', async () => {
      jest.spyOn(fs, 'existsSync').mockReturnValue(false);
      await service.createOrUpdateDeployContextFile(mockVirtualMachine, true);
      // expect not use rmsync
      expect(fs.rmSync).not.toHaveBeenCalled();
    });

    it('should call writeFileSync if path does not exists', async () => {
      jest.spyOn(fs, 'existsSync').mockReturnValue(true);
      jest.spyOn(fs, 'existsSync').mockReturnValue(false);
      await service.createOrUpdateDeployContextFile(mockVirtualMachine);
      // expect use writeFileSync
      expect(fs.writeFileSync).toHaveBeenCalled();
    });

    it('should not call writeFileSync if path does exists', async () => {
      jest.spyOn(fs, 'existsSync').mockReturnValue(true);
      jest.spyOn(fs, 'existsSync').mockReturnValue(true);
      await service.createOrUpdateDeployContextFile(mockVirtualMachine);
      // expect not use writeFileSync
      expect(fs.writeFileSync).not.toHaveBeenCalled();
    });

    it('should call getStringifyDeployContext if path does not exists', async () => {
      jest.spyOn(fs, 'existsSync').mockReturnValue(true);
      jest.spyOn(fs, 'existsSync').mockReturnValue(false);
      await service.createOrUpdateDeployContextFile(mockVirtualMachine);
      // expect use getStringifyDeployContext
      expect(service.getStringifyDeployContext).toHaveBeenCalledWith(
        mockVirtualMachine,
      );
    });

    it('should not call getStringifyDeployContext if path does exists', async () => {
      jest.spyOn(fs, 'existsSync').mockReturnValue(true);
      jest.spyOn(fs, 'existsSync').mockReturnValue(true);
      await service.createOrUpdateDeployContextFile(mockVirtualMachine);
      // expect not use getStringifyDeployContext
      expect(service.getStringifyDeployContext).not.toHaveBeenCalled();
    });
  });

  describe('getStringifyDeployContext', () => {
    const mockVirtualMachine = virtualMachineMock();
    const mockContext = contextMock({
      virtualMachine: mockVirtualMachine,
    });

    beforeEach(() => {
      jest.spyOn(contextsService, 'findOne').mockResolvedValue(mockContext);
      jest.spyOn(JSON, 'stringify').mockReturnValue('');
    });
    afterEach(() => {
      jest.clearAllMocks();
    });

    it('should be defined', () => {
      expect(service.getStringifyDeployContext).toBeDefined();
    });

    it('should call context.findOne with right arguments', async () => {
      await service.getStringifyDeployContext(mockVirtualMachine);
      expect(contextsService.findOne).toHaveBeenCalledWith({
        where: { virtualMachine: { id: mockVirtualMachine.id } },
        relations: { applications: true },
      });
    });

    it('should call JSON stringify', async () => {
      await service.getStringifyDeployContext(mockVirtualMachine);
      expect(JSON.stringify).toHaveBeenCalledWith(mockContext);
    });
  });
});
