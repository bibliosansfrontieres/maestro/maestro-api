import {
  BadRequestException,
  forwardRef,
  Inject,
  Injectable,
  Logger,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import {
  PostVirtualMachineRequest,
  PostVirtualMachineResponse,
} from './definitions/post-virtual-machine.definition';
import { InjectRepository } from '@nestjs/typeorm';
import { VirtualMachine } from './entities/virtual-machine.entity';
import { FindOneOptions, Repository } from 'typeorm';
import { generateRandomId } from 'src/utils/generate-random-id.util';
import { UsersService } from 'src/users/users.service';
import { ContextsService } from 'src/contexts/contexts.service';
import { GetVirtualMachineResponse } from './definitions/get-virtual-machine.definition';
import {
  GetVirtualMachinesRequest,
  GetVirtualMachinesResponse,
} from './definitions/get-virtual-machines.definition';
import { getManyAndCount } from 'src/utils/get-many-and-count.util';
import { PageOptions } from 'src/utils/pagination/page-options';
import { OmekaImporterService } from 'src/omeka/omeka-importer.service';
import { VirtualMachineStatusEnum } from './enums/virtual-machine-status.enum';
import { DockerService } from 'src/docker/docker.service';
import { join } from 'path';
import {
  existsSync,
  mkdirSync,
  readdirSync,
  readFileSync,
  rmSync,
  writeFileSync,
} from 'fs';
import * as dotenv from 'dotenv';
import { ConfigService } from '@nestjs/config';
import { OmekaService } from 'src/omeka/omeka.service';
import { LogsService } from 'src/logs/logs.service';
import { LogActionEnum } from 'src/logs/enums/log-action.enum';
import { LogEntityEnum } from 'src/logs/enums/log-entity.enum';
import { VirtualMachineError } from './entities/virtual-machine-error.entity';
import { IProjectErrors } from 'src/omeka/interfaces/project-errors.interface';
import { JwtService } from '@nestjs/jwt';
import { PatchVirtualMachineDeployStatusRequest } from './definitions/patch-virtual-machine-deploy-status.definition';
import {
  GetVirtualMachineLogsRequest,
  GetVirtualMachineLogsResponse,
} from './definitions/get-virtual-machine-logs.definition';
import { GetVirtualMachineLogsDownloadRequest } from './definitions/get-virtual-machine-logs-download.definition';
import { Log } from 'src/logs/entities/log.entity';
import {
  GetVirtualMachineHistoryRequest,
  GetVirtualMachineHistoryResponse,
} from './definitions/get-virtual-machine-history.definition';
import { LogOrderByEnum } from 'src/logs/enums/log-order-by.enum';
import { VirtualMachineHistoryEntity } from './enums/virtual-machine-history-entity.enum';
import { QueryHelper } from 'src/utils/query-helpers.util';
import { SocketGateway } from 'src/socket/socket.gateway';
import { VirtualMachineSavesService } from 'src/virtual-machine-saves/virtual-machine-saves.service';
import { VirtualMachineSave } from 'src/virtual-machine-saves/entities/virtual-machine-save.entity';
import { PatchVirtualMachineArchiveRequest } from './definitions/patch-virtual-machine-archive.definition';
import { VirtualMachineSaveTypeEnum } from 'src/virtual-machine-saves/enums/virtual-machine-save-type.enum';
import { VirtualMachineApprovalEnum } from './enums/virtual-machine-approval.enum';
import {
  GetVirtualMachineGroupsRequest,
  GetVirtualMachineGroupsResponse,
} from './definitions/get-virtual-machine-groups.definition';
import { Group } from 'src/groups/entities/group.entity';
import { VirtualMachineGroupOrderByEnum } from './enums/virtual-machine-group-order-by.enum';
import { GroupsService } from 'src/groups/groups.service';
import {
  GetVirtualMachineErrorsRequest,
  GetVirtualMachineErrorsResponse,
} from './definitions/get-virtual-machine-errors.definition';
import { userCanPatchVm } from 'src/utils/user-can-patch-vm.util';
import {
  PatchVirtualMachineRequest,
  PatchVirtualMachineResponse,
} from './definitions/patch-virtual-machine.definition';
import { BooleanEnum } from 'src/utils/enums/boolean.enum';
import { VirtualMachineContainerEnum } from './enums/virtual-machine-container.enum';
import { PostVirtualMachineDuplicateRequest } from './definitions/post-virtual-machine-duplicate.definition';
import { PostVirtualMachineFromSaveRequest } from './definitions/post-virtual-machine-from-save.definition';
import { VirtualMachineOrderByEnum } from './enums/virtual-machine-order-by.enum';
import {
  GetVirtualMachineDeploysRequest,
  GetVirtualMachineDeploysResponse,
} from './definitions/get-virtual-machine-deploys.definitions';
import { Deploy } from 'src/deploys/entities/deploy.entity';
import { VirtualMachineDeployOrderByEnum } from './enums/virtual-machine-deploy-order-by.enum';
import { DeploysService } from 'src/deploys/deploys.service';
import { PatchVirtualMachineApprovalApproveRequest } from './definitions/patch-virtual-machine-approval-approve.definition';
import { NotificationsService } from 'src/notifications/notifications.service';
import { NotificationTypeEnum } from 'src/notifications/enums/notification-type.enum';
import { LabelsService } from 'src/labels/labels.service';
import { ProjectsService } from 'src/projects/projects.service';
import { OmekaTransformerService } from 'src/omeka/omeka-transformer.service';
import { ProjectStatusEnum } from 'src/projects/enums/project-status.enum';
import { Git } from 'src/utils/git.util';
import { PageOrderEnum } from 'src/utils/pagination/enums/page-order.enum';
import { bash } from 'src/utils/bash.util';

@Injectable()
export class VirtualMachinesService {
  constructor(
    @InjectRepository(VirtualMachine)
    private virtualMachineRepository: Repository<VirtualMachine>,
    @InjectRepository(VirtualMachineError)
    private virtualMachineErrorRepository: Repository<VirtualMachineError>,
    private usersService: UsersService,
    @Inject(forwardRef(() => ContextsService))
    private contextsService: ContextsService,
    private omekaImporterService: OmekaImporterService,
    private dockerService: DockerService,
    private configService: ConfigService,
    private omekaService: OmekaService,
    private logsService: LogsService,
    private jwtService: JwtService,
    private socketGateway: SocketGateway,
    private virtualMachineSavesService: VirtualMachineSavesService,
    private groupsService: GroupsService,
    private deploysService: DeploysService,
    private notificationsService: NotificationsService,
    private labelsService: LabelsService,
    private projectsService: ProjectsService,
    private omekaTransformerService: OmekaTransformerService,
  ) {}

  async onModuleInit() {
    let virtualMachines = await this.virtualMachineRepository.find({
      where: { status: VirtualMachineStatusEnum.ONLINE },
    });
    virtualMachines = [];
    // TODO : we need to start vms
    if (virtualMachines.length === 0) {
      Logger.log('No virtual machines to start !', 'VirtualMachinesService');
      return;
    }

    Logger.warn('Starting X virtual machines...', 'VirtualMachinesService');
    for (const virtualMachine of virtualMachines) {
      Logger.warn(
        `Starting virtual machine #${virtualMachine.id}`,
        'VirtualMachinesService',
      );
      await this.dockerService.up(virtualMachine);
      Logger.log(
        `Virtual machine #${virtualMachine.id} started !`,
        'VirtualMachinesService',
      );
    }
    Logger.log(
      `${virtualMachines.length} virtual machines started !`,
      'VirtualMachinesService',
    );
  }

  async onModuleDestroy() {
    const virtualMachines = await this.virtualMachineRepository.find({
      where: { status: VirtualMachineStatusEnum.ONLINE },
    });
    if (virtualMachines.length === 0) {
      Logger.log('No virtual machines to stop !', 'VirtualMachinesService');
      return;
    }

    Logger.warn(
      `Stopping ${virtualMachines.length} virtual machines...`,
      'VirtualMachinesService',
    );
    for (const virtualMachine of virtualMachines) {
      Logger.warn(
        `Stopping virtual machine #${virtualMachine.id}`,
        'VirtualMachinesService',
      );
      await this.dockerService.kill(virtualMachine);
      await this.dockerService.down(virtualMachine);
      Logger.log(
        `Virtual machine #${virtualMachine.id} stopped !`,
        'VirtualMachinesService',
      );
    }
    Logger.log(
      `${virtualMachines.length} virtual machines stopped !`,
      'VirtualMachinesService',
    );
  }

  async checkVirtualMachinePermissions(
    userId: string,
    virtualMachine: VirtualMachine,
    creatorOnly = false,
  ) {
    const user = await this.usersService.findOne({
      where: { id: userId },
      relations: { groups: true, role: true },
    });
    if (!user) {
      throw new NotFoundException('user not found');
    }

    if (!userCanPatchVm(user, virtualMachine, creatorOnly)) {
      throw new UnauthorizedException();
    }
  }

  generateToken(id: string) {
    return this.jwtService.sign(
      { virtualMachineId: id },
      {
        expiresIn: '100y',
        secret: this.configService.get<string>('JWT_SECRET'),
      },
    );
  }

  async postVirtualMachineDuplicate(
    id: string,
    userId: string,
    postVirtualMachineDuplicateRequest: PostVirtualMachineDuplicateRequest,
  ) {
    const virtualMachine = await this.findOne({
      where: { id },
      relations: { context: true },
    });
    if (!virtualMachine) {
      throw new NotFoundException('virtual machine not found');
    }

    if (
      virtualMachine.status !== VirtualMachineStatusEnum.ONLINE &&
      virtualMachine.status !== VirtualMachineStatusEnum.OFFLINE
    ) {
      throw new NotFoundException('virtual machine not online/offline');
    }

    const save = await this.virtualMachineSavesService.create(
      { name: 'Duplicate', description: 'Virtual machine duplication' },
      virtualMachine,
      userId,
      VirtualMachineSaveTypeEnum.AUTOMATIC,
    );

    const duplicatedVirtualMachine = await this.postVirtualMachineFromSave(
      userId,
      postVirtualMachineDuplicateRequest,
      save.id,
    );

    return duplicatedVirtualMachine;
  }

  async postVirtualMachineFromSave(
    userId: string,
    postVirtualMachineFromSaveRequest: PostVirtualMachineFromSaveRequest,
    saveId: string,
  ) {
    const save = await this.virtualMachineSavesService.findOne({
      where: { id: saveId },
      relations: { virtualMachine: { project: true }, context: true },
    });
    if (!save) {
      throw new NotFoundException('virtual machine save not found');
    }

    const postVirtualMachineRequest: PostVirtualMachineRequest = {
      ...postVirtualMachineFromSaveRequest,
      projectId: save.virtualMachine.project.id,
      defaultLanguageIdentifier: save.virtualMachine.defaultLanguageIdentifier,
    };

    return this.create(userId, postVirtualMachineRequest, save);
  }

  async create(
    authorId: string,
    postVirtualMachineRequest: PostVirtualMachineRequest,
    save?: VirtualMachineSave,
  ) {
    let fromContext = undefined;
    if (save) {
      fromContext = await this.contextsService.findOne({
        where: { virtualMachineSave: { id: save.id } },
        relations: { applications: true },
      });
      if (!fromContext) {
        throw new NotFoundException('save context not found');
      }
    } else {
      fromContext = await this.contextsService.findOne({
        where: { isDefaultContext: true },
        relations: { applications: true },
      });
      if (!fromContext) {
        throw new NotFoundException('default context not found');
      }
    }

    const author = await this.usersService.findOne({ where: { id: authorId } });
    if (!author) {
      throw new NotFoundException('author not found');
    }

    const omekaProject = await this.omekaService.getProject(
      postVirtualMachineRequest.projectId,
    );
    const projectMetadatas = this.omekaTransformerService.omekaProjectToMaestro(
      omekaProject,
      [],
    );

    const context = await this.contextsService.createFromDefault();

    const id = await this.generateId();
    const token = this.generateToken(id);

    let project = await this.projectsService.findOne({
      where: { id: postVirtualMachineRequest.projectId },
    });
    if (!project) {
      project = await this.projectsService.create(projectMetadatas);
    }

    let virtualMachine = new VirtualMachine(
      id,
      postVirtualMachineRequest,
      author,
      context,
      token,
      project,
      save,
    );

    virtualMachine = await this.virtualMachineRepository.save(virtualMachine);

    this.checkProject(virtualMachine, save);

    await this.logsService.create({
      authorId,
      action: LogActionEnum.CREATE,
      entity: LogEntityEnum.VIRTUAL_MACHINE,
      entityId: virtualMachine.id,
      values: postVirtualMachineRequest,
    });

    const values = {
      ID: virtualMachine.id,
      Title: virtualMachine.title,
      URL: `http://${this.configService.get('MAESTRO_HOST')}/virtual-machines/${virtualMachine.id}`,
      'Omeka project': `https://omeka.tm.bsf-intranet.org/admin/items/show/${virtualMachine.project.id}`,
    };

    if (save) {
      values['From VM'] =
        `http://${this.configService.get('MAESTRO_HOST')}/virtual-machines/${save.virtualMachine.id}`;
    }

    await this.notificationsService.notify(
      NotificationTypeEnum.CREATE_VIRTUAL_MACHINE,
      author,
      values,
    );

    return new PostVirtualMachineResponse(virtualMachine);
  }

  async generateId() {
    let id = generateRandomId();
    let idExists = await this.findOne({ where: { id } });
    while (idExists) {
      id = generateRandomId();
      idExists = await this.findOne({ where: { id } });
    }
    return id;
  }

  async getVirtualMachineContainerInspect(id: string, container: string) {
    const virtualMachine = await this.findOne({ where: { id } });
    if (!virtualMachine) {
      throw new NotFoundException('virtual machine not found');
    }
    let isContainerCorrect = false;
    let valuesString = '';
    for (const value of Object.values(VirtualMachineContainerEnum)) {
      valuesString += value + ', ';
      if (value === container) {
        isContainerCorrect = true;
        break;
      }
    }
    if (!isContainerCorrect) {
      valuesString = valuesString.slice(0, -2);
      throw new BadRequestException(
        `Container must be one of: ${valuesString}`,
      );
    }
    const containerInspect = await this.dockerService.inspect(
      virtualMachine,
      container as VirtualMachineContainerEnum,
    );
    return containerInspect[0];
  }

  async getVirtualMachineContainerExposedPort(id: string, container: string) {
    const virtualMachine = await this.findOne({ where: { id } });
    if (!virtualMachine) {
      throw new NotFoundException('virtual machine not found');
    }
    let isContainerCorrect = false;
    let valuesString = '';
    for (const value of Object.values(VirtualMachineContainerEnum)) {
      valuesString += value + ', ';
      if (value === container) {
        isContainerCorrect = true;
        break;
      }
    }
    if (!isContainerCorrect) {
      valuesString = valuesString.slice(0, -2);
      throw new BadRequestException(
        `Container must be one of: ${valuesString}`,
      );
    }
    const containerInspect = await this.dockerService.inspect(
      virtualMachine,
      container as VirtualMachineContainerEnum,
    );
    return containerInspect[0].NetworkSettings.Ports['80/tcp'][0].HostPort;
  }

  async getVirtualMachine(id: string) {
    const virtualMachine = await this.findOne({
      where: { id },
      relations: {
        groups: true,
        context: { applications: { applicationTranslations: true } },
        virtualMachineErrors: true,
        user: true,
        project: true,
        originVirtualMachineSave: { virtualMachine: true },
      },
    });
    if (virtualMachine === null) {
      throw new NotFoundException('virtual machine not found');
    }

    return new GetVirtualMachineResponse(virtualMachine);
  }

  async getVirtualMachines(
    userId: string,
    getVirtualMachinesRequest: GetVirtualMachinesRequest,
  ) {
    const pageOptions = new PageOptions(getVirtualMachinesRequest);
    const virtualMachinesQuery = this.virtualMachineRepository
      .createQueryBuilder('virtualMachine')
      .leftJoinAndSelect('virtualMachine.user', 'user')
      .leftJoinAndSelect('virtualMachine.groups', 'group')
      .innerJoinAndSelect('virtualMachine.project', 'project');

    let { orderBy } = getVirtualMachinesRequest;

    orderBy = orderBy ? orderBy : VirtualMachineOrderByEnum.CREATED_AT;

    const queryHelper = new QueryHelper(virtualMachinesQuery);
    queryHelper.pagination(pageOptions, orderBy);

    if (getVirtualMachinesRequest.isUserVmsFiltered === BooleanEnum.TRUE) {
      queryHelper.andWhereEquals('user.id', { userId });
    }

    if (getVirtualMachinesRequest.statuses) {
      const statuses = getVirtualMachinesRequest.statuses.split(',');
      queryHelper.andWhereMultipleEquals('virtualMachine.status', statuses);
    }

    if (getVirtualMachinesRequest.users) {
      const users = getVirtualMachinesRequest.users.split(',');
      queryHelper.andWhereMultipleEquals('user.id', users);
    }

    if (getVirtualMachinesRequest.approvals) {
      const approvals = getVirtualMachinesRequest.approvals.split(',');
      queryHelper.andWhereMultipleEquals('virtualMachine.approval', approvals);
    }

    if (getVirtualMachinesRequest.groups) {
      const groups = getVirtualMachinesRequest.groups.split(',');
      queryHelper.andWhereMultipleEquals('group.id', groups);
    }

    if (getVirtualMachinesRequest.createdDateRange) {
      queryHelper.andWhereDateRange(
        'virtualMachine.createdAt',
        getVirtualMachinesRequest.createdDateRange,
      );
    }

    if (getVirtualMachinesRequest.updatedDateRange) {
      queryHelper.andWhereDateRange(
        'virtualMachine.updatedAt',
        getVirtualMachinesRequest.updatedDateRange,
      );
    }

    queryHelper.andWhereQuery(getVirtualMachinesRequest.query, [
      'virtualMachine.title',
      'virtualMachine.description',
      'virtualMachine.referenceUrl',
      'project.id',
      'virtualMachine.id',
    ]);
    const [count, virtualMachines] = await getManyAndCount<VirtualMachine>(
      virtualMachinesQuery,
      ['virtualMachine.id'],
      false,
    );

    return new GetVirtualMachinesResponse(virtualMachines, count, pageOptions);
  }

  async postVirtualMachineGroup(id: string, groupId: number, userId: string) {
    const virtualMachine = await this.findOne({
      where: { id },
      relations: { user: true, groups: true },
    });
    if (!virtualMachine) {
      throw new NotFoundException('virtual machine not found');
    }

    await this.checkVirtualMachinePermissions(userId, virtualMachine, true);

    const group = await this.groupsService.findOne({
      where: { id: groupId },
    });
    if (!group) {
      throw new NotFoundException('group not found');
    }

    const findGroup = virtualMachine.groups.find(
      (group) => group.id === groupId,
    );
    if (findGroup) {
      throw new BadRequestException('virtual machine already have this group');
    }

    if (Array.isArray(virtualMachine.groups)) {
      virtualMachine.groups.push(group);
    } else {
      virtualMachine.groups = [group];
    }

    await this.virtualMachineRepository.save(virtualMachine);

    await this.logsService.create({
      authorId: userId,
      action: LogActionEnum.UPDATE,
      entity: LogEntityEnum.VIRTUAL_MACHINE,
      entityId: virtualMachine.id,
      values: { groupId },
      description: 'Add virtual machine group',
    });

    return;
  }

  async deleteVirtualMachineGroup(id: string, groupId: number, userId: string) {
    const virtualMachine = await this.findOne({
      where: { id },
      relations: { user: true, groups: true },
    });
    if (!virtualMachine) {
      throw new NotFoundException('virtual machine not found');
    }

    await this.checkVirtualMachinePermissions(userId, virtualMachine, true);

    const group = await this.groupsService.findOne({
      where: { id: groupId },
    });
    if (!group) {
      throw new NotFoundException('group not found');
    }

    const findGroup = virtualMachine.groups.find(
      (group) => group.id === groupId,
    );
    if (!findGroup) {
      throw new BadRequestException("virtual machine don't have this group");
    }

    virtualMachine.groups = virtualMachine.groups.filter(
      (group) => group.id !== groupId,
    );

    await this.virtualMachineRepository.save(virtualMachine);

    await this.logsService.create({
      authorId: userId,
      action: LogActionEnum.UPDATE,
      entity: LogEntityEnum.VIRTUAL_MACHINE,
      entityId: virtualMachine.id,
      values: { groupId },
      description: 'Delete virtual machine group',
    });

    return;
  }

  async getVirtualMachineErrors(
    id: string,
    getVirtualMachineErrorsRequest: GetVirtualMachineErrorsRequest,
  ) {
    const virtualMachine = await this.findOne({ where: { id } });
    if (!virtualMachine) {
      throw new NotFoundException('virtual machine not found');
    }

    const pageOptions = new PageOptions(getVirtualMachineErrorsRequest);
    const errorsQuery = this.virtualMachineErrorRepository
      .createQueryBuilder('vmError')
      .innerJoin('vmError.virtualMachine', 'virtualMachine')
      .take(pageOptions.take)
      .skip(pageOptions.skip)
      .where('virtualMachine.id = :id', { id });

    const queryHelper = new QueryHelper(errorsQuery);
    queryHelper.pagination(pageOptions, 'vmError.id');

    const [count, errors] = await getManyAndCount<VirtualMachineError>(
      errorsQuery,
      ['vmError.id'],
    );

    return new GetVirtualMachineErrorsResponse(errors, count, pageOptions);
  }

  async getVirtualMachineGroups(
    id: string,
    getVirtualMachineGroupsRequest: GetVirtualMachineGroupsRequest,
  ) {
    const virtualMachine = await this.findOne({ where: { id } });
    if (!virtualMachine) {
      throw new NotFoundException('virtual machine not found');
    }

    let { orderBy } = getVirtualMachineGroupsRequest;

    orderBy = orderBy ? orderBy : VirtualMachineGroupOrderByEnum.NAME;

    const pageOptions = new PageOptions(getVirtualMachineGroupsRequest, {
      order: PageOrderEnum.ASC,
    });
    const groupsQuery = this.groupsService
      .createQueryBuilder('group')
      .innerJoin('group.virtualMachines', 'virtualMachine')
      .leftJoinAndSelect('group.users', 'user')
      .skip(pageOptions.skip)
      .take(pageOptions.take)
      .where('virtualMachine.id = :id', { id: id });

    const queryHelper = new QueryHelper(groupsQuery);

    queryHelper.pagination(pageOptions, orderBy);

    const [count, groups] = await getManyAndCount<Group>(
      groupsQuery,
      ['group.id'],
      false,
    );

    return new GetVirtualMachineGroupsResponse(groups, count, pageOptions);
  }

  async deleteAllErrors(virtualMachine: VirtualMachine) {
    await this.virtualMachineErrorRepository.delete({ virtualMachine });
  }

  async saveErrors(
    virtualMachine: VirtualMachine,
    projectErrors: IProjectErrors,
  ) {
    await this.deleteAllErrors(virtualMachine);
    for (const projectError of projectErrors.projectErrors) {
      const error = new VirtualMachineError(projectError, virtualMachine);
      await this.virtualMachineErrorRepository.save(error);
    }

    for (const packageError of projectErrors.packagesErrors) {
      const error = new VirtualMachineError(packageError, virtualMachine);
      await this.virtualMachineErrorRepository.save(error);
    }

    for (const itemError of projectErrors.itemsErrors) {
      const error = new VirtualMachineError(itemError, virtualMachine);
      await this.virtualMachineErrorRepository.save(error);
    }
  }

  async checkProject(
    virtualMachine: VirtualMachine,
    virtualMachineSave?: VirtualMachineSave,
    updating?: boolean,
  ) {
    if (virtualMachineSave) {
      await this.setStatus(
        virtualMachine,
        VirtualMachineStatusEnum.CREATING_VIRTUAL_MACHINE,
      );
      this.install(virtualMachine, virtualMachineSave, updating);
      return virtualMachine;
    }

    const importResult = await this.omekaImporterService.importProject(
      virtualMachine.project.id,
    );
    if ('count' in importResult) {
      await this.saveErrors(virtualMachine, importResult);
      await this.setStatus(
        virtualMachine,
        VirtualMachineStatusEnum.PROJECT_ERROR,
      );
      await this.notificationsService.notify(
        NotificationTypeEnum.VIRTUAL_MACHINE_ERROR,
        virtualMachine.user,
        {
          Reason: 'Project errors detected',
          ID: virtualMachine.id,
          Title: virtualMachine.title,
          URL: `http://${this.configService.get('MAESTRO_HOST')}/virtual-machines/${virtualMachine.id}`,
          'Omeka project': `https://omeka.tm.bsf-intranet.org/admin/items/show/${virtualMachine.project.id}`,
        },
      );
    } else {
      const project = await this.projectsService.findOne({
        where: { id: virtualMachine.project.id },
      });
      await this.projectsService.setStatus(project, ProjectStatusEnum.READY);
      await this.setStatus(
        virtualMachine,
        VirtualMachineStatusEnum.CREATING_VIRTUAL_MACHINE,
      );
      this.install(virtualMachine, virtualMachineSave, updating);
    }

    return virtualMachine;
  }

  async patchVirtualMachine(
    id: string,
    patchVirtualMachineRequest: PatchVirtualMachineRequest,
    userId: string,
  ) {
    const virtualMachine = await this.findOne({
      where: { id },
      relations: { groups: true, user: true },
    });
    if (!virtualMachine) {
      throw new NotFoundException('virtual machine not found');
    }

    await this.checkVirtualMachinePermissions(userId, virtualMachine);

    virtualMachine.title =
      patchVirtualMachineRequest.title || virtualMachine.title;
    virtualMachine.defaultLanguageIdentifier =
      patchVirtualMachineRequest.defaultLanguageIdentifier ||
      virtualMachine.defaultLanguageIdentifier;

    if (patchVirtualMachineRequest.description === '') {
      virtualMachine.description = null;
    } else {
      virtualMachine.description =
        patchVirtualMachineRequest.description || virtualMachine.description;
    }

    if (patchVirtualMachineRequest.referenceUrl === '') {
      virtualMachine.referenceUrl = null;
    } else {
      virtualMachine.referenceUrl =
        patchVirtualMachineRequest.referenceUrl || virtualMachine.referenceUrl;
    }
    await this.virtualMachineRepository.save(virtualMachine);

    await this.logsService.create({
      authorId: userId,
      action: LogActionEnum.UPDATE,
      entity: LogEntityEnum.VIRTUAL_MACHINE,
      entityId: virtualMachine.id,
      values: patchVirtualMachineRequest,
      description: 'Update virtual machine informations',
    });

    return new PatchVirtualMachineResponse(virtualMachine);
  }

  async patchVirtualMachineArchive(
    id: string,
    userId: string,
    { isArchived }: PatchVirtualMachineArchiveRequest,
  ) {
    const virtualMachine = await this.findOne({
      where: { id },
      relations: { groups: true, user: true },
    });
    if (!virtualMachine) {
      throw new NotFoundException('virtual machine not found');
    }

    await this.checkVirtualMachinePermissions(userId, virtualMachine);

    await this.logsService.create({
      authorId: userId,
      action: LogActionEnum.UPDATE,
      entity: LogEntityEnum.VIRTUAL_MACHINE,
      entityId: virtualMachine.id,
      description: `${isArchived ? 'Archive' : 'Unarchive'} virtual machine`,
    });

    virtualMachine.isArchived = isArchived;
    await this.virtualMachineRepository.save(virtualMachine);
    this.socketGateway.emitVirtualMachineIsArchived(virtualMachine);

    if (isArchived) {
      this.stopVirtualMachine(virtualMachine);
    }

    return;
  }

  async patchVirtualMachineStop(id: string, userId: string) {
    const virtualMachine = await this.findOne({
      where: { id },
      relations: { groups: true, user: true },
    });
    if (!virtualMachine) {
      throw new NotFoundException('virtual machine not found');
    }

    await this.checkVirtualMachinePermissions(userId, virtualMachine);

    if (virtualMachine.approval === VirtualMachineApprovalEnum.IN_PROGRESS) {
      throw new BadRequestException(
        'Cannot stop virtual machine with approval',
      );
    }

    if (virtualMachine.status !== VirtualMachineStatusEnum.ONLINE) {
      throw new BadRequestException(
        'Cannot stop virtual machine that is not online',
      );
    }

    if (this.isOldVirtualMachine(virtualMachine)) {
      await this.setStatus(
        virtualMachine,
        VirtualMachineStatusEnum.STOPPING_ERROR,
      );
      await this.notificationsService.notify(
        NotificationTypeEnum.VIRTUAL_MACHINE_ERROR,
        virtualMachine.user,
        { message: 'This VM needs to be manually updated !' },
      );
      throw new BadRequestException('Cannot stop old virtual machine');
    }

    await this.logsService.create({
      authorId: userId,
      action: LogActionEnum.UPDATE,
      entity: LogEntityEnum.VIRTUAL_MACHINE,
      entityId: virtualMachine.id,
      description: 'Stop virtual machine',
    });

    this.stopVirtualMachine(virtualMachine);

    return;
  }

  isOldVirtualMachine(virtualMachine: VirtualMachine): boolean {
    const deployPath = this.getVirtualMachineDeployPath(virtualMachine);

    if (!existsSync(join(deployPath, 'composes'))) {
      return true;
    }

    return false;
  }

  async patchVirtualMachineStart(id: string, userId: string) {
    const virtualMachine = await this.findOne({
      where: { id },
      relations: { groups: true, user: true, project: true },
    });
    if (!virtualMachine) {
      throw new NotFoundException('virtual machine not found');
    }

    await this.checkVirtualMachinePermissions(userId, virtualMachine);

    if (virtualMachine.status !== VirtualMachineStatusEnum.OFFLINE) {
      throw new BadRequestException(
        'Cannot start virtual machine that is not offline',
      );
    }

    if (virtualMachine.isArchived) {
      throw new BadRequestException('Cannot start archived virtual machine');
    }

    if (this.isOldVirtualMachine(virtualMachine)) {
      await this.setStatus(
        virtualMachine,
        VirtualMachineStatusEnum.STARTING_ERROR,
      );
      await this.notificationsService.notify(
        NotificationTypeEnum.VIRTUAL_MACHINE_ERROR,
        virtualMachine.user,
        { message: 'This VM needs to be manually updated !' },
      );
      throw new BadRequestException('Cannot start old virtual machine');
    }

    await this.logsService.create({
      authorId: userId,
      action: LogActionEnum.UPDATE,
      entity: LogEntityEnum.VIRTUAL_MACHINE,
      entityId: virtualMachine.id,
      description: 'Start virtual machine',
    });

    this.startVirtualMachine(virtualMachine);

    return;
  }

  async startVirtualMachine(virtualMachine: VirtualMachine) {
    let result = true;
    await this.setStatus(virtualMachine, VirtualMachineStatusEnum.STARTING);
    this.configureDeploy(virtualMachine);
    const upResult = await this.dockerService.up(virtualMachine);
    if (!upResult || upResult.raw !== '') {
      result = false;
      await this.setStatus(
        virtualMachine,
        VirtualMachineStatusEnum.STARTING_ERROR,
      );
    }

    await this.setStatus(virtualMachine, VirtualMachineStatusEnum.ONLINE);
    return result;
  }

  async stopVirtualMachine(virtualMachine: VirtualMachine) {
    let result = true;
    await this.setStatus(virtualMachine, VirtualMachineStatusEnum.STOPPING);
    const downResult = await this.dockerService.stop(virtualMachine);
    if (!downResult || downResult.raw !== '') {
      result = false;
      await this.setStatus(
        virtualMachine,
        VirtualMachineStatusEnum.STOPPING_ERROR,
      );
    }

    await this.setStatus(virtualMachine, VirtualMachineStatusEnum.OFFLINE);
    return result;
  }

  async patchVirtualMachineDeployStatus(
    id: string,
    { isDeployed }: PatchVirtualMachineDeployStatusRequest,
  ) {
    const virtualMachine = await this.findOne({
      where: { id },
      relations: { user: true },
    });
    if (isDeployed) {
      await this.setStatus(virtualMachine, VirtualMachineStatusEnum.ONLINE);
      await this.notificationsService.notify(
        NotificationTypeEnum.VIRTUAL_MACHINE_READY,
        virtualMachine.user,
        {
          ID: virtualMachine.id,
          Title: virtualMachine.title,
          URL: `http://${this.configService.get('MAESTRO_HOST')}/virtual-machines/${virtualMachine.id}`,
        },
      );
    } else {
      await this.setStatus(
        virtualMachine,
        VirtualMachineStatusEnum.INSTALLING_PROJECT_ERROR,
      );

      await this.notificationsService.notify(
        NotificationTypeEnum.VIRTUAL_MACHINE_ERROR,
        virtualMachine.user,
        {
          Reason: 'Error while installing project on OLIP',
          ID: virtualMachine.id,
          Title: virtualMachine.title,
          URL: `http://${this.configService.get('MAESTRO_HOST')}/virtual-machines/${virtualMachine.id}`,
        },
      );
    }
  }

  getVirtualMachineDeployPath(virtualMachine: VirtualMachine) {
    return join(__dirname, `../../data/virtual_machines/${virtualMachine.id}`);
  }

  createOlipFilesPath(virtualMachine: VirtualMachine) {
    Logger.debug('Creating olip-files path...', 'VirtualMachinesService');
    const deployPath = this.getVirtualMachineDeployPath(virtualMachine);
    const olipFilesPath = `${deployPath}/data/olip-files`;

    try {
      if (!existsSync(olipFilesPath)) {
        mkdirSync(olipFilesPath, { recursive: true });
        Logger.debug(
          'Succesfully created olip-files path !',
          'VirtualMachinesService',
        );
      }
    } catch (e) {
      console.error(e);
      Logger.error(
        'Error while creating olip-files path !',
        'VirtualMachinesService',
      );
    }
  }

  createInternetFlagFile(virtualMachine: VirtualMachine) {
    Logger.debug('Creating internet flag file...', 'VirtualMachinesService');

    const deployPath = this.getVirtualMachineDeployPath(virtualMachine);
    const networkPath = `${deployPath}/data/olip-files/network`;
    const internetFilePath = `${networkPath}/internet`;

    try {
      if (!existsSync(networkPath)) {
        mkdirSync(networkPath, { recursive: true });
      }
      Logger.debug(
        'Succesfully created network path !',
        'VirtualMachinesService',
      );
      if (!existsSync(networkPath)) {
        writeFileSync(internetFilePath, '');
      }
      Logger.debug(
        'Succesfully created internet flag file !',
        'VirtualMachinesService',
      );
    } catch (e) {
      console.error(e);
      Logger.error(
        'Error while creating internet flag file !',
        'VirtualMachinesService',
      );
    }
  }

  async createOrUpdateDeployContextFile(
    virtualMachine: VirtualMachine,
    update: boolean = false,
  ) {
    Logger.debug(
      'Creating or updating deploy context flag file...',
      'VirtualMachinesService',
    );

    const deployPath = this.getVirtualMachineDeployPath(virtualMachine);
    const olipFilesDeployPath = `${deployPath}/data/olip-files/deploy`;
    const deployContextFilePath = `${deployPath}/data/olip-files/deploy/context`;

    try {
      if (!existsSync(olipFilesDeployPath)) {
        mkdirSync(olipFilesDeployPath, { recursive: true });
      }
      Logger.debug(
        'Succesfully created olip-files/deploy path !',
        'VirtualMachinesService',
      );
    } catch (e) {
      console.error(e);
      Logger.error(
        'Error while creating olip-files/deploy path !',
        'VirtualMachinesService',
      );
    }

    if (update) {
      try {
        if (existsSync(deployContextFilePath)) {
          rmSync(deployContextFilePath, { recursive: true });
        }
        Logger.debug(
          'Succesfully removed olip-files/deploy path and file !',
          'VirtualMachinesService',
        );
      } catch (e) {
        console.error(e);
        Logger.error(
          'Error while removing olip-files/deploy path and file !',
          'VirtualMachinesService',
        );
      }
    }

    try {
      if (!existsSync(deployContextFilePath)) {
        const contextContent =
          await this.getStringifyDeployContext(virtualMachine);
        writeFileSync(deployContextFilePath, `${contextContent}\n\r`);
      }
      Logger.debug(
        'Succesfully wrote deploy flag file with context content !',
        'VirtualMachinesService',
      );
    } catch (e) {
      console.error(e);
      Logger.error(
        'Error while writing deploy flag file with context content !',
        'VirtualMachinesService',
      );
    }
  }

  async getStringifyDeployContext(
    virtualMachine: VirtualMachine,
  ): Promise<string> {
    const context = await this.contextsService.findOne({
      where: { virtualMachine: { id: virtualMachine.id } },
      relations: { applications: true },
    });
    return JSON.stringify(context);
  }

  async cloneDeploy(virtualMachine: VirtualMachine) {
    Logger.log(
      `Copying deploy template for virtual machine #${virtualMachine.id}`,
    );

    // Get virtual machine deploy path
    const deployPath = this.getVirtualMachineDeployPath(virtualMachine);

    // Prepare git
    const git = new Git({ workdir: deployPath });

    // Clone deploy repository
    await git.clone(this.configService.get('OLIP_DEPLOY_REPOSITORY'), {
      branch: this.configService.get('MAESTRO_BRANCH'),
    });
  }

  private replaceEnvVariables(
    parsedEnvFile: dotenv.DotenvParseOutput,
    envFileName: string,
  ) {
    const templateEnvFilePath = join(
      '../../data/templates/virtual_machines',
      envFileName,
    );
    if (existsSync(templateEnvFilePath)) {
      const templateEnvFileContent = readFileSync(templateEnvFilePath, 'utf8');
      const parsedTemplateEnvFile = dotenv.parse(templateEnvFileContent);
      for (const key in parsedEnvFile) {
        if (key in parsedTemplateEnvFile) {
          parsedEnvFile[key] = parsedTemplateEnvFile[key];
        }
      }
    }
    return parsedEnvFile;
  }

  private fillVariable(
    parsedEnvFile: dotenv.DotenvParseOutput,
    key: string,
    value: string,
  ) {
    if (key in parsedEnvFile) {
      parsedEnvFile[key] = value;
    }
  }

  configureEnvFile(virtualMachine: VirtualMachine, envFileName: string) {
    const deployPath = this.getVirtualMachineDeployPath(virtualMachine);
    const envFilePath = join(deployPath, 'envs/', envFileName);

    const envFileContent = readFileSync(envFilePath, 'utf8');
    const parsedEnvFile = dotenv.parse(envFileContent);

    // Replace variables that are found in template
    this.replaceEnvVariables(parsedEnvFile, envFileName);

    // Manage TO_FILL variables
    const branch = this.configService.get('MAESTRO_BRANCH');
    const olipServicesTag = branch === 'main' ? 'latest' : `${branch}`;

    this.fillVariable(parsedEnvFile, 'OLIP_SERVICES_TAG', olipServicesTag);
    this.fillVariable(parsedEnvFile, 'VM_ID', `${virtualMachine.id}`);
    this.fillVariable(
      parsedEnvFile,
      'OLIP_VOLUME',
      `${process.env.MAESTRO_VOLUME}/virtual_machines/${virtualMachine.id}/data`,
    );
    this.fillVariable(
      parsedEnvFile,
      'DEPLOY_PATH',
      `${process.env.MAESTRO_VOLUME}/virtual_machines/${virtualMachine.id}`,
    );
    this.fillVariable(
      parsedEnvFile,
      'OLIP_HOST',
      `${virtualMachine.id}.${this.configService.get('MAESTRO_VMS_HOST')}`,
    );
    this.fillVariable(
      parsedEnvFile,
      'TLS_ENABLED',
      this.configService.get('TLS_ENABLED') || 'false',
    );
    this.fillVariable(
      parsedEnvFile,
      'OLIP_RESOLVER_ID',
      this.configService.get('MAESTRO_RESOLVER_ID'),
    );
    this.fillVariable(
      parsedEnvFile,
      'OLIP_NETWORK',
      this.configService.get('MAESTRO_NETWORK_ID'),
    );
    this.fillVariable(parsedEnvFile, 'OLIP_NETWORK_EXTERNAL', 'true');
    this.fillVariable(
      parsedEnvFile,
      'S3_BUCKET_VOLUME',
      `${process.env.S3_BUCKET_VOLUME}`,
    );
    this.fillVariable(
      parsedEnvFile,
      'S3_BUCKET_URL',
      `${this.configService.get('S3_ENDPOINT')}/${this.configService.get('S3_BUCKET')}`,
    );
    this.fillVariable(
      parsedEnvFile,
      'OMEKA_PROJECT_ID',
      `${virtualMachine.project.id}`,
    );
    this.fillVariable(
      parsedEnvFile,
      'HAO_URL',
      `http://${virtualMachine.id}-hao`,
    );
    this.fillVariable(
      parsedEnvFile,
      'ELASTICSEARCH_API_URL',
      this.configService.get('ELASTICSEARCH_API_URL_FOR_OLIP'),
    );
    this.fillVariable(
      parsedEnvFile,
      'MAESTRO_API_URL',
      this.configService.get('MAESTRO_API_URL_FOR_OLIP'),
    );
    this.fillVariable(
      parsedEnvFile,
      'MAESTRO_API_TOKEN',
      `${virtualMachine.token}`,
    );
    this.fillVariable(
      parsedEnvFile,
      'DEPLOY_REPOSITORY_VOLUME',
      `${this.getVirtualMachineDeployPath(virtualMachine)}`,
    );
    this.fillVariable(
      parsedEnvFile,
      'VITE_AXIOS_BASEURL',
      `${this.configService.get('TLS_ENABLED') === 'true' ? 'https' : 'http'}://api.${virtualMachine.id}.${this.configService.get(
        'MAESTRO_VMS_HOST',
      )}/`,
    );
    this.fillVariable(
      parsedEnvFile,
      'VITE_DEFAULT_LANGUAGE',
      `${virtualMachine.defaultLanguageIdentifier}`,
    );
    this.fillVariable(
      parsedEnvFile,
      'DOCKER_PRIVATE_APPS_REPOSITORY_TOKEN',
      this.configService.get('DOCKER_PRIVATE_APPS_REPOSITORY_TOKEN'),
    );
    this.fillVariable(parsedEnvFile, 'APPS_IMAGES_TAG', olipServicesTag);

    const updatedEnvContent = Object.keys(parsedEnvFile)
      .map((key) => `${key}=${parsedEnvFile[key]}`)
      .join('\n');
    writeFileSync(envFilePath, updatedEnvContent);
  }

  async installDeploy(virtualMachine: VirtualMachine) {
    const deployPath = this.getVirtualMachineDeployPath(virtualMachine);
    const installScriptPath = join(deployPath, 'scripts/install.sh');
    const maestroBranch = this.configService.get('MAESTRO_BRANCH');
    const tag = maestroBranch === 'main' ? 'latest' : `${maestroBranch}`;

    // Run install script from deploy
    await bash(`bash ${installScriptPath} -t ${tag} --vm`);
  }

  async configureDeploy(virtualMachine: VirtualMachine) {
    Logger.log(
      `Configuring env files for virtual machine #${virtualMachine.id}`,
      'VirtualMachinesService',
    );

    const deployPath = this.getVirtualMachineDeployPath(virtualMachine);

    // Get all env files names
    const envFiles = readdirSync(`${deployPath}/envs`, { withFileTypes: true })
      .filter((dirent) => dirent.isFile() && dirent.name.endsWith('.env'))
      .map((dirent) => dirent.name);

    // Configure each env file
    for (const envFile of envFiles) {
      this.configureEnvFile(virtualMachine, envFile);
    }
  }

  async install(
    virtualMachine: VirtualMachine,
    virtualMachineSave?: VirtualMachineSave,
    updating?: boolean,
  ) {
    try {
      await this.cloneDeploy(virtualMachine);
      await this.installDeploy(virtualMachine);
      await this.configureDeploy(virtualMachine);
      this.createOlipFilesPath(virtualMachine);
      this.createInternetFlagFile(virtualMachine);
      await this.createOrUpdateDeployContextFile(virtualMachine);
    } catch (e) {
      console.error(e);
      Logger.error(
        `Error while creating virtual machine #${virtualMachine.id}`,
        'VirtualMachinesService',
      );
      await this.setStatus(
        virtualMachine,
        VirtualMachineStatusEnum.CREATING_VIRTUAL_MACHINE_ERROR,
      );
      await this.notificationsService.notify(
        NotificationTypeEnum.VIRTUAL_MACHINE_ERROR,
        virtualMachine.user,
        {
          Reason: 'Error while creating virtual machine',
          ID: virtualMachine.id,
          Title: virtualMachine.title,
          URL: `http://${this.configService.get('MAESTRO_HOST')}/virtual-machines/${virtualMachine.id}`,
        },
      );
      return;
    }

    if (virtualMachineSave) {
      Logger.log(
        `Importing database for virtual machine #${virtualMachine.id} from save #${virtualMachineSave.id}`,
        'VirtualMachinesService',
      );
      const importResult = await this.virtualMachineSavesService.importDatabase(
        virtualMachine,
        virtualMachineSave,
      );
      if (!importResult) {
        Logger.error(
          `Error while importing database for virtual machine #${virtualMachine.id} from save #${virtualMachineSave.id}`,
          'VirtualMachinesService',
        );
        await this.setStatus(
          virtualMachine,
          VirtualMachineStatusEnum.CREATING_VIRTUAL_MACHINE_ERROR,
        );
        await this.notificationsService.notify(
          NotificationTypeEnum.VIRTUAL_MACHINE_ERROR,
          virtualMachine.user,
          {
            Reason: 'Error while creating virtual machine',
            ID: virtualMachine.id,
            Title: virtualMachine.title,
            URL: `http://${this.configService.get('MAESTRO_HOST')}/virtual-machines/${virtualMachine.id}`,
          },
        );
        return;
      }
    }

    Logger.log(
      `Starting virtual machine #${virtualMachine.id}`,
      'VirtualMachinesService',
    );

    await this.setStatus(virtualMachine, VirtualMachineStatusEnum.STARTING);

    const upResult = await this.dockerService.up(virtualMachine);

    if (!upResult || upResult.raw !== '') {
      Logger.error(
        `Error while starting virtual machine #${virtualMachine.id}`,
        'VirtualMachinesService',
      );
      await this.setStatus(
        virtualMachine,
        VirtualMachineStatusEnum.STARTING_ERROR,
      );
      await this.notificationsService.notify(
        NotificationTypeEnum.VIRTUAL_MACHINE_ERROR,
        virtualMachine.user,
        {
          Reason: 'Error while starting virtual machine',
          ID: virtualMachine.id,
          Title: virtualMachine.title,
          URL: `http://${this.configService.get('MAESTRO_HOST')}/virtual-machines/${virtualMachine.id}`,
        },
      );
      return;
    }

    if (virtualMachineSave) {
      await this.setStatus(virtualMachine, VirtualMachineStatusEnum.ONLINE);
      await this.notificationsService.notify(
        NotificationTypeEnum.VIRTUAL_MACHINE_READY,
        virtualMachine.user,
        {
          ID: virtualMachine.id,
          Title: virtualMachine.title,
          URL: `http://${this.configService.get('MAESTRO_HOST')}/virtual-machines/${virtualMachine.id}`,
        },
      );
    } else {
      if (updating === true) {
        await this.setStatus(virtualMachine, VirtualMachineStatusEnum.ONLINE);
      } else {
        const actualVirtualMachine =
          await this.virtualMachineRepository.findOne({
            where: { id: virtualMachine.id },
          });
        if (!actualVirtualMachine) return;
        if (actualVirtualMachine.status === VirtualMachineStatusEnum.ONLINE)
          return;
        await this.setStatus(
          virtualMachine,
          VirtualMachineStatusEnum.INSTALLING_PROJECT,
        );
      }
    }
    return;
  }

  async setStatus(
    virtualMachine: VirtualMachine,
    status: VirtualMachineStatusEnum,
  ) {
    virtualMachine.status = status;
    await this.virtualMachineRepository.save(virtualMachine);
    this.socketGateway.emitVirtualMachineStatus(virtualMachine);
  }

  async setApproval(
    virtualMachine: VirtualMachine,
    approval: VirtualMachineApprovalEnum,
  ) {
    virtualMachine.approval = approval;
    await this.virtualMachineRepository.save(virtualMachine);
    this.socketGateway.emitVirtualMachineApproval(virtualMachine);
  }

  async getVirtualMachineLogs(
    id: string,
    getVirtualMachineLogsRequest: GetVirtualMachineLogsRequest,
  ) {
    const virtualMachine = await this.findOne({ where: { id } });
    if (!virtualMachine) {
      throw new NotFoundException('virtual machine not found');
    }

    let maxLines = 100;

    if (getVirtualMachineLogsRequest.maxLines) {
      if (isNaN(parseInt(getVirtualMachineLogsRequest.maxLines))) {
        throw new BadRequestException('maxLines must be a number');
      } else {
        maxLines = parseInt(getVirtualMachineLogsRequest.maxLines);
      }
    }

    const { html } = await this.dockerService.getParsedLogs(
      virtualMachine,
      getVirtualMachineLogsRequest.container,
      maxLines,
    );

    return new GetVirtualMachineLogsResponse(html);
  }

  async getVirtualMachineLogsDownload(
    id: string,
    getVirtualMachineLogsDownloadRequest: GetVirtualMachineLogsDownloadRequest,
  ) {
    const virtualMachine = await this.findOne({ where: { id } });
    if (!virtualMachine) {
      throw new NotFoundException('virtual machine not found');
    }

    const logs = await this.dockerService.getLogs(
      virtualMachine,
      getVirtualMachineLogsDownloadRequest.container,
      0,
    );

    return logs;
  }

  async getVirtualMachineHistory(
    id: string,
    getVirtualMachineHistoryRequest: GetVirtualMachineHistoryRequest,
  ) {
    const virtualMachine = await this.findOne({
      where: { id },
      relations: { context: true, virtualMachineSaves: { deploys: true } },
    });
    if (!virtualMachine) {
      throw new NotFoundException('virtual machine not found');
    }

    const pageOptions = new PageOptions(getVirtualMachineHistoryRequest);
    const { dateRange, query } = getVirtualMachineHistoryRequest;
    let { orderBy } = getVirtualMachineHistoryRequest;

    orderBy = orderBy ? orderBy : LogOrderByEnum.CREATED_AT;

    const logsQueryBuilder = this.logsService
      .getQueryBuilder('log')
      .innerJoinAndSelect('log.user', 'user');

    const queryHelper = new QueryHelper(logsQueryBuilder);

    queryHelper.pagination(pageOptions, orderBy);
    queryHelper.andWhereDateRange('log.createdAt', dateRange);

    if (getVirtualMachineHistoryRequest.userIds) {
      const userIds = getVirtualMachineHistoryRequest.userIds.split(',');
      queryHelper.andWhereMultipleEquals('user.id', userIds);
    }

    if (getVirtualMachineHistoryRequest.actions) {
      const actions = getVirtualMachineHistoryRequest.actions.split(',');
      queryHelper.andWhereMultipleEquals('action', actions);
    }

    if (getVirtualMachineHistoryRequest.levels) {
      const levels = getVirtualMachineHistoryRequest.levels.split(',');
      queryHelper.andWhereMultipleEquals('level', levels);
    }

    queryHelper.andWhereQuery(query, [
      'log.action',
      'log.entity',
      'log.values',
      'log.description',
      'log.level',
      'user.name',
    ]);

    const deployIds = [];
    if (virtualMachine.virtualMachineSaves) {
      for (const virtualMachineSave of virtualMachine.virtualMachineSaves) {
        if (virtualMachineSave.deploys) {
          for (const deploy of virtualMachineSave.deploys) {
            deployIds.push(deploy.id);
          }
        }
      }
    }

    const virtualMachineSaveIds = [];
    if (virtualMachine.virtualMachineSaves) {
      for (const virtualMachineSave of virtualMachine.virtualMachineSaves) {
        virtualMachineSaveIds.push(virtualMachineSave.id);
      }
    }

    if (getVirtualMachineHistoryRequest.entities) {
      const entities = getVirtualMachineHistoryRequest.entities.split(',');
      const sql = [];
      const properties = {};
      for (let i = 0; i < entities.length; i++) {
        const entity = entities[i];
        let entityId: number | string = virtualMachine.id;
        if (entity === VirtualMachineHistoryEntity.CONTEXT) {
          entityId = virtualMachine.context.id;
        }
        if (entity === VirtualMachineHistoryEntity.VIRTUAL_MACHINE_SAVE) {
          properties[`entity${i}`] = entity;
          properties[`entityId${i}`] = virtualMachineSaveIds;
          sql.push(`(entity = :entity${i} AND entityId IN (:...entityId${i}))`);
        } else if (entity === VirtualMachineHistoryEntity.DEPLOY) {
          properties[`entity${i}`] = entity;
          properties[`entityId${i}`] = deployIds;
          sql.push(`(entity = :entity${i} AND entityId IN (:...entityId${i}))`);
        } else {
          properties[`entity${i}`] = entity;
          properties[`entityId${i}`] = entityId;
          sql.push(`(entity = :entity${i} AND entityId = :entityId${i})`);
        }
      }
      if (sql.length > 0) {
        logsQueryBuilder.andWhere(`(${sql.join(' OR ')})`, properties);
      }
    } else {
      logsQueryBuilder.andWhere(
        '((entity = :entity1 AND entityId = :entityId1) OR (entity = :entity2 AND entityId = :entityId2) OR (entity = :entity3 AND entityId IN (:...entityIds3)) OR (entity = :entity4 AND entityId IN (:...entityIds4)))',
        {
          entity1: LogEntityEnum.VIRTUAL_MACHINE,
          entityId1: virtualMachine.id,
          entity2: LogEntityEnum.CONTEXT,
          entityId2: virtualMachine.context.id,
          entity3: LogEntityEnum.VIRTUAL_MACHINE_SAVE,
          entityIds3: virtualMachineSaveIds,
          entity4: LogEntityEnum.DEPLOY,
          entityIds4: deployIds,
        },
      );
    }

    const [count, logs] = await getManyAndCount<Log>(logsQueryBuilder, [
      'log.id',
    ]);

    return new GetVirtualMachineHistoryResponse(logs, count, pageOptions);
  }

  async getVirtualMachineDeploys(
    id: string,
    getVirtualMachineDeploysRequest: GetVirtualMachineDeploysRequest,
  ) {
    const virtualMachine = await this.findOne({
      where: { id },
      relations: { context: true, virtualMachineSaves: true },
    });
    if (!virtualMachine) {
      throw new NotFoundException('virtual machine not found');
    }

    const pageOptions = new PageOptions(getVirtualMachineDeploysRequest);
    let { orderBy } = getVirtualMachineDeploysRequest;

    orderBy = orderBy ? orderBy : VirtualMachineDeployOrderByEnum.CREATED_AT;

    const deploysQueryBuilder = this.deploysService
      .getQueryBuilder('deploy')
      .innerJoinAndSelect('deploy.user', 'user')
      .innerJoinAndSelect('deploy.device', 'device')
      .innerJoinAndSelect('device.fqdnMask', 'fqdnMask')
      .innerJoinAndSelect('deploy.virtualMachineSave', 'virtualMachineSave')
      .innerJoin('virtualMachineSave.virtualMachine', 'virtualMachine')
      .leftJoinAndSelect('deploy.labels', 'label')
      .where('virtualMachine.id = :vmId', { vmId: virtualMachine.id });

    const queryHelper = new QueryHelper(deploysQueryBuilder);
    queryHelper.pagination(pageOptions, orderBy);

    if (getVirtualMachineDeploysRequest.statuses) {
      const statuses = getVirtualMachineDeploysRequest.statuses.split(',');
      queryHelper.andWhereMultipleEquals('deploy.status', statuses);
    }

    if (getVirtualMachineDeploysRequest.userIds) {
      const userIds = getVirtualMachineDeploysRequest.userIds.split(',');
      queryHelper.andWhereMultipleEquals('user.id', userIds);
    }

    if (getVirtualMachineDeploysRequest.labelIds) {
      const labelIds = getVirtualMachineDeploysRequest.labelIds.split(',');
      queryHelper.andWhereMultipleIn('label.id', labelIds, 'AND');
    }

    queryHelper.andWhereDateRange(
      'deploy.createdAt',
      getVirtualMachineDeploysRequest.createdDateRange,
    );
    queryHelper.andWhereQuery(getVirtualMachineDeploysRequest.query, [
      'label.name',
    ]);

    const [count, deploys] = await getManyAndCount<Deploy>(
      deploysQueryBuilder,
      ['deploy.id'],
      false,
    );

    let sortedDeploys = deploys;

    if (getVirtualMachineDeploysRequest.labelIds) {
      for (const deploy of sortedDeploys) {
        deploy.labels = await this.labelsService.find({
          where: { deploys: { id: deploy.id } },
        });
      }
      const labelIds = getVirtualMachineDeploysRequest.labelIds.split(',');
      sortedDeploys = sortedDeploys.map((deploy) => {
        deploy.labels.sort((a, b) => {
          if (
            labelIds.includes(a.id.toString()) &&
            !labelIds.includes(b.id.toString())
          ) {
            return -1;
          } else if (
            !labelIds.includes(a.id.toString()) &&
            labelIds.includes(b.id.toString())
          ) {
            return 1;
          }
          return 0;
        });
        return deploy;
      });
    }

    return new GetVirtualMachineDeploysResponse(
      sortedDeploys,
      count,
      pageOptions,
    );
  }

  private async importDatabase(
    virtualMachine: VirtualMachine,
    virtualMachineSave: VirtualMachineSave,
    userId: string,
  ) {
    await this.virtualMachineSavesService.create(
      { name: `Before import save #${virtualMachineSave.id}` },
      virtualMachine,
      userId,
    );

    await this.logsService.create({
      authorId: userId,
      action: LogActionEnum.UPDATE,
      entity: LogEntityEnum.VIRTUAL_MACHINE,
      entityId: virtualMachine.id,
      values: { virtualMachineSaveId: virtualMachineSave.id },
      description: `Import save #${virtualMachineSave.id}`,
    });

    const stopResult = await this.stopVirtualMachine(virtualMachine);
    if (!stopResult) {
      return;
    }

    await this.virtualMachineSavesService.importDatabase(
      virtualMachine,
      virtualMachineSave,
    );

    await this.contextsService.restoreContext(
      virtualMachine.context,
      virtualMachineSave.context,
    );

    await this.setStatus(virtualMachine, VirtualMachineStatusEnum.STARTING);

    const startResult = await this.startVirtualMachine(virtualMachine);
    if (!startResult) {
      return;
    }

    await this.setStatus(virtualMachine, VirtualMachineStatusEnum.ONLINE);
  }

  async patchVirtualMachineImport(id: string, saveId: string, userId: string) {
    const virtualMachine = await this.findOne({
      where: { id },
      relations: {
        groups: true,
        user: true,
        context: { applications: true },
        project: true,
      },
    });
    if (!virtualMachine) {
      throw new NotFoundException('virtual machine not found');
    }

    await this.checkVirtualMachinePermissions(userId, virtualMachine);

    const virtualMachineSave = await this.virtualMachineSavesService.findOne({
      where: { id: saveId },
      relations: { virtualMachine: true, context: { applications: true } },
    });
    if (!virtualMachineSave) {
      throw new NotFoundException('virtual machine save not found');
    }

    await this.importDatabase(virtualMachine, virtualMachineSave, userId);
    await this.logsService.create({
      authorId: userId,
      action: LogActionEnum.IMPORT,
      entity: LogEntityEnum.VIRTUAL_MACHINE_SAVE,
      entityId: virtualMachineSave.id,
      values: {
        virtualMachineId: virtualMachine.id,
        virtualMachineSaveId: virtualMachineSave.id,
      },
      description: 'Import VM Save',
    });
    return;
  }

  async patchVirtualMachineApproval(id: string, userId: string) {
    const virtualMachine = await this.findOne({
      where: { id },
      relations: { groups: true, user: true, context: true, project: true },
    });
    if (!virtualMachine) {
      throw new NotFoundException('virtual machine not found');
    }

    await this.checkVirtualMachinePermissions(userId, virtualMachine);

    if (
      virtualMachine.status !== VirtualMachineStatusEnum.ONLINE &&
      virtualMachine.status !== VirtualMachineStatusEnum.OFFLINE
    ) {
      throw new BadRequestException('Virtual machine not online/offline');
    }

    if (virtualMachine.approval === VirtualMachineApprovalEnum.IN_PROGRESS) {
      throw new BadRequestException(
        'Virtual machine already in approval process',
      );
    }

    const save = await this.virtualMachineSavesService.create(
      {
        name: 'Before approval process save',
        description: 'This save will be restored after approval process',
      },
      virtualMachine,
      userId,
      VirtualMachineSaveTypeEnum.AUTOMATIC,
    );

    await this.logsService.create({
      authorId: userId,
      action: LogActionEnum.UPDATE,
      entity: LogEntityEnum.VIRTUAL_MACHINE,
      entityId: virtualMachine.id,
      values: { approval: VirtualMachineApprovalEnum.IN_PROGRESS },
      description: 'Start approval process',
    });

    virtualMachine.approvalSaveId = save.id;
    await this.virtualMachineRepository.save(virtualMachine);
    await this.setApproval(
      virtualMachine,
      VirtualMachineApprovalEnum.IN_PROGRESS,
    );

    if (virtualMachine.status === VirtualMachineStatusEnum.OFFLINE) {
      this.startVirtualMachine(virtualMachine);
    }

    return;
  }

  async approvalReject(
    virtualMachine: VirtualMachine,
    virtualMachineSave: VirtualMachineSave,
    userId: string,
  ) {
    await this.setApproval(virtualMachine, VirtualMachineApprovalEnum.NONE);
    await this.logsService.create({
      authorId: userId,
      action: LogActionEnum.UPDATE,
      entity: LogEntityEnum.VIRTUAL_MACHINE,
      entityId: virtualMachine.id,
      values: { approval: VirtualMachineApprovalEnum.NONE },
      description: 'Reject approval',
    });

    let result = await this.stopVirtualMachine(virtualMachine);
    if (!result) {
      return;
    }

    result = await this.virtualMachineSavesService.importDatabase(
      virtualMachine,
      virtualMachineSave,
    );
    if (!result) {
      this.setStatus(
        virtualMachine,
        VirtualMachineStatusEnum.IMPORT_DATABASE_ERROR,
      );
      return;
    }

    result = await this.startVirtualMachine(virtualMachine);
    if (!result) {
      return;
    }

    virtualMachine.approvalSaveId = null;
    await this.virtualMachineRepository.save(virtualMachine);

    await this.logsService.create({
      authorId: userId,
      action: LogActionEnum.IMPORT,
      entity: LogEntityEnum.VIRTUAL_MACHINE_SAVE,
      entityId: virtualMachineSave.id,
      values: {
        virtualMachineId: virtualMachine.id,
        virtualMachineSaveId: virtualMachineSave.id,
      },
      description: 'Import VM Save',
    });
  }

  async approvalApprove(
    virtualMachine: VirtualMachine,
    virtualMachineSave: VirtualMachineSave,
    userId: string,
    approveSaveName: string,
  ) {
    await this.setApproval(virtualMachine, VirtualMachineApprovalEnum.APPROVED);
    await this.logsService.create({
      authorId: userId,
      action: LogActionEnum.UPDATE,
      entity: LogEntityEnum.VIRTUAL_MACHINE,
      entityId: virtualMachine.id,
      values: { approval: VirtualMachineApprovalEnum.APPROVED },
      description: 'Approve',
    });

    let result = await this.stopVirtualMachine(virtualMachine);
    if (!result) {
      return;
    }

    result = await this.virtualMachineSavesService.importDatabase(
      virtualMachine,
      virtualMachineSave,
    );
    if (!result) {
      this.setStatus(
        virtualMachine,
        VirtualMachineStatusEnum.IMPORT_DATABASE_ERROR,
      );
      return;
    }

    await this.virtualMachineSavesService.create(
      { name: approveSaveName },
      virtualMachine,
      userId,
      VirtualMachineSaveTypeEnum.APPROVE,
    );

    result = await this.startVirtualMachine(virtualMachine);
    if (!result) {
      return;
    }

    await this.notificationsService.notify(
      NotificationTypeEnum.APPROVE_VIRTUAL_MACHINE,
      virtualMachine.user,
      {
        ID: virtualMachine.id,
        Title: virtualMachine.title,
        'Approval name': approveSaveName,
        URL: `http://${this.configService.get('MAESTRO_HOST')}/virtual-machines/${virtualMachine.id}`,
      },
    );

    await this.logsService.create({
      authorId: userId,
      action: LogActionEnum.IMPORT,
      entity: LogEntityEnum.VIRTUAL_MACHINE_SAVE,
      entityId: virtualMachineSave.id,
      values: {
        virtualMachineId: virtualMachine.id,
        virtualMachineSaveId: virtualMachineSave.id,
      },
      description: 'Import VM Save',
    });
  }

  async patchVirtualMachineApprovalReject(id: string, userId: string) {
    const virtualMachine = await this.findOne({
      where: { id },
      relations: { groups: true, user: true, project: true },
    });
    if (!virtualMachine) {
      throw new NotFoundException('virtual machine not found');
    }

    await this.checkVirtualMachinePermissions(userId, virtualMachine);

    if (virtualMachine.approval !== VirtualMachineApprovalEnum.IN_PROGRESS) {
      throw new BadRequestException('Virtual machine not in approval process');
    }

    const virtualMachineSave = await this.virtualMachineSavesService.findOne({
      where: { id: virtualMachine.approvalSaveId },
      relations: { virtualMachine: true },
    });
    if (!virtualMachineSave) {
      throw new NotFoundException('virtual machine save not found');
    }

    this.approvalReject(virtualMachine, virtualMachineSave, userId);

    return;
  }

  async patchVirtualMachineApprovalApprove(
    id: string,
    userId: string,
    patchVirtualMachineApprovalApproveRequest: PatchVirtualMachineApprovalApproveRequest,
  ) {
    const virtualMachine = await this.findOne({
      where: { id },
      relations: { groups: true, user: true, context: true, project: true },
    });
    if (!virtualMachine) {
      throw new NotFoundException('virtual machine not found');
    }

    await this.checkVirtualMachinePermissions(userId, virtualMachine);

    if (virtualMachine.approval !== VirtualMachineApprovalEnum.IN_PROGRESS) {
      throw new BadRequestException('Virtual machine not in approval process');
    }

    const virtualMachineSave = await this.virtualMachineSavesService.findOne({
      where: { id: virtualMachine.approvalSaveId },
      relations: { virtualMachine: true },
    });
    if (!virtualMachineSave) {
      throw new NotFoundException('virtual machine save not found');
    }

    this.approvalApprove(
      virtualMachine,
      virtualMachineSave,
      userId,
      patchVirtualMachineApprovalApproveRequest.name,
    );

    return;
  }

  async patchVirtualMachineRefreshProject(id: string, userId: string) {
    const virtualMachine = await this.findOne({
      where: { id },
      relations: { groups: true, user: true, project: true },
    });
    if (!virtualMachine) {
      throw new NotFoundException('virtual machine not found');
    }

    await this.checkVirtualMachinePermissions(userId, virtualMachine);

    // TODO : if i want to refresh project when i am online, i can. In order to update project.
    if (virtualMachine.status !== VirtualMachineStatusEnum.PROJECT_ERROR) {
      throw new BadRequestException('Virtual machine not project error');
    }

    await this.logsService.create({
      authorId: userId,
      action: LogActionEnum.UPDATE,
      entity: LogEntityEnum.VIRTUAL_MACHINE,
      entityId: virtualMachine.id,
      description: 'Refresh project',
    });

    await this.setStatus(
      virtualMachine,
      VirtualMachineStatusEnum.CHECKING_PROJECT,
    );
    this.checkProject(virtualMachine);

    return;
  }

  findOne(options?: FindOneOptions<VirtualMachine>) {
    return this.virtualMachineRepository.findOne(options);
  }
}
